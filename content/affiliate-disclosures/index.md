---
title: Affiliate Disclosures
author: Kipkoech Sang
type: page
date: 2022-01-23T19:13:55+00:00
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 24
rank_math_internal_links_processed:
  - 1

---
Have in mind that some of the links on this website are affiliate links. It helps us earn a commission whenever you make a purchase using those specific links. We do a thorough checkup before linking to those products because we want to give you something beneficial to your course. So welcome and thank you.
