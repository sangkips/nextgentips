---
title: How to install and configure Wine on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-12-12T08:55:34+00:00
url: /2021/12/12/how-to-install-and-configure-wine-on-ubuntu-21-10-21-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 66
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Wine is a compatibility layer capable of running Windows applications on several [POSIX][1]-compliant operating systems i.e Linux, macOS, and BSD. Instead of simulating internal Windows logic like a virtual machine or emulator, Wine translates Windows API calls into POSIX calls instantly eliminating the performance and memory penalties of other methods and allowing you to integrate Windows applications in your desktop. 

In this tutorial, I will take you through the installation steps of Wine in Ubuntu 21.10/21.04.

## What is new in Wine 7.0-rc1 release {#what-is-new-in-wine-7-0-rc1-release.wp-block-heading}

  * Reimplimentation of winMM joystick driver
  * All unix libraries converted to the syscall-based libraries

## Benefits of Using Wine {#benefits-of-using-wine.wp-block-heading}

  * Unix has made it possible to write powerful scripts, Wine makes it possible to call windows applications from scripts that can also laverage Unix environment.
  * Wine makes it possible to access Windows applications remotely.
  * Wine makes it possible to use thin clients i.e install Wine and you will easily connect to windows applications using x-terminal.
  * Wine is open-source so you can easily extend to suite your needs.
  * Wine can be used to make existing windows applications available on the web by using VNC and its Java/HTML5 client.

## Install Wine 7.0 on Ubuntu 21.10/21.04 {#install-wine-7-0-on-ubuntu-21-10-21-04.wp-block-heading}

Wine comes preinstalled with Ubuntu systems, but there is one thing to consider, for 64-bit architecture you need to enable 32 bit for Wine to run effectively.

First, let&#8217;s run system updates with the following command. 

<pre class="wp-block-code"><code>$ sudo apt update $$ apt upgrade -y</code></pre>

When both updates and upgrades are complete, now you can enable 32-bit architecture. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dpkg --add-architecture i386</code></pre>

We need to add the repository key by downloading from the following:

<pre class="wp-block-code"><code>$ wget -nc https://dl.winehq.org/wine-builds/winehq.key</code></pre>

<pre class="wp-block-code"><code>Output
--2021-12-12 07:25:43--  https://dl.winehq.org/wine-builds/winehq.key
Resolving dl.winehq.org (dl.winehq.org)... 151.101.18.217
Connecting to dl.winehq.org (dl.winehq.org)|151.101.18.217|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3220 (3.1K) &#91;application/pgp-keys]
Saving to: ‘winehq.key’

winehq.key                   100%&#91;===========================================&gt;]   3.14K  --.-KB/s    in 0s      

2021-12-12 07:25:43 (34.9 MB/s) - ‘winehq.key’ saved &#91;3220/3220]</code></pre>

Add key with the following command 

<pre class="wp-block-code"><code>$ sudo apt-key add winehq.key</code></pre>

We have downloaded and added the key, next is to add the following WineHQ repository into our system.

<pre class="wp-block-code"><code>$ sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ impish main'</code></pre>

<pre class="wp-block-code"><code>output
Repository: 'deb https://dl.winehq.org/wine-builds/ubuntu/$(lsb_release -cs) main'
Description:
Archive for codename: -cs) components: main
More info: https://dl.winehq.org/wine-builds/ubuntu/$(lsb_release
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.</code></pre>

Press enter to allow installation to complete.

If it happens you get an error &#8220;The repository &#8216;https://dl.winehq.org/wine-builds/ubuntu/$(lsb_release -cs) Release&#8217; does not have a Release file.&#8221; make sure all the dependencies are installed. To install most of the dependencies run the following command.

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common</code></pre>

Then now you need to run system update again for the changes to take effect

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

## Install wine stable release  {#install-wine-stable-release.wp-block-heading}

To install wine run the following command

<pre class="wp-block-code"><code>$ sudo apt install --install-recommends winehq-devel</code></pre>

Press Y to allow installation to continue.

<pre class="wp-block-code"><code>Sample output
Selecting previously unselected package wine-devel.
Preparing to unpack .../wine-devel_7.0~rc1~impish-1_amd64.deb ...
Unpacking wine-devel (7.0~rc1~impish-1) ...
Selecting previously unselected package winehq-devel.
Preparing to unpack .../winehq-devel_7.0~rc1~impish-1_amd64.deb ...
Unpacking winehq-devel (7.0~rc1~impish-1) ...
Setting up wine-devel-i386:i386 (7.0~rc1~impish-1) ...
Setting up wine-devel-amd64 (7.0~rc1~impish-1) ...
Setting up wine-devel (7.0~rc1~impish-1) ...
Setting up winehq-devel (7.0~rc1~impish-1) ...</code></pre>

Check the version of the installed wine

<pre class="wp-block-code"><code>$ wine --version
wine-7.0-rc1</code></pre>

After installation, we need to configure wine. Run **winecfg** on your terminal. It will install gecko and Mono into your system.

<pre class="wp-block-code"><code>$ winecfg</code></pre>

## Conclusion  {#conclusion.wp-block-heading}

I hope this tutorial has taught you how to install and configure wine on Ubuntu 21.10. Welcome back again for more tutorials of this kind.

 [1]: https://www.gnu.org/software/libc/manual/html_node/POSIX.html