---
title: How to undo a git merge
author: Kipkoech Sang
type: post
date: 2022-10-30T08:05:27+00:00
url: /2022/10/30/how-to-undo-a-git-merge/
categories:
  - Git

---
In this tutorial, we will learn how to undo a merge in git.

Have you been in a position where you pushed your code production codebase without realizing you were not working on your branch. This sometimes happens if you don&#8217;t have restrictions on the production branch.

<a href="https://git-scm.com/" target="_blank" rel="noreferrer noopener">Git</a> is a free and open-source distributed version control system designed mostly for developers. If you have developers from all over the world and you want to collaborate on some projects git will come to the rescue.

### Basic git commands {.wp-block-heading}

Before learning anything in development, it&#8217;s very important to learn some basic git commands that let you go before embarking on high-level commands. These commands are:

**Git init**: 

This creates a git repository. It can be done on an existing project that has not been versioned or can be used to initialize the very first project you are working on. So this is the very first command you will use if you want to version-control your project.

<pre class="wp-block-code"><code>git init</code></pre>

**Git status**

Git status is used to display the state of a working directory and the staging area. It lets you see the changes that have been made to your project. This is a very handy command you need to always use in your development cycle because it lets you see files which are staged, untracked and unstaged.

<pre class="wp-block-code"><code>git status </code></pre>

**Git add**

Git add is used to add changes to the working directory to the staging area. It tells git that you want to include the changes made to the files. Any changes made and added are not recorded until you do a git commit.

<pre class="wp-block-code"><code>git add &lt;file></code></pre>

**Git commit**

Git commit captures the snapshot of the project&#8217;s staged changes. Commit is the equivalent to save because it saves changes made into the remote repository. When committing it&#8217;s advisable to include a message about what changes you have introduces so that other developers using your code will know what you have made. It must be descriptive as possible.

<pre class="wp-block-code"><code>git commit -m "commit message"</code></pre>

**Git push**

Git push is used to upload the content of the local repository to the remote repository such as GitHub, or Bitbucket. It lets you push the specific changes you have committed to the remote. We only push when we have tested our code to see if it really working as desired by the feature being introduced. You must specify the <a href="https://www.atlassian.com/git/tutorials/using-branches#:~:text=The%20git%20branch%20command%20lets,checkout%20and%20git%20merge%20commands." target="_blank" rel="noreferrer noopener">branch</a> you are pushing to while doing a git push.

<pre class="wp-block-code"><code>git push &lt;remote branch></code></pre>

An example is when you want to push to the **origin main branch**, this is only done when working on you own project with no team collaboration.

<pre class="wp-block-code"><code>git push -u origin main</code></pre>

### Undoing a git merge {.wp-block-heading}

Let&#8217;s say you are in a team working on a certain feature, and you have tested your code ready to push to the development branch where all other changes meet. You submitted a pull request and it&#8217;s accepted and <a href="https://www.atlassian.com/git/tutorials/using-branches/git-merge" target="_blank" rel="noreferrer noopener">merge</a> is done. On working again you realize that the changes made were not up to the standard, so we need to remove those changes from the development branch or maybe also at the main branch. This is how we undo the changes.

First, check the commit logs you have so that you can revert back to the commit hash you want to return to. Git stores all the commit logs, to check the logs use `<mark style="background-color:#abb8c3" class="has-inline-color">git log</mark>`

<pre class="wp-block-code"><code>$ git log
commit 779394253ddd30358abb67926 (HEAD -> main, origin/main, origin/HEAD)
Author: 
Date:   Fri Oct 28 14:16:06 2022 +0300

    added static files

commit 7346995e68df35a88096b7f267
Author: 
Date:   Fri Oct 28 14:03:35 2022 +0300

    changed debug to false
</code></pre>

For our case, if you want to get only the harshes of where you want to revert to use `<mark style="background-color:#abb8c3" class="has-inline-color">git reflog</mark>`

<pre class="wp-block-code"><code>$ git reflog
7793942 (HEAD -> main, origin/main, origin/HEAD) HEAD@{0}: commit: added static files
7346995 HEAD@{1}: commit: changed debug to false
4365c73 HEAD@{2}: commit: added README file
acaf6c2 HEAD@{3}: commit: added bio to serializer
570ee4f HEAD@{4}: commit: added a README file
0ed2b3f HEAD@{5}: commit: added a README file
9f0fe0a HEAD@{6}: commit: remove id field from serializers
96dac36 HEAD@{7}: commit: added cors headers
e632fc6 HEAD@{8}: commit: set debug to false
365ff6d HEAD@{9}: commit: added models+urls+views+serializers</code></pre>

### Undoing a pushed git merge {.wp-block-heading}

To undo a pushed git merge, we use `<mark style="background-color:#abb8c3" class="has-inline-color">git revert</mark>` command. Git revert will undo the changes to the repository&#8217;s commit history. A revert operation will take the specified commit, inverse the changes from that commit, and create a new revert commit. The ref pointers are then updated to point at the new revert commit making it the tip of the branch.

<pre class="wp-block-code"><code>git revert -m 1 &lt;commit hash></code></pre>

**-m 1** will tell Git to keep the parent side of the merge.

### Using Git reset command {.wp-block-heading}

Git reset command is used to undo local changes Git repository. 

<pre class="wp-block-code"><code>git reset --hard &lt;commit hash></code></pre>

Using `<mark style="background-color:#abb8c3" class="has-inline-color">--hard</mark>` is very dangerous because the commit history ref pointers are updated to the specified commit and any previous pending changes to the staging index and the working directory get reset to match the state of the commit tree. This means any pending work that was hanging out in the staging index and working directory will be lost.

You can also use `<mark style="background-color:#abb8c3" class="has-inline-color">git reset --hard HEAD~1</mark>` command when you don&#8217;t have the commit hash