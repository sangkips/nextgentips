---
title: How to install RabbitMQ on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-04-29T19:57:19+00:00
url: /2022/04/29/how-to-install-rabbitmq-on-ubuntu-22-04/
rank_math_seo_score:
  - 70
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_focus_keyword:
  - rabbitmq
rank_math_analytic_object_id:
  - 171
categories:
  - Linux

---
RabbitMQ is an open-source message broker software. It&#8217;s a software where queues are defined and to which applications connect in order to transfer a message or messages. Think of it as a middle man who acts as a broker. They can be used to reduce loads and delivery times for web application servers by delegating tasks that take up a lot of resources and or time to a third party that has no other job

## Reasons for Using RabbitMQ {.wp-block-heading}

Message queueing allows web servers to respond to requests quickly instead of being forced to perform resource-heavy procedures on the spot that may cause delays.

Message queueing is important when you want to distribute a message to multiple consumers and also to balanced loading between worker nods.

## Installing RabbitMQ 3.9 on Ubuntu 22.04 {.wp-block-heading}

RabbitMQ is included in Ubuntu repositories but it lacked behind in terms of version releases. It is not the updated version because it takes longer to be updated.

### 1. Update system repositories  {.wp-block-heading}

We need to start off by updating our repositories to make them up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y </code></pre>

### 2. Install Erlang {.wp-block-heading}

Erlang/OTP platform is a complex system consisting of many smaller modules. RabbitMQ requires Erlang to operate.

Let&#8217;s start by adding Erlang to the repository manually. Use the following commands:

<pre class="wp-block-code"><code>wget https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb
sudo dpkg -i erlang-solutions_2.0_all.deb</code></pre>

Next, we need to add Erlang public key to the apt-secure repository.

<pre class="wp-block-code"><code>wget https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc
sudo apt-key add erlang_solutions.asc</code></pre>

Lastly is to install Erlang but first run the system update again for the changes to take effect.

<pre class="wp-block-code"><code>sudo apt update
sudo apt install erlang -y</code></pre>

### 3. Install RabbitMQ on Ubuntu 22.04 {.wp-block-heading}

Now that we have satisfied all the requirements, we can now install RabbitMQ found on Ubuntu repositories with the following command.

<pre class="wp-block-code"><code>sudo apt install rabbitmq-server -y</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>The following NEW packages will be installed:
  rabbitmq-server socat
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 15.5 MB of archives.
After this operation, 24.3 MB of additional disk space will be used.
Get:1 http://mirrors.digitalocean.com/ubuntu jammy/main amd64 socat amd64 1.7.4.1-3ubuntu4 &#91;349 kB]
Get:2 http://mirrors.digitalocean.com/ubuntu jammy/main amd64 rabbitmq-server all 3.9.13-1 &#91;15.2 MB]
Fetched 15.5 MB in 0s (37.3 MB/s)     
Selecting previously unselected package socat.
(Reading database ... 114923 files and directories currently installed.)
Preparing to unpack .../socat_1.7.4.1-3ubuntu4_amd64.deb ...
Unpacking socat (1.7.4.1-3ubuntu4) ...
Selecting previously unselected package rabbitmq-server.
Preparing to unpack .../rabbitmq-server_3.9.13-1_all.deb ...
Unpacking rabbitmq-server (3.9.13-1) ...
Setting up socat (1.7.4.1-3ubuntu4) ...
Setting up rabbitmq-server (3.9.13-1) ...
Adding group `rabbitmq' (GID 122) ...
Done.
Adding system user `rabbitmq' (UID 114) ...
Adding new user `rabbitmq' (UID 114) with group `rabbitmq' ...
Not creating home directory `/var/lib/rabbitmq'.
Created symlink /etc/systemd/system/multi-user.target.wants/rabbitmq-server.service → /lib/systemd/system/rabbitmq-server.service.
Processing triggers for man-db (2.10.2-1) ...</code></pre>

Use **apt cache-policy rabbitmq-server** to check your installed RabbitMQ.

<pre class="wp-block-code"><code>apt cache-policy rabbitmq-server </code></pre>

You will see the following output.

<pre class="wp-block-code"><code>#output
rabbitmq-server:
  Installed: 3.9.13-1
  Candidate: 3.9.13-1
  Version table:
 *** 3.9.13-1 500
        500 http://mirrors.digitalocean.com/ubuntu jammy/main amd64 Packages
        100 /var/lib/dpkg/status</code></pre>

### 4. Configuring RabbitMQ  {.wp-block-heading}

After installation is complete, we now need to do some configuration on RabbitMQ.

First, start the RabbitMQ service with the following command.

<pre class="wp-block-code"><code>sudo systemctl start rabbitmq-server</code></pre>

Check the status if RabbitMQ is running with the following command:

<pre class="wp-block-code"><code>$ &lt;strong>sudo systemctl status rabbitmq-server&lt;/strong>
● rabbitmq-server.service - RabbitMQ Messaging Server
     Loaded: loaded (/lib/systemd/system/rabbitmq-server.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2022-04-29 19:38:53 UTC; 2min 35s ago
   Main PID: 17434 (beam.smp)
      Tasks: 21 (limit: 1119)
     Memory: 108.8M
        CPU: 8.051s
     CGroup: /system.slice/rabbitmq-server.service
             ├─17434 /usr/lib/erlang/erts-12.2.1/bin/beam.smp -W w -MBas ageffcbf -MHas ageffcbf -MBlmbcs 512 -MHlmbcs 512 -MMmcs 30 -P 10485>
             ├─17445 erl_child_setup 65536
             ├─17492 inet_gethost 4
             └─17493 inet_gethost 4
lines 1-12/12 (END)</code></pre>

Enable RabbitMQ on boot so that you don&#8217;t need to start every time when you reboot your system.

<pre class="wp-block-code"><code>sudo systemctl enable rabbitmq-server</code></pre>

### 5. Enable RabbitMQ dashboard. (Optional) {.wp-block-heading}

You can enable dashboard management for RabbitMQ with the following command.

<pre class="wp-block-code"><code>sudo rabbitmq-plugins enable rabbitmq_management</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Enabling plugins on node rabbit@ubuntu:
rabbitmq_management
The following plugins have been configured:
  rabbitmq_management
  rabbitmq_management_agent
  rabbitmq_web_dispatch
Applying plugin configuration to rabbit@ubuntu...
The following plugins have been enabled:
  rabbitmq_management
  rabbitmq_management_agent
  rabbitmq_web_dispatch

started 3 plugins.</code></pre>

You can now access the dashboard using **http://<your-server-ip>:15672.** but you will need to create admin user first.

15672 is the port RabbitMQ is served into.

You can create an admin login or you can use the quest if you only want to see what&#8217;s in it but if you want to do some operations, you will need to create the admin.

<pre class="wp-block-code"><code>$ sudo rabbitmqctl add_user admin passwd
Adding user "admin" ...
Password: 
passwd
Done. Don't forget to grant the user permissions to some virtual hosts! See 'rabbitmqctl help set_permissions' to learn more.

$ sudo rabbitmqctl set_user_tags admin administrator
Setting tags for user "admin" to &#91;administrator] ...</code></pre><figure class="wp-block-image size-large is-resized">

<img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-29-22-49-40.png?resize=810%2C374&#038;ssl=1" alt="Nextgentips. RabbitMQ dashboard" class="wp-image-1321" width="810" height="374" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-29-22-49-40.png?resize=1024%2C474&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-29-22-49-40.png?resize=300%2C139&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-29-22-49-40.png?resize=768%2C356&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-29-22-49-40.png?w=1252&ssl=1 1252w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips. RabbitMQ dashboard</figcaption></figure> 

## Conclusion {.wp-block-heading}

We have successfully installed RabbitMQ on Ubuntu 22.04. To know more on the management commands head over to <a href="https://www.rabbitmq.com/getstarted.html" target="_blank" rel="noreferrer noopener">RabbitMQ getting started</a>. I hope you enjoyed and thank you for reading.