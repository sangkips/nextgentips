---
title: How to install Docker Compose V2 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-05-06T08:46:41+00:00
url: /2022/05/06/how-to-install-docker-compose-v2-on-ubuntu-22-04/
rank_math_seo_score:
  - 11
rank_math_primary_category:
  - 62
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 174
categories:
  - Containers

---
Docker is a set of platform-as-a-service product that uses OS-level virtualization to deliver software in packages called containers. Containers are usually isolated from one another and bundled with their own software libraries and configuration files, they can communicate with each other through well-defined channels.

Docker makes it possible to get more apps running on the same old servers and also makes it easy to package and ship programs.

Docker-compose is a tool for defining and running multi-container Docker application. It uses YAML files to configure its application services.

## Steps on using compose {.wp-block-heading}

  * First define your app&#8217;s environment with **Dockerfile** so that it can be reproduced anywhere.
  * Define the services that make up your app in **docker-compose.yaml** so they can be run together in an isolated environment.
  * Lastly run **docker compose up** to start and run your entire app.

## Prerequisites  {.wp-block-heading}

  * Have Docker up and running on your Ubuntu 22.04. 
  * Make sure you are running on sudo privileges 
  * Ubuntu 22.04 server up and running.

## Installing Docker compose v2 on Ubuntu 22.04 {.wp-block-heading}

The difference between docker compose v1 and v2 is that v1 uses hyphen when running commands. On v2 they remove that hyphen between different commands. For example is: for docker v1 **docker-compose up** but for v2 we use **docker compose up** 

### Update system repositories  {.wp-block-heading}

Begin by updating and upgrading your system repositories in order to make them up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### Install Docker on Ubuntu 22.04 {.wp-block-heading}

As a prerequisite, we need to have docker installed on our system before we can continue with the docker-compose installation.

Let&#8217;s first begin by installing some prerequisites on our system

<pre class="wp-block-code"><code>sudo apt install apt-transport-https ca-certificates curl software-properties-common</code></pre>

Then we need to add a GPG key into our system repository to validate the docker images.

<pre class="wp-block-code"><code>curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg</code></pre>

Next is to add the Docker repository to apt so that docker will get to know where to access it resources files.

<pre class="wp-block-code"><code>echo "deb &#91;arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list &gt; /dev/null</code></pre>

Lastly, let&#8217;s do a system update once more to refresh our repositories.

<pre class="wp-block-code"><code>sudo apt update</code></pre>

we can now install docker in Ubuntu 22.04 with this command **sudo apt install docker-ce** but first, we can check if docker is available in our repositories with this command.

<pre class="wp-block-code"><code>$ apt-cache policy docker-ce
docker-ce:
  Installed: (none)
  Candidate: 5:20.10.15~3-0~ubuntu-jammy
  Version table:
     5:20.10.15~3-0~ubuntu-jammy 500
        500 https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages
     5:20.10.14~3-0~ubuntu-jammy 500
        500 https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages
     5:20.10.13~3-0~ubuntu-jammy 500
        500 https://download.docker.com/linux/ubuntu jammy/stable amd64 Packages
</code></pre>

Notice we are installing docker-ce, this is the community version not the enterprise version.

### Install docker  {.wp-block-heading}

To install docker run this command on your terminal.

<pre class="wp-block-code"><code>sudo apt install docker-ce -y</code></pre>

You will see this output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  containerd.io docker-ce-cli docker-ce-rootless-extras docker-scan-plugin
  libslirp0 pigz slirp4netns
Suggested packages:
  aufs-tools cgroupfs-mount | cgroup-lite
The following NEW packages will be installed:
  containerd.io docker-ce docker-ce-cli docker-ce-rootless-extras
  docker-scan-plugin libslirp0 pigz slirp4netns
0 upgraded, 8 newly installed, 0 to remove and 0 not upgraded.
Need to get 101 MB of archives.
After this operation, 421 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

Verify that the docker engine is running with the **hello-world** command.

<pre class="wp-block-code"><code>sudo docker-run hello world</code></pre>

### Installing Docker compose on Ubuntu 22.04 {.wp-block-heading}

Now it&#8217;s time we install docker-compose on our system.

First, let&#8217;s create a directory in the home folder 

<pre class="wp-block-code"><code>mkdir -p ~/.docker/cli-plugins/</code></pre>

Next is to download the latest stable release from <a href="https://github.com/docker/compose/releases" target="_blank" rel="noreferrer noopener">docker releases page</a>. 

<pre class="wp-block-code"><code>curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose</code></pre>

Make sure you give the right permission so that you will be in a position to execute docker-compose. Use this command to give requisite permission.

<pre class="wp-block-code"><code>chmod +x ~/.docker/cli-plugins/docker-compose</code></pre>

You can check the version of docker compose installed.

<pre class="wp-block-code"><code>$ docker compose version
Docker Compose version v2.5.0</code></pre>

### Create docker-compose.yml file {.wp-block-heading}

<pre class="wp-block-code"><code>mkdir ~/demo
cd ~/demo
nano docker-compose.yml</code></pre>

Paste the following into docker-compose.yml file

<pre class="wp-block-code"><code>version: '3.9'

services:
  redis:
    image: redis:6.2-alpine
    ports:
      - 6379:6379
    command: redis-server --save 60 1 --requirepass MDNcVb924a --loglevel warning</code></pre>

To start the container engine we use **docker compose up -d**

<pre class="wp-block-code"><code>$ docker compose up -d 
&#91;+] Running 7/7
 ⠿ redis Pulled                                                                12.7s
   ⠿ df9b9388f04a Pull complete                                                 3.0s
   ⠿ 192e03523482 Pull complete                                                 3.1s
   ⠿ 7151bccd2756 Pull complete                                                 3.3s
   ⠿ 16ca30b6b4df Pull complete                                                 6.3s
   ⠿ b8157a69ff12 Pull complete                                                 6.3s
   ⠿ 02fb42b94dab Pull complete                                                 6.4s
&#91;+] Running 2/2
 ⠿ Network demo_default    Created                                              0.1s
 ⠿ Container demo-redis-1  Started </code></pre>

Verify the container processes using:

<pre class="wp-block-code"><code>$ docker compose ps
NAME                COMMAND                  SERVICE             STATUS              PORTS
demo-redis-1        "docker-entrypoint.s…"   redis               running             0.0.0.0:6379->6379/tcp, :::6379->6379/tcp</code></pre>

Let&#8217;s login to our Redis container using the following command.

<pre class="wp-block-code"><code>/data # redis-cli
127.0.0.1:6379> keys pattern
(error) NOAUTH Authentication required.
127.0.0.1:6379> keys *
(error) NOAUTH Authentication required.
127.0.0.1:6379> auth MDNcVb924a
OK
127.0.0.1:6379> keys pattern
(empty array)
127.0.0.1:6379> 
</code></pre>

### Uninstall Docker Engine  {.wp-block-heading}

When you are done experimenting with the docker engine you can install using the following command.

<pre class="wp-block-code"><code>sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-compose-plugin</code></pre>

To delete images, containers, volumes, or customized config files you need to delete them manually by using the following commands.

<pre class="wp-block-code"><code>$ sudo rm -rf /var/lib/docker
$ sudo rm -rf /var/lib/containerd</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Docker compose v2 on Ubuntu 22.04.