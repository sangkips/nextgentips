---
title: How to install Visual Studio Code on ArchLinux
author: Kipkoech Sang
type: post
date: 2021-11-30T15:58:12+00:00
url: /2021/11/30/how-to-install-visual-studio-code-6-2-on-archlinux/
ss_ss_click_share_count_twitter:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 80
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this article we are going to learn how to install Visual Studio Code also known as (VScode) on ArchLinux. 

Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built-in Javascript, Node.js, and Typescript. One can do programming for almost all languages with ease with Visual Studio Code. The languages supported are, Go, PHP, C++, C#, Java, Python, and also Microsoft.NET.

## Why do we like Visual Studio Code? {#why-do-we-like-visual-studio-code.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Related Articles {#related-articles.wp-block-heading}

  * [How to install Visual Studio Code on Ubuntu 21.10][1]
  * [How to install Visual Studio Code on Ubuntu 21.04][2]

## Prerequisites {#prerequisites.wp-block-heading}

  * Have ArchLinux Distro
  * Have basic understanding of terminal
  * Have a user with sudo privileges.

There are two ways to install Visual Studio Code

  * Installing via AUR packages
  * Installing via Snap packages

## Table of Contents {#table-of-contents.wp-block-heading}

  1. Update system repositories 
  2. Install Visual Studio Code from AUR or Snap
  3. Conclusion

## Installing Visual Studio Code 6.2 via AUR Packages {#installing-visual-studio-code-6-2-via-aur-packages.wp-block-heading}

The Arch User Repository (AUR) is a community-driven repository for Arch users. It contains package descriptions ([PKGBUILDs][3]) that allow you to compile a package from the source with [makepkg][4] and then install it via [Pacman][5].

## Steps to follow while installing from AUR packages  {#steps-to-follow-while-installing-from-aur-packages.wp-block-heading}

  1. Acquire the build files including **PKGBUILDs**
  2. Verify that the **PKGBUILDs** are not malicious
  3. Run **makepkg** in the directory where files are saved.
  4. Run _**pacman -U package**_ to install the package.

Before installing ensure you install necessary tools including base-devel.

## Install Visual Studio Code  {#install-visual-studio-code.wp-block-heading}

First, install the base-devel so that it can install all the required packages into the system.

<pre class="wp-block-code"><code>$ sudo pacman -S git base-devel</code></pre>

Sample output

<pre class="wp-block-code"><code>warning: git-2.34.1-1 is up to date -- reinstalling
:: There are 24 members in group base-devel:
:: Repository core
   1) autoconf  2) automake  3) binutils  4) bison  5) fakeroot  6) file
   7) findutils  8) flex  9) gawk  10) gcc  11) gettext  12) grep  13) groff
   14) gzip  15) libtool  16) m4  17) make  18) pacman  19) patch  20) pkgconf
   21) sed  22) sudo  23) texinfo  24) which</code></pre>

To begin our installation, download the package from AUR package with the following command:

<pre class="wp-block-code"><code>$ git clone https://aur.archlinux.org/visual-studio-code-bin</code></pre>

Sample output

<pre class="wp-block-code"><code> Cloning into 'visual-studio-code-bin'...
remote: Enumerating objects: 530, done.
remote: Counting objects: 100% (530/530), done.
remote: Compressing objects: 100% (365/365), done.
remote: Total 530 (delta 188), reused 544 (delta 182), pack-reused 0
Receiving objects: 100% (530/530), 215.78 KiB | 346.00 KiB/s, done.
Resolving deltas: 100% (188/188), done.</code></pre>

Then you can cd into the directory you extracted the Visual Studio Code.

Now we can run makepkg. This will download the code, compile and package it accordingly.

<pre class="wp-block-code"><code>$ makepkg -si</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>==&gt; Making package: visual-studio-code-bin 1.62.3-1 (Tue 30 Nov 2021 03:31:22 PM UTC)
==&gt; Checking runtime dependencies...
==&gt; Installing missing dependencies...
&#91;sudo] password for nextgentips: 
resolving dependencies...
looking for conflicting packages...
warning: dependency cycle detected:
warning: harfbuzz will be installed before its freetype2 dependency
warning: dependency cycle detected:
warning: libglvnd will be installed before its mesa dependency
.....</code></pre>

## Installing Visual Studio Code 6.2 via snap packages  {#installing-visual-studio-code-6-2-via-snap-packages.wp-block-heading}

The first thing to do is to clone the git repository. Use the following command 

<pre class="wp-block-code"><code>$ git clone https://aur.archlinux.org/snapd.git</code></pre>

Sample output will be as follows 

<pre class="wp-block-code"><code>Cloning into 'snapd'...
remote: Enumerating objects: 562, done.
remote: Counting objects: 100% (562/562), done.
remote: Compressing objects: 100% (375/375), done.
remote: Total 562 (delta 188), reused 553 (delta 182), pack-reused 0
Receiving objects: 100% (562/562), 215.78 KiB | 346.00 KiB/s, done.
Resolving deltas: 100% (188/188), done.
</code></pre>

Then you need to cd into snapd package in order to install the Visual Studio Code 6.2

<pre class="wp-block-code"><code>$ cd snapd</code></pre>

When inside snapd, run the following code to install the vscode.

<pre class="wp-block-code"><code>$ makepkg -si
==&gt; Making package: visual-studio-code-bin 1.62.3-1 (Tue 30 Nov 2021 03:31:22 PM UTC)
==&gt; Checking runtime dependencies...
==&gt; Installing missing dependencies...</code></pre>

To open Visual studio code type **code** in your terminal and it will open the VScode

<pre class="wp-block-code"><code>$ code</code></pre>

## Conclusion {#conclusion.wp-block-heading}

You have successfully installed Visual Studio Code. I hope you have learned something. You can consult [Arch Wiki documentation.][6]

 [1]: https://nextgentips.com/2021/11/06/how-to-install-visual-studio-code-on-ubuntu-21-10/
 [2]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
 [3]: https://wiki.archlinux.org/title/PKGBUILD
 [4]: https://wiki.archlinux.org/title/Makepkg
 [5]: https://wiki.archlinux.org/title/Pacman#Additional_commands
 [6]: https://wiki.archlinux.org/title/Arch_User_Repository#Installing_and_upgrading_packages