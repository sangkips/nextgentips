---
title: How to install Rails 7 on Manjaro Linux
author: Kipkoech Sang
type: post
date: 2022-11-17T07:26:44+00:00
url: /2022/11/17/how-to-install-rails-7-on-manjaro-linux/
categories:
  - Programming

---
Rails is a web development framework written in Ruby programming language. It allows you to write less code but Rails is more of an opinionated language, so use it to accomplish whatever task you are trying to build. Rails believe in the principle of DRY, that is do not repeat yourself by all means 

## Installing Rails {.wp-block-heading}

Before installing Rails, you need to meet some prerequisites first, that is, have Ruby, nodejs, and Sqlite3 installed first before running Rails.

Most of the Linux-based OSes come preinstalled with Sqlite3, so we don&#8217;t need to install it. Check the version you have with your system.

<pre class="wp-block-code"><code>sqlite3 --version
3.39.4 2022-09-29 15:55:41 a29f9949895322123f7c38fbe94c649a9d6e6c9cd0c3b41c96d694552f26alt1</code></pre>

Check Ruby version also

<pre class="wp-block-code"><code>ruby -v
ruby 3.0.4p208 (2022-04-12 revision 3fa771dded) &#91;x86_64-linux]
#gem
gem -v
3.3.25
</code></pre>

Now we can go ahead and install Rails 

<pre class="wp-block-code"><code>gem install rails</code></pre>

You will be in a position to see the following as an output

<pre class="wp-block-code"><code>Fetching concurrent-ruby-1.1.10.gem
Fetching tzinfo-2.0.5.gem
Fetching i18n-1.12.0.gem
Fetching zeitwerk-2.6.6.gem
Fetching thor-1.2.1.gem
Fetching method_source-1.0.0.gem
Fetching activesupport-7.0.4.gem
Fetching nokogiri-1.13.9-x86_64-linux.gem
Fetching crass-1.0.6.gem
Fetching loofah-2.19.0.gem
Fetching rails-html-sanitizer-1.4.3.gem
Fetching rails-dom-testing-2.0.3.gem
Fetching rack-2.2.4.gem
Fetching rack-test-2.0.2.gem
Fetching erubi-1.11.0.gem
Fetching builder-3.2.4.gem
Fetching actionview-7.0.4.gem
Fetching actionpack-7.0.4.gem
Fetching railties-7.0.4.gem
Fetching mini_mime-1.1.2.gem
.....</code></pre>

If you happen to get the following error,

<pre class="wp-block-code"><code>WARNING:  You don't have /home/sang-pc/.local/share/gem/ruby/3.0.0/bin in your PATH,
	  gem executables will not run.
Successfully installed rails-7.0.4
Parsing documentation for rails-7.0.4
Done installing documentation for rails after 0 seconds
1 gem installed</code></pre>

Do the following on your terminal:

<pre class="wp-block-code"><code>export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"
gem list
gem update</code></pre>

Go back and install rails again `<mark style="background-color:#abb8c3" class="has-inline-color">gem install rails</mark>`. This round you will get a success message.

<pre class="wp-block-code"><code>Successfully installed rails-7.0.4
Parsing documentation for rails-7.0.4
Done installing documentation for rails after 0 seconds
1 gem installed</code></pre>

If you check the rails version this time round you will be in a position to see the version without any error.

<pre class="wp-block-code"><code>rails --version
Rails 7.0.4</code></pre>

Congratulations, you have successfully installed rails 7. Next, we will tackle how to scaffold your first application in rails