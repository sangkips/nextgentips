---
title: How to install Go 1.18 on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-12-21T13:41:13+00:00
url: /2021/12/21/how-to-install-go-1-18-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 58
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial, we are going to explore how to install go on Fedora 35.

[Golang][1] is an open-source programming language that is easy to learn and use. It is built-in concurrency and has a robust standard library. It is reliable, builds fast, and efficient software that scales fast.

Its concurrency mechanisms make it easy to write programs that get the most out of multicore and networked machines, while its novel-type systems enable flexible and modular program constructions.

Go compiles quickly to machine code and has the convenience of garbage collection and the power of run-time reflection.

In this guide, we are going to learn how to install golang 1.18 on Fedora 35.

Go is not yet released. There is so much work in progress with all the documentation.

The changes that have been introduced are the following:

  * Go 1.18 includes an implementation of [generic][2] features. This include backward-compatibity changes to the language.
  * The syntax for function and type declarations now accepts type parameters.
  * Parameterized functions and types can be instatiated by following them with a list of type arguments in square brackets.
  * the syntax for interface types now permits the embedding of arbitrary types as well as union.
  * The new predeclared identifier any is an alias for the empty interface. It maybe used instaead of interface {}.
  * The go 1.18 compiler now correctly reports declared but not used errors for variables that are set inside a function literal but are never used.
  * The go 1.18 compiler now reports an overflow when passing a rune constant expression. 
  * Go 1.18 introduces the new GOAMD64 environment variable which selects a version of AMD64 architecture.
  * Go get no longer builds or install packages in module-aware mod.
  * gofmt now reads and formats input files concurrently with a memory limit proportional GOMAXPROCS.
  * The vet tool is updated to support generic code.
  * The garbage collector now includes non-hip sources of garbage colector work when determining how frequently to run.

Get more information on [go 1.18 release notes][3].

## Related Articles  {.wp-block-heading}

  * [How to install Go 1.17 on Ubuntu 20.04][4]

## Install go on Fedora 35 {.wp-block-heading}

To install go we need to follow the following process.

## 1. Run system update {.wp-block-heading}

We run system update in order to make all our repositories up to date. To run the system update open your terminal and run the following command.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

## 2. Installing Go {.wp-block-heading}

To install go we need to download it from go download page. we are going to download the beta release. We are going to use the curl command to download.

<pre class="wp-block-code"><code>$ curl -LO https://go.dev/dl/go1.18beta1.linux-amd64.tar.gz</code></pre>

when the download is complete, extract the archive downloaded to your desired location. I will be using **/usr/local** directory.

<pre class="wp-block-code"><code>$ sudo tar -C /usr/local -xzf go1.18beta1.linux-amd64.tar.gz</code></pre>

After extraction is complete move ahead and set up the go environment.

## Set golang environment {.wp-block-heading}

To set up go environment we need to define the root of golang packages. We normally use **GOROOT** and **GOPATH** to define that environment. We need to set up the **GOROOT** location where the packages are installed.

<pre class="wp-block-code"><code>$ export GOROOT=/usr/local/go</code></pre>

Next, we will need to set up the GOPATH. Let&#8217;s set up GOPATH in the $HOME/go directory.

<pre class="wp-block-code"><code>$ export GOPATH=$HOME/go</code></pre>

Now we need to append the go binary PATH so that we can be able to access the program system-wide. Use the following command.

<pre class="wp-block-code"><code>$ export PATH=$GOPATH/bin:$GOROOT/bin:$PATH</code></pre>

To apply the changes we have made above, run the following command:

<pre class="wp-block-code"><code>$ source ~/.bashrc</code></pre>

Lastly, we can do the verification with the following;

<pre class="wp-block-code"><code>$ go version
go version go1.18beta1 linux/amd64</code></pre>

Let&#8217;s create a program to check all our settings. We will create a simple one.

Create a file hello.go on the main directory. Input the following.

<pre class="wp-block-code"><code>$ package main

import "fmt"

func main(){
	fmt.Println("Hello Nextgentips
}</code></pre>

To run the program use the following command;

<pre class="wp-block-code"><code>$ go run hello.go
Hello Nextgentips</code></pre>

## Conclusion. {.wp-block-heading}

We have successfully installed go 1.18beta1 on our Fedora 35 distro, go ahead and spin your programs. In case of any problem feel free to contact us, we will be happy to help.

 [1]: https://go.dev/doc/
 [2]: https://go.dev/blog/why-generics
 [3]: https://tip.golang.org/doc/go1.18
 [4]: https://nextgentips.com/2021/12/11/how-to-install-go-1-17-on-ubuntu-20-04/