---
title: How to install and Setup Microsoft Teams on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2022-01-10T12:51:54+00:00
url: /2022/01/10/how-to-install-and-setup-microsoft-teams-on-ubuntu-21-10/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:13:"66.249.75.102";i:1;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 39
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In today&#8217;s guide, we are going to explore how to install and setup Microsoft Teams in our ubuntu 20.04

Teams is a hub for teamwork that brings together chat, video meeting, calling, and collaboration. Teams primarily compete with slack in the essence that both have chat options in their structure. 

Microsoft Teams on opensource Linux enables high-quality experience collaboration between the open source community.

## Installing Microsoft Teams on Ubuntu 21.10 {.wp-block-heading}

I am going to show you two methods of installing Teams

  * Installing using snap method
  * Through downloading into your system and extracting 

## Installing Microsft Teams via download method. {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

Before we can do any download, we need to first update our Ubuntu repositories. We need to make them up to date so that we can not fall into errors while making our installation.

<pre class="wp-block-code"><code>$ sudo apt update 
$ sudo apt upgrade -y</code></pre>

You can reboot the system afterwards for the changes to take effect.

## 2. Download Microsoft Teams  {.wp-block-heading}

Head over to the [Microsoft download][1] page and copy the link for the Linux DEB. I am going to download using the wget command. Wget command allows us to download the executable file into our home directory then from there you can extract your content to the directory of your choice.

So what you do is to head over to the download page, right click and copy the link address of the Linux DEB file.

<pre class="wp-block-code"><code>$ wget https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/teams_1.4.00.26453_amd64.deb</code></pre>

This is what you will get as an output

<pre class="wp-block-code"><code>Output
Resolving packages.microsoft.com (packages.microsoft.com)... 13.81.215.193
Connecting to packages.microsoft.com (packages.microsoft.com)|13.81.215.193|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 84503304 (81M) &#91;application/octet-stream]
Saving to: ‘teams_1.4.00.26453_amd64.deb’

teams_1.4.00.26453_amd64.deb 100%&#91;===========================================>]  80.59M   189MB/s    in 0.4s    

2022-01-10 11:34:08 (189 MB/s) - ‘teams_1.4.00.26453_amd64.deb’ saved &#91;84503304/84503304]</code></pre>

After the download is complete, then you can download it then go to properties and copy the name contents if you have done the actual download.

<pre class="wp-block-code"><code>$ sudo dpkg -i teams_1.4.00.26453_amd64.deb</code></pre>

This will be the output you will get.

<pre class="wp-block-code"><code>(Reading database ... 106584 files and directories currently installed.)
Preparing to unpack teams_1.4.00.26453_amd64.deb ...
Unpacking teams (1.4.00.26453) over (1.4.00.26453) ...
Setting up teams (1.4.00.26453) ...</code></pre>

Allow the installation by pressing Y.

After this teams will be installed, and to launch it just type **teams** on your terminal

<pre class="wp-block-code"><code>$ teams </code></pre>

We need to remove Microsoft Teams packages before we can go ahead to install using snap.

To remove use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt remove teams </code></pre>

<pre class="wp-block-code"><code>Output
The following packages were automatically installed and are no longer required:
  adwaita-icon-theme alsa-topology-conf alsa-ucm-conf at-spi2-core dconf-gsettings-backend dconf-service
  fontconfig fontconfig-config fonts-dejavu-core gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme
  libasound2 libasound2-data libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3
  libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcolord2 libcups2 libdatrie1 libdconf1
  libdeflate0 libepoxy0 libfontconfig1 libgbm1 libgdk-pixbuf-2.0-0 libgdk-pixbuf-xlib-2.0-0 libgdk-pixbuf2.0-0
  libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common
  libharfbuzz0b libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libpango-1.0-0 libpangocairo-1.0-0
  libpangoft2-1.0-0 libpixman-1-0 librsvg2-2 librsvg2-common libthai-data libthai0 libtiff5 libwayland-client0
  libwayland-cursor0 libwayland-egl1 libwayland-server0 libwebp6 libx11-xcb1 libxcb-dri3-0 libxcb-render0
  libxcb-shm0 libxcomposite1 libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxrandr2
  libxrender1 libxtst6 ubuntu-mono x11-common
Use 'sudo apt autoremove' to remove them.
The following packages will be REMOVED:
  &lt;strong>teams&lt;/strong>
0 upgraded, 0 newly installed, 1 to remove and 0 not upgraded.
After this operation, 303 MB disk space will be freed.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow removal of teams to continue 

## Installing Microsoft Teams using Snap method {.wp-block-heading}

Using snap method is the easiest means and most appropriate, this is because snaps tend to update its packages automatically, so you will get the latest version without updating yourself.

To install the latest teams use the following command;

<pre class="wp-block-code"><code>$ sudo snap install teams-for-linux --edge</code></pre>

Its only one command and your install is complete 

This will be the output

<pre class="wp-block-code"><code>Output
teams-for-linux (edge) 1.0.22 from ismael (ismaelmartinez) installed</code></pre>

## Setting up Microsoft Teams  {.wp-block-heading}

Once you have started Microsoft Teams you will have to sign in with your email address<figure class="wp-block-image size-full is-resized">

<img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-10-15-15-45.png?resize=700%2C559&#038;ssl=1" alt="Microsoft Teams Login page " class="wp-image-949" width="700" height="559" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-10-15-15-45.png?w=820&ssl=1 820w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-10-15-15-45.png?resize=300%2C240&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-10-15-15-45.png?resize=768%2C613&ssl=1 768w" sizes="(max-width: 700px) 100vw, 700px" data-recalc-dims="1" /> <figcaption>Microsoft Teams Login page</figcaption></figure> 

Login to your account and get the code in order to invite your team into teams.

## Conclusion {.wp-block-heading}

We have successfully installed Microsoft Teams on our Ubuntu 21.10. Continue experimenting on this to learn more and Thank you for reading this article.

 [1]: https://www.microsoft.com/en-us/microsoft-teams/download-app#allDevicesSection