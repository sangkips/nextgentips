---
title: How to install go 1.18 on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-05-21T07:20:41+00:00
url: /2022/05/21/how-to-install-go-1-18-on-fedora-36/
rank_math_seo_score:
  - 20
rank_math_primary_category:
  - 36
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 177
categories:
  - Programming

---
<a href="https://go.dev/doc/" target="_blank" rel="noreferrer noopener">Golang</a>&nbsp;is an open-source programming language that is easy to learn and use. It is built-in concurrency and has a robust standard library. It is reliable, builds fast, and efficient software that scales fast.

Its concurrency mechanisms make it easy to write programs that get the most out of multicore and networked machines, while its novel-type systems enable flexible and modular program constructions.

Go compiles quickly to machine code and has the convenience of garbage collection and the power of run-time reflection.

In this guide, we are going to learn how to install golang 1.18 on Fedora 36.

There is so much work in progress with all the documentation so keep checking the go documentation for any changes.

The changes that have been introduced are the following:

  * Go 1.18 includes an implementation of <a href="https://go.dev/blog/why-generics" target="_blank" rel="noreferrer noopener">generic</a> features. This includes backward-compatibility changes to the language.
  * The syntax for function and type declarations now accepts type parameters.
  * Parameterized functions and types can be instantiated by following them with a list of type arguments in square brackets.
  * The syntax for interface types now permits the embedding of arbitrary types and unions.
  * The new predeclared identifier is an alias for the empty interface. It may be used instead of interface {}.
  * The go 1.18 compiler now correctly reports declared but not used errors for variables that are set inside a function literal but are never used.
  * The go 1.18 compiler now reports an overflow when passing a rune constant expression.
  * Go 1.18 introduces the new GOAMD64 environment variable which selects a version of AMD64 architecture.
  * Go 1.18 get no longer builds or installs packages in the module-aware mod.
  * gofmt now reads and formats input files concurrently with a memory limit proportional GOMAXPROCS.
  * The vet tool is updated to support generic code.
  * The garbage collector now includes non-hip sources of garbage collector work when determining how frequently to run.

## Related Articles {.wp-block-heading}

  * [How to install Go 1.18 on Fedora 35][1]
  * [How to install Go 1.17 on Ubuntu 20.04][2]

## Installing Go 1.18 on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system updates {.wp-block-heading}

To begin with, we need to run system updates in order to make our current repositories up to date. To do so we need to run the following command on our terminal.

<pre class="wp-block-code"><code>sudo dnf update -y</code></pre>

When updates are complete, go ahead to download go from its download page.

## 2. Installing Go {.wp-block-heading}

To install go we need to download it from go download page. we are going to download the beta release. We are going to use the curl command to download.

<pre class="wp-block-code"><code>$ curl -LO https://go.dev/dl/go1.18.2.linux-amd64.tar.gz</code></pre>

when the download is complete, extract the archive downloaded to your desired location. I will be using**&nbsp;/usr/local**&nbsp;directory.

<pre class="wp-block-code"><code>$ sudo tar -C /usr/local -xzf go1.18.2.linux-amd64.tar.gz</code></pre>

After extraction is complete move ahead and set up the go environment.

## 3. Setting Go Environment {.wp-block-heading}

To set up go environment we need to define the root of golang packages. We normally use&nbsp;**GOROOT&nbsp;**and&nbsp;**GOPATH**&nbsp;to define that environment. We need to set up the&nbsp;**GOROOT**&nbsp;location where the packages are installed.

<pre class="wp-block-code"><code>$ export GOROOT=/usr/local/go</code></pre>

Next, we will need to set up the GOPATH. Let’s set up GOPATH in the $HOME/go directory.

<pre class="wp-block-code"><code>$ export GOPATH=$HOME/go</code></pre>

Now we need to append the go binary PATH so that we can access the program system-wide. Use the following command.

<pre class="wp-block-code"><code>$ export PATH=$GOPATH/bin:$GOROOT/bin:$PATH</code></pre>

To apply the changes we have made above, run the following command:

<pre class="wp-block-code"><code>source ~/.bashrc</code></pre>

Lastly, we can do the verification with the following;

<pre class="wp-block-code"><code>go version
go version go1.18.2 linux/amd64</code></pre>

Let’s create a program to check all our settings. We will create a simple one.

Create a file main.go on the main directory.

<pre class="wp-block-code"><code>touch main.go</code></pre>

On your favorite text editor do the following

<pre class="wp-block-code"><code>sudo vi main.go</code></pre>

Insert the below code

<pre class="wp-block-code"><code>$ package main

import "fmt"

func main(){
	fmt.Println("Hello Fedora 36")
}</code></pre>

To run the program use the following command;

<pre class="wp-block-code"><code>go run main.go
Hello Fedora 36</code></pre>

## Conclusion {.wp-block-heading}

Now you know how to install golang 1.18 in Fedora 36. You can consult the <a href="https://go.dev/dl/" target="_blank" rel="noreferrer noopener">go documentation</a> in case you have any problems.

 [1]: https://nextgentips.com/2021/12/21/how-to-install-go-1-18-on-fedora-35/
 [2]: https://nextgentips.com/2021/12/11/how-to-install-go-1-17-on-ubuntu-20-04/