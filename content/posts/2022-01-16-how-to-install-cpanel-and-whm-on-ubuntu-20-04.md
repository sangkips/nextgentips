---
title: How to install Cpanel and WHM on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-16T20:39:14+00:00
url: /2022/01/16/how-to-install-cpanel-and-whm-on-ubuntu-20-04/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:15:"148.251.225.193";i:1;s:13:"88.99.148.111";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 31
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to **install Cpanel on Ubuntu 20.04**. Also, I will show you the **free alternatives to Cpanel**. This will make a big impact while trying to choose which free web hosting to use without worrying about the licenses. Cpanel provides a way where you can send emails by providing **email hosting services** which makes it easy to communicate with your customers.

Cpanel is a web hosting control panel. It provides a GUI interface where administrators key in the information they want to configure, this simplifies the process of hosting a web site to the website end-user.

## Cpanel free alternatives  {.wp-block-heading}

We have a ton of free and open-source alternatives to Cpanel. This includes 

  * CyberPanel free version
  * Ajenti 
  * Zpanel
  * Virtualmin GPL version
  * Froxlor
  * ISPconfig
  * aapanel

We have other tones of services, so it&#8217;s up to you the user to choose which satisfies your needs.

Check out these articles to get to know more about the above few alternatives.

  * [How to install aapanel 6 on Fedora 34/35][1]
  * [How to install and configure CyberPanel on CentOS 8][2]
  * [How to install Ajenti 2 Control Panel on Debian 11][3]

## Uses of Cpanel {.wp-block-heading}

Cpanel allows you to do the following:

  * **Publish new websites** after you have registered your domain either with godaddy or namecheap, I prefer using namecheap because its easy to navigate
  * Helps you organize web files 
  * You can cretate email accounts to use 
  * You can manage domains 
  * Check the status of your website
  * Run troubleshooting tasks within your website

## Prerequisites  {.wp-block-heading}

  * Make sure you have Ubuntu 20.04 up and running 
  * You have root access 
  * You have a hostname with fully qualified registered domain name 
  * Disable OS firewalls 
  * Make sure SELinux is disable if you have installed 
  * Make sure you have installedPerl 

## Install Cpanel on Ubuntu 20.04 {.wp-block-heading}

### 1. Run system updates  {.wp-block-heading}

It is always recommended to run system installation for freshly installed systems in order to make system repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

### 2. Install Screen for Ubuntu 20.04 {.wp-block-heading}

It is recommended that you install the screen on your server and run the Cpanel installation script inside the screen. To install the screen run the following.

Check if the screen is installed first

<pre class="wp-block-code"><code>$ screen --version
Screen version 4.08.00 (GNU) 05-Feb-20</code></pre>

If it&#8217;s not there, install the screen with the following command;

<pre class="wp-block-code"><code>apt install screen</code></pre>

Another thing to note is to make sure you have a fully qualified hostname. So to check your hostname type hostname on the command line.

<pre class="wp-block-code"><code>$ hostname 
ubuntu</code></pre>

Set a new hostname by editing the **/etc/hostname** 

<pre class="wp-block-code"><code>$ nano /etc/hostname</code></pre>

Remove Ubuntu and add the following 

<pre class="wp-block-code"><code>46.101.75.141 demo.nextgentips.com server</code></pre>

Restart your server for the changes to take effect.

### 3. Installing cpanel on Ubuntu 20.04 {.wp-block-heading}

To start installation run the **screen command** first.

<pre class="wp-block-code"><code>$ screen</code></pre>

If by chance you don&#8217;t have the screen installed run the following to install it

<pre class="wp-block-code"><code>$ apt install screen</code></pre>

Now to install Cpanel run the following command on your terminal

<pre class="wp-block-code"><code>$ cd /home && curl -o latest -L https://securedownloads.cpanel.net/latest && sh latest</code></pre>

Sample output will look like this

<pre class="wp-block-code"><code>output
&#91;2022-01-16 20:02:30 +0000] &#91;51136] ( INFO): Flushing the task queue
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): cPanel install finished in 14 minutes and 2 seconds!
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): Congratulations! Your installation of cPanel & WHM 11.100 is now complete. The next step is to configure your server. 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): Before you configure your server, ensure that your firewall allows access on port 2087.
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): After ensuring that your firewall allows access on port 2087, you can configure your server.
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 1. Open your preferred browser
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 2. Navigate to the following url using the address bar and enter this one-time autologin url:
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): https://46-101-75-141.cprapid.com:2087/cpsess6486417479/login/?session=root%3aXiINeEcF3a4g_a_A%3acreate_user_session%2c1740a98f4b1988077787ce31bf2e7ac2
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): After the login url expires you generate a new one using the 'whmlogin' command or manually login at:
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): https://46.101.75.141:2087
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): Visit https://go.cpanel.net/whminit for more information about first-time configuration of your server.
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): Visit http://support.cpanel.net or https://go.cpanel.net/allfaq for additional support
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): 
&#91;2022-01-16 20:02:31 +0000] &#91;51136] ( INFO): Thank you for installing cPanel & WHM 11.100!</code></pre>

The installation will take time, so relax and wait for it to finish.

It will install many dependencies including MariaDB for database management.

Head over to your favorite browser and type https://<your\_IP\_address>:2087

Alternatively, you are given a one-time URL to login with, use root as username and password set. Note: make sure you set the password first to continue using Cpanel.

You will get a one-time URL like this 

<pre class="wp-block-code"><code>$ https://46-101-75-141.cprapid.com:2087/cpsess6486417479/login/?session=root%3aXiINeEcF3a4g_a_A%3acreate_user_session%2c1740a98f4b1988077787ce31bf2e7ac2</code></pre>

When the above login URL expires you can generate a new one with this command 

<pre class="wp-block-code"><code>$ whmlogin</code></pre>

<pre class="wp-block-code"><code># WHM
http://&lt;ip_address>:2087</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="632" height="447" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-16-23-09-34.png?resize=632%2C447&#038;ssl=1" alt="Cpanel install login" class="wp-image-995" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-16-23-09-34.png?w=632&ssl=1 632w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-16-23-09-34.png?resize=300%2C212&ssl=1 300w" sizes="(max-width: 632px) 100vw, 632px" data-recalc-dims="1" /> <figcaption>Cpanel install login</figcaption></figure> 

In case you are not familiar with configuration head over to this [configuration site][4]

## Conclusion {.wp-block-heading}

Congratulation, you have learned how to install Cpanel on Ubuntu 20.04. In case of anything consult the documentation.

 [1]: https://nextgentips.com/2021/11/05/how-to-install-aapanel-6-on-fedora-34/
 [2]: https://nextgentips.com/2021/11/13/how-to-install-and-configure-cyberpanel-on-centos-8/
 [3]: https://nextgentips.com/2021/11/26/how-to-install-ajenti-2-control-panel-on-debian-11/
 [4]: https://go.cpanel.net/whminit "configuration site"