---
title: How to install pgAdmin 4 on Debian 11
author: Kipkoech Sang
type: post
date: 2022-10-11T14:40:36+00:00
url: /2022/10/11/how-to-install-pgadmin-4-on-ubuntu-22-04-2/
categories:
  - Linux

---
<p id="block-8da8d042-655d-49f9-ae39-60b136f95f11">
  In this tutorial guide, I will be taking you through the installation of pgAdmin 4 on Debian 11.
</p>

<p id="block-c25f20f0-7ea5-4f87-b871-3272bf0392b2">
  pgAdmin 4 is a free and open-source management tool for Postgres. Its desktop runtime written in NWjs allows it to run standalone for individual users, or the web applications code may be directly deployed on a web server for use by the web browser.
</p>

<p id="block-b96aa803-9e57-4a28-97a6-9f516bcf6cce">
  pgAdmin 4 is a complete rewrite of pgAdmin, built using Python and Java
</p>

### New features of pgAdmin 4 {#block-f81df0c8-ff6e-457d-8729-c12961d7be8c.wp-block-heading}

<p id="block-fb43e20f-7564-4121-b03b-d3b8c79c78ed">
  The following new features were introduced for the latest release of 6.2. these features are mostly for bug fixes.
</p>

<ul id="block-81054e13-22bd-407d-949c-1a17dd7d9455">
  <li>
    It added support for <a href="https://www.postgresql.org/docs/9.1/functions-aggregate.html">Aggregate and Operator node</a> in view-only mode
  </li>
  <li>
    It ensures that users will be able to modify the REMOTE_USER environment variable as per their environment by introducing the new config parameter WEBSERVER_REMOTE_USER.
  </li>
</ul>

<p id="block-ce654752-ed84-46b8-ba31-7f339de0408b">
  You can refer to <a href="https://www.pgadmin.org/docs/pgadmin4/latest/release_notes_6_2.html">pgAdmin 4 documentation</a> for more concerning the bug fixes introduced with the current version 6.2.
</p>

## Prerequisites {#block-c746f701-5a2e-4894-b8e6-88785971d6e3.wp-block-heading}

<ol id="block-10ca7bf7-1f6d-40fa-bd3e-730677d2864f">
  <li>
    Have a PostgreSQL server running on Debian 11
  </li>
  <li>
    Debian 11 server
  </li>
  <li>
    User with sudo privileges
  </li>
  <li>
    Access to the internet.
  </li>
</ol>

## Table of Contents {#block-43c267d8-5e2a-44e1-9b9c-908adcbe8eee.wp-block-heading}

<ol id="block-fa7c52ac-335a-461f-a90c-f75db930e984">
  <li>
    Update Debian 11 repositories
  </li>
  <li>
    Install PostgreSQL server
  </li>
  <li>
    Install the public key for the repository
  </li>
  <li>
    Create repository configuration files
  </li>
  <li>
    Install pgAdmin 4
  </li>
  <li>
    Configure web server
  </li>
  <li>
    Conclusion
  </li>
</ol>

### 1. Run Debian 11 updates {#block-49e49563-682b-414f-9ef1-ab9d0c7ec7a4.wp-block-heading}

<p id="block-8a3de422-35c8-4597-a0cb-d6a0ebf9931c">
  In order to begin our installation, we will first update our repositories in order to make them up to date. on your terminal run the following commands;
</p>

<pre id="block-fe517968-2da7-4bd5-bba3-132be6ed5533" class="wp-block-code"><code>$ sudo apt update&lt;br>$ sudo apt upgrade -y</code></pre>

<p id="block-723e9c58-a14b-4d94-9c83-6ea378fb7899">
  The commands above will update the system and upgrade packages accordingly.
</p>

## 2. Install PostgreSQL 14 server {#block-6f2317eb-84b4-41c5-a39e-349b349bbb23.wp-block-heading}

<p id="block-c6a093f3-a1ac-4ab9-b04f-457e35016207">
  To install PostgreSQL 14 server, follow the following steps;
</p>

### Create a directory {#block-3eeed6d7-dae1-4044-ad31-8c6e1d228c39.wp-block-heading}

<pre id="block-5cd2f51e-56e7-46af-872f-6c3faaa1292c" class="wp-block-code"><code>$ sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt (lsb_release -cs)-pgdg main" &gt; /etc/apt/sources.list.d/pgdg.list'</code></pre>

<p id="block-21e29ea6-f550-4aae-b177-24d6da74ab9c">
  After you have added the following script, now you can add the GPG key.
</p>

### Import GPG Key {#block-66f03823-31df-4488-a0da-af62c9c1dd1c.wp-block-heading}

<p id="block-ac3f4be5-91aa-4c1d-a9ec-8fb16d5121f1">
  Import the GPG key pair in order to for it to be signed correctly. Use the following command to import the key.
</p>

<pre id="block-f350a59d-f4dc-4982-90b9-1693e5010cbe" class="wp-block-code"><code>$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -</code></pre>

<p id="block-b5216a2e-3426-4ea4-bc31-8994c940bc3e">
  You will get an ok as output meaning you have successfully added the GPG key.
</p>

<p id="block-c630c8eb-aeb2-4f65-9a73-625c7d566007">
  Next, we will run the updates.
</p>

<pre id="block-fc816ae3-108f-444a-8389-f91da6fab511" class="wp-block-code"><code>$ sudo apt update</code></pre>

<p id="block-03d3873d-0e00-4b0b-a749-d07a60c19100">
  Now we can install the latest version of PostgreSQL
</p>

### Install PostgreSQL 14 on Debian 11 {#block-233ffcfe-abe6-4314-a7a4-8d7672308f22.wp-block-heading}

<p id="block-8cbd5622-cc19-4d63-bc57-d50f0aa81eb0">
  Use the following command to install a specific version of PostgreSQL
</p>

<pre id="block-d8bdc3a9-2afa-433c-8e80-43a6c90b5b4f" class="wp-block-code"><code>$ sudo apt install postgresql-14 -y</code></pre>

<p id="block-7df76e3f-a123-4a46-abf8-202e2899c4ed">
  Sample output will be as follows
</p>

<pre id="block-59e90047-7d0f-499f-baaf-034056ca9b89" class="wp-block-code"><code>...
Creating config file /etc/postgresql-common/createcluster.conf with new version
Building PostgreSQL dictionaries from installed myspell/hunspell packages...
Removing obsolete dictionary files:
Created symlink /etc/systemd/system/multi-user.target.wants/postgresql.service →
 /lib/systemd/system/postgresql.service.
Setting up postgresql-14 (14.5-0ubuntu0.22.04.1) ...
Creating new PostgreSQL cluster 14/main ...
/usr/lib/postgresql/14/bin/initdb -D /var/lib/postgresql/14/main --auth-local pe
er --auth-host scram-sha-256 --no-instructions
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "C.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".</code></pre>

<p id="block-76a181fb-08d9-46e4-8062-29a4d8f1b38b">
  We have successfully installed PostgreSQL. We can proceed to install pgAdmin 4.
</p>

### 3. Add public key for pgAdmin 4  {#block-a72756da-0a48-48a8-baa8-87e922020f99.wp-block-heading}

<p id="block-57056868-0805-4c40-b16a-2af2e9cc6496">
  Use the following command to add the public key to your repository.
</p>

<pre id="block-6d680d8c-479a-4fea-a421-3d6d7f42628b" class="wp-block-code"><code>$ curl -fsSL https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/pgadmin.gpg</code></pre>

### 4. Create pgAdmin 4 repository file {#block-be078508-53f3-446c-995e-341c8637472e.wp-block-heading}

<p id="block-0ff3327f-ac5e-4d55-8145-cdd42bca8e52">
  Then we can go ahead and add a configuration file that can aid in pgAdmin 4 installation.
</p>

<pre id="block-a809cef9-8d6e-4a30-9549-0a0bac948cd3" class="wp-block-code"><code>$ sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" &gt; /etc/apt/sources.list.d/pgadmin4.list'</code></pre>

<p id="block-f9f8e3f4-ed8a-4422-95fc-e575368bab75">
  Update your system now for the changes to take effect before installing the pgAdmin 4.
</p>

### 5. Install pgAdmin 4  {#block-2a74834e-f8fc-4879-af32-a1ca902b8dd0.wp-block-heading}

<p id="block-9fbf9b6c-bb97-4326-8d3d-61a5db4b6092">
  We will install pgAdmin 4 for both web and desktop environments.
</p>

<pre id="block-57606a3a-96a4-4200-b519-eead7d0a8831" class="wp-block-code"><code>$ sudo apt install pgadmin4</code></pre>

<p id="block-be28b6cb-d816-4f64-91f9-f9e780069b49">
  Sample output from the installation
</p>

<pre id="block-a19f0069-eecd-484e-b255-230a111b372e" class="wp-block-code"><code>...
Created symlink /etc/systemd/system/multi-user.target.wants/apache2.service → /lib/systemd/system/apache2.service.
Created symlink /etc/systemd/system/multi-user.target.wants/apache-htcacheclean.service → /lib/systemd/system/apache-htcacheclean.service.
Setting up libxaw7:amd64 (2:1.0.13-1.1) ...
Setting up libapache2-mod-wsgi-py3 (4.7.1-3build2) ...
apache2_invoke: Enable module wsgi
Setting up x11-xserver-utils (7.7+8build1) ...
Setting up x11-utils (7.7+5build1) ...
Setting up pgadmin4-web (6.2) ...
Setting up pgadmin4 (6.2) ...
Setting up liblwp-protocol-https-perl (6.10-1) ...
Setting up libwww-perl (6.53-1) ...
Setting up libxml-parser-perl:amd64 (2.46-2) ...                           </code></pre>

<p id="block-eb3a17f5-9bab-4ed1-8d1e-fbe78d877116">
  Having completed the installation of pgAdmin 4 we now need to connect with our Apache web server
</p>

### 6. Configure Apache for pgAdmin 4 {#block-671b5f7f-81c2-4348-ae70-ed4d99b7a335.wp-block-heading}

<p id="block-ec521df2-7efc-4429-b542-a84b08f36c63">
  Let&#8217;s serve our pgAdmin 4 using an Apache webserver.
</p>

<p id="block-07329084-f486-4ef0-8b33-d7504e7fb11f">
  To do so we need to initiate the Apache webserver configuration with the following script.
</p>

<pre id="block-c6f55263-5dab-477d-b86b-e751d519624f" class="wp-block-code"><code>$ sudo /usr/pgadmin4/bin/setup-web.sh</code></pre>

<p id="block-1ddd25c5-5883-4181-88ca-7fecea70c29f">
  You will need to configure the email address and password to continue using it.
</p>

<p id="block-e385dd59-6f36-4f45-93de-705705e125ad">
  The following is the output you will get.
</p>

<pre id="block-a553bcfc-0a02-44c1-aa4e-ef4bec9f9875" class="wp-block-code"><code>...
Setting up pgAdmin 4 in web mode on a Debian based platform...
Creating configuration database...
NOTE: Configuring authentication for SERVER mode.

Enter the email address and password to use for the initial pgAdmin user account:

Email address: nextgentips01@gmail.com
Password: 
Retype password:
pgAdmin 4 - Application Initialisation
======================================

Creating storage and log directories...
We can now configure the Apache Web server for you. This involves enabling the wsgi module and configuring the pgAdmin 4 application to mount at /pgadmin4. Do you wish to continue (y/n)? y
The Apache web server is running and must be restarted for the pgAdmin 4 installation to complete. Continue (y/n)? y
Apache successfully restarted. You can now start using pgAdmin 4 in web mode at http://127.0.0.1/pgadmin4</code></pre>

<p id="block-9b82d9c9-550e-4c55-a380-2491479ca9ac">
  To access pgadmin 4 from the browser go to this web address <strong>http://<your_server_IP>/pgadmin4</strong>
</p><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="312" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-11-17-19-58.png?resize=810%2C312&#038;ssl=1" alt="nextgentips: pgAdmin4 " class="wp-image-1582" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-11-17-19-58.png?w=915&ssl=1 915w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-11-17-19-58.png?resize=300%2C115&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-11-17-19-58.png?resize=768%2C295&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: pgAdmin4 </figcaption></figure> 

<p id="block-995ee126-16ac-4f5c-b730-57bd8fd3bc21">
  Use the credentials you set above to login
</p><figure class="wp-block-image" id="block-6ad1a579-1026-4106-bf32-e8dc07c4cd88">

<img decoding="async" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-19-13-51-15.png?resize=810%2C464&#038;ssl=1" alt="pgAdmin Dashboard" data-recalc-dims="1" /> <figcaption>pgAdmin Dashboard</figcaption></figure> 

## Conclusion {#block-82817028-15c1-499e-8b6f-b226328c45c8.wp-block-heading}

<p id="block-091da145-4905-43f6-933b-67cadcd69aa7">
  We have successfully installed pgAdmin 4 in our system. Go ahead and experiment more to gain more insight.
</p>