---
title: How to Install MongoDB 5 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-26T18:18:08+00:00
url: /2021/12/26/how-to-install-mongodb-5-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 50
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial guide, we are going to learn how to install MongoDB 5.0 on Ubuntu 20.04. 

MongoDB is an open-source NoSQL database that provides high throughput for data-driven applications. Unlike relational databases such as MySQL, Oracle, and SQL servers which store data in tables according to a rigid schema, MongoDB stores data in documents with flexible schema.

## Why do we need MongoDB? {.wp-block-heading}

  * MongoDB makes it easy for developers to store structured or unstructured data, it uses JSON-like format to store documents. This format directly maps to native objects in most programming languages, making natural database for most developers.
  * MongoDB is built on a scale-out architecture making popular for developers developing scalable applications with evolving data schemas.
  * MongoDB is available in every major public cloud provider such as Azure, AWS, GCP making it easy for developers to deploy to any cloud provider of choice.
  * MongoDB supports rapid iterative development where developers collaborate with larger teams.
  * In MongoDB records are stored as documents in compressed&nbsp;[BSON][1]&nbsp;files.

MongoDB documents can be retrieved in JSON formats which has many benefits such as:

  * it is human readable, makes it easy to be read
  * It is a natural form to store data
  * You can nest JSON to to store complex data objects.
  * Documents maps to objects in most popular programming languages.
  * Structured and unstructured information can be stored in the same document
  * JSON has a flexible and dynamic schema, so adding fields or leaving a field out is not a problem.

## Prerequisites  {.wp-block-heading}

  * Have Ubuntu 20.04 server running 
  * Have a user with sudo priviliges 
  * MongoDB only supports 64 bits version of Linux, so make sure you are running x64.

## Related Articles  {.wp-block-heading}

  * [How to Install MongoDB with Podman on Rocky Linux 8][2]
  * [How to install MongoDB 4.4 on Fedora 35][3]
  * [How to Deploy MongoDB with Docker][4]
  * [How to install MongoDB 5 on Ubuntu 21.04][5]

## Installing MongoDB 5 on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

To begin our installation, we need to update our repositories to make them up to date. open your terminal and type in the following command;

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Add MongoDB GPG key  {.wp-block-heading}

To begin with, let&#8217;s import the GPG key used to sign MongoDB. To import use the following command on your terminal.

<pre class="wp-block-code"><code>$ wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -</code></pre>

You will get ok as the output on the above command. If so you can move ahead.

## 3. Create a list file for MongoDB {.wp-block-heading}

We need to create sources.list.d file in our MongoDB repository. This repository helps us to easily add new repositories without the need to edit the central file. 

To add this file use the following command on your terminal;

<pre class="wp-block-code"><code>$ echo "deb &#91; arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu \
	focal/mongodb-org/5.0 \
	multiverse" | \
	sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list</code></pre>

You need to run the system update again to update our repositories. Make sure when you you update, you can see the Mongodb-org added otherwise it won&#8217;t work.

## 4. Install MongoDB on Ubuntu 20.04 {.wp-block-heading}

Having satisfied all the requirements we can now run the MongoDB installer in our system.

<pre class="wp-block-code"><code>$ sudo apt install mongodb-org -y</code></pre>

Sample output

<pre class="wp-block-code"><code>Output
 The following additional packages will be installed:
  mongodb-database-tools mongodb-mongosh mongodb-org-database mongodb-org-database-tools-extra
  mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools
The following NEW packages will be installed:
  mongodb-database-tools mongodb-mongosh mongodb-org mongodb-org-database mongodb-org-database-tools-extra
  mongodb-org-mongos mongodb-org-server mongodb-org-shell mongodb-org-tools
0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.</code></pre>

To check the version of installed Mongod, run the following command;

<pre class="wp-block-code"><code>$ mongod --version
db version v5.0.5
Build Info: {
    "version": "5.0.5",
    "gitVersion": "d65fd89df3fc039b5c55933c0f71d647a54510ae",
    "openSSLVersion": "OpenSSL 1.1.1f  31 Mar 2020",
    "modules": &#91;],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "ubuntu2004",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}</code></pre>

## 5. Configuring MongoDB {.wp-block-heading}

To start using MongoDB we need to start the MongoDB.service, to do that run the following command;

<pre class="wp-block-code"><code>$ sudo systemctl start mongod.service</code></pre>

Then after starting you need to enable it to start every time you boot up your device.

<pre class="wp-block-code"><code>$ sudo systemctl enable mongod.service </code></pre>

Lastly, you can check the status of our MongoDB service if it is running as expected.

<pre class="wp-block-code"><code>$ sudo systemctl status mongod.service</code></pre>

<pre class="wp-block-code"><code>Output
● mongod.service - MongoDB Database Server
     Loaded: loaded (/lib/systemd/system/mongod.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2021-12-26 17:58:47 UTC; 1min 5s ago
       Docs: https://docs.mongodb.org/manual
   Main PID: 16016 (mongod)
     Memory: 63.5M
     CGroup: /system.slice/mongod.service
             └─16016 /usr/bin/mongod --config /etc/mongod.conf

Dec 26 17:58:47 ubuntu systemd&#91;1]: Started MongoDB Database Server.</code></pre>

Our mongod service is and running as expected.

To start using MongoDB type on your terminal **mongosh**, it will show something like thus output;

<pre class="wp-block-code"><code>Output
Current Mongosh Log ID: 61c8aee64a2d61cf7a3bb6c7
Connecting to:          mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000
Using MongoDB:          5.0.5
Using Mongosh:          1.1.7

For mongosh info see: https://docs.mongodb.com/mongodb-shell/


To help improve our products, anonymous usage data is collected and sent to MongoDB periodically (https://www.mongodb.com/legal/privacy-policy).
You can opt-out by running the disableTelemetry() command.

------
   The server generated these startup warnings when booting:
   2021-12-26T17:58:48.132+00:00: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine. See http://dochub.mongodb.org/core/prodnotes-filesystem
   2021-12-26T17:58:48.724+00:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
------

test> </code></pre>

Now you are good to run your first database instance.

If you want to uninstall MongoDB, first you will have to stop the MongoDB running instance and then remove the packages

**Stop MongoDB instance** 

<pre class="wp-block-code"><code>$ sudo systemctl stop mongod.service </code></pre>

**Remove mongod packages** 

To completely remove the packages run the following command;

<pre class="wp-block-code"><code>$ sudo apt purge mongodb-org*</code></pre>

## Conclusion. {.wp-block-heading}

We have learned how to install MongoDB on Ubuntu 20.04. I am glad you have enjoyed the tutorial. Feel free to comment in case you are faced with a challenge.

 [1]: https://www.mongodb.com/json-and-bson
 [2]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-with-podman-on-rocky-linux-8/
 [3]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-4-4-on-fedora-35/
 [4]: https://nextgentips.com/2021/10/26/how-to-deploy-mongodb-with-docker/
 [5]: https://nextgentips.com/2021/10/26/how-to-install-mongodb-5-on-ubuntu-21-04/