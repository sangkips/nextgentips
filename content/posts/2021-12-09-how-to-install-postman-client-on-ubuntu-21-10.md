---
title: How to install Postman client on Ubuntu 21.10|20.04|22.04
author: Kipkoech Sang
type: post
date: 2021-12-09T10:45:20+00:00
url: /2021/12/09/how-to-install-postman-client-on-ubuntu-21-10/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 71
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
Postman is an API platform for building and using APIs. Postman simplifies each step of the API lifecycle and streamlines collaborations so you can create better APIs faster.

So what is API? API is an acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other.

In this tutorial, I will take you through the installation of Postman on Ubuntu 21.10

## What is Postman used for? {.wp-block-heading}

Postman is an application used for API testing. It is an HTTP client that tests HTTP requests, utilizing a graphical interface, through which we obtain different types of responses that need to be subsequently validated.

## How does Postman work? {.wp-block-heading}

Postman works in the backend by creating requests and giving responses to make sure that it contains elements needed in an API. Postman allows us to build, test, and modify scripts. Postman can make requests like Get, Post, Delete, Put and Patch.

## Automated Testing with Postman {.wp-block-heading}

Postman tests are automated by creating test suits that can run every time. The benefit of automated tests is to eliminate human error as much as possible. Postman can automate tasks such as:

  * Functional tests. This is a type of software testing that validates the software system against functional requirements specified during project inception. It involve black box testing and its not concerned about any source code. It involves checking the interface, APIs, database etc.
  * Integration tests. This is where individual software modules are combined and tested as a group. It is conducted to evaluate the compliance of a system
  * Unit tests. A unit is a smallest piece of code that can be logically isolated in a system. Unit testing can be done on almost everything from a line of code, a method or a class.
  * Regression tests. This is a type of testing that is done to verify that a code change in the software does not impact the existing functionality of the system.

## Reasons for Automating Postman API Tests {.wp-block-heading}

  * To make scaling easy. As programs grow chances of breakage tend to increase. So to avoid breakage try to create more tests and create bug resistant programs and have tests frequently.
  * To streamline development and quality assurance with a CI/CD pipeline. Postman allows you to reuse your test suites to create a CI/CD so you can test at every push. You can seamlessly integrate your Postman instance with Jenkins to create your own CI/CD pipeline or add it to your existing pipeline as a build step.
  * To avoid tedious work. Doing automated tests means you will spend less money on Quality assurance, reduce time spend in office by employees and also spend less time in debugging codes.
  * Reduce feedback cycles and improve insights. feedback is very important while doing any project, so faster feedback streamlines communication across all teams. This will help in reduction in bugs which can arise during the process.
  * To remove human error, improve accuracy. Automated tests remove human error as much as possible.
  * To reduce the need for specialist skills
  * To enable testing be done simultaneously across platforms and devices. In this age many platforms exist such as mobile, cloud, automated tests helps to bridge the gap on compatibility across different operating systems, platforms and devices. Make sure everything runs seamlessly with no problems.
  * To accelerate time to market. We are trying to reduce the time duration in which the project is released to the market. Less time spent on the project means less costs to the entire project life cycle.

## Why do we need Postman? {.wp-block-heading}

We require a postman because of the following reasons:

  * It is free and easy to use. Postman is free to download and use for up to 3 members team. You can download and send requests as you wish.
  * It has a larger community base. Larger community base means in case of a problem you have a wide network to ask for assistance and also project maintenance is not an issue so improvement of the project will be key.
  * It has wide support for all APIs and schema. Postman has a built in support for popular data formats such OpenAPI, GraphQL and RAML. You can make any kind of API call and you can inspect very large responses with ease.
  * It is extensible. You can customize Postman to your needs with the use of Postman API. You can also Integrate test suites into your preferred CI/CD service with&nbsp;[Newman][1], our command line collection runner

## Related Articles  {.wp-block-heading}

  * [How to install Postman on Debian 11][2]

## Prerequisites  {.wp-block-heading}

  * Have a basic knowledge of terminal
  * Have Ubuntu 21.10 up and running 
  * Have user with non root or with sudo privileges. Check this article on how to create user with sudo privileges. [How to setup Ubuntu 20.04 Server for the first time][3]

## Install Postman client on Ubuntu 21.10 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

We need to update our system repositories to make them up to date. Run system updates with the following command.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When both updates and upgrades are complete proceed to download Postman.

## 2. Installing Postman from the source  {.wp-block-heading}

To install Postman from the source, go to the Postman [download page][4]. Now you can run the following command to download with wget as an alternative way. I am going to use wget command.

<pre class="wp-block-code"><code>$ wget https://dl.pstmn.io/download/latest/linux64 -O postman-linux-x64.tar.gz</code></pre>

<pre class="wp-block-code"><code>output
--2021-12-09 10:32:39--  https://dl.pstmn.io/download/latest/linux64
Resolving dl.pstmn.io (dl.pstmn.io)... 13.227.219.93, 13.227.219.54, 13.227.219.120, ...
Connecting to dl.pstmn.io (dl.pstmn.io)|13.227.219.93|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 154124109 (147M) &#91;application/gzip]
Saving to: ‘postman-linux-x64.tar.gz’

postman-linux-x64.tar.gz     100%&#91;===========================================&gt;] 146.98M   170MB/s    in 0.9s    

2021-12-09 10:32:40 (170 MB/s) - ‘postman-linux-x64.tar.gz’ saved &#91;154124109/154124109]</code></pre>

When the download is complete, unpack the compressed tarball to opt directory.

<pre class="wp-block-code"><code>$ sudo tar -xzf postman-linux-x64.tar.gz -C /opt</code></pre>

Now that you have extracted all the contents to the opt directory, now you can create a symbolic link with the following command.

<pre class="wp-block-code"><code>$ sudo ln -s /opt/Postman/Postman /usr/bin/postman</code></pre>

Lastly, to launch Postman type **postman** on your terminal

<pre class="wp-block-code"><code>$ postman</code></pre>

## Create a desktop launcher {.wp-block-heading}

To launch Postman from the icon launcher, you need to create a desktop icon. Create a postman.desktop file and save in this location.

<pre class="wp-block-code"><code> /usr/share/applications/Postman.desktop</code></pre>

Now we can create .desktop icon with the following command;

<pre class="wp-block-code"><code>$ sudo nano /usr/share/applications/Postman.desktop</code></pre>

Enter the following content 

<pre class="wp-block-code"><code>&#91;Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=/opt/Postman/app/Postman %U
Icon=/opt/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;</code></pre>

Save and close the file. You can now be able to launch Postman from the desktop launcher.

## Conclusion {.wp-block-heading}

Happy that you have learned something from this tutorial.

 [1]: https://www.npmjs.com/package/newman
 [2]: https://nextgentips.com/2021/10/20/how-to-install-postman-on-debian-11/
 [3]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [4]: https://www.postman.com/downloads/