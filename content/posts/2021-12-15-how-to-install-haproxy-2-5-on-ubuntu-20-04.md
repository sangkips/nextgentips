---
title: How to install HAProxy 2.5 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-15T14:47:14+00:00
url: /2021/12/15/how-to-install-haproxy-2-5-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 62
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
HAProxy is a free, very fast, and reliable reverse proxy offering high availability, load balancing, and proxying for TCP and HTTP-based applications. It is suited for very high-traffic websites and runs the most visited sites.

**What is load balancing?**

Load balancing ensures the availability, uptime, and performance of your servers, websites, and applications during traffic spikes. 

In this tutorial, we are going to learn how to install the latest HAProxy on our Ubuntu 20.04.

## What makes HAProxy so good? {.wp-block-heading}

  * Its **Perfomance** is on another level. HAProxy scales very fast with threads. It can reach 2 million requests over SSL and 100Gpbs for forwarded traffic. This is made possible by event-driven architecture that makes it reacts extremely quickly to any input/output requests.
  * **Reliability** of HAProxy is exceptional. Alot of effort is made in the direction of making HAProxy more reliable. The development process encourages quality with a long term development cycle. User who use HAProxy have faced minimal problems because the team who develops this are maintained for 5 years in the process of development.
  * It has **harden security**. HAProxy employs a number of security mechanism such as, use of chroot, priviledge drops, fork prevention, strict protocol, and detailed traces incase of vialotion detection. 

## New Features of HAProxy {.wp-block-heading}

  * It provided better usability around setting variables 
  * Has better and descriptive error reporting and logging 
  * Has enhanced HTTP and web socket support. 
  * The Lua integration now supports an HTTP client for initiating HTTP requests and their is early support for QUIC and HTTP/3
  * HAProxy gives you best in class observability into the traffic that passes throuh it. 

## Installing HAProxy on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system updates {.wp-block-heading}

In oreder to make our repositories current, we need to update our system with the following update command.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates are complete, next is to add Vbernat PPA.

## 2. Enable Vbernet PPA for HAProxy {.wp-block-heading}

The reason we are using the PPA here is to ensure that we install the latest version of HAProxy with the newest cool features. Add the following command to install software common properties.

<pre class="wp-block-code"><code>$ sudo apt-get install --no-install-recommends software-properties-common</code></pre>

This command helps you to manage any PPAs you installed in your system. 

Next is to add Vbernet PPA to our system with the following command.

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:vbernat/haproxy-2.5</code></pre>

sample output

<pre class="wp-block-code"><code>Output
HAProxy is a free, very fast and reliable solution offering high availability, load balancing, and proxying for TCP and HTTP-based applications. It is particularly suited for web sites crawling under very high loads while needing persistence or Layer7 processing. Supporting tens of thousands of connections is clearly realistic with todays hardware. Its mode of operation makes its integration into existing architectures very easy and riskless, while still offering the possibility not to expose fragile web servers to the Net.

This PPA contains packages for HAProxy 2.5.
 More info: https://launchpad.net/~vbernat/+archive/ubuntu/haproxy-2.5
Press &#91;ENTER] to continue or Ctrl-c to cancel adding it.</code></pre>

Press enter to allow the installation to continue. 

You need to run system-wide updates again for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update
&lt;strong>Hit:1 http://ppa.launchpad.net/vbernat/haproxy-2.5/ubuntu focal InRelease&lt;/strong>
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:3 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease                                            
Hit:4 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease   </code></pre>

## 3. Install HAProxy 2.5 {.wp-block-heading}

To install the latest HAProxy run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install haproxy=2.5.\*</code></pre>

Sample output

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Selected version '2.5.0-1ppa1~focal' (HAProxy 2.5:20.04/focal &#91;amd64]) for 'haproxy'
The following additional packages will be installed:
  liblua5.3-0
Suggested packages:
  vim-haproxy haproxy-doc
The following NEW packages will be installed:
  haproxy liblua5.3-0
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 1753 kB of archives.
After this operation, 4356 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue

Adding **2.5.\*** tells apt tells apt to maintain the latest version of HAProxy.

To check the version of HAProxy installed, use the following command.

<pre class="wp-block-code"><code>$ haproxy -v
HAProxy version 2.5.0-1ppa1~focal 2021/11/26 - https://haproxy.org/
Status: stable branch - will stop receiving fixes around Q1 2023.
Known bugs: http://www.haproxy.org/bugs/bugs-2.5.0.html
Running on: Linux 5.4.0-91-generic #102-Ubuntu SMP Fri Nov 5 16:31:28 UTC 2021 x86_64</code></pre>

## Conclusion {.wp-block-heading}

In this guide we have learned how to install HAProxy 2.5 load balancer on Ubuntu 20.04. For more information you can consult [HAProxy rich documentation][1].

 [1]: http://www.haproxy.org/