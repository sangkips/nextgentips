---
title: How to create a Custom User in Django
author: Kipkoech Sang
type: post
date: 2022-11-15T17:37:25+00:00
url: /2022/11/15/how-to-create-a-custom-user-in-django/
categories:
  - Programming

---
This tutorial will teach how to create a custom user in Django.

Django comes with good authentication, which uses a username and password, but sometimes one is unsatisfied with what Django offers. You might want to change the way users login into your system, such as using email and password, here you will have to create a custom user model to gather for your needs. 

## Ways you can create a custom user model {.wp-block-heading}

### Extending the existing user model {.wp-block-heading}

Extending the existing user depends on two things:

  * If the changes you are trying to make are behavioral and don&#8217;t require any modification in what appears in the database, here you can use a proxy model. A proxy model creates inheritance without creating a new table in the database. For example, changing the default ordering style, this doesn&#8217;t create any new tables.
  * If the changes you are trying to make will affect the behavior of the database, such as having a user and a profile section. Here you can use a OneToOneField so that you will be in a position to create only one relationship to each field. Here are trying to store extra information about the user but it doesn&#8217;t relate in any way to the authentication process.

### Substituting a custom user model {.wp-block-heading}

In some instances, it&#8217;s better to use an email address to authenticate users instead of a username. Django allows overriding the default User model by providing `<strong>AUTH_USER_MODEL</strong>` that reference to the custom user. This is done in the settings.

<pre class="wp-block-code"><code>AUTH_USER_MODEL = 'accounts.CustomUser'</code></pre>

It&#8217;s highly recommended to set up the custom user model at the beginning stage of initiating the project to avoid creating conflicts with foreign keys created in the database. If this happens you will have to re-engineer the database to avoid losing precious data. So do it before that first migration.

### Extending AbstractUser {.wp-block-heading}

You use this method when you are satisfied with how Django handles the authentication process, your aim is only to add information directly to the user model without creating an extra class. 

### Extending AbstractBaseUser {.wp-block-heading}

Here a new user inherits from the AbstractBaseUser subclass. Use this option if you&#8217;re creating a new user entirely. You use it whenever you have specific requirements in relation to authentication. For example, I will prefer to get an authentication token via email. 

To make it easy to include Django&#8217;s permissions framework, you will have to consider using permissionsMixin because it gives you methods and database fields necessary to support Django&#8217;s permission model. 

The easiest way to construct a compliant custom user model is to inherit from `<mark style="background-color:#abb8c3" class="has-inline-color">AbstractBaseUser</mark>`. AbstractBaseUser provides the core implementation of a user model, including hashed passwords and tokenized password resets. 

### Steps to follow {.wp-block-heading}

### 1. Create Django project {.wp-block-heading}

To start, create a Django project with this article <a href="https://nextgentips.com/2022/05/03/how-to-set-up-a-python-django-application-using-django-4-0/" target="_blank" rel="noopener" title="">How to set up a Python Django Application using Django 4.0</a>

After you are done setting up the Django project, we can proceed to create the user. 

### 2. Create a custom user by extending AbstractBaseUser {.wp-block-heading}

Go to models in your app and use the following code snippet to create a manager. 

<pre class="wp-block-code"><code># models.py
from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

class CustomUserManager(BaseUserManager):

    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """

    def createUser(self, email, username, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError("Provide an email address")

        email=self.normalize_email(email)
        user=self.model(email=email, username=username, **extra_fields)

        user.set_password(password)

        user.save()

        return user

    def create_superuser(self, email, username, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError("Superuser must have is_staff=True.'")

        if extra_fields.get('is_superuser') is not True:
            raise ValueError("Superuser must have is_staff=True.'")

        return self.createUser(email, username, password, **extra_fields)</code></pre>

The next thing is to create a custom user class. Use the following snippet 

<pre class="wp-block-code"><code># models.py
class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.CharField(max_length=50, unique=True)
    username = models.CharField(max_length=100, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    phone_number = PhoneField(blank=True, help_text='Contact phone number')
    user_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    
    objects = CustomUserManager()
    USERNAME_FIELD='email'
    REQUIRED_FIELDS=&#91;'username', 'phone_number']

    def __str__(self):
        return self.username</code></pre>

Head over to `settings.py` and add `<mark style="background-color:#abb8c3" class="has-inline-color">AUTH_USER_MODEL='accounts.CustomUser'</mark>`

From here, you can run migrations and createsuperuser to test with. At this point, you can now continue working on extending the project.

I hope you enjoy it.