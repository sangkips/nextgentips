---
title: How to install Anaconda in Manjaro Linux
author: Kipkoech Sang
type: post
date: 2022-11-20T05:32:08+00:00
url: /2022/11/20/how-to-install-anaconda-in-manjaro-linux/
categories:
  - Data Science

---
Anaconda is a distribution of the python and R programming languages for scientific computing, that aims to simplify package management and deployment.

Anaconda is a solution for data science and machine learning, it&#8217;s very versatile. The reason many love Anaconda is:

  * Can deliver your data strategy easily
  * It maximizes flexibility and control 
  * Products get to the market faster

## Install Anaconda {.wp-block-heading}

It is recommended that when installing Anaconda, don&#8217;t install using AUR packages, because AUR installs to the /opt directory which requires root to execute and Anaconda wants only the user with Sudo privileges. 

### 1. Update system repositories  {.wp-block-heading}

To update the Manjaro Linux we use the following command.

<pre class="wp-block-code"><code>sudo pacman -Syu</code></pre>

### 2. Install Gui dependencies  {.wp-block-heading}

We need to install the following packages to enable us to be in a position to use Gui functionalities. Go to your terminal and run the following

<pre class="wp-block-code"><code>sudo pacman -Sy libxau libxi libxss libxtst libxcursor libxcomposite libxdamage libxfixes libxrandr libxrender mesa-libgl  alsa-lib libglvnd</code></pre>

### 2. Download Anaconda for Linux {.wp-block-heading}

Go to this <a href="https://www.anaconda.com/products/distribution#linux" target="_blank" rel="noopener" title="">link</a> in order to download the latest distribution of Anaconda 

<pre class="wp-block-code"><code>wget https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh</code></pre>

After the download is complete, proceed to check the authenticity of the Anaconda with the use of the following sha script. Make sure you are in the directory you downloaded your Anaconda.

<pre class="wp-block-code"><code>$ shasum -a 256 Anaconda3-2022.10-Linux-x86_64.sh
e7ecbccngtxs6577ebd7e1f211c59df2e37bc6959f2235d387e08c9026666acd  Anaconda3-2022.10-Linux-x86_64.sh</code></pre>

### 3. Install Anaconda  {.wp-block-heading}

Now, let&#8217;s start installing Anaconda with the use of bash. Make sure you have Python 3.7 and above.

<pre class="wp-block-code"><code>bash Anaconda3-2022.10-Linux-x86_64.sh</code></pre>

Accept all the prompts given in the process for Anaconda to begin the installation. 

<pre class="wp-block-code"><code>Welcome to Anaconda3 2022.10

In order to continue the installation process, please review the license
agreement.
Please, press ENTER to continue
&gt;&gt;&gt; 
Executing transaction: | 

    Installed package of scikit-learn can be accelerated using scikit-learn-intelex.
    More details are available here: https://intel.github.io/scikit-learn-intelex

    For example:

        $ conda install scikit-learn-intelex
        $ python -m sklearnex my_application.py
done
installation finished.
Do you wish the installer to initialize Anaconda3
by running conda init? &#91;yes|no]
&#91;no] &gt;&gt;&gt; yes

==&gt; For changes to take effect, close and re-open your current shell. &lt;==

If you'd prefer that conda's base environment not be activated on startup, 
   set the auto_activate_base parameter to false: 

conda config --set auto_activate_base false

Thank you for installing Anaconda3!</code></pre>

### 4. Activate the virtual environment {.wp-block-heading}

Anaconda needs to be run in a virtual environment in order to isolate its working environment with your system to avoid installing dependencies globally.

To activate you may use of the following command

<pre class="wp-block-code"><code>source &lt;path to conda&gt;/bin/activate</code></pre>

If you&#8217;d prefer that conda&#8217;s base environment not be activated on startup, set the auto\_activate\_base parameter to false: I will prefer this option because I run other processes with my machine.

<pre class="wp-block-code"><code>conda config --set auto_activate_base false</code></pre>

Verify that conda has been installed with the following command.

<pre class="wp-block-code"><code>conda list</code></pre>

You will be in a position to see many packages installed.

<pre class="wp-block-code"><code># packages in environment at /home/sang-pc/anaconda3:
#
# Name                    Version                   Build  Channel
_ipyw_jlab_nb_ext_conf    0.1.0            py39h06a4308_1  
_libgcc_mutex             0.1                        main  
_openmp_mutex             5.1                       1_gnu  
alabaster                 0.7.12             pyhd3eb1b0_0  
&lt;strong>anaconda                  2022.10                  py39_0  
anaconda-client           1.11.0           py39h06a4308_0  
anaconda-navigator        2.3.1            py39h06a4308_0  &lt;/strong>
anaconda-project 
....</code></pre>

You can also use `<mark style="background-color:#abb8c3" class="has-inline-color">python</mark>` command to verify Anaconda.

<pre class="wp-block-code"><code>$ python
Python 3.9.13 (main, Aug 25 2022, 23:26:10) 
&#91;GCC 11.2.0] :: &lt;strong>Anaconda, Inc. on linux&lt;/strong>
Type "help", "copyright", "credits" or "license" for more information.
&gt;&gt;&gt; </code></pre>

Another way to verify is to use `<mark style="background-color:#abb8c3" class="has-inline-color">anaconda-navigator</mark>`, this will open a GUI application.

<pre class="wp-block-code"><code>anaconda-navigator</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="411" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-20-08-28-37.png?resize=810%2C411&#038;ssl=1" alt="nextgentips:Anaconda dashboard UI" class="wp-image-1655" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-20-08-28-37.png?resize=1024%2C519&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-20-08-28-37.png?resize=300%2C152&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-20-08-28-37.png?resize=768%2C389&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-20-08-28-37.png?w=1362&ssl=1 1362w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips:Anaconda dashboard UI</figcaption></figure> 

### Uninstall Anaconda  {.wp-block-heading}

To fully uninstall Anaconda from your system, use the following command.

<pre class="wp-block-code"><code>conda install anaconda-clean</code></pre>

Follow the promts in order to run uninstaller

<pre class="wp-block-code"><code>Collecting package metadata (current_repodata.json): done
Solving environment: done

## Package Plan ##

  environment location: /home/sang-pc/anaconda3

  added / updated specs:
    - anaconda-clean


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    anaconda-clean-1.1.1       |   py39h06a4308_0           8 KB
    ------------------------------------------------------------
                                           Total:           8 KB

The following NEW packages will be INSTALLED:

  anaconda-clean     pkgs/main/linux-64::anaconda-clean-1.1.1-py39h06a4308_0 None
Proceed (&#91;y]/n)? y</code></pre>

Then we need to use `<mark style="background-color:#abb8c3" class="has-inline-color">anaconda-clean</mark>` we have installed above to remove all the Anaconda packages. If you don&#8217;t want to be asked about each file and directory append `<mark style="background-color:#abb8c3" class="has-inline-color">--yes</mark>`

<pre class="wp-block-code"><code>anaconda-clean --yes</code></pre>

Then we need to remove the backup files made by anaconda from our system with the following command 

<pre class="wp-block-code"><code>rm -rf anaconda3</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Anaconda on Manjaro Linux. Go ahead and experiment with it now.