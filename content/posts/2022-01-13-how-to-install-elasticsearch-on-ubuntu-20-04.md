---
title: How to install Elasticsearch on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-13T12:28:04+00:00
url: /2022/01/13/how-to-install-elasticsearch-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 35
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Elasticsearch on Ubuntu 20.04.

[Elasticsearch][1] is a distributed, free and open search and analytic engine for data e.g textual, numerical, structured, and unstructured data. 

Elasticsearch is known for its simple rest API, distributed in nature, speed, and scalability. These are the features that make Elasticsearch useful. 

Elasticsearch is the central component of Elastic Stack often referred to as ELK that is Elasticsearch, Logstash, and Kibana which are free and open tools for data ingestion, enrichment, storage, analysis, and visualization.

## Prerequisites  {.wp-block-heading}

  * Have an Ubuntu 20.04 which is upto date
  * Know basic terminal commands 

## Installing Elasticsearch on Ubuntu 20.04 {.wp-block-heading}

## 1. Install system update {.wp-block-heading}

Before we can begin the installation, we need to update our system repositories to make them up to date.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y 
# restart the system as well for changes to take effect immediately.</code></pre>

## 2. import Elasticsearch PGP key {.wp-block-heading}

PGP key is for signing the Elasticsearch. We need to download it which helps with security concerns.

<pre class="wp-block-code"><code>$ wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -</code></pre>

You may need to install **apt-transport-https as** prerequisites even though Ubuntu 20.04 comes preinstalled but its always necessary as a precaution. To install run the following command

<pre class="wp-block-code"><code>$ sudo apt install apt-transport-https</code></pre>

Then we need to add Elastic to **source.list.d** with the following command 

<pre class="wp-block-code"><code>$ echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list</code></pre>

After, you need to update your system repositories again

<pre class="wp-block-code"><code>$ sudo apt update 
Hit:1 http://security.ubuntu.com/ubuntu focal-security InRelease
&lt;strong>Get:2 https://artifacts.elastic.co/packages/7.x/apt stable InRelease &#91;13.7 kB] &lt;/strong>                                 
Hit:3 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:4 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
Hit:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Get:7 https://artifacts.elastic.co/packages/7.x/apt stable/main amd64 Packages &#91;89.7 kB]
Fetched 103 kB in 1s (129 kB/s)    
Reading package lists... Done
Building dependency tree       
Reading state information... Done
All packages are up to date.</code></pre>

## 3. Install Elasticsearch on Ubuntu 20.04 {.wp-block-heading}

We are now ready to install Elasticsearch. Use the following command to install 

<pre class="wp-block-code"><code>$ sudo apt install elasticsearch</code></pre>

You will get the following output

<pre class="wp-block-code"><code>Output
The following NEW packages will be installed:
  &lt;strong>elasticsearch&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 344 MB of archives.
After this operation, 552 MB of additional disk space will be used.
Get:1 https://artifacts.elastic.co/packages/7.x/apt stable/main amd64 elasticsearch amd64 7.16.2 &#91;344 MB]
Fetched 344 MB in 6s (58.7 MB/s)        
Selecting previously unselected package elasticsearch.
(Reading database ... 94648 files and directories currently installed.)
Preparing to unpack .../elasticsearch_7.16.2_amd64.deb ...
Creating elasticsearch group... OK
Creating elasticsearch user... OK
Unpacking elasticsearch (7.16.2) ...
Setting up elasticsearch (7.16.2) ...
### NOT starting on installation, please execute the following statements to configure elasticsearch service to start automatically using systemd
 sudo systemctl daemon-reload
 sudo systemctl enable elasticsearch.service
### You can start elasticsearch service by executing
 sudo systemctl start elasticsearch.service
Created elasticsearch keystore in /etc/elasticsearch/elasticsearch.keystore
Processing triggers for systemd (245.4-4ubuntu3.15) ...</code></pre>

## 4. Configuring Elasticsearch {.wp-block-heading}

After installation we will need to edit the main configuration file, that is **elasticsearch.yaml** file. It is located at the **/etc/elasticsearch**. 

We need to adjust the network hosts&#8217; config. Elasticsearch listens on **port 9200** and therefore to restrict outside access of your Elasticsearch, we need to only allow localhost access. Uncomment the line **network.host**

Open your preferred editor 

<pre class="wp-block-code"><code>$ sudo nano /etc/elasticsearch/elasticsearch.yml</code></pre>

Now we need to reload and start Elasticsearch service with **systemctl** command as specified at the output above 

<pre class="wp-block-code"><code>$ sudo systemctl daemon-reload
$ sudo systemctl enable elasticsearch.service
$ sudo systemctl start elasticsearch.service</code></pre>

You can check the status of Elasticsearch with the following command

<pre class="wp-block-code"><code>$ sudo systemctl status elasticsearch</code></pre>

The following will be the output

<pre class="wp-block-code"><code>output
● elasticsearch.service - Elasticsearch
     Loaded: loaded (/lib/systemd/system/elasticsearch.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2022-01-13 12:13:42 UTC; 1min 38s ago
       Docs: https://www.elastic.co
   Main PID: 17888 (java)
      Tasks: 59 (limit: 1136)
     Memory: 703.1M
     CGroup: /system.slice/elasticsearch.service
             ├─17888 /usr/share/elasticsearch/jdk/bin/java -Xshare:auto -Des.networkaddress.cache.ttl=60 -Des.ne>
             └─18057 /usr/share/elasticsearch/modules/x-pack-ml/platform/linux-x86_64/bin/controller

Jan 13 12:12:56 ubuntu systemd&#91;1]: Starting Elasticsearch...
Jan 13 12:13:42 ubuntu systemd&#91;1]: Started Elasticsearch.</code></pre>

If the service is up and running then we are good to continue 

## 5. Testing Elasticsearch {.wp-block-heading}

We can test the Elasticsearch if its nodes are running on port 9200 by sending a request

<pre class="wp-block-code"><code>$ curl -X GET 'http://localhost:9200'</code></pre>

You will get the following as output

<pre class="wp-block-code"><code>output
{
  "name" : "ubuntu",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "QNag836iRrOdwCC6kYlV9A",
  "version" : {
    "number" : "7.16.2",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "2b937c44140b6559905130a8650c64dbd0879cfb",
    "build_date" : "2021-12-18T19:42:46.604893745Z",
    "build_snapshot" : false,
    "lucene_version" : "8.10.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}</code></pre>

As you can see from the output above its running as required on port 9200

## Conclusion {.wp-block-heading}

We have successfully installed and configured Elasticsearch on Ubuntu 20.04. You can get detailed views on [Elasticsearch documentation][2].

 [1]: https://www.elastic.co/elasticsearch/ "Elasticsearch"
 [2]: https://www.elastic.co/guide/en/elasticsearch/reference/7.9/deb.html "Elasticsearch documentation"