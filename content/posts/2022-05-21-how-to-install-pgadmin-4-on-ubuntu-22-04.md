---
title: How to install pgAdmin 4 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-05-21T12:08:01+00:00
url: /2022/05/21/how-to-install-pgadmin-4-on-ubuntu-22-04/
rank_math_seo_score:
  - 9
rank_math_primary_category:
  - 6
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 179
categories:
  - Linux

---
In this tutorial guide, I will be taking you through the installation of pgAdmin 4 version 6.9 on Ubuntu 22.04.

pgAdmin 4 is a free and open-source management tool for Postgres. Its desktop runtime written in NWjs allows it to run standalone for individual users, or the web applications code may be directly deployed on a web server for use by the web browser.

pgAdmin 4 is a complete rewrite of pgAdmin, built using Python and Java

## New features of pgAdmin 4 version 6.9 {.wp-block-heading}

The following are the notable changes introduced to pgAdmin 4 version 6.9

  * The status bar was added to the Query Tool.
  * It added the capability to deploy PostgreSQL servers on EDB BigAnimal. this feature allows users to deploy PostgreSQL and EDB and Advanced Server on the EDB BigAnimal cloud.
  * Introduced port Query tool to react. This tool is a major upgrade to the status bar added to Query Tool. 
  * It added the added options to ignore owner and ignore whitespace on the schema diff panel.
  * It added support to Azure AD OAUTH2 authentication.
  * Relocate GIS button to the left side of the results table. 
  * It ensures that row numbers should be visible in the view when scrolling horizontally.
  * It ensures that columns should always be visible in the import and export dialogs.

### Install pgAdmin 4 on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

The first thing to do is to update repositories to make them up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Add pgAdmin 4 repository to Ubuntu 22.04 {.wp-block-heading}

pgAdmin 4 is available from Debian packages. We just need to add pgAdmin 4 to our Ubuntu repository.

<pre class="wp-block-code"><code># gpg key 
$ curl  -fsSL https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/pgadmin.gpg</code></pre>

Add repo to Debian packages like this 

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list'</code></pre>

### 3. Install pgAdmin 4 {.wp-block-heading}

To install pgAdmin 4, first, run system updates again to update all repositories for changes introduced to take effect.

<pre class="wp-block-code"><code>$ sudo apt update -y
$ sudo apt install pgadmin4</code></pre>

Sample output

<pre class="wp-block-code"><code># output
The following packages were automatically installed and are no longer required:
  linux-headers-5.15.0-25 linux-headers-5.15.0-25-generic
  linux-image-5.15.0-25-generic linux-modules-5.15.0-25-generic
  linux-modules-extra-5.15.0-25-generic
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  libapache2-mod-wsgi-py3 pgadmin4-desktop pgadmin4-server pgadmin4-web
  postgresql-client postgresql-client-14 postgresql-client-common
Suggested packages:
 &lt;strong> postgresql-14 postgresql-doc-14&lt;/strong>
The following NEW packages will be installed:
  libapache2-mod-wsgi-py3 pgadmin4 pgadmin4-desktop pgadmin4-server
  pgadmin4-web postgresql-client postgresql-client-14 postgresql-client-common
0 upgraded, 8 newly installed, 0 to remove and 0 not upgraded.
Need to get 195 MB of archives.
After this operation, 4,456 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

To start pgadmin4 head over to Ubuntu 22.04 dock and click on show applications and search for pgadmin4. you need to set password so that you will use this password while working with PostgreSQL services.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="419" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-21-14-59-40.png?resize=810%2C419&#038;ssl=1" alt="pgAdmin4 dashboard" class="wp-image-1350" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-21-14-59-40.png?w=1020&ssl=1 1020w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-21-14-59-40.png?resize=300%2C155&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-21-14-59-40.png?resize=768%2C398&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>pgAdmin4 dashboard</figcaption></figure>