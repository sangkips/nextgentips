---
title: How to install PHP 8.1 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-08T05:45:54+00:00
url: /2022/07/08/how-to-install-php-8-1-on-ubuntu-22-04/
categories:
  - Programming

---
In this tutorial, we will learn how to install the current stable release version of PHP 8.1 on Ubuntu 22.04.

[PHP][1]&nbsp;is a general-purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is:

  * **Fast** – PHP is fast because it runs on its own memory space. Also, PHP has a Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * Flexible – PHP is flexible because it allows for almost all databases. MySQL is the de-facto database to connect to because it is open source.
  * Free and open source
  * PHP is forgiving – Why do we say PHP is forgiving? It is forgiving, meaning learning is not so hard and therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Features of PHP 8.1  {.wp-block-heading}

PHP 8.1 is a major update release version. It contains many new features and optimizations such as:

  * It introduced the Enumerations feature. Enumerations or Enum feature allow a developer to define a custom type that is limited to one of the discrete numbers of possible values. This is helpful while defining the domain model.
  * Improvement of RFC read-only property version 2. This prevents modification of the property after initialization.
  * The introduction of RFC fibers calls for action. This seeks to eliminate synchronous and asynchronous function calls by allowing functions to become interruptible without affecting the entire function.
  * Introduction of a non-return type. This indicates that a function either exits throws an exception or doesn’t terminate.
  * Introduction of first-class callable actions.
  * Introduction of [fsync][2] and [fdatasync][3] functions
  * Introduction of [array\_is\_list][4] functions
  * Introduction of explicit octal numeral notation

## Installing PHP 8.1 on Ubuntu 22.04 {#installing-php-8-1-on-ubuntu-20-04.wp-block-heading}

### 1. Run system updates {#1-run-system-updates.wp-block-heading}

The first thing to do in a new system is to update our repositories in order to make them up to date. Run upgrade command also.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Add Ondrej Sury PPA repository {#2-add-ondrej-sury-ppa-repository.wp-block-heading}

To run PHP 8.1 on Ubuntu 22.04, we need to add Ondrej Sury PPA into our system. This is the maintainer of the PHP repository at the moment. This PPA is not currently checked so installing from it will not guarantee 100% results.

To add this PPA use the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:ondrej/php</code></pre>

You will see the following as the output.

<pre class="wp-block-code"><code>PPA publishes dbgsym, you may need to include 'main/debug' component
Repository: 'deb https://ppa.launchpadcontent.net/ondrej/php/ubuntu/ jammy main'
Description:
Co-installable PHP versions: PHP 5.6, PHP 7.x and most requested extensions are included. Only Supported Versions of PHP (http://php.net/supported-versions.php) for Supported Ubuntu Releases (https://wiki.ubuntu.com/Releases) are provided. Don't ask for end-of-life PHP versions or Ubuntu release, they won't be provided.

Debian oldstable and stable packages are provided as well: https://deb.sury.org/#debian-dpa

You can get more information about the packages at https://deb.sury.org

IMPORTANT: The &lt;foo>-backports is now required on older Ubuntu releases.

BUGS&FEATURES: This PPA now has a issue tracker:
https:&#47;&#47;deb.sury.org/#bug-reporting

CAVEATS:
1. If you are using php-gearman, you need to add ppa:ondrej/pkg-gearman
2. If you are using apache2, you are advised to add ppa:ondrej/apache2
3. If you are using nginx, you are advised to add ppa:ondrej/nginx-mainline
   or ppa:ondrej/nginx

PLEASE READ: If you like my work and want to give me a little motivation, please consider donating regularly: https://donate.sury.org/

WARNING: add-apt-repository is broken with non-UTF-8 locales, see
https://github.com/oerdnj/deb.sury.org/issues/56 for workaround:

# LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
More info: https://launchpad.net/~ondrej/+archive/ubuntu/php
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.
Adding deb entry to /etc/apt/sources.list.d/ondrej-ubuntu-php-jammy.list
Adding disabled deb-src entry to /etc/apt/sources.list.d/ondrej-ubuntu-php-jammy.list
Adding key to /etc/apt/trusted.gpg.d/ondrej-ubuntu-php.gpg with fingerprint 14AA40EC0831756756D7F66C4F4EA0AAE5267A6C
Hit:1 http://mirrors.linode.com/ubuntu jammy InRelease
Hit:2 http://mirrors.linode.com/ubuntu jammy-updates InRelease                 
Hit:3 http://mirrors.linode.com/ubuntu jammy-backports InRelease               
Hit:4 http://security.ubuntu.com/ubuntu jammy-security InRelease               
Get:5 https://ppa.launchpadcontent.net/ondrej/php/ubuntu jammy InRelease &#91;23.9 kB]
Get:6 https://ppa.launchpadcontent.net/ondrej/php/ubuntu jammy/main amd64 Packages &#91;92.9 kB]
Get:7 https://ppa.launchpadcontent.net/ondrej/php/ubuntu jammy/main Translation-en &#91;29.1 kB]
Fetched 146 kB in 2s (68.5 kB/s)                                 
Reading package lists... Done</code></pre>

You need to update your system repositories again for the changes to take effect.

<pre class="wp-block-code"><code>sudo apt update </code></pre>

### Install PHP 8.1 on Ubuntu 22.04 {.wp-block-heading}

After you have updated your system, it&#8217;s now time to run the installation, with the following command.

<pre class="wp-block-code"><code>sudo apt install php8.1 -y</code></pre>

You will be in a position to see the following as output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapache2-mod-php8.1 libapr1
  libaprutil1 libaprutil1-dbd-sqlite3 libaprutil1-ldap liblua5.3-0 php-common
  php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser
  php-pear
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapache2-mod-php8.1 libapr1
  libaprutil1 libaprutil1-dbd-sqlite3 libaprutil1-ldap liblua5.3-0 php-common
  php8.1 php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
0 upgraded, 17 newly installed, 0 to remove and 1 not upgraded.
Need to get 7227 kB of archives.
After this operation, 29.6 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

Now we can check to see the version of PHP installed 

<pre class="wp-block-code"><code>php --version
PHP 8.1.7 (cli) (built: Jun 25 2022 08:13:46) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.7, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.7, Copyright (c), by Zend Technologies</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install PHP 8.1 on Ubuntu 22.04, now you can proceed to write your programs using this awesome language. Consult [PHP documentation][5] in case you experience any difficulty. Happy coding!

## Related Articles {#related-articles.wp-block-heading}

  * [How to install PHP 8.1 on CentOS 8/RHEL 8][6]
  * [How to install PHP 8.1 on Ubuntu 21.04][7]
  * [How to Install PHP 8 on Ubuntu 21.04][8]
  * [How to install PHP 8.1 on Ubuntu 21.10][9]

 [1]: https://www.php.net/
 [2]: https://www.php.net/manual/en/function.fsync.php
 [3]: https://www.php.net/manual/en/function.fdatasync.php
 [4]: https://www.php.net/manual/en/function.array-is-list.php
 [5]: https://www.php.net/docs.php
 [6]: https://nextgentips.com/2021/12/04/how-to-install-php-8-1-on-centos-8-rhel-8/
 [7]: https://nextgentips.com/2021/12/03/how-to-install-php-8-1-on-ubuntu-21-04/
 [8]: https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/
 [9]: https://nextgentips.com/2021/12/26/how-to-install-php-8-1-on-ubuntu-21-10/