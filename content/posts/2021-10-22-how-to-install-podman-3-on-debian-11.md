---
title: How to install Podman 3 on Debian 11
author: Kipkoech Sang
type: post
date: 2021-10-22T13:53:19+00:00
url: /2021/10/22/how-to-install-podman-3-on-debian-11/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 129
rank_math_internal_links_processed:
  - 1
categories:
  - Kubernetes

---
In this guide, we are going to learn how to install Podman 3 on Debian 11 system. We will start by defining what a Podman is, why we need Podman over other service providers.

## What is Podman? {#what-is-podman.wp-block-heading}

Podman is a daemon-less, open-source, Linux native tool designed to make it easy to find, run, build, share and deploy applications using Open Container Initiative (OCI) containers and container images. Containers can be run as root or as a regular users.

Podman manages the entire container ecosystem including pods, container images, container volumes using the [libpod][1] library. Libpod is a library used to create container pods. So what is a pod anyway? Pods are the smallest deployable units of computing that can be created and managed in Kubernetes. It shares both network and storage resources. 

## How is Podman different from Docker? {#how-is-podman-different-from-docker.wp-block-heading}

Podman is a container management technology so is Docker. If both are container management technologies why have one over the other? check out this article about [docker][2] for more insight.

  * Docker uses daemon to manage container activity. It uses what we called containerd daemon. The main work of containerd daemon is to pull container names from the container registry. On the other hand Podman takes daemonless approach using [conmon][3] technology. Conmon writes the contents of containers streams to a log file so they can be read after the containers death. 
  * Podman allow user to create pods. A pod is a way to group containers together. Docker does not support the idea of pods.
  * Podman is more secure than Docker because it does not require root access during its operation. Podaman images are created under the OCI standards that can allow Podman to interact directly with kernel, containers and images from different registries.
  * Podman allows storing of images and containers in different registries while Docker stores images locally.

## Features of Podman {#features-of-podman.wp-block-heading}

The reason why Podman is gaining momentum is because of the following features:

  * Podman images are created according to OCI standards which can become easy while pushing to other registries.
  * Podman allows users to run operations as root or non root, this will make Podman more secure 
  * It uses [Builder tool][4] to manage or manipulate or create images within a running container. 
  * Podman has support for Pods which help in managing groups of containers together.
  * Podman has ability to isolate resources for containers and pods
  * Podman has support for multiple means to securely download images including trust and image verification.
  * Podman support multiple image formats including the OCI and Docker image formats.

## Install Podman on Debian 11 {#install-podman-on-debian-11.wp-block-heading}

We can install Podman in two ways:

  1. Using Podman package repository
  2. Building from the source.

## Installing Podman from Package repository {#installing-podman-from-package-repository.wp-block-heading}

For Debian 11 we can use the following command to install

<pre class="wp-block-code"><code>$ sudo apt install podman -y</code></pre>

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  buildah catatonit conmon containernetworking-plugins crun dbus-user-session dconf-gsettings-backend
  dconf-service fuse-overlayfs fuse3 glib-networking glib-networking-common glib-networking-services
  golang-github-containernetworking-plugin-dnsname golang-github-containers-common
  golang-github-containers-image gsettings-desktop-schemas libarchive13 libavahi-client3 libavahi-common-data
  libavahi-common3 libavahi-glib1 libdconf1 libfuse3-3 libgpgme11 libostree-1-1 libproxy1v5 libslirp0
  libsoup2.4-1 libyajl2 slirp4netns uidmap
Suggested packages:
  containers-storage lrzip docker-compose
The following NEW packages will be installed:
  buildah catatonit conmon containernetworking-plugins crun dbus-user-session dconf-gsettings-backend
  dconf-service fuse-overlayfs fuse3 glib-networking glib-networking-common glib-networking-services
  golang-github-containernetworking-plugin-dnsname golang-github-containers-common
  golang-github-containers-image gsettings-desktop-schemas libarchive13 libavahi-client3 libavahi-common-data
  libavahi-common3 libavahi-glib1 libdconf1 libfuse3-3 libgpgme11 libostree-1-1 libproxy1v5 libslirp0
  libsoup2.4-1 libyajl2 podman slirp4netns uidmap
0 upgraded, 33 newly installed, 0 to remove and 68 not upgraded.</code></pre>

Let us check the version of the Podman installed with the following command:

<pre class="wp-block-code"><code>$ sudo podman --version</code></pre>

<pre class="wp-block-code"><code># output
podman version 3.0.1</code></pre>

Now that Podman has been installed, we can also check the configuration files with the following command:

<pre class="wp-block-code"><code>$ podman info</code></pre>

<pre class="wp-block-code"><code># podman info
host:
  arch: amd64
  buildahVersion: 1.19.6
  cgroupManager: systemd
  cgroupVersion: v2
  conmon:
    package: 'conmon: /usr/bin/conmon'
    path: /usr/bin/conmon
    version: 'conmon version 2.0.25, commit: unknown'
  cpus: 1
  distribution:
    distribution: debian
    version: "11"
  eventLogger: journald
  hostname: debian-11
  idMappings:
    gidmap: null
    uidmap: null
  kernel: 5.10.0-7-amd64
  linkmode: dynamic
  memFree: 358334464
  memTotal: 1023942656
  ociRuntime:
    name: crun
    package: 'crun: /usr/bin/crun'
    path: /usr/bin/crun
    version: |-
      crun version 0.17
      commit: 0e9229ae34caaebcb86f1fde18de3acaf18c6d9a
      spec: 1.0.0
      +SYSTEMD +SELINUX +APPARMOR +CAP +SECCOMP +EBPF +YAJL
  os: linux
  remoteSocket:
    exists: true
    path: /run/podman/podman.sock
  security:
    apparmorEnabled: true
    capabilities: CAP_CHOWN,CAP_DAC_OVERRIDE,CAP_FOWNER,CAP_FSETID,CAP_KILL,CAP_NET_BIND_SERVICE,CAP_SETFCAP,CAP_SETGID,CAP_SETPCAP,CAP_SETUID,CAP_SYS_CHROOT
    rootless: false
    seccompEnabled: true
    selinuxEnabled: false
  slirp4netns:
    executable: ""
    package: ""
    version: ""
  swapFree: 0
  swapTotal: 0
  uptime: 5h 34m 3.55s (Approximately 0.21 days)
registries: {}
store:
  configFile: /etc/containers/storage.conf
  containerStore:
    number: 0
    paused: 0
    running: 0
    stopped: 0
  graphDriverName: overlay
  graphOptions: {}
  graphRoot: /var/lib/containers/storage
  graphStatus:
    Backing Filesystem: extfs
    Native Overlay Diff: "true"
    Supports d_type: "true"
    Using metacopy: "false"
  imageStore:
    number: 0
  runRoot: /run/containers/storage
  volumePath: /var/lib/containers/storage/volumes
version:
  APIVersion: 3.0.0
  Built: 0
  BuiltTime: Thu Jan  1 00:00:00 1970
  GitCommit: ""
  GoVersion: go1.15.9
  OsArch: linux/amd64
  Version: 3.0.1</code></pre>

## Manage Containers using Podman {#manage-containers-using-podman.wp-block-heading}

We can search for the Podman images with the following command:

<pre class="wp-block-code"><code>$ sudo podman search alpine</code></pre>

To list the downloaded images, we can use the following command:

<pre class="wp-block-code"><code>$ sudo podman images</code></pre>

To pull images we can use the following command:

<pre class="wp-block-code"><code># sudo podman pull alpine</code></pre>

<pre class="wp-block-code"><code>Output
$ sudo podman pull alpine
Resolved "alpine" as an alias (/etc/containers/registries.conf.d/shortnames.conf)
Trying to pull docker.io/library/alpine:latest...
Getting image source signatures
Copying blob a0d0a0d46f8b done  
Copying config 14119a10ab done  
Writing manifest to image destination
Storing signatures
14119a10abf4669e8cdbdff324a9f9605d99697215a0d21c360fe8dfa8471bab</code></pre>

Check the downloaded image with the following command:

<pre class="wp-block-code"><code>$ podman images </code></pre>

<pre class="wp-block-code"><code># podman images
REPOSITORY                TAG     IMAGE ID      CREATED      SIZE
docker.io/library/alpine  latest  14119a10abf4  8 weeks ago  5.87 MB</code></pre>

And that is all, we have installed Podman 3 in our Debian 11 system. For more information check the [Podman Documentation][5]

 [1]: https://github.com/dankohn/libpod
 [2]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [3]: https://github.com/containers/conmon
 [4]: https://github.com/containers/buildah
 [5]: https://docs.podman.io/en/latest/