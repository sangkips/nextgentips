---
title: How to install Postman on Debian 11
author: Kipkoech Sang
type: post
date: 2021-10-20T11:51:05+00:00
url: /2021/10/20/how-to-install-postman-on-debian-11/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 130
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Postman is an API platform for building and using APIs. Postman simplifies each step of the API lifecycle and streamlines collaborations so you can create better APIs faster. 

So what is API? API is an acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. 

## What is Postman used for? {.wp-block-heading}

Postman is an application used for API testing. It is an HTTP client that tests HTTP requests, utilizing a graphical interface, through which we obtain different types of responses that need to be subsequently validated. 

## How does Postman work? {.wp-block-heading}

Postman works in the backend by creating requests and giving responses to make sure that it contains elements needed in an API. Postman allows us to build, test, and modify scripts. Postman can make requests like Get, Post, Delete, Put and Patch. 

## Automated Testing with Postman {.wp-block-heading}

Postman tests are automated by creating test suits that can run every time. The benefit of automated tests is to eliminate human error as much as possible. Postman can automate tasks such as:

  * Functional tests. This is a type of software testing that validates the software system against functional requirements specified during project inception. It involve black box testing and its not concerned about any source code. It involves checking the interface, APIs, database etc.
  * Integration tests. This is where individual software modules are combined and tested as a group. It is conducted to evaluate the compliance of a system
  * Unit tests. A unit is a smallest piece of code that can be logically isolated in a system. Unit testing can be done on almost everything from a line of code, a method or a class. 
  * Regression tests. This is a type of testing that is done to verify that a code change in the software does not impact the existing functionality of the system.

## Reasons for Automating Postman API Tests {.wp-block-heading}

  * To make scaling easy. As programs grow chances of breakage tend to increase. So to avoid breakage try to create more tests and create bug resistant programs and have tests frequently.
  * To streamline development and quality assurance with a CI/CD pipeline. Postman allows you to reuse your test suites to create a CI/CD so you can test at every push. You can seamlessly integrate your Postman instance with Jenkins to create your own CI/CD pipeline or add it to your existing pipeline as a build step.
  * To avoid tedious work. Doing automated tests means you will spend less money on Quality assurance, reduce time spend in office by employees and also spend less time in debugging codes.
  * Reduce feedback cycles and improve insights. feedback is very important while doing any project, so faster feedback streamlines communication across all teams. This will help in reduction in bugs which can arise during the process.
  * To remove human error, improve accuracy. Automated tests remove human error as much as possible.
  * To reduce the need for specialist skills 
  * To enable testing be done simultaneously across platforms and devices. In this age many platforms exist such as mobile, cloud, automated tests helps to bridge the gap on compatibility across different operating systems, platforms and devices. Make sure everything runs seamlessly with no problems.
  * To accelerate time to market. We are trying to reduce the time duration in which the project is released to the market. Less time spent on the project means less costs to the entire project life cycle. 

## Why do we need Postman? {.wp-block-heading}

We require a postman because of the following reasons:

  * It is free and easy to use. Postman is free to download and use for up to 3 members team. You can download and send requests as you wish.
  * It has a larger community base. Larger community base means in case of a problem you have a wide network to ask for assistance and also project maintenance is not an issue so improvement of the project will be key.
  * It has wide support for all APIs and schema. Postman has a built in support for popular data formats such OpenAPI, GraphQL and RAML. You can make any kind of API call and you can inspect very large responses with ease.
  * It is extensible. You can customize Postman to your needs with the use of Postman API. You can also Integrate test suites into your preferred CI/CD service with [Newman][1], our command line collection runner.

## 1. Installing Postman via Snap {.wp-block-heading}

Snaps are applications packaged with all their dependencies to run on all popular Linux distributions with a single build. They update automatically and can roll back easily.

So first thing first, we have to enable snapd on our system by using the following commands:

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt install snapd -y
$ sudo snap install core </code></pre>

<pre class="wp-block-code"><code>Output

Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  liblzo2-2 squashfs-tools
Suggested packages:
  zenity | kdialog
The following NEW packages will be installed:
  liblzo2-2 snapd squashfs-tools
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 13.5 MB of archives.
After this operation, 56.9 MB of additional disk space will be used.
Get:1 http://security.debian.org/debian-security bullseye-security/main amd64 squashfs-tools amd64 1:4.4-2+deb11u2 &#91;135 kB]
Get:2 http://deb.debian.org/debian bullseye/main amd64 liblzo2-2 amd64 2.10-2 &#91;56.9 kB]
Get:3 http://deb.debian.org/debian bullseye/main amd64 snapd amd64 2.49-1+b5 &#91;13.3 MB]
Fetched 13.5 MB in 0s (113 MB/s)
Selecting previously unselected package liblzo2-2:amd64.
(Reading database ... 29867 files and directories currently installed.)
Preparing to unpack .../liblzo2-2_2.10-2_amd64.deb ...
Unpacking liblzo2-2:amd64 (2.10-2) ...
Selecting previously unselected package squashfs-tools.
Preparing to unpack .../squashfs-tools_1%3a4.4-2+deb11u2_amd64.deb ...
Unpacking squashfs-tools (1:4.4-2+deb11u2) ...
Selecting previously unselected package snapd.
Preparing to unpack .../snapd_2.49-1+b5_amd64.deb ...
Unpacking snapd (2.49-1+b5) ...
Setting up liblzo2-2:amd64 (2.10-2) ...
Setting up squashfs-tools (1:4.4-2+deb11u2) ...
Setting up snapd (2.49-1+b5) ...
Created symlink /etc/systemd/system/multi-user.target.wants/snapd.recovery-chooser-trigger.service → /lib/systemd/system/snapd.recovery-chooser-trigger.service.
Created symlink /etc/systemd/system/multi-user.target.wants/snapd.seeded.service → /lib/systemd/system/snapd.seeded.service.
Created symlink /etc/systemd/system/cloud-final.service.wants/snapd.seeded.service → /lib/systemd/system/snapd.seeded.service.
Created symlink /etc/systemd/system/multi-user.target.wants/snapd.service → /lib/systemd/system/snapd.service.
Created symlink /etc/systemd/system/sockets.target.wants/snapd.socket → /lib/systemd/system/snapd.socket.
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for dbus (1.12.20-2) ...
Processing triggers for libc-bin (2.31-13+deb11u2) ...</code></pre>

Lastly we can install Postman with the following command:

<pre class="wp-block-code"><code>$ sudo snap install postman </code></pre>

<pre class="wp-block-code"><code>Output
postman (v8/stable) 8.12.4 from Postman, Inc. (postman-inc✓) installed</code></pre>

## 2. Installing Postman Manually {.wp-block-heading}

Run the following command to download the app

<pre class="wp-block-code"><code>$ wget https://dl.pstmn.io/download/latest/linux64 -O postman-linux-x64.tar.gz</code></pre>

After the download is complete extract it to the /opt directory with the following command:

<pre class="wp-block-code"><code>$ sudo tar -xzf postman-linux-x64.tar.gz -C /opt</code></pre>

When you have extracted the contents to the /opt directory, you need to create a symbolic link 

<pre class="wp-block-code"><code>$ sudo ln -s /opt/Postman/Postman /usr/bin/postman</code></pre>

After all, processes are complete, type the postman in the command line and see what will happen.

<pre class="wp-block-code"><code>$ postman</code></pre>

You will be greeted with the following window:<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="396" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-27-11-19-23.png?resize=810%2C396&#038;ssl=1" alt="Postman Dashboard" class="wp-image-738" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-27-11-19-23.png?resize=1024%2C500&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-27-11-19-23.png?resize=300%2C147&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-27-11-19-23.png?resize=768%2C375&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-27-11-19-23.png?w=1296&ssl=1 1296w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Postman Dashboard</figcaption></figure> 

## Conclusion {.wp-block-heading}

Congratulations you have installed Postman on Debian 11. It&#8217;s now your time to practice along as you familiarize yourself further. In case of any doubt always refer to the [Postman documentation.][2]

 [1]: https://www.npmjs.com/package/newman
 [2]: https://www.postman.com/product/what-is-postman/