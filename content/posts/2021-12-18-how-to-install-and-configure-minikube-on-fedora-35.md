---
title: How to install and use Minikube on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-12-18T13:43:39+00:00
url: /2021/12/18/how-to-install-and-configure-minikube-on-fedora-35/
ss_ss_click_share_count_twitter:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 59
rank_math_internal_links_processed:
  - 1
categories:
  - Kubernetes

---
Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your local machine. Kubernetes quickly set up a local Kubernetes cluster on Linux, Windows, and macOS.

It is always advisable before diving into Kubernetes, you will need a minimal Kubernetes setup. Such a setup should spin up fast and integrate well with other tools.

Minikube is the best fit because of the following:

  * It runs on Windows, Linux and MacOS.
  * It supports the latest Kubernetes release
  * It supports multiple container runtimes i.e Containerd, KVM, Docker, Podman etc
  * It has supports for adbvanced features such as Load balancing, Featuregates and filesystems mounts.
  * It has support for Addons. Addons being a marketplace for developers to share configurations for running services on Minikube.
  * It supports CI environments

## Prerequisites  {.wp-block-heading}

To run Kubernetes effectively you need to allocate the following to Minikube on your system.

  * 2 CPUs or more
  * 2 GB of memory
  * 20 GB or more of free disk space
  * Reliable internet connections.
  * Conatiner or virtual machine manager such as Docker, KVM, Podman, Virtual Box etc.

## Install Minikube on Fedora 35 {.wp-block-heading}

Let&#8217;s now dive into installing and playing with Kubernetes. I am going to be using Podman for running Kubernetes, You can spin up using Docker, Virtualbox, or KVM whichever you understand well.

## 1. Run system updates  {.wp-block-heading}

To make sure our repositories are up to date, we need to run system-wide updates on our distro. Open your terminal and input the following command;

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When updates are complete, proceed to install download the Minikube binary.

Before we can continue make sure Podman is up and running. 

Check this if using Ubuntu distro. 

  * [How to install Podman 3 on Debian 11][1]

If you opted for VirtualBox on Ubuntu, check this article.

  * [How to install Oracle VirtualBox 6.1 on Ubuntu 21.04][2]

## 2. Install Podman on Fedora 35. {.wp-block-heading}

To install Podman, run the following command.

<pre class="wp-block-code"><code>$ sudo dnf install podman</code></pre>

Sample output.

<pre class="wp-block-code"><code>Output
Installing:
 podman                                 x86_64            3:3.4.4-1.fc35                updates             12 M
Installing dependencies:
 conmon                                 x86_64            2:2.0.30-2.fc35               fedora              56 k
 container-selinux                      noarch            2:2.170.0-2.fc35              updates             50 k
 containernetworking-plugins            x86_64            1.0.1-1.fc35                  fedora             8.7 M
 containers-common                      noarch            4:1-32.fc35                   updates             76 k
 criu                                   x86_64            3.16.1-2.fc35                 updates            529 k
 criu-libs                              x86_64            3.16.1-2.fc35                 updates             31 k
 crun                                   x86_64            1.3-1.fc35                    updates            174 k
 dnsmasq                                x86_64            2.86-3.fc35                   updates            333 k
 fuse-common                            x86_64            3.10.5-1.fc35                 fedora             8.3 k
 fuse3                                  x86_64            3.10.5-1.fc35                 fedora              54 k
 fuse3-libs                             x86_64            3.10.5-1.fc35                 fedora              92 k
 iptables-legacy                        x86_64            1.8.7-13.fc35                 fedora              53 k
 libbsd                                 x86_64            0.10.0-8.fc35                 fedora             104 k
 libnet                                 x86_64            1.2-4.fc35                    fedora              57 k
 libnftnl                               x86_64            1.2.0-2.fc35                  fedora              82 k
 libslirp                               x86_64            4.6.1-2.fc35                  fedora              72 k
 nftables                               x86_64            1:1.0.0-1.fc35                fedora             373 k
 shadow-utils-subid                     x86_64            2:4.9-8.fc35                  updates             89 k
 yajl                                   x86_64            2.1.0-17.fc35                 fedora              37 k
Installing weak dependencies:
 catatonit                              x86_64            0.1.7-1.fc35                  updates            316 k
 fuse-overlayfs                         x86_64            1.7.1-2.fc35                  fedora              72 k
 podman-gvproxy                         x86_64            3:3.4.4-1.fc35                updates            3.7 M
 podman-plugins                         x86_64            3:3.4.4-1.fc35                updates            2.6 M
 slirp4netns                            x86_64            1.1.12-2.fc35                 fedora              55 k

Transaction Summary
=================================================================================================================
Install  25 Packages

Total download size: 30 M
Installed size: 137 M
Is this ok &#91;y/N]: </code></pre>

Press y to allow installation to continue. This process will install Podman and its dependencies.

You can check the version of Podman installed with the following command.

<pre class="wp-block-code"><code>$ podman -v
podman version 3.4.4</code></pre>

To run Minikube you need to be using user passwordless sudo. Check this article on how to create user. 

  * [How to Install Telegram Desktop on ArchLinux][3]

When you have set up the user using the following link, proceed to make the user-created passwordless by doing the following;

Uncomment this line from /etc/sudoers. use nano/etc/sudoers command

<pre class="wp-block-code"><code>$ nano/etc/sudoers</code></pre>

<pre class="wp-block-code"><code># %wheel ALL=(ALL)       NOPASSWD: ALL</code></pre>

## 3. Download Minikube binary {.wp-block-heading}

To download Minikube binary, run the following command and make sure you make it executable for it to function globally.

<pre class="wp-block-code"><code>$ wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 -O /usr/local/bin/minikube</code></pre>

Sample output

<pre class="wp-block-code"><code>output
--2021-12-18 11:05:16--  https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
Resolving storage.googleapis.com (storage.googleapis.com)... 142.250.178.16, 172.217.169.80, 142.250.200.48, ...
Connecting to storage.googleapis.com (storage.googleapis.com)|142.250.178.16|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 69568775 (66M) &#91;application/octet-stream]
Saving to: ‘/usr/local/bin/minikube’

/usr/local/bin/minikube      100%&#91;===========================================&gt;]  66.35M   137MB/s    in 0.5s    

2021-12-18 11:05:18 (137 MB/s) - ‘/usr/local/bin/minikube’ saved &#91;69568775/69568775]</code></pre>

Make it executable with the following command:

<pre class="wp-block-code"><code>$ sudo chmod 0755 /usr/local/bin/minikube </code></pre>

After all, this is done, make sure you are running a passwordless sudo to make Minikube run efficiently.

If you can be able to run **sudo podman ps** without asking for a password then you are good to go, but if you can&#8217;t run this then you need to ensure you change it to be passwordless.

Let&#8217;s check out this;

<pre class="wp-block-code"><code>$ sudo podman ps
CONTAINER ID  IMAGE       COMMAND     CREATED     STATUS      PORTS       NAMES</code></pre>

## 4. Configure Minikube to us Podman {.wp-block-heading}

Now that we have downloaded our Minikube binary, we can now configure it to use Podman. Use the following command to run the Minikube configuration.

<pre class="wp-block-code"><code>$ minikube config set driver podman</code></pre>

You will be greeted with the following message.

<pre class="wp-block-code"><code>Output
! These changes will take effect upon a minikube delete and then a minikube start</code></pre>

We can run our first instance. Run the following command to initiate the instance.

<pre class="wp-block-code"><code>$ minikube start</code></pre>

If you are getting an error like the one below, ensure you add 2 CPUs and 2GB of memory.

<pre class="wp-block-code"><code>Output
* minikube v1.24.0 on Fedora 35 (kvm/amd64)
* Using the podman driver based on user configuration

&lt;strong>X Exiting due to RSRC_INSUFFICIENT_CORES: Requested cpu count 2 is greater than the available cpus of 1&lt;/strong></code></pre>

To navigate through the following error, we need to set the number of CPUs to 2, and to do that run the following command to enable 2 CPUs on Podman.

Run **podman info** to check the configuration of Podman.

<pre class="wp-block-code"><code>$ podman info</code></pre>

To set memory and CPU use the following command;

<pre class="wp-block-code"><code>$ minikube stop</code></pre>

Then set memory 

<pre class="wp-block-code"><code>$ minikube config set memory 2048  </code></pre>

Then set CPU

<pre class="wp-block-code"><code>$ minikube config set cpu 2</code></pre>

After this run minikube start

<pre class="wp-block-code"><code>$ minikube start</code></pre>

The output for minikube start is this;

<pre class="wp-block-code"><code>Output
* minikube v1.24.0 on Fedora 35 (kvm/amd64)
* Using the podman driver based on user configuration

X The requested memory allocation of 1965MiB does not leave room for system overhead (total system memory: 1965MiB). You may face stability issues.
* Suggestion: Start minikube with less memory allocated: 'minikube start --memory=1965mb'

* Starting control plane node minikube in cluster minikube
* Pulling base image ...
* Downloading Kubernetes v1.22.3 preload ...
    &gt; preloaded-images-k8s-v13-v1...: 501.73 MiB / 501.73 MiB  100.00% 128.75 M
    &gt; gcr.io/k8s-minikube/kicbase: 355.78 MiB / 355.78 MiB  100.00% 27.42 MiB p
E1218 12:37:09.436094   19995 cache.go:201] Error downloading kic artifacts:  not yet implemented, see issue #8426
* Creating podman container (CPUs=2, Memory=1965MB) ...
* Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Verifying Kubernetes components...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Enabled addons: storage-provisioner, default-storageclass
* &lt;strong>kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'&lt;/strong>
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default</code></pre>

## 5. Use kubectl to interact with your cluster {.wp-block-heading}

kubectl comes with Minikube so we don&#8217;t need to install it. 

Let&#8217;s see how we use kubectl.

**Check the instance with Minikube**

<pre class="wp-block-code"><code>$ minikube kubectl get nodes</code></pre>

Output will look like this;

<pre class="wp-block-code"><code>Output
&gt; kubectl.sha256: 64 B / 64 B &#91;--------------------------] 100.00% ? p/s 0s
    &gt; kubectl: 44.73 MiB / 44.73 MiB &#91;-------------] 100.00% 1.81 GiB p/s 200ms
NAME       STATUS   ROLES                  AGE   VERSION
minikube   Ready    control-plane,master   21m   v1.22.3</code></pre>

**Check the instance with kubectl**

From what I can see from the output above kubectl was not installed, so I will have to do install manually.

<pre class="wp-block-code"><code>$ cat &lt;&lt;EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
&#91;kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF</code></pre>

Then install with the following command;

<pre class="wp-block-code"><code>sudo dnf install kubectl -y</code></pre>

You can check the version of installed kubectl with the following command 

<pre class="wp-block-code"><code>$ kubectl version</code></pre>

The above command will show both client and server version

<pre class="wp-block-code"><code>Output
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.1", GitCommit:"86ec240af8cbd1b60bcc4c03c20da9b98005b92e", GitTreeState:"clean", BuildDate:"2021-12-16T11:41:01Z", GoVersion:"go1.17.5", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.3", GitCommit:"c92036820499fedefec0f847e2054d824aea6cd1", GitTreeState:"clean", BuildDate:"2021-10-27T18:35:25Z", GoVersion:"go1.16.9", Compiler:"gc", Platform:"linux/amd64"}</code></pre>

You might need also to get the cluster info, you can use the following command;

<pre class="wp-block-code"><code>$ kubectl cluster-info
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.</code></pre>

Now, you can run **kubectl get nodes**. This time it will run successfully.

<pre class="wp-block-code"><code>$ kubectl get nodes 
NAME       STATUS   ROLES                  AGE   VERSION
minikube   Ready    control-plane,master   54m   v1.22.3</code></pre>

To see the minikube dashboard run the following command;

<pre class="wp-block-code"><code>$ minikube dashboard</code></pre>

Sample output will look like this;

<pre class="wp-block-code"><code>Output
* Enabling dashboard ...
  - Using image kubernetesui/dashboard:v2.3.1
  - Using image kubernetesui/metrics-scraper:v1.0.7
* Verifying dashboard health ...
* Launching proxy ...
* Verifying proxy health ...
* Opening http://127.0.0.1:35755/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
  http:&#47;&#47;127.0.0.1:35755/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/</code></pre>

Open your favorite browser and access Kubernetes 

## Conclusion  {.wp-block-heading}

We have successfully installed Minikube on Fedora 35 and learned how to configure it. In case of a problem, you can consult [Minikube documentation][4].

 [1]: https://nextgentips.com/2021/10/22/how-to-install-podman-3-on-debian-11/
 [2]: https://nextgentips.com/2021/10/29/how-to-install-oracle-virtualbox-6-1-on-ubuntu-21-04/
 [3]: https://nextgentips.com/2021/12/01/how-to-install-telegram-desktop-on-archlinux/
 [4]: https://minikube.sigs.k8s.io/docs/