---
title: Getting started with Amazon Elastic Beanstalk
author: Kipkoech Sang
type: post
date: 2022-10-07T15:43:54+00:00
url: /2022/10/07/getting-started-with-amazon-elastic-beanstalk/
categories:
  - AWS
  - Elastic Beanstalk

---
In this tutorial, we are going to learn how to get started with Elastic Beanstalk. 

Amazon Elastic Beanstalk is an easy-to-use service for deploying and scaling web applications created with java, .NET, Python, Golang, PHP, Node.js, and Ruby. You can quickly deploy and manage applications in the AWS cloud without worrying about the infrastructure that runs the application.

### Benefits of using Amazon Elastic Beanstalk {.wp-block-heading}

  * It reduces management complexity. You are only required to upload your app and Elastic Beanstalk automatically handles the details of provisioning, creating a load balancer, scaling, and health monitoring via CloudWatch.
  * Every app is fully scalable because Elastic Beanstalk automatically scales every app smoothly depending on the resources allocated in every instance.
  * Developers have complete infrastructure control.
  * It&#8217;s fast and simple to deploy apps using Amazon Elastic Beanstalk.
  * It supports apps written in Go, Java, .NET, Node.js, PHP, Python, and Ruby. This shows that Elastic Beanstalk supports a large base of programming languages.
  * Elastic Beanstalk has a console where you can easily interact with the environment easily.

### Getting started with Elastic Beanstalk. {.wp-block-heading}

To start deploying your application, follow the following steps.

Sign into your account and choose the region you will like to host your application. On the dashboard search for the Elastic Beanstalk. Click create, this will take you to Elastic Beanstalk getting started page. Input the application name, choose the platform to use, choose the way to create your application and finally click create application. Give it a few minutes for the application to be created.

As an example, we can create an Amazon Elastic Beanstalk using starter by following this link. <a href="https://console.aws.amazon.com/elasticbeanstalk/home#/gettingStarted?applicationName=getting-started-app" target="_blank" rel="noreferrer noopener" title="">https://console.aws.amazon.com/elasticbeanstalk/home#/gettingStarted?applicationName=getting-started-app</a>

During creation, the following is created in the process.

  * An Amazon Elastic Compute Cloud instance is created (EC2)
  * Amazon EC2 Security group is created.
  * An Amazon S3 bucket is created.
  * Amazon CloudWatch alarms
  * Amazon CloudFormation stack 
  * A domain name

If you are successful you will see something like the image below.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="357" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-24-24.png?resize=810%2C357&#038;ssl=1" alt="Elastic Beanstalk health" class="wp-image-1575" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-24-24.png?resize=1024%2C451&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-24-24.png?resize=300%2C132&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-24-24.png?resize=768%2C338&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-24-24.png?w=1209&ssl=1 1209w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Elastic Beanstalk health</figcaption></figure> 

If your installation passes all the health checks, then if you click on the link saying getting started it will take you to the following page.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="189" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-29-26.png?resize=810%2C189&#038;ssl=1" alt="nextgentips: EBS welcome page" class="wp-image-1576" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-29-26.png?resize=1024%2C239&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-29-26.png?resize=300%2C70&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-29-26.png?resize=768%2C179&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-29-26.png?w=1225&ssl=1 1225w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>nextgentips: EBS welcome page</figcaption></figure> 

### Deploying a new version of the application {.wp-block-heading}

Let&#8217;s say you have added some features to your application and you want to upgrade the running version on the AWS. This is how you can synchronize changes in your environment.

  1. Download the application that matches the platform you use at the beginning. To get those sample apps use this link <a href="https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/GettingStarted.DeployApp.html" target="_blank" rel="noreferrer noopener">https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/GettingStarted.DeployApp.html</a>. In my case, I am using the go platform. 
  2. Go to the Elastic Beanstalk console and choose Environment and select your environment. In my case, the name of the environment is called Gettingstartedapp-env.
  3. Click the name of your environment and click on the upload and deploy. Choose the file to upload and click deploy<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="608" height="475" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-44-43.png?resize=608%2C475&#038;ssl=1" alt="nextgentips: EBS upload and deploy" class="wp-image-1577" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-44-43.png?w=608&ssl=1 608w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-07-17-44-43.png?resize=300%2C234&ssl=1 300w" sizes="(max-width: 608px) 100vw, 608px" data-recalc-dims="1" /> <figcaption>nextgentips: EBS upload and deploy</figcaption></figure> 

### Running a configuration change {.wp-block-heading}

Whenever you feel like you are not satisfied with the way the application is running, for example maybe your web application is compute intensive and you would like to reduce the resources, Elastic Beanstalk has to update its environment for changes to take effect. 

Changes being made will depend, some will require you to delete the application and recreate while others will effect the changes immediately.

  1. Open Elastic Beanstalk console and click on the environments in order to locate your created
  2. Click Configuration and choose what you will like to edit, for my case let me edit capacity
  3. Choose what you will like to change and click apply.

### Terminating the environment  {.wp-block-heading}

When you are done with the environment, always terminate to avoid extra charges being billed. To delete follow the following.

  1. Go to Applications and choose the name of the app to delete.
  2. In the navigation, pane choose Application versions 
  3. When on the Applications versions choose the version you will like to delete.
  4. Choose Actions and then delete
  5. Select Delete versions from Amazon S3
  6. Click delete and wait for it to complete
  7. Then we also need to terminate the environment by going to environment actions and choosing terminate, Confirm that you will like to terminate the instance and click terminate.
  8. Lastly is to delete the app. On the navigation, pane choose the name of the app, choose Actions and Delete Application, confirm that you will like to delete and click on delete button.

Congrats, we have successfully learned how to create, configure, and lastly terminate Amazon Elastic Beanstalk.