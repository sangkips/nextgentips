---
title: How to install Visual Studio Code on Ubuntu 20.04|22.04
author: Kipkoech Sang
type: post
date: 2022-07-04T07:09:10+00:00
url: /2022/07/04/how-to-install-visual-studio-code-on-ubuntu-20-0422-04/
categories:
  - Linux

---
In this article we are going to learn how to install Visual Studio code also know as (VScode) on Ubuntu 22.04 Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built in Javascript, Node.js and Typescript. One can do programming for almost languages with ease with Visual Studio Code. The languages supported are like, Go, PHP, C++, C#, Java, Python and also .NET.

## Why do we like Visual Studio Code? {.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Related Content {.wp-block-heading}

  * [How to install Visual Studio Code on Ubuntu 21.04][1]

## Prerequisites {.wp-block-heading}

  * Ubuntu 22.04 workstation
  * Command line basics
  * Internet access

## Table of Contents {.wp-block-heading}

  * Update the system
  * Install Vscode via snap
  * Launch Vscode

## Install Visual Studio code {.wp-block-heading}

VScode can be installed either from the snap store or downloaded directly from source.

### Install visual studio code via .deb package  {.wp-block-heading}

### 1. Update System repositories  {.wp-block-heading}

before you can begin the installation, make sure you run system updates in order to make your repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Add repository key {.wp-block-heading}

A key is used to sign the Vscode repositories, for automatic updates to occur. So we need to add this signing key to our apt repository first.

First install wget if not already installed in your system.

<pre class="wp-block-code"><code>$ sudo apt install wget gpg
# Add key
$ wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg</code></pre>

Once the key has been added, then install, add microsoft vscode to trusted packages.

<pre class="wp-block-code"><code>$ sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/</code></pre>

After this, we need to add Visual studio code to apt repositories using the following script.

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb &#91;arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'</code></pre>

Run system updates again to effect the changes in your syatem.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

### Install Visual Studio Code on Ubuntu 22.04|20.04 {.wp-block-heading}

If you have successfully added everything correctly, then the last thing is to run the installer.

<pre class="wp-block-code"><code>$ sudo apt install code </code></pre>

You should see the following sample output.

<pre class="wp-block-code"><code>#Output 
The following additional packages will be installed:
  adwaita-icon-theme at-spi2-core dconf-gsettings-backend dconf-service
  fontconfig fontconfig-config fonts-dejavu-core gsettings-desktop-schemas
  gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme
  libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3
  libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcolord2
  libcups2 libdatrie1 libdconf1 libdeflate0 libepoxy0 libfontconfig1 libgbm1
  libgdk-pixbuf-2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common
  libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common libharfbuzz0b
  libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libpango-1.0-0
  libpangocairo-1.0-0 libpangoft2-1.0-0 libpixman-1-0 librsvg2-2
  librsvg2-common libsecret-1-0 libsecret-common libthai-data libthai0
  libtiff5 libwayland-client0 libwayland-cursor0 libwayland-egl1
  libwayland-server0 libwebp7 libxcb-render0 libxcb-shm0 libxcomposite1
  libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0
  libxkbfile1 libxrandr2 libxrender1 libxss1 libxtst6 session-migration
  ubuntu-mono x11-common
Suggested packages:
  colord cups-common gvfs liblcms2-utils librsvg2-bin
The following NEW packages will be installed:
  adwaita-icon-theme at-spi2-core code dconf-gsettings-backend dconf-service
  fontconfig fontconfig-config fonts-dejavu-core gsettings-desktop-schemas
  gtk-update-icon-cache hicolor-icon-theme humanity-icon-theme
  libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3
  libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcolord2
  libcups2 libdatrie1 libdconf1 libdeflate0 libepoxy0 libfontconfig1 libgbm1
  libgdk-pixbuf-2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common
  libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common libharfbuzz0b
  libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libpango-1.0-0
  libpangocairo-1.0-0 libpangoft2-1.0-0 libpixman-1-0 librsvg2-2
  librsvg2-common libsecret-1-0 libsecret-common libthai-data libthai0
  libtiff5 libwayland-client0 libwayland-cursor0 libwayland-egl1
  libwayland-server0 libwebp7 libxcb-render0 libxcb-shm0 libxcomposite1
  libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0
  libxkbfile1 libxrandr2 libxrender1 libxss1 libxtst6 session-migration
  ubuntu-mono x11-common
0 upgraded, 74 newly installed, 0 to remove and 0 not upgraded.
Need to get 101 MB of archives.
After this operation, 425 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

### Install Visual Studio Code via Snap. {.wp-block-heading}

Snap packages can be installed directly from either the command line or through Ubuntu software repository.

Let us first update our system repository with the following command:

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the update and upgrade is complete, we can install VScode classic into our system.

<pre class="wp-block-code"><code>$ sudo snap install --classic code </code></pre>

<pre class="wp-block-code"><code>Output 
# sudo snap install --classic code 
code 6cba118a from Visual Studio Code (vscode✓) installed</code></pre>

We can start Visual Studio code with the following command:

<pre class="wp-block-code"><code>$ code </code></pre>

### Setting Visual Studio code as default editor {.wp-block-heading}

To set Vscode as a default editor, Debian-based systems allow setting default using Debian based alternatives.

<pre class="wp-block-code"><code>$ sudo update-alternatives --set editor /usr/bin/code</code></pre>

## Conclusion {.wp-block-heading}

We have installed Visual Studio Code in our Ubuntu 22.04 workstation. Go ahead and configure to suite your programming needs. Consult [Visual studio code documentation][2] if you face any problem.

 [1]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
 [2]: https://code.visualstudio.com/