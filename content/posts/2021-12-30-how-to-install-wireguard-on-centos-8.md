---
title: How to install Wireguard on CentOS 8
author: Kipkoech Sang
type: post
date: 2021-12-30T08:59:16+00:00
url: /2021/12/30/how-to-install-wireguard-on-centos-8/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 46
rank_math_internal_links_processed:
  - 1
categories:
  - Security

---
In this tutorial, we are going to learn how to install Wireguard VPN on CentOS 8. 

Wireguard is a communication protocol that is free and open-source software that implements encrypted virtual private networks(VPN) and was designed with some goals, to provide ease of use, high-speed performance, and low attack space.

Wireguard aims for better performance and more power than [IPsec][1] and [OpenVPN][2]. Wireguard protocol passes traffic over [UDP][3]. Let us define all these terms we have used so far. It utilizes state-of-the-art cryptography.

**Virtual Private Network** (VPN)- A VPN is an encrypted connection over the internet from a device to a network. The encrypted connection ensures that data is safely transmitted. It prevents unauthorized people from eavesdropping on the traffic and allows the user to conduct work remotely.

**IPSec** is a group of protocols that are used together to set up encrypted connections between devices. It helps keep data sent over a private network secure from intruders. It works by encrypting IP packets along with authenticating the source where they came from.

**OpenVPN** is a VPN system that implements techniques to create secure point-to-point or site-to-site in routed configurations and remote access facilities. OpenVPN allows peers to authenticate each other using pre-shared secret keys, passwords, or usernames.

## Characteristics of Wireguard {.wp-block-heading}

  * It is simple and easy to use. Wireguard aims to be as easy as to configure and deploy as SSH. It is capable of roaming between IP addresses. It provides an extremely basic yet powerful interface.
  * Wireguard has minimal attack service. It has been designed with ease of implementation and simplicity in mind. It is meant to be easily in very few lines of code as compared to others such as OpenVPN which is implemented with very large code bases hence can be audited easily by security experts.
  * Wireguard has high performance. Wireguard lives under Linux Kernel which means secure networking can be very high speed. It is both suitable for both small embedded devices such as smart phones and fully loaded backbone routers.

## Related Articles  {.wp-block-heading}

  * [How to install Wireguard on Debian 11/Ubuntu 21.04][4]

## Installing Wireguard on CentOS 8 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

To begin the installation, we need to make our repositories up to date. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo yum update -y</code></pre>

## 2. Install and enable EPEL repository {.wp-block-heading}

EPEL provides add-on software packages for Linux distributions. Use the following command to enable this repository.

<pre class="wp-block-code"><code>$ sudo yum install epel-release</code></pre>

The following packages will be installed in the process.

<pre class="wp-block-code"><code>Output
 Package                       Architecture            Version                     Repository               Size
=================================================================================================================
Installing:
 epel-release                  noarch                  8-11.el8                    extras                   24 k

Transaction Summary
=================================================================================================================
Install  1 Package

Total download size: 24 k
Installed size: 35 k
Is this ok &#91;y/N]: y
Downloading Packages:
epel-release-8-11.el8.noarch.rpm                                               1.5 MB/s |  24 kB     00:00    
---------------------------------------------------------------------------------------------------------------
Total                                                                          103 kB/s |  24 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                       1/1 
  Installing       : epel-release-8-11.el8.noarch                                                          1/1 
  Running scriptlet: epel-release-8-11.el8.noarch                                                          1/1 
  Verifying        : epel-release-8-11.el8.noarch                                                          1/1 

Installed:
  epel-release-8-11.el8.noarch                                                                                 

Complete!</code></pre>

## 3. Enable PowerTools  {.wp-block-heading}

PowerTools is a repository that acts as a container for libraries and developer tools. This repository is not enabled by default, so we need to enable it ourselves. To enable use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo yum config-manager --set-enabled powertools</code></pre>

## 4. Setup Wireguard reository. {.wp-block-heading}

Before we can install Wireguard, we need to set up a Wireguard repository on our system. To enable it use the following command;

<pre class="wp-block-code"><code>$ sudo yum copr enable jdoss/wireguard</code></pre>

<pre class="wp-block-code"><code>Output
Enabling a Copr repository. Please note that this repository is not part
of the main distribution, and quality may vary.

The Fedora Project does not exercise any power over the contents of
this repository beyond the rules outlined in the Copr FAQ at
&lt;https://docs.pagure.org/copr.copr/user_documentation.html#what-i-can-build-in-copr>,
and packages are not held to any quality or security level.

Please do not file bug reports about these packages in Fedora
Bugzilla. In case of problems, contact the owner of this repository.

Do you really want to enable copr.fedorainfracloud.org/jdoss/wireguard? &#91;y/N]: y
Repository successfully enabled.</code></pre>

## 5. Install Wireguard on CentOS 8 {.wp-block-heading}

After we have covered all the requirements, then we can now install Wireguard. This installation will install Wireguard tools necessary for providing wg binary for controlling the Wireguard server.

<pre class="wp-block-code"><code>$ sudo yum install wireguard-dkms wireguard-tools</code></pre>

This command will install many dependencies, give it time to finish the installation.

<pre class="wp-block-code"><code>Output
Installing:
 wireguard-dkms         noarch 1:1.0.20211208-1.el8                   copr:copr.fedorainfracloud.org:jdoss:wireguard
                                                                                                         322 k
 wireguard-tools        x86_64 1.0.20210914-1.el8                     epel                               125 k
Installing dependencies:
 bc                     x86_64 1.07.1-5.el8                           baseos                             129 k
 binutils               x86_64 2.30-108.el8_5.1                       baseos                             5.8 M
 cpp                    x86_64 8.5.0-4.el8_5                          appstream                           10 M
 dkms                   noarch 3.0.3-1.el8                            epel                                62 k
 elfutils-libelf-devel  x86_64 0.185-1.el8                            baseos                              59 k
 gcc                    x86_64 8.5.0-4.el8_5                          appstream                           23 M</code></pre>

## Conclusion {.wp-block-heading}

We have installed Wireguard on CentOS 8 through compilation from the source. In case you face any challenges you can always consult the Wireguard [documentation][5]

 [1]: https://www.cloudflare.com/learning/network-layer/what-is-ipsec/#:~:text=IPsec%20is%20a%20group%20of%20protocols%20that%20are%20used%20together,where%20the%20packets%20come%20from.
 [2]: https://openvpn.net/what-is-a-vpn/
 [3]: https://en.wikipedia.org/wiki/User_Datagram_Protocol
 [4]: https://nextgentips.com/2021/10/25/how-to-install-wireguard-on-debian-11-ubuntu-21-04/
 [5]: https://www.wireguard.com/