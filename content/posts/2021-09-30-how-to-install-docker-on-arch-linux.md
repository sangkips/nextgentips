---
title: How to install Docker on Arch Linux
author: Kipkoech Sang
type: post
date: 2021-09-30T08:29:53+00:00
url: /2021/09/30/how-to-install-docker-on-arch-linux/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
ss_ss_click_share_count_twitter:
  - 1
rank_math_primary_category:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 153
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
In this tutorial we are going to learn how to install docker on Arch Linux, Manjaro specifically. First we need to know what docker is.

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
Docker is an open source platform for building, deploying, and managing containerized applications. So what is a container? A container is a standard unit of software that packages up code and all its dependencies so that application runs quickly and reliably from one computing environment to another. A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings. So essentially a container contained everything required to run an application and therefore we don&#8217;t need to worry about files being installed first in the host computer. We can run many containers on the host machine without affecting one 

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
another. Here we are with Docker. Docker provides us with a way to create, run, manage and communicate with containers.

## Why Docker? {.wp-block-heading}

We love Docker because it is an open source containerized platform with the following capabilities:

  * The app is lightweight and has more granular updates. With docker only one process can run in each container. This makes it possible to build an application that can continue running even if one of its parts is pulled down for an update, it will still continue working as if nothing is wrong.
  * Docker container images are very portable. This means Docker run without any modification across a data center, desktop environment or even on a server on the cloud environment.
  * Docker works with what we call container versioning. This is where docker tracks the versions of a container image , whenever their is an update and rollback is required it is done seamlessly. It can also update the repository between an existing version and new one. 
  * Docker run on automated scripts, it can automatically build a container based on application source code provided.

## Docker used terms  {.wp-block-heading}

  * **Docker Cluster:** this is a group of machines that work together to run workloads and provides high availability.
  * **Docker Compose:** This is a tool used for running multi container applications on docker. A compose file is used to define how one or more containers that make up your application are configured.
  * **Docker Container**: A container is a running instance of a docker image.
  * **Docker Hub:** This is a centralized resource for working with docker. It is a central repository for container images. 
  * **Dockerfile:** This is a text document that contains all the commands a user can call on a command line to assemble an image.
  * **Docker build:** Docker build command builds an image from a dockerfile and context. The build context is a set of files at a specified location PATH or URL.

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
We are going to install the 
**Official** and the **Development** version of docker.

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

**Official version of Docker.**

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
We will be using Pacman to install the official binaries from the community.

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
First open a terminal and type:

<pre class="wp-block-code"><code># sudo pacman -S docker</code></pre>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

**Installing the Development version of docker.**

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
The development version is in the Arch User Repository. We need to clone it and build it from the source. Open a terminal and clone the repository first. You need to have git installed first in your system. Git is free and open source version control repository management tool.

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
Lets install Git first. Open a terminal and type:

<pre class="wp-block-code"><code># sudo pacman Syu

# sudo pacman -S git</code></pre>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
Next lets clone the the repository so that we can build from the source as it is in 
<a rel="noreferrer noopener" href="https://aur.archlinux.org/packages/docker-git/" target="_blank">Arch User Repository</a>

<pre class="wp-block-code"><code># &lt;meta http-equiv="content-type" content="text/html; charset=utf-8">git clone https://aur.archlinux.org/docker-git.git</code></pre>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
For us to build from the source we need some tools to aid us in building it, these tools falls under &#8216;base-devel&#8217; package. Now we need to download the required packages using pacman.

<pre class="wp-block-code"><code># sudo pacman -S base-devel</code></pre>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
Go to the repository folder and build it using &#8216;mkpkg-sri&#8217;

<pre class="wp-block-code"><code># cd docker-git/

# makepkg -sri</code></pre>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

**Starting the Docker service on startup**

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
Before using docker, we need to enable docker daemon using 
**systemctl start** command.

<pre class="wp-block-code"><code># &lt;meta http-equiv="content-type" content="text/html; charset=utf-8">sudo systemctl start docker.service</code></pre>

It always become a nuisance starting the daemon every time you boot up your operating system, we need to ensure our daemon is up and running every time by doing the following:

<meta http-equiv="content-type" content="text/html; charset=utf-8" />


<pre class="wp-block-code"><code># sudo systemctl enable docker.service</code></pre>

## Adding User to a group. {.wp-block-heading}

Adding users in Docker is a way to make all management work become easy. We create users which can run day today activities without undermining the functionality of the system. Anyone you add to the group has root rights. We use the following command to do so.

<pre class="wp-block-code"><code>$ sudo usermod -aG docker $USER</code></pre>

After adding user, verify that you can docker commands without sudo. Use the following command to do so

<pre class="wp-block-code"><code>$ docker run hello-world</code></pre>

## Useful Docker commands  {.wp-block-heading}

### Docker run {.wp-block-heading}

Docker run creates and start a container. 

### Docker start  {.wp-block-heading}

It starts a stopped container 

### Docker stop {.wp-block-heading}

It stops a running container 

### Docker volume create {.wp-block-heading}

It creates a volume to be used by the container 

### Docker rm {.wp-block-heading}

It removes a container 

### Docker ps {.wp-block-heading}

It shows the health status of the container 

## Conclusion {.wp-block-heading}

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
In this guide we have learned how to install docker on arch Linux and how to start the docker service. Finally you can check the docker guides whenever you get in trouble. Happy coding!