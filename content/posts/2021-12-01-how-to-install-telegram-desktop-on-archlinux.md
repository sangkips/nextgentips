---
title: How to Install Telegram Desktop on ArchLinux
author: Kipkoech Sang
type: post
date: 2021-12-01T09:05:32+00:00
url: /2021/12/01/how-to-install-telegram-desktop-on-archlinux/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 79
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Telegram desktop on our ArchLinux. 

Telegram is a freeware, cross-platform, cloud-based instant messaging service. The service provides end-to-end encrypted video calling, VoIP, file sharing, etc.

## Reasons for Using Telegram {.wp-block-heading}

  * Telegram is simple to use
  * Telegram messages are highly encrypted, so security is guaranteed. because it has self distract mechanism.
  * It delivers messages faster than other platforms
  * It has an open API where everyone can contribute.
  * It has no limits on the size of media files you use
  * It can sync with all other devices 

## Prerequisites {.wp-block-heading}

  * Make sure you have git installed.
  * Have ArchLinux distro up and running 
  * Make sure you have user with sudo privileges 

## Table of Contents {.wp-block-heading}

  1. Install git in ArchLinux
  2. Install sudo command
  3. Add users with sudo privileges
  4. Install Telegram on ArchLinux
  5. Clone Telegram desktop
  6. Compile the packages 

## 1. Install Git in ArchLinux {.wp-block-heading}

Git is software for tracking changes in a set of files, usually used for coordinating work among programmers doing work by collaborating on code development. So we are installing git here because [AUR packages][1] are developed by different maintainers and all the source codes are pushed into the community repository. Let&#8217;s install git with the following comamnd.

<pre class="wp-block-code"><code>$ pacman -Sy git</code></pre>

You will get the following sample output

<pre class="wp-block-code"><code>$ :: Synchronizing package databases...
 core is up to date
 extra is up to date
 community is up to date
resolving dependencies...
looking for conflicting packages...

Packages (7) db-5.3.28-5  gdbm-1.22-1  perl-5.34.0-2  perl-error-0.17029-3
             perl-mailtools-2.21-5  perl-timedate-2.33-3  git-2.34.1-1

Total Download Size:   22.73 MiB
Total Installed Size:  99.20 MiB

:: Proceed with installation? &#91;Y/n] Y
...
:: Running post-transaction hooks...
(1/4) Creating system user accounts...
Creating group git with gid 975.
Creating user git (git daemon user) with uid 975 and gid 975.
(2/4) Reloading system manager configuration...
  Skipped: Current root is not booted.
(3/4) Arming ConditionNeedsUpdate...
(4/4) Warn about old perl modules</code></pre>

## 2. Install sudo command to Archlinux {.wp-block-heading}

Also, you need to install sudo so that you can use it later while creating a new user. I am installing sudo because I am running my ArchLinux distro inside a Docker container. Run the following command to install sudo

<pre class="wp-block-code"><code>$ pacman -Sy sudo</code></pre>

Sample output

<pre class="wp-block-code"><code>:: Synchronizing package databases...
 core                  137.4 KiB   113 KiB/s 00:01 &#91;######################] 100%
 extra                1586.5 KiB   474 KiB/s 00:03 &#91;######################] 100%
 community               5.8 MiB   521 KiB/s 00:11 &#91;######################] 100%
resolving dependencies...
looking for conflicting packages...

Packages (1) sudo-1.9.8.p2-3

Total Download Size:   1.57 MiB
Total Installed Size:  6.78 MiB

:: Proceed with installation? &#91;Y/n] Y</code></pre>

Press Y to continue with the installation.

## 3. Add sudo user to ArchLinux {.wp-block-heading}

To add a new user with sudo privileges, use the following command on the terminal

<pre class="wp-block-code"><code>$ useradd --create-home nextgentips</code></pre>

User will be created but you need to create a password for the new user. Use the following command.

<pre class="wp-block-code"><code>$ passwd nextgentips</code></pre>

It will prompt you to create a password.

When you have successfully created a password move ahead and give privileges to the new user with the wheel command. Wheel group gives sudo privileges to the new user. To add the new user use the following command;

<pre class="wp-block-code"><code>$ usermod --append --groups wheel nextgentips</code></pre>

Next is to install your prefered text editor. I am going to install nano for now but you can install whichever you like i.e vim

<pre class="wp-block-code"><code>$ pacman -Sy nano</code></pre>

Sample output

<pre class="wp-block-code"><code>:: Synchronizing package databases...
 core is up to date
 extra is up to date
 community is up to date
resolving dependencies...
looking for conflicting packages...

Packages (1) nano-5.9-1

Total Download Size:   0.58 MiB
Total Installed Size:  2.43 MiB

:: Proceed with installation? &#91;Y/n] y
:: Retrieving packages...
 nano-5.9-1-x86_64     594.8 KiB   380 KiB/s 00:02 &#91;######################] 100%
(1/1) checking keys in keyring                     &#91;######################] 100%
(1/1) checking package integrity                   &#91;######################] 100%
(1/1) loading package files                        &#91;######################] 100%
(1/1) checking for file conflicts                  &#91;######################] 100%
:: Processing package changes...
(1/1) installing nano                              &#91;######################] 100%
:: Running post-transaction hooks...
(1/1) Arming ConditionNeedsUpdate...</code></pre>

You need to edit the sudoers file. Go to /etc/sudoers and comment out &#8220;# %wheel All=(ALL) ALL&#8221;

Use the following command;

<pre class="wp-block-code"><code>$ nano /etc/sudoers</code></pre>

To switch to the new user, use the following command;

<pre class="wp-block-code"><code>$ sudo - nextgentips</code></pre>

Test your new user with **whoami** command 

<pre class="wp-block-code"><code>$ whoami</code></pre>

If you get **root** as output then you are good to install Telegram now.

## 4. Install Telegram on ArchLinux {.wp-block-heading}

To begin our installation, we need to have base-devel packages in our system. Do install with the following command;

<pre class="wp-block-code"><code>$ sudo pacman -Sy git base-devel</code></pre>

You will get the following sample output. All those are packages required.

<pre class="wp-block-code"><code>:: Synchronizing package databases...
 core                  137.4 KiB   116 KiB/s 00:01 &#91;######################] 100%
 extra                1586.5 KiB   525 KiB/s 00:03 &#91;######################] 100%
 community               5.8 MiB   363 KiB/s 00:16 &#91;######################] 100%
&#91;nextgentips@8be4dc75a112 ~]$ sudo pacman -S git base-devel
warning: git-2.34.1-1 is up to date -- reinstalling
:: There are 24 members in group base-devel:
:: Repository core
   1) autoconf  2) automake  3) binutils  4) bison  5) fakeroot  6) file
   7) findutils  8) flex  9) gawk  10) gcc  11) gettext  12) grep  13) groff
   14) gzip  15) libtool  16) m4  17) make  18) pacman  19) patch  20) pkgconf
   21) sed  22) sudo  23) texinfo  24) which
Enter a selection (default=all):</code></pre>

Press enter to install all the packages or any number to install the specific one.

## 5. Clone Telegram from git repository {.wp-block-heading}

To install the telegram desktop in our system, use the following command to clone the repository.

<pre class="wp-block-code"><code>$ git clone https://aur.archlinux.org/telegram-desktop-git.git</code></pre>

The output will look as follows;

<pre class="wp-block-code"><code>Cloning into 'telegram-desktop-git'...
remote: Enumerating objects: 174, done.
remote: Counting objects: 100% (174/174), done.
remote: Compressing objects: 100% (149/149), done.
remote: Total 174 (delta 29), reused 167 (delta 24), pack-reused 0
Receiving objects: 100% (174/174), 101.32 KiB | 816.00 KiB/s, done.
Resolving deltas: 100% (29/29), done.</code></pre>

Cd into the directory you have cloned your package

<pre class="wp-block-code"><code>$ cd telegram-desktop-git.git</code></pre>

## 6. Compile Telegram desktop {.wp-block-heading}

To compile the packages use the following command;

<pre class="wp-block-code"><code>$ makepkg -si</code></pre>

<pre class="wp-block-code"><code>==> Making package: telegram-desktop-git 3.1.9.r31.g7222bc63f-1 (Wed 01 Dec 2021 07:08:10 AM UTC)
==> Checking runtime dependencies...
==> Installing missing dependencies...
&#91;sudo] password for nextgentips: 
Input your password to allow installation to continue.</code></pre>

This will run the **[PKGBUILDs][2]** files. Wait for it to finish before testing your Telegram.

## Conclusion. {.wp-block-heading}

And up to here, we have successfully installed telegram desktop. Go ahead and launch to enjoy telegram services.

 [1]: https://wiki.archlinux.org/title/Arch_User_Repository#Prerequisites
 [2]: https://wiki.archlinux.org/title/PKGBUILD