---
title: How to install and configure Coder on GCP
author: Kipkoech Sang
type: post
date: 2022-11-15T13:58:13+00:00
url: /2022/11/15/how-to-install-and-configure-coder-on-gcp/
categories:
  - Linux

---
Coder is an open-source platform for creating and managing developer workspaces on your preferred cloud such as AWS, GCP, or Azure, and server environment. 

By building on top of common development interfaces and infrastructure tools such as Terraform, Coder aims to make the process of provisioning and accessing remote workspaces approachable for organizations. Coder workspaces are represented with Terraform. 

### Reasons for remote development. {.wp-block-heading}

  * Increased speed when it comes to IDE loading, code compilation, and running of large workloads such as those of microservices.
  * Improved compatibility because remote workspaces share infrastructure configurations with others, this helps to reduce configuration issues.
  * Terraform, Docker and other container-centric environments make developer onboarding and troubleshooting much easier.
  * Increased security because cloud services are much more secure as compared to in-house data centers. 

### Terms to note {.wp-block-heading}

### Agents {.wp-block-heading}

An `<mark style="background-color:#abb8c3" class="has-inline-color">agent</mark>` is a Coder service that runs within a user&#8217;s remote workspace. It provides a consistent interface for `<mark style="background-color:#abb8c3" class="has-inline-color">coderd</mark>` and clients to communicate with the workspace regardless of the operating system, or the cloud your infrastructure resides.

### Coderd {.wp-block-heading}

It&#8217;s the service created whenever a coder server is run. Its a small API that connects workspaces and users together.

### Provisionerd {.wp-block-heading}

This is the infrastructure provider, currently, it only has Terraform. Provisioners can have different Terraform versions that satisfy their own requirements.

### Workspaces  {.wp-block-heading}

This is a set of cloud resources such as VMs, Kubernetes clusters, storage buckets, etc.

### Installing Coder in Google Cloud Platform {.wp-block-heading}

### 1. Setting up GCP VM {.wp-block-heading}

To begin installing the coder, we need to set up the Compute engine API. Go to create an instance and give an instance name, **Allow both HTTPs and HTTP traffic and lastly allow full access to cloud API and click create.** <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="592" height="451" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-10-30.png?resize=592%2C451&#038;ssl=1" alt="nextgentips:GCP instance" class="wp-image-1625" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-10-30.png?w=592&ssl=1 592w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-10-30.png?resize=300%2C229&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-10-30.png?resize=105%2C80&ssl=1 105w" sizes="(max-width: 592px) 100vw, 592px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips GCP instance</figcaption></figure> 

### 2. SSH into the instance {.wp-block-heading}

The next step is to ssh into the instance to begin working on your instance. Click ssh and open open in window browser, this will open a terminal.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="684" height="251" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-17-56.png?resize=684%2C251&#038;ssl=1" alt="nextgentips:ssh" class="wp-image-1626" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-17-56.png?w=684&ssl=1 684w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-17-56.png?resize=300%2C110&ssl=1 300w" sizes="(max-width: 684px) 100vw, 684px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips:ssh</figcaption></figure> 

### 3. Install Coder {.wp-block-heading}

Head over to the terminal you opened in the step 2 above and run the the below command.

<pre class="wp-block-code"><code>curl -fsSL https://coder.com/install.sh | sh  </code></pre>

You will be in a position to see the following as the output. Follow the steps given to set up the coder.

<pre class="wp-block-code"><code>Debian GNU/Linux 11 (bullseye)
Installing v0.12.7 of the amd64 deb package from GitHub.

+ mkdir -p ~/.cache/coder
+ curl -#fL -o ~/.cache/coder/coder_0.12.7_amd64.deb.incomplete -C - https://github.com/coder/coder/releases/download/v0.12.7/coder_0.12.7_linux_amd64.deb
######################################################################## 100.0%
+ mv ~/.cache/coder/coder_0.12.7_amd64.deb.incomplete ~/.cache/coder/coder_0.12.7_amd64.deb
+ sudo dpkg --force-confdef --force-confold -i ~/.cache/coder/coder_0.12.7_amd64.deb
Selecting previously unselected package coder.
(Reading database ... 54255 files and directories currently installed.)
Preparing to unpack .../coder/coder_0.12.7_amd64.deb ...
Unpacking coder (0.12.7-1) ...
Setting up coder (0.12.7-1) ...

deb package has been installed.

To run Coder as a system service:

  # (Optional) Set up an external access URL
  $ sudo vim /etc/coder.d/coder.env
  # Use systemd to start Coder now and on reboot
  $ sudo systemctl enable --now coder
  # View the logs to ensure a successful start
  $ journalctl -u coder.service -b

Or, just run the server directly:

  $ coder server</code></pre>

### 4. Setting up Coder {.wp-block-heading}

Let&#8217;s follow the steps given when we run the coder script as shown in the output above. First edit `<mark style="background-color:#abb8c3" class="has-inline-color">coder.env</mark>` file. Use vim to open the file. 

<pre class="wp-block-code"><code>sudo vim /etc/coder.d/coder.env</code></pre>

Add `<mark style="background-color:#abb8c3" class="has-inline-color">CODER_TUNNEL=true</mark>` save and exit.

Use systemd to start Coder now and on reboot

<pre class="wp-block-code"><code>sudo systemctl enable --now coder</code></pre>

You can view the logs to ensure a successful start of the Coder.

<pre class="wp-block-code"><code>journalctl -u coder.service -b</code></pre>

Here is the output of the journalctl.

<pre class="wp-block-code"><code>-- Journal begins at Tue 2022-11-15 11:12:48 UTC, ends at Tue 2022-11-15 11:38:04 UTC>
Nov 15 11:37:56 coder systemd&#91;1]: Starting "Coder - Self-hosted developer workspaces >
Nov 15 11:37:56 coder coder&#91;732]: Coder v0.12.7+49b340e - Remote development on your >
Nov 15 11:37:56 coder coder&#91;732]: Using built-in PostgreSQL (/home/coder/.config/code>
Nov 15 11:38:00 coder coder&#91;732]: Opening tunnel so workspaces can connect to your de>
Nov 15 11:38:00 coder coder&#91;732]: Error picking closest dev tunnel: socket: permissio>
Nov 15 11:38:00 coder coder&#91;732]: Defaulting to US East Pittsburgh
Nov 15 11:38:00 coder coder&#91;732]: Using tunnel in US East Pittsburgh with latency  0s>
Nov 15 11:38:00 coder coder&#91;732]:&lt;strong> View the Web UI: https://fcca2ed0f46486478a4ce1aebc>&lt;/strong>
Nov 15 11:38:04 coder coder&#91;732]: Get started by creating the first user (in a new te>
Nov 15 11:38:04 coder coder&#91;732]:  coder login https://fcca2ed0f46486478a4ce1aebc0a79>
Nov 15 11:38:04 coder coder&#91;732]: ==> Logs will stream in below (press ctrl+c to grac>
Nov 15 11:38:04 coder systemd&#91;1]: Started "Coder - Self-hosted developer workspaces o>
lines 1-13/13 (END)</code></pre>

We can now open the web user interface using the following given URL.

<pre class="wp-block-code"><code>coder login https://fcca2ed0f46486478a4ce1aebc0a790b.pit-1.try.coder.app</code></pre><figure class="wp-block-image size-full is-resized">

<img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-47-09.png?resize=384%2C474&#038;ssl=1" alt="nextgentips: coder login" class="wp-image-1627" width="384" height="474" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-47-09.png?w=420&ssl=1 420w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-47-09.png?resize=243%2C300&ssl=1 243w" sizes="(max-width: 384px) 100vw, 384px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips: coder login</figcaption></figure> 

When you have successfully created your account you are taken to workspaces. Here we can can our workspace with the use of a template.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="333" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-51-13.png?resize=810%2C333&#038;ssl=1" alt="nextgentips:Worksapce " class="wp-image-1628" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-51-13.png?resize=1024%2C421&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-51-13.png?resize=300%2C123&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-51-13.png?resize=768%2C316&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-51-13.png?w=1319&ssl=1 1319w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips:Worksapce </figcaption></figure> 

### Creating a workspace from the template  {.wp-block-heading}

To begin creating a workspace, click on create from template.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="296" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-56-06.png?resize=810%2C296&#038;ssl=1" alt="nextgentips:template" class="wp-image-1629" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-56-06.png?resize=1024%2C374&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-56-06.png?resize=300%2C109&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-56-06.png?resize=768%2C280&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-15-14-56-06.png?w=1321&ssl=1 1321w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">nextgentips:template</figcaption></figure> 

Go to your terminal and run the following command to start build in templates

<pre class="wp-block-code"><code>coder template init </code></pre>

This is what you will get from using the above command.

<pre class="wp-block-code"><code>A template defines infrastructure as code to be provisioned for individual      
developer workspaces. Select an example to be copied to the active directory:   
                                                                                
Type to search

  > Develop in an ECS-hosted container
      Get started with Linux development on AWS ECS.                            
      https:&#47;&#47;github.com/coder/coder/tree/main/examples/templates/aws-ecs-container

    Develop in Linux on AWS EC2
      Get started with Linux development on AWS EC2.                            
      https://github.com/coder/coder/tree/main/examples/templates/aws-linux

    Develop in Windows on AWS
      Get started with Windows development on AWS.                              
      https://github.com/coder/coder/tree/main/examples/templates/aws-windows

    Develop in Linux on Azure
      Get started with Linux development on Microsoft Azure.                    
      https://github.com/coder/coder/tree/main/examples/templates/azure-linux

    Develop in Linux on a Digital Ocean Droplet
      Get started with Linux development on a Digital Ocean Droplet.            
      https://github.com/coder/coder/tree/main/examples/templates/do-linux

    Develop in Docker
      Run workspaces on a Docker host using registry images                     
      https://github.com/coder/coder/tree/main/examples/templates/docker

    Develop code-server in Docker
      Run code-server in a Docker development environment                       
      https://github.com/coder/coder/tree/main/examples/templates/docker-code-server</code></pre>

Choose the cloud provider you want to use. Here I will be using the GCP because I was running GCP.

When you click on GCP you will get the following output, cd into that directory and run `<mark style="background-color:#abb8c3" class="has-inline-color">coder templates create</mark>` to start creating the template.

<pre class="wp-block-code"><code>A template defines infrastructure as code to be provisioned for individual      
developer workspaces. Select an example to be copied to the active directory:   
                                                                                
Extracting  gcp-linux  to ./gcp-linux...
Create your template by running:
                                                            
   &lt;strong>cd ./gcp-linux && coder templates create &lt;/strong>                

Examples provide a starting point and are expected to be edited! 🎨   </code></pre>

If you get an error that you need to login use the following command to login.

Open the following in your browser:

<pre class="wp-block-code"><code>#To login
$ coder login https://fcca2ed0f46486478a4ce1aebc0a790b.pit-1.try.coder.app   
To get auth token
$ https://fcca2ed0f46486478a4ce1aebc0a790b.pit-1.try.coder.app/cli-auth

Paste your token here:
Welcome to Coder, sangkips! You're authenticated.</code></pre>

From here you will be in position to go into your workspace and start coding.