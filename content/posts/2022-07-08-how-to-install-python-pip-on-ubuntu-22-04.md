---
title: How to install Python Pip on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-08T06:08:31+00:00
url: /2022/07/08/how-to-install-python-pip-on-ubuntu-22-04/
categories:
  - Programming

---
In this tutorial, you will learn how to install Python pip on Ubuntu 22.04.

[Pip][1] is a package management system written in Python and is used to install and manage software packages. It connects to an online repository of public packages called the Python Package Index. It can also be configured to connect to other package repositories.

Usually, Pip is automatically installed while working in a virtual environment, and also if you are using Python that has not been modified by the redistributor to remove ensure pip.

Pip is recommended tool for installing Python packages. For you to start using pip, you need to have python3 installed on your system.

## Installing Python pip on Ubuntu 22.04 {.wp-block-heading}

You can install pip either through a virtual environment or while installing Python. It depends on which one you prefer. I will be using a virtual environment to install pip on Ubuntu 22.04.

### 1. Run system updates {.wp-block-heading}

First, we need to update our repositories to make them up to date by issuing update and upgrade commands on our terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Create a virtual environment {.wp-block-heading}

A virtual environment is used to manage Python packages for different projects. It creates an isolated environment for pip packages.

Begin by creating a directory where your virtual environment will be located.

<pre class="wp-block-code"><code>$ sudo mkdir nextgen
$ cd nextgen</code></pre>

Next, create a virtual environment

<pre class="wp-block-code"><code>$ python3 -m venv env</code></pre>

**env** is the name given to a virtual environment.

In case you encounter the following error, follow the instruction in order to make necessary changes.

<pre class="wp-block-code"><code>The virtual environment was not created successfully because ensurepip is not
available.  On Debian/Ubuntu systems, you need to install the python3-venv
package using the following command.

    apt install python3.10-venv

You may need to use sudo with that command.  After installing the python3-venv
package, recreate your virtual environment.

Failing command: &#91;'/root/nextgen/env/bin/python3', '-Im', 'ensurepip', '--upgrade', '--default-pip']
</code></pre>

Correct this error by running 

<pre class="wp-block-code"><code>sudo apt install python3.10-venv</code></pre>

From the above, you will see the following output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  python3-pip-whl python3-setuptools-whl
The following NEW packages will be installed:
  python3-pip-whl python3-setuptools-whl python3.10-venv
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 2473 kB of archives.
After this operation, 2882 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

The next thing is to run `<mark style="background-color:#abb8c3" class="has-inline-color">python3 -m venv env</mark>` again.

<pre class="wp-block-code"><code>sudo python3 -m venv env</code></pre>

You need to activate the virtual environment to start using it.

<pre class="wp-block-code"><code>$ source env/bin/activate 
(env) root@localhost:~/nextgen# </code></pre>

Now that we have activated the virtual environment, you can now check if pip has been installed, because pip is installed while creating the virtual environment.

<pre class="wp-block-code"><code>pip --version
pip 22.0.2 from /root/nextgen/env/lib/python3.10/site-packages/pip (python 3.10)</code></pre>

## Conclusion {.wp-block-heading}

We have learned how to install pip on Ubuntu 22.04 because this is the starting point for any python programming. I am glad you have enjoyed the tutorial. Feel free to comment in case you are faced with a challenge.

 [1]: https://pypi.org/project/pip/