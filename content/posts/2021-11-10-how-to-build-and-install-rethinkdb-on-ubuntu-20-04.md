---
title: How to Build and Install RethinkDB on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-11-10T19:37:21+00:00
url: /2021/11/10/how-to-build-and-install-rethinkdb-on-ubuntu-20-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 106
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this article I will be guiding you on how to install RethinkDB on Ubuntu 20.04

RethinkDB is an open-source, scalable JSON database built from the ground up for the realtime web. It inverts the traditional database architecture by exposing an exciting new access model, instead of polling for changes, the developer can tell RethinkDB to continuously push updated query results to applications in realtime. RethinkDB’s realtime push architecture dramatically reduces the time and effort necessary to build scalable realtime apps. It is a great option when you need real time feeds to your data.

RethinkDB is very useful when your application needs real time feeds to your data. RethinkDB query-response database access model works well on the web because it maps directly to HTTPs response request. 

RethinkDB works well in the following fields:

  * Developing realtime analytic applications 
  * Working on multiplayer games 
  * Working with realtime money markets such as New York stock Exchange
  * Working with connected devices 
  * Developing collaborative web and mobile applications.

## Installing RethinkDB  {.wp-block-heading}

In this guide I will be showing you how to install RethinkDB by compiling from the source.

## 1. Install RethinkDB dependencies  {.wp-block-heading}

To first install RethinkDB we need to update our system and install the following dependencies

  * libprotobuf-dev libcurl4-openssl-dev
  * build-essential protobuf-compiler python
  * libboost-all-dev libncursess5-dev
  * libjemalloc-dev wget m4

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

When the update and upgrades are complete, you need to install the following dependencies so that it allows us to build RethinkDB effectively. Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt-get install build-essential protobuf-compiler python \
                     libprotobuf-dev libcurl4-openssl-dev \
                     libboost-all-dev libncurses5-dev \
                     libjemalloc-dev wget m4</code></pre>

After installation is complete then we need download RethinkDB from its repository. The effective way to do is to use wget command. Download and extract the tar into your system with the following command;

<pre class="wp-block-code"><code>$ wget https://download.rethinkdb.com/repository/raw/dist/rethinkdb-2.4.1.tgz</code></pre>

Extract the downloaded file to your system with the following command;

<pre class="wp-block-code"><code>$ tar xf rethinkdb-2.4.1.tgz</code></pre>

It is now time to build our server but first cd into RethinkDB repository

<pre class="wp-block-code"><code>$ cd rethinkdb-2.4.1</code></pre>

Pass this command to run configuration files; followed by command make. make command is used to build and maintain groups of programs and files from the source code.

<pre class="wp-block-code"><code>$ ./configure --allow-fetch
$ make </code></pre>

This process will take long so wait for it to complete before running the installation command. When the process is complete you can then run the following command to install RethinkDB.

<pre class="wp-block-code"><code>$ sudo make install </code></pre>

This will start the installation of RethinkDB into your system.

## Conclusion {.wp-block-heading}

You have installed RethinkDB successfully by building from the source. Take time to understand the underlying architecture surrounding RethinkDB. For any queery dont hesitate to consult the [documetation.][1]

## Related Articles  {.wp-block-heading}

  * [Databases you can use for free][2]

 [1]: https://rethinkdb.com/docs
 [2]: https://nextgentips.com/2021/10/02/databases-you-can-use-for-free/