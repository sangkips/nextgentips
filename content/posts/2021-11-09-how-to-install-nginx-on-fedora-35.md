---
title: How to install Nginx on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-09T10:31:22+00:00
url: /2021/11/09/how-to-install-nginx-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 107
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial i will show you how to install Nginx on Fedora 35.

Nginx is a fast and lightweight web, http load balancer, reverse proxy and http cache server. Its scalability and efficiency makes Nginx both suitable for small and high traffic servers. It can also function as a proxy server for email IMAP,POP3 and SMTP.

Nginx has proved to be ideal web server for many web task because it can handle a high volume of connections. Nginx is frequently placed between clients and a second server to serve as an SSL/TLS terminator or web accelerator. Dynamic sites build with languages like PHP, node.js deploy Nginx as content cache and reverse proxy to reduce load on application servers and make the most effective use of the underlying hardware.

## Prerequisites  {.wp-block-heading}

To install Nginx on Fedora 35, we need to have the following:

  * Fedora 35 server having users with sudo priviledges
  * Internet connection
  * Familiarity with command line interface

## Related articles  {.wp-block-heading}

  * [How to setup Ubuntu 20.04 Server for the first time][1]

## Install Nginx via DNF  {.wp-block-heading}

We use [dnf][2] command to run installation for the latest version of Fedora including the recent release of Fedora 35. So what we need to do is to first update the system with the following command.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When the update is complete, then we can install Nginx with the following command:

<pre class="wp-block-code"><code>sudo dnf install nginx </code></pre>

Press **y** when prompted to allow the installation to continue. During installation all the dependency packages are installed so you don&#8217;t have to worry about them.

Now After instalaltion is complete we need to start the Nginx server to start operational. We do that with the following command;

<pre class="wp-block-code"><code>$ systemctl start nginx.service </code></pre>

Another important aspect of Nginx is to enable it to start on reboot so that you can not be doing it manually every time you reboot the system. Use the **enable** command to accomplish this;

<pre class="wp-block-code"><code>$ systemctl enable nginx.service</code></pre>

This command will create system link pointing to the nginx service.

Check your firewall if it is enable, so that you will be able to pass HTTP traffic in the server. We need to enable the firewall in order to filter traffic into our server. RPM uses [firewalld][3] to set up firewall. To set up firewalld use the following command

<pre class="wp-block-code"><code>$ sudo dnf install firewalld -y</code></pre>

When the installation is complete, now we can start our firewalld with the following command;

<pre class="wp-block-code"><code>$ systemctl start firewalld</code></pre>

You can now check the status of firewalld with the following command;

<pre class="wp-block-code"><code>$ systemctl status firewalld</code></pre>

Sample Output

<pre class="wp-block-code"><code># systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-09 09:24:20 UTC; 1min 18s ago
       Docs: man:firewalld(1)
   Main PID: 20276 (firewalld)
      Tasks: 2 (limit: 2336)
     Memory: 24.2M
        CPU: 546ms
     CGroup: /system.slice/firewalld.service
             └─20276 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid

Nov 09 09:24:19 fedora systemd&#91;1]: Starting firewalld - dynamic firewall daemon...
Nov 09 09:24:20 fedora systemd&#91;1]: Started firewalld - dynamic firewall daemon.</code></pre>

Firewalld is now running, we need to allow Nginx to to pass the firewall rules. We need to permanently allow **HTTP** to pass which is running on port 80 by default.

We need to add **http** with the following command;

<pre class="wp-block-code"><code>$ sudo firewall-cmd --permanent --add-service=http</code></pre>

You will get a success message which shows it had added the http onto allowed rules. You will need to reload the firewall for the changes to take effect. 

<pre class="wp-block-code"><code>$ firewall-cmd --reload</code></pre>

If you get a success message you are good to go.

You can now verify if the changes was indeed accepted.

<pre class="wp-block-code"><code>sudo firewall-cmd --permanent --list-all</code></pre>

Sample output will be as follows

<pre class="wp-block-code"><code># firewall-cmd --permanent --list-all
public
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 
  services: dhcpv6-client http mdns ssh
  ports: 
  protocols: 
  forward: no
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: </code></pre>

We can now go ahead and check on our browser if our installation was successful. Enter your server IP address on the browser. If you dont know the IP address you can check with the following command;

<pre class="wp-block-code"><code>$ curl -4 icanhazip.com</code></pre>

This command will give you the IP address of your server. Type that IP address on your browser. <figure class="wp-block-image size-large is-resized">

<img decoding="async" loading="lazy" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-09-13-16-43-3-1024x586.png?resize=810%2C463&#038;ssl=1" alt="Nginx image" class="wp-image-553" width="810" height="463" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-09-13-16-43-3.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-09-13-16-43-3.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-09-13-16-43-3.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-09-13-16-43-3.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nginx image</figcaption></figure> 

## Conclusion  {.wp-block-heading}

You have learn how to install Nginx on Fedora 35 server. You can go ahead and configure it to server your website correctly.

 [1]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [2]: https://fedoraproject.org/wiki/DNF?rd=Dnf
 [3]: https://docs.fedoraproject.org/en-US/quick-docs/firewalld/