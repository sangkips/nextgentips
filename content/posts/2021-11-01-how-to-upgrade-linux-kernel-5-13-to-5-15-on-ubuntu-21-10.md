---
title: How to upgrade Linux Kernel 5.13 to 5.15 on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-11-01T11:58:04+00:00
url: /2021/11/01/how-to-upgrade-linux-kernel-5-13-to-5-15-on-ubuntu-21-10/
ss_ss_click_share_count_facebook:
  - 1
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:14:"105.161.84.104";i:1;s:13:"18.234.30.224";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 114
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to upgrade Linux Kernel 5.13 to 5.15 on Ubuntu 21.10.

Linux 5.15 mainline was released recently by **Linux Torvalds** with better new features to try out. The mainline tree is maintained by Linus Torvalds and It is where all new features are added and releases always come from.

The new release has come with notable features such as:

  * The new release added new NTFS3 file system developed by Paragon software.
  * Introduction of ASUS ACPI platform to support Asus computers 
  * AMD Zen 3 APU introduced for temperature monitoring 
  * Apple M1 IOMMU introduced to tap in on the apple products 
  * RDNA2 PCI IDs introduced to give good graphics.

You can learn many more features introduced from this [article][1].

## Related materials {.wp-block-heading}

  * [What is new with Ubuntu 21.10 “Impish Indri”][2]

## Installing Linux Kernel 5.15 {.wp-block-heading}

First, we can check to see the kernel version we are running currently. **uname -r** command

<pre class="wp-block-code"><code>$ uname -r
5.13.0-20-generic</code></pre>

For my case am running Linux Kernel 5.13. We can upgrade to the newer Linux kernel 5.15. Follow along for details.

Let&#8217;s update our system first so that all repositories are up to date. To update the system we used the following command.

<pre class="wp-block-code"><code>$ sudo apt update && sudo apt upgrade -y </code></pre>

After the process is complete we can now download the Linux kernel headers from Ubuntu kernel mainline page. Use the following code to accomplish that:

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb</code></pre>

<pre class="wp-block-code"><code># wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb
--2021-11-01 10:58:10--  https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb
Resolving kernel.ubuntu.com (kernel.ubuntu.com)... 91.189.94.216
Connecting to kernel.ubuntu.com (kernel.ubuntu.com)|91.189.94.216|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 12189104 (12M) &#91;application/x-debian-package]
Saving to: ‘linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb’

linux-headers-5.15.0-051500r 100%&#91;===========================================&gt;]  11.62M  16.1MB/s    in 0.7s    

2021-11-01 10:58:11 (16.1 MB/s) - ‘linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb’ saved &#91;12189104/12189104]</code></pre>

  * **wget**: this is used to retrieved content from the website.
  * **wget -c**: instruct the system to continue downloading partially downloaded images. 

Next, we can download Linux Generic headers with the following command.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-headers-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb</code></pre>

When this is complete we can now download Linux kernel image from the following:

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-image-unsigned-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb</code></pre>

Lastly, we can download the modules required to build the kernel.

<pre class="wp-block-code"><code>$ wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15-rc7/amd64/linux-modules-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb</code></pre>

Install new kernel now with the following command:

<pre class="wp-block-code"><code>$ # sudo dpkg -i *.deb
Selecting previously unselected package linux-headers-5.15.0-051500rc7-generic.
(Reading database ... 62253 files and directories currently installed.)
Preparing to unpack linux-headers-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb ...
Unpacking linux-headers-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
Selecting previously unselected package linux-headers-5.15.0-051500rc7.
Preparing to unpack linux-headers-5.15.0-051500rc7_5.15.0-051500rc7.202110251930_all.deb ...
Unpacking linux-headers-5.15.0-051500rc7 (5.15.0-051500rc7.202110251930) ...
Selecting previously unselected package linux-image-unsigned-5.15.0-051500rc7-generic.
Preparing to unpack linux-image-unsigned-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb ...
Unpacking linux-image-unsigned-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
Selecting previously unselected package linux-modules-5.15.0-051500rc7-generic.
Preparing to unpack linux-modules-5.15.0-051500rc7-generic_5.15.0-051500rc7.202110251930_amd64.deb ...
Unpacking linux-modules-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
Setting up linux-headers-5.15.0-051500rc7 (5.15.0-051500rc7.202110251930) ...
Setting up linux-headers-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
Setting up linux-image-unsigned-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
I: /boot/vmlinuz is now a symlink to vmlinuz-5.15.0-051500rc7-generic
I: /boot/initrd.img is now a symlink to initrd.img-5.15.0-051500rc7-generic
Setting up linux-modules-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
Processing triggers for linux-image-unsigned-5.15.0-051500rc7-generic (5.15.0-051500rc7.202110251930) ...
/etc/kernel/postinst.d/initramfs-tools:
update-initramfs: Generating /boot/initrd.img-5.15.0-051500rc7-generic
/etc/kernel/postinst.d/zz-update-grub:
Sourcing file `/etc/default/grub'
Sourcing file `/etc/default/grub.d/50-cloudimg-settings.cfg'
Sourcing file `/etc/default/grub.d/init-select.cfg'
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.15.0-051500rc7-generic
Found initrd image: /boot/initrd.img-5.15.0-051500rc7-generic
Found linux image: /boot/vmlinuz-5.13.0-20-generic
Found initrd image: /boot/initrd.img-5.13.0-20-generic
done</code></pre>

## Reboot the system {.wp-block-heading}

After installation is complete we can reboot the system for the new changes to take effect.

<pre class="wp-block-code"><code>$ sudo shutdown -r now </code></pre>

Now you can check the version of the Linux kernel you are running again to see if we are successful with our installation 

<pre class="wp-block-code"><code># uname -r
5.15.0-051500rc7-generic</code></pre>

To check the operating system you are running, use the following command to do so:

<pre class="wp-block-code"><code># lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 21.10
Release:        21.10
Codename:       impish</code></pre>

Congratulations you have upgraded from Linux kernel 5.13-generic to 5.15-generic.

## Conclusion {.wp-block-heading}

We have successfully upgraded our Linux Kernel from 5.13 to 5.15. If you encounter any problem feel free to [contact us][3] or you can consult [Linux Mainline Documentation.][4]

 [1]: https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.15-Feature-Look
 [2]: https://nextgentips.com/2021/10/14/what-is-new-with-ubuntu-21-10-impish-indri/
 [3]: https://nextgentips.com/contact-us/
 [4]: https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.15/