---
title: How to install PHP 8.1 on Debian 11
author: Kipkoech Sang
type: post
date: 2022-01-31T11:08:28+00:00
url: /2022/01/31/how-to-install-php-8-1-on-debian-11/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 15
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial, I am going to show you how to install the current stable release version of PHP 8.1 on Debian 11.

[PHP][1]&nbsp;is a general-purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is:

  * **Fast**&nbsp;– PHP is fast because it runs on its own memory space. Also PHP has Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * **Flexible&nbsp;**– PHP is flexible because it allows to almost all databases. MySQL being the de-facto database to connect to because it is open source.
  * Free and open source
  * **PHP is forgiving&nbsp;**– Why do we say PHP is forgiving? It is forgiving meaning its learning it is not so hard therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Prerequisites for Installing PHP 8.1 {#prerequisites-for-installing-php-8-1.wp-block-heading}

  * Before you can use PHP you must have&nbsp;**web server installed**. Apache web server is the most widely used with PHP.
  * **Database**&nbsp;must be there for PHP to run. It works well on all databases such Oracle, MySQL, PostgreSQL.
  * **PHP parser**&nbsp;must be there. What is PHP parser if you can ask? PHP Parser is a library that takes a source code written in PHP, passes it through a lexical analyzer and creates its respective syntax tree.

PHP 8.1 is a major update release version. It contains many new features and optimizations e.g

  * It introduced Enumerations feature. Enumerations or Enum feature allow a developer to define a custom type that is limited to one of the discrete number of possible values. This is helpful while defining domain model.
  * Improvement of RFC readonly property version 2. This prevents modification of the property after initialization.
  * Introduction of RFC fibers call action. This seeks to eliminate synchronous and asychronous function call by allowing functions to become interruptible without affecting the entire fuction.
  * Introduction of a non return type. This indicates that a function either exits, throws an exception or doesn’t terminate.
  * Introduction of first class collable actions.
  * Introduction of&nbsp;[fsync][2]&nbsp;and&nbsp;[fdatasync][3]&nbsp;functions
  * Introduction of&nbsp;[array\_is\_list][4]&nbsp;functions
  * Introduction of explicit octal numeral notation

## Related Articles {#related-articles.wp-block-heading}

  * <a href="https://nextgentips.com/2021/12/04/how-to-install-php-8-1-on-centos-8-rhel-8/" target="_blank" rel="noreferrer noopener">How to install PHP 8.1 on CentOS 8/RHEL 8</a>
  * <a href="https://nextgentips.com/2021/12/04/how-to-install-php-8-1-on-centos-8-rhel-8/" target="_blank" rel="noreferrer noopener">How to install PHP 8.1 on Ubuntu</a> [21.04][5]
  * <a href="https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/" target="_blank" rel="noreferrer noopener">How to Install PHP 8 on Ubuntu 21.04</a>
  * <a href="https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/" target="_blank" rel="noreferrer noopener">How to install PHP 8.1 on Ubuntu 21.10</a>
  * <a href="https://nextgentips.com/2022/01/31/how-to-install-php-8-1-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install PHP 8.1 on Ubuntu 20.04</a>

## Installing PHP 8.1 on Debian 11 {#installing-php-8-1-on-debian-11.wp-block-heading}

### 1. Run system updates  {#1-run-system-updates.wp-block-heading}

To begin with let&#8217;s update system repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Php dependencies  {#2-install-php-dependencies.wp-block-heading}

For PHP in Debian, we need to install some dependencies into our system. We will install the following dependencies.

<pre class="wp-block-code"><code>$ sudo apt install apt-transport-https lsb-release ca-certificates curl</code></pre>

<pre class="wp-block-code"><code># output 
Building dependency tree... Done
Reading state information... Done
apt-transport-https is already the newest version (2.2.4).
ca-certificates is already the newest version (20210119).
curl is already the newest version (7.74.0-1.3+deb11u1).
lsb-release is already the newest version (11.1.0).
lsb-release set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

### 3. Download Sury PPA {#3-download-sury-ppa.wp-block-heading}

Lets now download PPA to be used to install PHP 8.1

<pre class="wp-block-code"><code>$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg</code></pre>

### 4. Create PHP file directory {#4-create-php-file-directory.wp-block-heading}

Create a PHP file directory where all PHP files will be located.

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'</code></pre>

We need to run system updates again so that changes will take effect immediately 

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

### 5. Install PHP 8.1 on Debian 11 {#5-install-php-8-1-on-debian-11.wp-block-heading}

Now we can install PHP 8.1 on Debian with the following command;

<pre class="wp-block-code"><code>$ sudo apt install php8.1</code></pre>

<pre class="wp-block-code"><code># output
The following additional packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils bzip2 libapache2-mod-php8.1 libapr1 libaprutil1 libaprutil1-dbd-sqlite3
  libaprutil1-ldap libgdbm-compat4 libjansson4 liblua5.3-0 libperl5.32 libsodium23 mailcap mime-support perl
  perl-modules-5.32 php-common php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser bzip2-doc php-pear perl-doc
  libterm-readline-gnu-perl | libterm-readline-perl-perl make libtap-harness-archive-perl
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils bzip2 libapache2-mod-php8.1 libapr1 libaprutil1 libaprutil1-dbd-sqlite3
  libaprutil1-ldap libgdbm-compat4 libjansson4 liblua5.3-0 libperl5.32 libsodium23 mailcap mime-support perl
  perl-modules-5.32 php-common php8.1 php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
0 upgraded, 26 newly installed, 0 to remove and 0 not upgraded.
Need to get 14.3 MB of archives.
After this operation, 77.2 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Lets check the version of installed PHP now.

<pre class="wp-block-code"><code>$ php --version
&lt;strong>PHP 8.1.2 (cli)&lt;/strong> (built: Jan 27 2022 12:22:31) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.2, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.2, Copyright (c), by Zend Technologies</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install PHP 8.1 on Debian 11, now you can proceed to write your programs using this awesome language. Consult [PHP documentation][6] in case you experience any difficulty. I hope you have learned something new today.

 [1]: https://www.php.net/
 [2]: https://www.php.net/manual/en/function.fsync.php
 [3]: https://www.php.net/manual/en/function.fdatasync.php
 [4]: https://www.php.net/manual/en/function.array-is-list.php
 [5]: https://nextgentips.com/2021/12/03/how-to-install-php-8-1-on-ubuntu-21-04/
 [6]: https://www.php.net/docs.php