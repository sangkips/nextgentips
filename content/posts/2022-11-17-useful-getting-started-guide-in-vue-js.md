---
title: Useful getting started guide in Vue js
author: Kipkoech Sang
type: post
date: 2022-11-17T13:30:06+00:00
url: /2022/11/17/useful-getting-started-guide-in-vue-js/
categories:
  - Programming

---
Vue js is a Javascript framework for building user interfaces. It builds on top of standard HTML, CSS, and Javascript, so it is better to know them before diving into Vue js.

## What you need to know {.wp-block-heading}

### Declarative rendering {.wp-block-heading}

Declarative rendering is using the template syntax that extends the HTML. You describe how an HTML should look based on the Javascript state. For example.

<pre class="wp-block-code"><code>&lt;script>
  export default {
    data() {
     return {
       message: "Hello from Vue js"
   }
 }
}
&lt;/script>

&lt;template>
&lt;h1>{{message}}&lt;/h1>
&lt;/template></code></pre>

From the above, it will bring `<mark style="background-color:#abb8c3" class="has-inline-color">Hello from Vue js</mark>`, its because the `<mark style="background-color:#abb8c3" class="has-inline-color">message</mark>` property is made available from the template`.`

### Binding in Vue js {.wp-block-heading}

Data binding in Vue means that the place where Vue is used or displayed in the template is directly linked or bound to the source of the data which is the data object inside the Vue instance.

We use `<mark style="background-color:#abb8c3" class="has-inline-color">v-bind</mark>` to bind n attribute to dynamic value. Example 

<pre class="wp-block-code"><code>&lt;h1 v-bind:id="dynamicId">&lt;/h1></code></pre>

You can also use the shorthand version because we use this frequently.

<pre class="wp-block-code"><code>&lt;h1 :id="dynamicId">&lt;/h1></code></pre>

Example in code 

<pre class="wp-block-code"><code>&lt;script>
export default {
  data() {
    return {
      myMessage: 'auto'
    }
  }
}
&lt;/script>

&lt;template>
  &lt;h1 :class="myMessage">Auto garage&lt;/h1>
&lt;/template>

&lt;style>
.auto {
  color: green;
}
&lt;/style></code></pre>

The output from the above is `Auto <mark style="background-color:#abb8c3" class="has-inline-color">garage with green letters</mark>`

### Vue Event Listeners {.wp-block-heading}

To listen to dom events in Vue, we use `<mark style="background-color:#abb8c3" class="has-inline-color">v-on</mark>`, event listener is a procedure that waits for an event to occur before being triggered.

<pre class="wp-block-code"><code>&lt;button v-on:click="increment">{{number}}&lt;/button></code></pre>

Also, you can do a shorthand version of this with the following:

<pre class="wp-block-code"><code>&lt;button @click="increment">{{number}}&lt;/button></code></pre>

To access a component inside a method function, use the `<mark style="background-color:#abb8c3" class="has-inline-color">this</mark>` keyword.

### Working with Forms {.wp-block-heading}

Use `<mark style="background-color:#abb8c3" class="has-inline-color">v-bind</mark>` and `<mark style="background-color:#abb8c3" class="has-inline-color">v-on</mark>` together to create a two-way binding on form input. 

<pre class="wp-block-code"><code>&lt;input :value="text"@input="onInput"></code></pre>

Sometimes the above becomes too much work to write. To simplify we use the `<mark style="background-color:#abb8c3" class="has-inline-color">v-model</mark>`.

<pre class="wp-block-code"><code>&lt;input v-model="text"></code></pre>

### Conditional Inheritance  {.wp-block-heading}

When we want to write conditions in Vue we us `<mark style="background-color:#abb8c3" class="has-inline-color">v-if</mark>` to conditionally render an element. You only render when the condition is true, when it&#8217;s false the value is removed from the dom.

<pre class="wp-block-code"><code>&lt;h1 v-if="like">I like playing&lt;/h1></code></pre>

For an else and else if statement we use `v-else` and `<mark style="background-color:#abb8c3" class="has-inline-color">v-else-if</mark>` respectively.

### List Rendering {.wp-block-heading}

To list items in an array, use `<mark style="background-color:#abb8c3" class="has-inline-color">v-for</mark>` like the example below.

<pre class="wp-block-code"><code>&lt;li v-for="todo in todos":key="todo.id">{{todo.message}}&lt;/li></code></pre>

If you want to add a new item to the array use `<mark style="background-color:#abb8c3" class="has-inline-color">push</mark>` keyword, for example `<mark style="background-color:#abb8c3" class="has-inline-color">this.todos.push(<array>)</mark>`