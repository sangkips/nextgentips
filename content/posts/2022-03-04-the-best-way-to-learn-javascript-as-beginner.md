---
title: The best way to learn JavaScript as beginner.
author: Kipkoech Sang
type: post
date: 2022-03-04T19:02:32+00:00
url: /2022/03/04/the-best-way-to-learn-javascript-as-beginner/
rank_math_primary_category:
  - 36
rank_math_description:
  - JavaScript is the most popular programming of the web alongside HTNL and CSS. Its the most widely used programming language on the web.
rank_math_focus_keyword:
  - best way to learn javascript
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 1
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial we are going to learn the best way to learn JavaScript as a beginner. <a href="https://www.w3schools.com/js/" target="_blank" rel="noreferrer noopener">JavaScript</a> is the most popular programming of the web alongside HTML and CSS.

Have you wandered how you can start programming as a beginner? Programming is not the best medicine for most IT forks. As a technology enthusiast like me, I will choose other things other than programming. But for a programmer it comes with many benefits, working from home for example, flexible time, more benefits like pay. There is more to programming and the best way to start as a beginner is to start a frontend web developer.

For you to begin as a frontend web developer you have to learn HTML, CSS and most importantly JavaScript. Later you can transitioned to backend developer using Node, Python, Ruby on rails and or Golang programming among others.

## The best way to learn JavaScript {.wp-block-heading}

Here is the best road map to learning JavaScript.

  1. **Have a willing mindset**. For one to succeed and be on the game of programming, have a willing mindset to learn. Start from somewhere at the bottom and work your way up. Within no time you start grasping the content. Programming is all about thinking, if you can figure out a problem put down in code and you will be surprise with the solution you will get.
  2. **Learn enough basics**. Learning any programming language requires one to build a solid foundation. For JavaScript before diving in, make sure you have learn enough HTML and CSS before moving into JavaScript. You cant build a website without the 3 parts. You start with the HTML which gives you the interface, move to CSS for styling and then JavaScript for making your website responsive. HTML will not be that much if I can say, but CSS is the overwhelming part. You need to know way more things in order to style your website. Have you seen a very boring website which doesn&#8217;t look appealing? This will drive away your potential clients. Nobody wants a pure HTML website, clients want something responsive, something that attracts your eye. 
  3. When you have mastered enough HTML and CSS move ahead and jump right in to JavaScript. Here you will also have to learn from the basics. **Learn the building blocks of JavaScript** like declaration of variables, arrays, working with numbers, strings, how to declare a function, what is an object and when you are done with the basics then move to practice.
  4. **Write more code.** The best part of any programming is to build muscle memory and to do that is to start writing code and more code, this way your memory will start storing things easily. Take it like a small child who is starting to walk, you will practice and practice until you get it. In programming always make Google as your friend. You will find turn of information as you move forward.
  5. **Build more projects.** In any programming the best way to know if you have learn something is to build a program, this is the same to JavaScript. Build many programs, this way you will learn more. My advice is for you to not only build responsive sites but also include something with a backend too, this way you will be more marketable, this is called being a full sack developer.
  6. **Contribute to open-source projects**. Any budding programmers love to contribute to open source communities. This way you will teach and outsource your knowledge. You will be in a position to contribute, this way you will build not only a community but also your potential employer might be in the community.

## HTML tips {.wp-block-heading}

What I have learned alot from programming is to know the tips and tricks in order to help you avoid taking much time. For HTML the best thing to know is to have some small tricks to help you avoid writing too much and repetitive code. As a begginer, its best to learn the hard way first but if you are like me I prefer some precise shortcuts.

HTML things to master.

  * **How to create your first HTML skeleton**. The best text editor to use here is visual studio code. When you opened your Vscode as it is called, first create a project and create mostly one index.html, style.css and main.js file. This are the basics but as the project grows you can add more. To start working on html hit on **shift 1 and press tab, this way it will prefill your editor with some sk**eleton html files. Use it to your advantage.
  * **Create a navbar**. To create a navbar type **nav.navbar**. It will create something like this: 

<pre class="wp-block-code"><code>&lt;nav class="navbar"&gt;&lt;/nav&gt;</code></pre>

  * Create a div container. To create a div container type **navbar__container**(double underscore). You will get this:

<pre class="wp-block-code"><code>&lt;div class="navbar__container"&gt;&lt;/div&gt;</code></pre>

  * Create href links with id type **a#navbar__logo** for example when you want to create a link for the logo. 

<pre class="wp-block-code"><code>&lt;a href="#logo" id="navbar__logo"&gt;Code&lt;/a&gt;</code></pre>

  * To create buttons, **a.button#signup** if you want to create a signup button.

<pre class="wp-block-code"><code>&lt;a href="#signup" class="button" id="signup"&gt;Sign Up&lt;/a&gt;</code></pre>

  * If you want a navbar menu, **ul.navbar__menu**

<pre class="wp-block-code"><code>&lt;ul class="navbar__menu"&gt;&lt;/ul&gt;</code></pre>

  * And for list items **li.navbar__item**
  * 

<pre class="wp-block-code"><code>&lt;li class="navbar__item"&gt;&lt;/li&gt;</code></pre>