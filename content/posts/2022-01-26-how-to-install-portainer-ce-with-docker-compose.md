---
title: How to install Portainer CE with Docker-Compose
author: Kipkoech Sang
type: post
date: 2022-01-26T12:33:37+00:00
url: /2022/01/26/how-to-install-portainer-ce-with-docker-compose/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 21
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Portainer CE with Docker-compose.

Portainer is a free and open-source lightweight service delivery platform for containerized applications that can be used to manage Docker, Kubernetes, Docker swarm, etc. The application is simple to deploy and use. The application allows you to manage all your container services via smart GUIs or an extensive API, this makes the developers’ work easier.

Portainer gives developers a chance to deploy, manage, and troubleshoot containerized applications without needing to deeply have experience with Kubernetes. This is awesome in my view.

In this tutorial we are going to learn how to install Portainer inside a docker container, also we will learn the **uses of Portainer**, and what are **Portainer agents**. Also, we need to understand **Portainer ports** i.e which ports do Portainer uses to communicate with the world. So let’s dive in

We have two editions of Portainer, the Portainer community edition which is free to use, and the Portainer Business Edition which requires one to purchase the license fee to use and has more features compared to the community edition.

### Why do you need Portainer? {#why-you-need-portainer.wp-block-heading}

The reason why we need to use Portainer in our case is because of the following reasons:

  * Portainers streamline the operations of container management so that enterprises can focus on other pressing needs.
  * Portainer removes the complexity associated with deploying and managing containers
  * Portainer is the best tool to offer security to every enterprise giving good governance to any institution.
  * Portainer works well with Docker, Kubernetes, and Docker swarm, meaning you have a ton of applications to deploy with in case you are not well familiar with one, you can switch to another.
  * Portainer can manage networks for you within the GUI, OS your work is to add, remove, or edit network endpoints configurations with ease.
  * Portainers easily manages containers for you easily.

### What are Portainer Agents? {#what-are-portainer-agents.wp-block-heading}

Portainer has two elements, the Portainer server, and the Portainer agent. Think of a Portainer agent as the helper, it gets the information on behalf of the server, then goes ahead to negotiate how that info will be conveyed to the intended recipient. This is also how a Portainer agent works, Portainer agents are deployed to each node in your cluster and configured to report back to the Portainer server container.

A single Portainer server can accept connections from many Portainer agents.

### Portainer edge agent {#portainer-edge-agent.wp-block-heading}

Think of a situation where you are working outside of your organization network, here the Portainer agents cant communicate with the Portainer server as intended because you are on a separate network. In this situation Poratiner edge agents come in, the remote environment only needs to access the Portainer server rather than the Portainer server needing access to the remote environment. This communication is managed by the Portainer edge agent.

### Portainer ports {#portainer-ports.wp-block-heading}

Every application which needs to communicate with the outside world requires some ports to be open so that it allows full access to the website resources. This is the same case with the Portainer, it requires certain ports for communication.

Portainer Server listens on port 9443 for UI and API and exposes a TCP tunnel server on port 8000.

Portainer agents listen on port 9001.

## Prerequisites {#prerequisites.wp-block-heading}

  * You need to have Docker version 20 + for you to run Portainer 2 and above.
  * You need to have Docker-compose 
  * Have basic knowledge of the command line
  * 20 + GB disk space
  * Have sudo access

### Install Portainer CE with Docker-Compose {#install-portainer-ce-with-docker-compose.wp-block-heading}

We are going to install the Portainer community edition because it’s free. I am also going to run docker on Ubuntu 20.04

### 1. Run system updates {#1-run-system-updates.wp-block-heading}

To start off, we need to run system updates in order to make our repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade
# you can reboot your system afterwards</code></pre>

### 2. Install Docker on Ubuntu 20.04 {#2-install-docker-on-ubuntu-20-04.wp-block-heading}

We need to first have Docker up and running before we can install Portainer. Check out this article on&nbsp;[how to install Docker on Ubuntu 20.04][1]. Definitely, you can use other distributions as you like.

So to install follow these steps

Remove installed docker if you had installed another version

<pre class="wp-block-code"><code>$ sudo apt remove docker </code></pre>

Then install Docker

<pre class="wp-block-code"><code>$ sudo apt install docker.io -y</code></pre>

You can check the version of the installed Docker with:

<pre class="wp-block-code"><code>$ docker --version
Docker version 20.10.7, build 20.10.7-0ubuntu5~20.04.2</code></pre>

We have successfully installed Docker version 20.10.7 suitable for Portainer installation

You need to enable and start the docker

<pre class="wp-block-code"><code># enable docker 
$ sudo systemctl enable docker 
# start docker 
$ sudo systemctl start docker 
# check status 
$ sudo systemctl status docker </code></pre>

<pre class="wp-block-code"><code># docker start output
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-01-26 11:56:42 UTC; 1min 23s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 9157 (dockerd)
      Tasks: 9
     Memory: 41.2M
     CGroup: /system.slice/docker.service
             └─9157 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock</code></pre>

### 3. Install Docker-Compose  {#3-install-docker-compose.wp-block-heading}

Let&#8217;s run the curl command in order to download compose into our system.

<pre class="wp-block-code"><code>$ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose</code></pre>

This will download compose 1.29, to install a different version change 1.29 to whatever you like.

Make it executable with the following command:

<pre class="wp-block-code"><code>$ sudo chmod +x /usr/local/bin/docker-compose</code></pre>

Check the docker-compose version.

<pre class="wp-block-code"><code>$ docker-compose --version</code></pre>

### 4. Install Portainer with Docker-compose {#4-install-portainer-with-docker-compose.wp-block-heading}

We will install Portainer with Docker using docker-compose 

<pre class="wp-block-code"><code>$ sudo mkdir -p portainer 
$ cd portainer </code></pre>

Use your favorite text editor to add the following to the docker-compose.yml file

<pre class="wp-block-code"><code>sudo vi docker-compose.yml</code></pre>

<pre class="wp-block-code"><code># docker-compose.yml 
version: '3.9'

services:
  portainer:
    image: portainer/portainer-ce:latest
    container_name: portainer
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./portainer-data:/data
    ports:
      - 9000:9000</code></pre>

Power up the container with the **docker-compose up** command 

<pre class="wp-block-code"><code>$ docker-compose up -d</code></pre>

You will see the following output.

<pre class="wp-block-code"><code>#output
Creating network "portainer_default" with the default driver
Pulling portainer (portainer/portainer-ce:latest)...
latest: Pulling from portainer/portainer-ce
0ea73420e2bb: Pull complete
c367f59be2e1: Pull complete
b71b88d796e2: Pull complete
Digest: sha256:4f126c5114b63e9d1bceb4b368944d14323329a9a0d4e7bb7eb53c9b7435d498
Status: Downloaded newer image for portainer/portainer-ce:latest
Creating portainer ... done</code></pre>

Go to your favorite browser and open http://<ip_address>:9000

<pre class="wp-block-code"><code>$ http://&lt;ip_address&gt;:9000</code></pre>

## Conclusion {#conclusion.wp-block-heading}

Congratulations, you have successfully installed Portainer with docker. If you got any issues, always consult the&nbsp;[Portainer documentation][2].

 [1]: https://nextgentips.com/2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/
 [2]: https://docs.portainer.io/v/ce-2.11/start/install/server/docker/linux