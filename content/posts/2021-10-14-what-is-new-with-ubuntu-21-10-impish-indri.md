---
title: What is new with Ubuntu 21.10 “Impish Indri”
author: Kipkoech Sang
type: post
date: 2021-10-14T18:58:11+00:00
url: /2021/10/14/what-is-new-with-ubuntu-21-10-impish-indri/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 136
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:28:"91.230.41.203, 99.82.170.160";}'
rank_math_internal_links_processed:
  - 1
categories:
  - News

---
The code name Impish Indri was derived from two different words..Impish means to do slightly naughty things for fun while Indri is a lemur native to Madagascar that spends the majority of its time up off the ground and in the trees.

Ubuntu 21.10 will only be supported for 9 months so if you need a long term support you might consider using Ubuntu 20.04.

## Ubuntu 21.10 new features  {.wp-block-heading}

### Ubuntu 21.10 Desktop {.wp-block-heading}

  * [Wayland sessions][1] are now available while using Nvidia proprietary driver.
  * [Pulse audio 15][2] introduces support for Bluetooth LDAC and APTx codecs as well as HFP Bluetooth profiles providing better audio quality.
  * The recovery key feature at installation time has been improved. The recovery key is now optional, stronger and editable.

### GNOME {.wp-block-heading}

  * Ubuntu 21.10 uses [GNOME 40][3] as its default desktop experience. Workspaces are now arranged horizontally, and the overview and app grid are accessed vertically. Each direction has accompanying keyboard shortcuts, touchpad gestures and mouse actions.
  * New multitouch gestures are included that make it easier to enter/exit the workspace switcher, and are only available in the Wayland session by default.

## Linux Kernel {.wp-block-heading}

[Linux 5.13][4] introduces support for new hardware such as:

  * Future Intel and AMD chips, such as Intel Alderlake S or AMD Adebaran.
  * Rudimentary Apple M1 support.
  * [Microsoft Surface Laptops and tablets&nbsp;][5]

### Toolchain Upgrades {.wp-block-heading}

  * [GCC][6] was updated to the 11.2.0 release, [binutils][7] to 2.37, and [glibc][8] to 2.34. [LLVM][9] now defaults to version 13. golang defaults to version 1.17.x. rustc defaults to version 1.51.
  * [OpenJDK 18][10] is now available (but not used for package builds).

### Security Improvements {.wp-block-heading}

[nftables][11] is now the default backend for the firewall.

### Base System {.wp-block-heading}

systemd is being switched to the “unified” cgroup hierarchy (cgroup v2) by default

### Updated Applications {.wp-block-heading}

  * Firefox version 93 is now&nbsp;[seeded as a snap by default][12], instead of a deb package.
  * LibreOffice defaults to version 7.2.1
  * Thunderbird defaults to version 91.1.2

### Ubuntu 21.10 Server {.wp-block-heading}

  * OpenLDAP has been updated to 2.5.6
  * Telegraf has been updated to 1.19.2
  * PHP now defaults to version 8.0.8
  * Apache has been updated to 2.4.48
  * QEMU was updated to the 6.0 release.
  * Libvirt has been updated to version 7.6
  * Open vSwitch has been updated to 2.16
  * Chrony has been updated to version 4.1
  * Bind9 has been updated to 9.16.15
  * Containerd has been updated to version 1.5.5
  * Runc has been updated to version 1.0.1
  * Corosync has been updated to version 3.1.2
  * Fence-agents has been split into curated and non-curated agents
  * Resource-agents has been split into curated and non-curated agents

## Openstack {.wp-block-heading}

Ubuntu 21.10 includes the latest OpenStack release, [Xena][13]

If you want to read more refer to Ubuntu 21.10 [documention][14].

 [1]: https://wiki.archlinux.org/title/wayland
 [2]: https://www.freedesktop.org/wiki/Software/PulseAudio/Notes/15.0/
 [3]: https://forty.gnome.org/
 [4]: https://kernelnewbies.org/Linux_5.13
 [5]: https://github.com/linux-surface/linux-surface/wiki/Supported-Devices-and-Features#surface-laptops
 [6]: https://gcc.gnu.org/
 [7]: https://www.gnu.org/software/binutils/
 [8]: https://www.gnu.org/software/libc/libc.html
 [9]: https://llvm.org/
 [10]: https://openjdk.java.net/projects/jdk/18/
 [11]: https://wiki.nftables.org/wiki-nftables/index.php/What_is_nftables%3F
 [12]: https://discourse.ubuntu.com/t/feature-freeze-exception-seeding-the-official-firefox-snap-in-ubuntu-desktop/24210
 [13]: https://www.openstack.org/software/xena/
 [14]: https://discourse.ubuntu.com/t/impish-indri-release-notes/21951