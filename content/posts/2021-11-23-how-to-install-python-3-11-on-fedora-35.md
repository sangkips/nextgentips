---
title: How to install Python 3.11 on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-23T18:23:06+00:00
url: /2021/11/23/how-to-install-python-3-11-on-fedora-35/
mtm_data:
  - 'a:1:{i:0;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}}'
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 87
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Python 3.11 on Fedora 35.

Python programming language is an interpreted high-level general-purpose programming language. Its design philosophy emphasizes code readability with its use of significant code indentation.

We can begin by installing a python 3 interpreter on our system. This is a program that reads Python programs and carries out their instructions, you need it before you can begin python programming.

## Python 3.11 new Features {.wp-block-heading}

  * Introduction of&nbsp;**enhanced error locations in tracebacks**. Whenever you are printing tracebacks, the intepreter will now point to the exact expressions that caused the error instead of just the line.&nbsp;**Traceback**&nbsp;is a report containing the function calls made in your code at a specific point. When your program results in exception, Python will print the current traceback to help you know what went wrong. In Python it is best to read traceback from bottom up. This is very helpful if since the traceback is printed out and your terminal usually ends up at the bottom of the output, giving you the perfect place to start reading the traceback.
  * The introduction of column information for code objects. The information used by enhanced traceback feature is made available as a general API taht can be used to corelate byte code inmstaructions with source code. code objects are a low level detail of the CPython implementation.
  * Asynchronous comprehensions are now allowed inside comprehensions in asynchronous functions. Outer comprehensions implicitly become asynchronous.
  * Introduction of support for PEP 515-style initialization of fraction from string.
  * Addition of math.cbrt(): which return the cube root of x.
  * A new function&nbsp;**operator.call&nbsp;**has been added.
  * For Sqlite3, you can now disable the autorizer by passing None to set_authorizer().
  * Collation name create_collation() can now contain a unicode character. Collation names with invalid characters can now raise UnicodeEncodeError instead of sqlite3.programmingerror.
  * On Unix, time.sleep() now uses the clock_nanosleep() or nanosleep() function if available.
  * The Unicode database have been updated to version 14.0.0
  * Method calls with keywords are now faster due to bytecode changes which avoid creating bound method instances.

## Related Articles  {.wp-block-heading}

  * [How to install Python 3.11 on Ubuntu 20.04][1]
  * [Python 3.11 new and deprecated Features][2]
  * [How to install Python 3.9 interpreter on Ubuntu 20.04][3]

## Prerequisites  {.wp-block-heading}

  * You need to have Fedora 35 up and running 
  * Have a user with sudo privileges if you are not root 
  * Have basic knowledge of command line 

## Table of Contents  {.wp-block-heading}

  1. Run system wide updates
  2. Install Python 3.11 with DNF
  3. Enable updates-testing repository

## 1. Run system updates {.wp-block-heading}

Run system updates to make the system up to date. This will make system repositories up to date. Run the following command on your terminal;

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

After the updates are complete then the next step is to install Python 3.11.

## 2. Install Python 3.11 on Fedora {.wp-block-heading}

In Fedora, you can easily installed Python 3.11 from the official software repository using dnf. Use the following command to install Python 3.11

<pre class="wp-block-code"><code>$ sudo dnf install python3.11</code></pre>

Instalation will begin and you will see the following as output.

<pre class="wp-block-code"><code>Sample output
DigitalOcean Droplet Agent                                                        46 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                         Architecture        Version                          Repository            Size
=================================================================================================================
Installing:
 &lt;strong>python3.11                      x86_64              3.11.0~a1-1.fc35 &lt;/strong>                updates               24 M
Installing dependencies:
 fontconfig                      x86_64              2.13.94-3.fc35                   fedora               272 k
 libX11                          x86_64              1.7.2-3.fc35                     fedora               645 k
 libX11-common                   noarch              1.7.2-3.fc35                     fedora               152 k
 libXau                          x86_64              1.0.9-7.fc35                     fedora                30 k
 libXft                          x86_64              2.3.3-7.fc35                     fedora                61 k
 libXrender                      x86_64              0.9.10-15.fc35                   fedora                27 k
 libpkgconf                      x86_64              1.8.0-1.fc35                     fedora                36 k
 libxcb                          x86_64              1.13.1-8.fc35                    fedora               223 k
 pkgconf                         x86_64              1.8.0-1.fc35                     fedora                41 k
 pkgconf-m4                      noarch              1.8.0-1.fc35                     fedora                14 k
 pkgconf-pkg-config              x86_64              1.8.0-1.fc35                     fedora                10 k
 tcl                             x86_64              1:8.6.10-5.fc34                  fedora               1.1 M
 tk                              x86_64              1:8.6.10-7.fc35                  fedora               1.6 M
 xml-common                      noarch              0.6.3-57.fc35                    fedora                31 k

Transaction Summary
=================================================================================================================
Install  15 Packages

Total download size: 28 M
Installed size: 125 M
Is this ok &#91;y/N]: y</code></pre>

Press Y to allow installation to complete. You can now check the version of Python with the following command;

<pre class="wp-block-code"><code>$ python3.11 --version
Python 3.11.0a1</code></pre>

## 3. Enable updates-testing repository {.wp-block-heading}

Sometimes you might need to enable updates-testing repository to get the very latest pre-releases. Use the following command iy you face any challenges with installing latest pre-release version.

<pre class="wp-block-code"><code>$ sudo dnf install --enablerepo=updates-testing python3.11</code></pre>

## 4. Run Python 3.11 test {.wp-block-heading}

In order to know that Python installed is really working, we need open Python 3.11 interpretor and start testing;

<pre class="wp-block-code"><code>$ python3.11 
Python 3.11.0a1 (default, Oct  6 2021, 00:00:00) &#91;GCC 11.2.1 20210728 (Red Hat 11.2.1-1)] on linux
Type "help", "copyright", "credits" or "license" for more information.
&gt;&gt;&gt; </code></pre>

<pre class="wp-block-code"><code>Sample Python3.11 operations from terminal
Python 3.11.0a1 (default, Oct  6 2021, 00:00:00) &#91;GCC 11.2.1 20210728 (Red Hat 11.2.1-1)] on linux
Type "help", "copyright", "credits" or "license" for more information.
&gt;&gt;&gt; 3+2
5
&gt;&gt;&gt; print(my name)
  File "&lt;stdin&gt;", line 1
    &lt;strong>print(my name)
          ^^^^^^^&lt;/strong>
SyntaxError: invalid syntax. Perhaps you forgot a comma?
&gt;&gt;&gt; 69-10
59
&gt;&gt;&gt; name ='john'
&gt;&gt;&gt; print(name)
john</code></pre>

The highlighted print(my name) shows enhanced error locations in traceback I was talking about from the beggining of this article.

## Conclusion {.wp-block-heading}

We have successfully installed Python 3.11 on our Fedora workstation. Explore more from the [Python documentation][4].

 [1]: https://nextgentips.com/2021/11/23/how-to-install-python-3-11-on-ubuntu-20-04/
 [2]: https://nextgentips.com/2021/11/23/python-3-11-new-and-deprecated-features/
 [3]: https://nextgentips.com/2021/10/04/install-python-3-on-ubuntu-20-04/
 [4]: https://docs.python.org/3.11/whatsnew/3.11.html