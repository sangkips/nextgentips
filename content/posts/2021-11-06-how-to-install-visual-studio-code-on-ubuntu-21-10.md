---
title: How to install Visual Studio Code on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-11-06T12:57:59+00:00
url: /2021/11/06/how-to-install-visual-studio-code-on-ubuntu-21-10/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 109
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this article we are going to learn how to install Visual Studio code also know as (VScode) on Ubuntu 21.10. Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built in Javascript, Node.js and Typescript. One can do programming for almost languages with ease with Visual Studio Code. The languages supported are like, Go, PHP, C++, C#, Java, Python and also .NET.

## Why do we like Visual Studio Code? {.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Related Content {.wp-block-heading}

  * [How to install Visual Studio Code on Ubuntu 21.04][1]

## Prerequisites {.wp-block-heading}

  * Ubuntu 21.10 workstation
  * Command line basics 
  * Internet access

## Table of Contents  {.wp-block-heading}

  * Update the system 
  * Install Vscode via snap
  * Launch Vscode 

## Install Visual Studio code {.wp-block-heading}

VScode can be installed either from the snap store or downloaded directly from source.

### Install Visual Studio Code via Snap. {.wp-block-heading}

Snap packages can be installed directly from either the command line or through Ubuntu software repository.

Let us first update our system repository with the following command:

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the update and upgrade is complete, we can install VScode classic into our system.

<pre class="wp-block-code"><code>$ sudo snap install --classic code </code></pre>

<pre class="wp-block-code"><code>Output 
# sudo snap install --classic code 
code 6cba118a from Visual Studio Code (vscode✓) installed</code></pre>

We can start Visual Studio code with the following command:

<pre class="wp-block-code"><code>$ code </code></pre>

## Conclusion {.wp-block-heading}

We have installed Visual Studio Code in our Ubuntu 21.10 workstation. Go ahead and configure to suite your programming needs. Consult [Visual studio code documentation][2] if you face any problem.

 [1]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
 [2]: https://code.visualstudio.com/