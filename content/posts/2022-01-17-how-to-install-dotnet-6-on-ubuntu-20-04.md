---
title: How to install Dotnet 6 on ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-17T11:19:39+00:00
url: /2022/01/17/how-to-install-dotnet-6-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 30
rank_math_internal_links_processed:
  - 1
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:28:"5.255.231.204, 99.82.162.201";}'
categories:
  - Programming

---
In this tutorial, we are going to learn how to **install dotnet 6 on Ubuntu 20.04**. Also in this tutorial, I will tell you about the **new features of dotnet 6**, **what is dotnet 6**. Let&#8217;s jump right in.

Dotnet is a free cross-platform and open-source developer platform for building web, mobile, microservices, cloud, and many other applications. You can write your dotnet applications with C#, F#, and visual basic. 

## What dotnet 6 can do {.wp-block-heading}

Dotnet 6 improve on the features introduced during the dotnet 5 phase. 

  * Simplified development by reducuing the codebase on C# making it easy to read and analyze any code written in C#, F#.
  * Improved performance by lowering compute costs on the cloud.
  * improved productivity with the introduction of new git tooling, intelligent code editing, provision of robust diagnostics and testing tools.

## New fatures in dotnet 6 {.wp-block-heading}

Notable features introduced include the following:

  * **System.io.filestream** have been rewritten to provide better performance and reliability on windows platforms 
  * Introduction of **profile-guided optimization** where the JIT compiler generates optimized codes
  * Introduction of **crossgen2** which provide head of time compilation (AOT) to improve the startup time of the app.
  * Introduction of Arm64 support 
  * Introduction of hot reload that is a feature that lets you modify your apps&#8217; source code and instantly apply those changes to your running application.
  * It adds a new source generator for system.text.json 
  * Introduction of a new document object model (DOM) which supplement pre-existing readonly dom 

Read about many other new features in detail [here][1].

## Prerequisites  {.wp-block-heading}

Before installing dotnet make sure you have the following:

  * Ubuntu 20.04 
  * Sudo or root privileges 
  * Basic command line terminal experience 

## Install dotnet 6 on Ubuntu 20.04 {.wp-block-heading}

### 1. Run system updates  {.wp-block-heading}

Before running any installation in any Linux distribution make sure you run a system update in order to make the system repositories up to date. Use the following command

<pre class="wp-block-code"><code># &lt;strong>update&lt;/strong>
$ sudo apt update
# &lt;strong>upgrade&lt;/strong>
$ sudo apt upgrade -y</code></pre>

### 2. Add Microsoft package signing key {.wp-block-heading}

Download Microsoft key and install it into the list of trusted keys with the following command.

<pre class="wp-block-code"><code>$ wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb</code></pre>

You will get an output like this 

<pre class="wp-block-code"><code>output
--2022-01-17 10:43:37--  https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb
Resolving packages.microsoft.com (packages.microsoft.com)... 13.81.215.193
Connecting to packages.microsoft.com (packages.microsoft.com)|13.81.215.193|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3124 (3.1K) &#91;application/octet-stream]
Saving to: ‘packages-microsoft-prod.deb’

packages-microsoft-prod.deb  100%&#91;===========================================>]   3.05K  --.-KB/s    in 0s      

2022-01-17 10:43:37 (327 MB/s) - ‘packages-microsoft-prod.deb’ saved &#91;3124/3124]</code></pre>

After the download is complete, then you need to install with the following command;

<pre class="wp-block-code"><code>$ sudo dpkg -i packages-microsoft-prod.deb</code></pre>

The output will look like this

<pre class="wp-block-code"><code>Output
Selecting previously unselected package packages-microsoft-prod.
(Reading database ... 94650 files and directories currently installed.)
Preparing to unpack packages-microsoft-prod.deb ...
Unpacking packages-microsoft-prod (1.0-ubuntu20.04.1) ...
Setting up packages-microsoft-prod (1.0-ubuntu20.04.1) ...</code></pre>

Before you can install SDK make sure to update your system again

<pre class="wp-block-code"><code>$ sudo apt update
Hit:2 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                    
Hit:3 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease                                            
Hit:4 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease                                          
&lt;strong>Get:5 https://packages.microsoft.com/ubuntu/20.04/prod focal InRelease &#91;10.5 kB]&lt;/strong>                      
Hit:6 http://security.ubuntu.com/ubuntu focal-security InRelease                
Get:7 https://packages.microsoft.com/ubuntu/20.04/prod focal/main amd64 Packages &#91;122 kB]</code></pre>

### 3. Install dotnet 6 SDK {.wp-block-heading}

SDK allows you to develop apps with the .Net platform, so it&#8217;s required that be installed before running the installation of .Net 6. So to install SDK run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install -y dotnet-sdk-6.0</code></pre>

This will be the sample output

<pre class="wp-block-code"><code>Output
Welcome to .NET!
---------------------
Learn more about .NET: https://aka.ms/dotnet-docs
Use 'dotnet --help' to see available commands or visit: https://aka.ms/dotnet-cli-docs

Telemetry
---------
The .NET tools collect usage data in order to help us improve your experience. It is collected by Microsoft and shared with the community. You can opt-out of telemetry by setting the DOTNET_CLI_TELEMETRY_OPTOUT environment variable to '1' or 'true' using your favorite shell.

Read more about .NET CLI Tools telemetry: https://aka.ms/dotnet-cli-telemetry

Configuring...
--------------
A command is running to populate your local package cache to improve restore speed and enable offline access. This command takes up to one minute to complete and only runs once.
Processing triggers for man-db (2.9.1-1) ...</code></pre>

### 4. Install dotnet 6 runtime  {.wp-block-heading}

We are going to install ASP.core runtime. Runtime allows you to run apps that were developed using the .NET framework. 

Alternatively, you can install dotnet-runtime. We do this to make all the apps developed using .NET frameworks become compatible because this is always a pain when it comes to Microsoft products.

**Install ASP.Core runtime** **environment** 

<pre class="wp-block-code"><code>$ sudo apt install -y aspnetcore-runtime-6.0</code></pre>

I can see that there is no need to install this because SDK install with it in the process. Check out this output

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
aspnetcore-runtime-6.0 is already the newest version (6.0.1-1).
aspnetcore-runtime-6.0 set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

**Install dotnet runtime environment** 

To install dotnet-runtime run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install -y dotnet-runtime-6.0</code></pre>

Lets check the version of dotnet installed with this command.

<pre class="wp-block-code"><code>$ dotnet --version
6.0.101</code></pre>

## Conclusion {.wp-block-heading}

Congratulations, you have successfully installed the dotnet framework on Ubuntu 20.04. Go ahead and begin your coding challenges.

 [1]: https://docs.microsoft.com/en-us/dotnet/core/whats-new/dotnet-6 "here"