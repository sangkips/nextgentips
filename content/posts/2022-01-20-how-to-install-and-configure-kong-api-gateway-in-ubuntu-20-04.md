---
title: How to install and Configure Kong API Gateway in Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-20T08:06:27+00:00
url: /2022/01/20/how-to-install-and-configure-kong-api-gateway-in-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 29
rank_math_internal_links_processed:
  - 1
post_view:
  - 1
is_post_view:
  - 'a:1:{i:0;s:29:"110.93.246.157, 13.248.107.41";}'
categories:
  - Linux

---
In this tutorial, we are going to learn how to install and configure Kong API Gateway. Also, we are going to understand **what Kong API gateway is**, **what is an API**?, have a looked at **Kong API gateway pricing**, and lastly, **how Kong API gateway works**. So let&#8217;s dive in. 

## What is Kong API Gateway? {.wp-block-heading}

Kong API Gateway is a lightweight, fast, flexible cloud-native API Gateway. And what is an API Gateway? An API Gateway is a reverse proxy that lets you manage, configure and route requests to your API. Think of an API like traffic lights control, it tells traffic when to pass and in which direction. It cant allow traffic to collide. This is the same thing with an API Gateway.

## How does Kong API Gateway works? {.wp-block-heading}

Kong API Gateway acts as a proxy for application microservices, It exposes public-facing endpoints, routing incoming traffic to their respective destinations, and returns feedback to the sender. Think of it as a link between the sender and the receiver, You receive information to check the destination, send it to where it is required, and when the receiver gets the information, you are the one to conveyed it back to the original sender. That&#8217;s how Kong API Gateway works.

## Kong API Gateway pricing  {.wp-block-heading}

When you are thinking of pricing, it gives you a clue on what to install, if you are an individual who wants to just install and experiment with it then community edition or the free version is the best for you but if you are an organization then Enterprise edition fits your bill because of the many features and dedicated support you get. So if you ask is it free? Yes, it&#8217;s free to the extent there are some features you will not get on the free version.

Enterprise version uses a pay-as-you-go basis where you will pay what you have consumed. Plus edition you pay $ 250 service per month which is fairly good. 

## Prerequisites  {.wp-block-heading}

  * Make sure you have sudo privileges or you are the root user.
  * Have PostgreSQL installed. check this out on [how to install PostgreSQL 14 on Ubuntu 20.04][1]
  * 

## Install the latest version of Kong API on Ubuntu 20.04 {.wp-block-heading}

### 1. Run system update {.wp-block-heading}

The first thing to do is to run system updates in order to make system repositories up to date. This way you will avoid many errors as you go along. 

<pre class="wp-block-code"><code># updates and Upgrades 
$ sudo apt update && apt Upgrade -y
# restart if possible </code></pre>

### 2. Download the Kong Package  {.wp-block-heading}

To install kong we need to install the packages. We are going to use wget command to accomplish our task.

<pre class="wp-block-code"><code>$ wget kong-2.7.0.amd64.deb "https://download.konghq.com/gateway-2.x-ubuntu-$(lsb_release -cs)/pool/all/k/kong/kong_2.7.0_amd64.deb"</code></pre>

You can change kong 2.7.0 to the version you want. At the time of this writing version 2.7.0 is the latest. 

### 3. Install Kong Package {.wp-block-heading}

After the download is over, we will install the package with the following command. In my case, I am installing the Kong OSS version which is the community version.

<pre class="wp-block-code"><code>$ sudo dpkg -i kong_2.7.0_amd64.deb</code></pre>

For my case I am running into an error like this;

<pre class="wp-block-code"><code>(Reading database ... 96790 files and directories currently installed.)
Preparing to unpack kong_2.7.0_amd64.deb ...
Unpacking kong (2.7.0) over (2.7.0) ...
dpkg: dependency problems prevent configuration of kong:
 kong depends on zlib1g-dev; however:
 &lt;strong> Package zlib1g-dev is not installed.&lt;/strong>

dpkg: error processing package kong (--install):
 dependency problems - leaving unconfigured
Errors were encountered while processing:
 kong</code></pre>

For this case I haven&#8217;t fulfilled the kong dependency, so to fix this run this command to install missing dependencies 

<pre class="wp-block-code"><code>$ sudo apt --fix-broken install </code></pre>

This will install zlib1g-dev successfully.

Go back and run **sudo dpkg -i kong\_2.7.0\_amd64.deb**

<pre class="wp-block-code"><code>$ sudo dpkg -i kong_2.7.0_amd64.deb</code></pre>

This will be the output on your terminal.

<pre class="wp-block-code"><code>Output
(Reading database ... 100507 files and directories currently installed.)
Preparing to unpack kong_2.7.0_amd64.deb ...
Unpacking kong (2.7.0) over (2.7.0) ...
Setting up kong (2.7.0) ...</code></pre>

To check if you have successfully installed Kong API Gateway run **kong roar** command

<pre class="wp-block-code"><code>$ kong roar</code></pre>

The output you will get will look like this:

<pre class="wp-block-code"><code>Kong, Monolith destroyer.

     /\  ____
     &lt;&gt; ( oo )
     &lt;&gt;_| ^^ |_
     &lt;&gt;   @    \
    /~~\ . . _ |
   /~~~~\    | |
  /~~~~~~\/ _| |
  |&#91;]&#91;]&#91;]/ / &#91;m]
  |&#91;]&#91;]&#91;&#91;m]
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;]&#91;]&#91;]|
  |&#91;|--|]|
  |&#91;|  |]|
  ========
 ==========
 |&#91;&#91;    ]]|
 ==========</code></pre>

### 4. Install PostgreSQL database {.wp-block-heading}

Here we are installing PostgreSQL because it is the only database that will be supported in the long term. Cassandra is supported currently but it will not be supported in the future versions so better we go with PostgreSQL. 

Run the following command to create a PostgreSQL file repository

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" &gt; /etc/apt/sources.list.d/pgdg.list'</code></pre>

Then we need to import GPG key with the following command 

<pre class="wp-block-code"><code>$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -</code></pre>

We need to run updates for the changes to take effect.

Lastly, install PostgreSQL 13 with the following:

<pre class="wp-block-code"><code>$ sudo apt install postgresql-13 -y </code></pre>

Launch Postgres 

<pre class="wp-block-code"><code>$ sudo -i -u postgres</code></pre>

<pre class="wp-block-code"><code># postgres# psql</code></pre>

**Provision PostgreSQL database**

We need to create the DB\_NAME, DB\_USER, DB_PASSWORD;

<pre class="wp-block-code"><code># CREATE USER kong; CREATE DATABASE kong OWNER kong; ALTER USER kong with password '$passwd';</code></pre>

### 5. Configuring Kong on Ubuntu 20.04 {.wp-block-heading}

Now that we have PostgreSQL in place let&#8217;s now configure Kong.

Kong configuration file is located at **/etc/kong/kong.conf.default**. We need to copy the files and make a copy. To do so do the following 

<pre class="wp-block-code"><code>$ cp /etc/kong/kong.conf.default /etc/kong/kong.conf</code></pre>

Now we have to update the /etc/kong/kong.conf file. Use your favorite text editor

<pre class="wp-block-code"><code>$ sudo nano /etc/kong/kong.conf</code></pre>

Add dbname=kong, dbuser=kong, and dbpass=&#8221;your password&#8221;

Then export the password

<pre class="wp-block-code"><code>$ export KONG_PASSWORD=$passwd</code></pre>

## 6. Start kong API Gateway {.wp-block-heading}

To start kong we are going to start with the following command 

<pre class="wp-block-code"><code>$ kong migrations bootstrap -c /etc/kong/kong.conf</code></pre>

Sample output will look like this

<pre class="wp-block-code"><code>....
migrating oauth2 on database 'kong'...
oauth2 migrated up to: 000_base_oauth2 (executed)
oauth2 migrated up to: 003_130_to_140 (executed)
oauth2 migrated up to: 004_200_to_210 (executed)
oauth2 migrated up to: 005_210_to_211 (executed)
migrating rate-limiting on database 'kong'...
rate-limiting migrated up to: 000_base_rate_limiting (executed)
rate-limiting migrated up to: 003_10_to_112 (executed)
rate-limiting migrated up to: 004_200_to_210 (executed)
migrating response-ratelimiting on database 'kong'...
response-ratelimiting migrated up to: 000_base_response_rate_limiting (executed)
migrating session on database 'kong'...
session migrated up to: 000_base_session (executed)
session migrated up to: 001_add_ttl_index (executed)
42 migrations processed
42 executed
Database is up-to-date</code></pre>

Then use **kong start** command to start kong 

<pre class="wp-block-code"><code>$ kong start -c /etc/kong/kong.conf</code></pre>

Check the status of Kon API Gateway

<pre class="wp-block-code"><code>$ culr localhost:80001/status
{"database":{"reachable":true},"server":{"connections_waiting":0,"connections_handled":2,"total_requests":2,"connections_active":1,"connections_reading":0,"connections_writing":1,"connections_accepted":2},"memory":{"workers_lua_vms":&#91;{"pid":15997,"http_allocated_gc":"39.02 MiB"}],"lua_shared_dicts":{"kong_rate_limiting_counters":{"allocated_slabs":"0.08 MiB","capacity":"12.00 MiB"},"kong_core_db_cache":{"allocated_slabs":"0.76 MiB","capacity":"128.00 MiB"},"kong_core_db_cache_miss":{"allocated_slabs":"0.09 MiB","capacity":"12.00 MiB"},"kong_db_cache":{"allocated_slabs":"0.76 MiB","capacity":"128.00 MiB"},"kong_db_cache_miss":{"allocated_slabs":"0.08 MiB","capacity":"12.00 MiB"},"prometheus_metrics":{"allocated_slabs":"0.04 MiB","capacity":"5.00 MiB"},"kong":{"allocated_slabs":"0.04 MiB","capacity":"5.00 MiB"},"kong_locks":{"allocated_slabs":"0.06 MiB","capacity":"8.00 MiB"},"kong_healthchecks":{"allocated_slabs":"0.04 MiB","capacity":"5.00 MiB"},"kong_process_events":{"allocated_slabs":"0.04 MiB","cap</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Kong API Gateway on our Ubuntu 20.04. Kong API currently worked with PostgreSQL 13 and below. For PostgreSQL 14 it&#8217;s a work in progress. In case of any issue, you can consult [Kong API documentation][2].

 [1]: https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/ "how to install PostgreSQL 14 on Ubuntu 20.04"
 [2]: https://docs.konghq.com/gateway/2.7.x/install-and-run/ubuntu/ "Kong API documentation"