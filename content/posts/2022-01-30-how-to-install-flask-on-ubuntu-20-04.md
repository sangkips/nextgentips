---
title: How to install Flask on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-30T10:09:17+00:00
url: /2022/01/30/how-to-install-flask-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 18
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial guide, we are going to explore how to install the Flask framework on Ubuntu 20.04.

Flask is a microweb framework written in Python. It is classified as a microframework because it doesn’t require particular libraries or tools. It **has no database abstraction layer**, **form validation,** or any other components where pre-existing third-party libraries provide common functions.

Flask depends on&nbsp;[jinja][1]&nbsp;template engine and the Werkzeug&nbsp;[WSGI][2]&nbsp;toolkit.

## Prerequisites {#prerequisites.wp-block-heading}

  * Flask need Python 3.6 and newer in order to run
  * You need to have virtual environment up an running
  * Ubuntu 20.04 server up and running.
  * Have a user with sudo privileges.

## Dependencies {#dependencies.wp-block-heading}

Flask requires the following dependencies.

  * **Werkzeug.** This implements WSGI which is the standard Python interface between applications and servers.
  * **Jinja.** It is a template language that renders the pages to your application data.
  * **MarkupSafe.** It escapes untrusted input when rendering templates to avoid injection attacks.
  * **ItsDangerous.** It securely signs data to ensure data integrity
  * **Click.** This is a framework for writing command line applications

## Related Articles  {#related-articles.wp-block-heading}

  * [How to install Flask on Ubuntu 21.10][3]

## Install Flask on Ubuntu 20.04 {#install-flask-on-ubuntu-20-04.wp-block-heading}

### 1. Run system updates  {#install-flask-on-ubuntu-20-04.wp-block-heading}

To begin our installation we need to update system repositories in order to make them up to date. To do so, use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y
# reboot your system if need be.</code></pre>

### 2. Create a Virtual Environment  {#2-create-a-virtual-environment.wp-block-heading}

It is recommended to use a virtual environment to manage your projects while both in development and in production, this is because the more Python projects you have, the more likely that you will need to work on different versions of Python libraries. Newer versions of libraries for a certain project can bring compatibility issues in another project. 

Start by creating a project directory.

<pre class="wp-block-code"><code>$ sudo mkdir nextgentips
$ cd nextgentips
$ python3 -m venv myvenv</code></pre>

Python 3.8 comes preinstalled with Ubuntu 20.04. check it out 

<pre class="wp-block-code"><code>$ python3
&lt;strong>Python 3.8.10 (default&lt;/strong>, Nov 26 2021, 20:14:08) 
&#91;GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> </code></pre>

I know many will encounter this error message “**The virtual environment was not created successfully because ensurepip is not available. On Debian/Ubuntu systems, you need to install the python3-venv”**

<pre class="wp-block-code"><code># error message
 “&lt;strong>The virtual environment was not created successfully because ensurepip is not available. On Debian/Ubuntu systems, you need to install the python3-venv”&lt;/strong></code></pre>

What we need to do is to install a virtual environment first. Use the following command to create a virtual environment.

<pre class="wp-block-code"><code>$ sudo apt install python3.8-venv</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
 &lt;strong> python-pip-whl&lt;/strong>
The following NEW packages will be installed:
  &lt;strong>python-pip-whl python3.8-venv&lt;/strong>
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 1811 kB of archives.
After this operation, 2339 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Python comes bundled with the <a href="https://docs.python.org/3/library/venv.html#module-venv" target="_blank" rel="noreferrer noopener" title="venv">venv</a> module to create a virtual environment.

Now if you run **python3 -m venv myvenv** it will run successfully.

<pre class="wp-block-code"><code>$ python3 -m venv myvenv</code></pre>

### 3. Activate virtual environment  {#3-activate-virtual-environment.wp-block-heading}

Before working on any project, we need to activate the virtual environment. To do so use the following command 

<pre class="wp-block-code"><code>source myvenv/bin/activate</code></pre>

Your output must look like this 

<pre class="wp-block-code"><code># output
(myvenv) root@ubuntu:~/nextgentips# </code></pre>

### 4. Install Flask on Ubuntu 20.04 {#4-install-flask-on-ubuntu-20-04.wp-block-heading}

Inside the activated virtual environment, you can now install the Flask framework. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ pip install flask</code></pre>

The output will look like this.

<pre class="wp-block-code"><code># output
Collecting flask
  Downloading Flask-2.0.2-py3-none-any.whl (95 kB)
     |████████████████████████████████| 95 kB 938 kB/s 
Collecting Jinja2>=3.0
  Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
     |████████████████████████████████| 133 kB 4.7 MB/s 
Collecting Werkzeug>=2.0
  Downloading Werkzeug-2.0.2-py3-none-any.whl (288 kB)
     |████████████████████████████████| 288 kB 21.9 MB/s 
Collecting click>=7.1.2
  Downloading click-8.0.3-py3-none-any.whl (97 kB)
     |████████████████████████████████| 97 kB 4.2 MB/s 
Collecting itsdangerous>=2.0
  Downloading itsdangerous-2.0.1-py3-none-any.whl (18 kB)
Collecting MarkupSafe>=2.0
  Downloading MarkupSafe-2.0.1-cp38-cp38-manylinux2010_x86_64.whl (30 kB)
Installing collected packages: MarkupSafe, Jinja2, Werkzeug, click, itsdangerous, flask
Successfully installed Jinja2-3.0.3 MarkupSafe-2.0.1 Werkzeug-2.0.2 click-8.0.3 flask-2.0.2 itsdangerous-2.0.1</code></pre>

You can check the version of Flask you are running with the following command.

<pre class="wp-block-code"><code>$ flask --version
Python 3.8.10
Flask 2.0.2
Werkzeug 2.0.2</code></pre>

I am running Python 3.8.10 which comes preinstalled with Ubuntu 20.04, you don’t need to install it.

### 5. Test Flask  {#5-test-flask.wp-block-heading}

Let&#8217;s write one program 

<pre class="wp-block-code"><code>from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_Nextgentips():
    return "&lt;p>Hello, Nextgentips!&lt;/p>"</code></pre>

NB: 

  * **from flask import Flask**: It import flask class.
  * **app = Flask(\_\_name\_\_)**: Its the name of the application module. Its required so that Flask knows where to look for resources such as static files and templates.
  * **@app.route(&#8220;/&#8221;)**: It tell Flask what URL should trigger our function 

Save as **next.py**.

To run the application use **flask run**. But first, tell Flask terminal what application it&#8217;s working on by exporting **FLASK_APP**

<pre class="wp-block-code"><code>$ export FLASK_APP=next</code></pre>

Run the application

<pre class="wp-block-code"><code>$ &lt;strong>flask run&lt;/strong>
* Serving Flask app 'next' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)</code></pre>

## 5. Conclusion {#5-conclusion.wp-block-heading}

Congratulations! You have installed Flask and now you are ready to jump into creating awesome projects with Flask. To get started go to [Flask getting started page][4] to start writing your Flask project. Happy that you have learned something today. Follow along for more tutorials.

 [1]: https://jinja.palletsprojects.com/en/3.0.x/
 [2]: https://werkzeug.palletsprojects.com/en/2.0.x/
 [3]: https://nextgentips.com/2021/12/13/how-to-install-flask-on-ubuntu-21-10/
 [4]: https://flask.palletsprojects.com/en/2.0.x/quickstart/