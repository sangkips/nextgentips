---
title: How to install Oracle VirtualBox 6.1 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-29T14:09:08+00:00
url: /2021/10/29/how-to-install-oracle-virtualbox-6-1-on-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
ss_ss_click_share_count_linkedin:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 117
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Oracle VirtualBox on Ubuntu 21.04 and learn about its rich feature content.

VirtualBox is free, open-source virtualization software. So what is virtualization? It is the process of running a virtual instance of a computer system in a layer abstracted from the actual hardware.

## Importance of Virtualization {.wp-block-heading}

Why is virtualization technology so important nowadays? Let&#8217;s look at some of the reasons why we need it:

  * Think of it this way, you want to run more than two operating system in one computer, what are your options here? Can you install one operating system then use it after you are done you remove it? This will prove unachievable. To solve this problem virtualization comes into play. Set a virtual environment, install as many operating systems as you wish, all will operate as if running on different hardware.
  * It tend to become too easy to install different software. Tedious and complex software programs can be bundled into virtual machines so that it can be shipped entirely into targeted machines.
  * Testing tend to become more easy to do in a virtual environment. Before shipping certain software programs you can do thorough testing in a control environment before going to live productions. If the software program passes the test then you can push to live production.
  * Reducing infrastructure costs is always the priority for most companies. To do that you can implement virtualization because it will significantly reduce hardware and electricity cost 

## Prerequisites  {.wp-block-heading}

Before you can do the installation make sure you have the following:

  * Qt 5.6.2 or later required 
  * Stable internet connection
  * SDL 1.2.7 or later. This is for graphics 
  * GNU compiler 

## Installing Oracle VirtualBox on Ubuntu 21.04 {.wp-block-heading}

The first procedure is to update our repository.

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

After updates are complete do the upgrade to ensure that the system is up to date:

<pre class="wp-block-code"><code>$ sudo apt upgrade -y</code></pre>

When the upgrade is complete, then it&#8217;s time for the installation of the VirtualBox. Use the following command to do so

<pre class="wp-block-code"><code>$ sudo apt install virtualbox</code></pre>

<pre class="wp-block-code"><code># ....
vboxdrv.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.11.0-18-generic/updates/dkms/

vboxnetadp.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.11.0-18-generic/updates/dkms/

vboxnetflt.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.11.0-18-generic/updates/dkms/

depmod...

DKMS: install completed.
Building initial module for 5.11.0-38-generic
EFI variables are not supported on this system
/sys/firmware/efi/efivars not found, aborting.
Done.

vboxdrv.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.11.0-38-generic/updates/dkms/

vboxnetadp.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.11.0-38-generic/updates/dkms/

vboxnetflt.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.11.0-38-generic/updates/dkms/

depmod...

DKMS: install completed.
Setting up libgtk-3-common (3.24.25-1ubuntu4.1) ...
Setting up libpangocairo-1.0-0:amd64 (1.48.2-1build2) ...
Setting up libc-devtools (2.33-0ubuntu5) ...
Setting up gsettings-desktop-schemas (3.38.0-1ubuntu1) ...
Setting up libinput10:amd64 (1.16.4-3ubuntu2) ...
Setting up gtk-update-icon-cache (3.24.25-1ubuntu4.1) ...
Setting up libqt5gui5:amd64 (5.15.2+dfsg-5ubuntu1) ...
Setting up libqt5widgets5:amd64 (5.15.2+dfsg-5ubuntu1) ...
Setting up librsvg2-2:amd64 (2.50.3+dfsg-1) ...
Setting up libqt5printsupport5:amd64 (5.15.2+dfsg-5ubuntu1) ...
Setting up librsvg2-common:amd64 (2.50.3+dfsg-1) ...
Setting up libqt5opengl5:amd64 (5.15.2+dfsg-5ubuntu1) ...
Setting up libgdk-pixbuf2.0-bin (2.42.2+dfsg-1build1) ...
Setting up virtualbox (6.1.26-dfsg-3~ubuntu1.21.04.1) ...
Setting up libqt5x11extras5:amd64 (5.15.2-2) ...
Setting up libqt5svg5:amd64 (5.15.2-3) ...
Setting up virtualbox-qt (6.1.26-dfsg-3~ubuntu1.21.04.1) ...
Setting up adwaita-icon-theme (3.38.0-1ubuntu2) ...
update-alternatives: using /usr/share/icons/Adwaita/cursor.theme to provide /usr/share/icons/default/index.theme (x-cursor-theme) in auto mode
Setting up humanity-icon-theme (0.6.15) ...
Setting up ubuntu-mono (20.10-0ubuntu1) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for shared-mime-info (2.0-1) ...
Processing triggers for udev (247.3-3ubuntu3.6) ...
Processing triggers for libglib2.0-0:amd64 (2.68.1-1~ubuntu21.04.1) ...
Processing triggers for libc-bin (2.33-0ubuntu5) ...
Setting up glib-networking:amd64 (2.66.0-2) ...
Setting up libsoup2.4-1:amd64 (2.72.0-3) ...
Setting up libsoup-gnome2.4-1:amd64 (2.72.0-3) ...
Setting up librest-0.7-0:amd64 (0.8.1-1.1) ...
Setting up libgtk-3-0:amd64 (3.24.25-1ubuntu4.1) ...
Setting up libgtk-3-bin (3.24.25-1ubuntu4.1) ...
Setting up qt5-gtk-platformtheme:amd64 (5.15.2+dfsg-5ubuntu1) ...
Processing triggers for libgdk-pixbuf-2.0-0:amd64 (2.42.2+dfsg-1build1) ...
Processing triggers for libc-bin (2.33-0ubuntu5) ...
...</code></pre>

It installs all the prerequisites needed at a go. This is the best way to install VirtualBox if you don&#8217;t want problems. Building from the source requires that you understand every aspect of the commands you are using. This is best for starters.

## Start Oracle VirtualBox {.wp-block-heading}

You can start Oracle VirtualBox by typing **VirtualBox** on the command line

<pre class="wp-block-code"><code>$ virtualbox</code></pre>

To work on the new virtual machine click the new icon a pop-up screen will show where you will give a name to your project and select machine type. Click next to continue 

Choose your memory size and click next

Choose hard disk and select on create a virtual hard disk now

Click create to create a virtual hard disk. Choose VDI and click next

Select dynamically allocated and click next.

Choose file location for your virtual machine. Leave it to the default folder for now and choose the hard disk you require then click create

Next, you need now to have your USB stick containing your preferred operating system in order to continue with the installation.

## Conclusion {.wp-block-heading}

We have learned how to install VirtualBox on our Ubuntu 21.04 and also saw how to set up VirtualBox for the first time. Next now is for you to install your preferred operating system with ease. If in case you face any challenge do consult [VirtualBox Documentation][1]

You can learn Docker for more advanced features. Check this article as a start

  * [How to install Docker on Arch Linux][2]

 [1]: https://www.virtualbox.org/manual/ch01.html#virt-why-useful
 [2]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/