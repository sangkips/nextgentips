---
title: How to install and Configure pgAdmin 4 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-04T18:30:01+00:00
url: /2022/01/04/how-to-install-and-configure-pgadmin-4-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 42
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, I will be taking through the installation of pgAdmin 4 version 6.3 on Ubuntu 20.04.

pgAdmin 4 is a free and open-source management tool for Postgres. Its desktop runtime written in NWjs allows it to run standalone for individual users, or the web applications code may be directly deployed on a web server for use by the web browser.

pgAdmin 4 is a complete rewrite of pgAdmin, built using Python and Java

## New features of pgAdmin 4 version 6.3 {.wp-block-heading}

The following new features were introduced for the latest release of 6.2. these features are mostly for bug fixes.

  * It added support for Two-factor authentication for improving security. Two-factor authentication (2FA) is an extra layer of security used when logging into websites. 
  * Added support to disable the auto-discovery of the database servers. The feature allows one to disable the auto-discovery of database servers
  * Include GSSAPI support in the PostgreSQL libraries and utilities in MacOS

You can refer to [pgAdmin 4 documentation][1] for more concerning the bug fixes introduced with the current version 6.3

## Related Articles {.wp-block-heading}

  * [How to Install pgAdmin 4 on CentOS 8][2]
  * [How to install pgAdmin 4 version 6.2 on Ubuntu 21.10][3]

To install pgAdmin 4, we are going to set up Python environment. We will be working on a virtual environment to install pgAdmin 4.

## Install pgAdmin 4 on Ubuntu 20.04 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

To begin our installation we need to make our system repositories up to date by running system-wide updates with the following command.

<pre class="wp-block-code"><code>$ sudo system update
$ sudo system upgrade -y
$ sudo reboot -n</code></pre>

## 2. Setup Python Environment {.wp-block-heading}

To ensure that you don&#8217;t run into any problem make sure Python is installed. If not installed check this tutorial out first. [How to install Python 3.9 interpreter on Ubuntu 20.04][4]

If you have installed Python successfully, you can now set up the virtual environment.

First, create a pgadmin folder under opt directory.

<pre class="wp-block-code"><code>$ sudo mkdir -p /opt/pgadmin</code></pre>

Next is to create a virtual environment, but first ensure that ensurepip is running. To install this run the following command so that you don&#8217;t run into errors.

<pre class="wp-block-code"><code>$ sudo apt install python3.8-venv </code></pre>

This command will install python3.8-venv package plus many additional dependency packages 

<pre class="wp-block-code"><code>Sample Output
The following packages were automatically installed and are no longer required:
  libfprint-2-tod1 libgtkglext1 libllvm9 libminizip1 libpangox-1.0-0 shim
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  libpython3.8 libpython3.8-dev libpython3.8-minimal libpython3.8-stdlib
  python3.8 python3.8-dev python3.8-minimal
Suggested packages:
  python3.8-doc binfmt-support
The following NEW packages will be installed:
  python3.8-venv
The following packages will be upgraded:
  libpython3.8 libpython3.8-dev libpython3.8-minimal libpython3.8-stdlib
  python3.8 python3.8-dev python3.8-minimal
7 upgraded, 1 newly installed, 0 to remove and 91 not upgraded.
Need to get 5,444 B/10.8 MB of archives.
After this operation, 33.8 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y
....</code></pre>

<pre class="wp-block-code"><code>$ python3 -m venv /opt/pgdmin/venv </code></pre>

The next process is to activate the virtual environment. Do so with the following command.

<pre class="wp-block-code"><code>$ source /opt/pgadmin/venv/bin/activate</code></pre>

## 3. Install pgAdmin on Ubuntu 20.04 {.wp-block-heading}

Having created a virtual environment, we can now install our pgAdmin using pip utility.

<pre class="wp-block-code"><code>$ pip install pgadmin4 </code></pre>

<pre class="wp-block-code"><code>Sample output
Collecting pgadmin4
  Downloading pgadmin4-6.3-py3-none-any.whl (114.2 MB)
     |████████████████████████████████| 114.2 MB 583 kB/s 
WARNING: Retrying (Retry(total=4, connect=None, read=None, redirect=None, status=None)) after connection broken by 'ReadTimeoutError("HTTPSConnectionPool(host='pypi.org', port=443): Read timed out. (read timeout=15)")': /simple/six/
Collecting six==1.*
  Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
Collecting python-dateutil==2.*
  Downloading python_dateutil-2.8.2-py2.py3-none-any.whl (247 kB)
     |████████████████████████████████| 247 kB 302 kB/s 
Collecting Flask-SocketIO>=5.0.1
  Downloading Flask_SocketIO-5.1.1-py3-none-any.whl (17 kB)
Collecting Flask==2.*
  Downloading Flask-2.0.2-py3-none-any.whl (95 kB)
     |████████████████████████████████| 95 kB 527 kB/s 
Collecting Flask-Paranoid==0.*
  Downloading Flask_Paranoid-0.2.0-py2.py3-none-any.whl (4.1 kB)
Collecting psycopg2-binary==2.8.*
  Downloading psycopg2_binary-2.8.6-cp38-cp38-manylinux1_x86_64.whl (3.0 MB)
     |████████████████████████████████| 3.0 MB 338 kB/s 
Collecting SQLAlchemy==1.4.*
  Downloading SQLAlchemy-1.4.29-cp38-cp38-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl (1.6 MB)
     |████████████████████████████████| 1.6 MB 141 kB/s 
Collecting cryptography==3.*
  Downloading cryptography-3.4.8-cp36-abi3-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (3.2 MB)
     |████████████████████████████████| 3.2 MB 533 kB/s 
Collecting sqlparse==0.*
  Downloading sqlparse-0.4.2-py3-none-any.whl (42 kB)
     |████████████████████████████████| 42 kB 263 kB/s 
Collecting ldap3==2.*
  Downloading ldap3-2.9.1-py2.py3-none-any.whl (432 kB)
     |████████████████████████████████| 432 kB 197 kB/s 
Collecting eventlet==0.33.0
</code></pre>

## 4. Run pgadmin 4 setup.py {.wp-block-heading}

First, we need to create a directory where pgAdmin stays 

<pre class="wp-block-code"><code>$ mkdir /var/lib/pgadmin
$ mkdir /var/log/pgadmin</code></pre>

Then run the setup.py with the following command

<pre class="wp-block-code"><code>$ python3 /opt/pgadmin/venv/lib/python3.8/site-packages/pgadmin4/setup.py</code></pre>

You will get the following output

<pre class="wp-block-code"><code>Output
NOTE: Configuring authentication for SERVER mode.

Enter the email address and password to use for the initial pgAdmin user account:

Email address: nextgentips01@gmail.com
Password: 
Retype password:
pgAdmin 4 - Application Initialisation
======================================
</code></pre>

Note the email and password used.

## 5. Add pgAdmin user  {.wp-block-heading}

The pgadmin user to be added will run all the pgAdmin processes. So to add the user run the following command on your terminal.

<pre class="wp-block-code"><code>$ adduser --system --group --home var/lib/pgadmin --disabled-login --shell /usr/sbin/nologin/pgadmin </code></pre>

Then you need to allow the user to access the **/var/log/pgadmin**, **/var/lib/pgadmin** and **opt/pgadmin** directories.

<pre class="wp-block-code"><code>$ chown -R pgadmin:pgadmin /var/log/pgadmin /var/lib/pgadmin/ /opt/pgadmin</code></pre>

That is all for now, to access the pgAdmin on the browser, type pgadmin4 on the terminal and use the HTTP link provided. To login into pgAdmin 4 use the email and password you setup before.

<pre class="wp-block-code"><code>$ pgadmin4
Starting pgAdmin 4. Please navigate to &lt;strong>http://127.0.0.1:5050&lt;/strong> in your browser.
2022-01-04 21:16:16,920: WARNING	werkzeug:	WebSocket transport not available. Install simple-websocket for improved performance.
 * Serving Flask app 'pgadmin' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed and configured pgAdmin 4 on our system. Continue visiting this site for informative tutorials.

 [1]: https://www.pgadmin.org/docs/pgadmin4/latest/release_notes_6_2.html
 [2]: https://nextgentips.com/2021/11/29/how-to-install-pgadmin-4-on-centos-8/
 [3]: https://nextgentips.com/2021/11/19/how-to-install-pgadmin-4-version-6-2-on-ubuntu-20-04/
 [4]: https://nextgentips.com/2021/10/04/install-python-3-on-ubuntu-20-04/