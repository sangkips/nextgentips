---
title: How to install PHP 8.1 on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-12-03T10:36:52+00:00
url: /2021/12/03/how-to-install-php-8-1-on-ubuntu-21-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:12:"200.8.204.33";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 77
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial, I am going to show you how to install the current stable release version of PHP 8.1 on Ubuntu 21.04.

PHP is a general-purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is:

  * **Fast** &#8211; PHP is fast because it runs on its own memory space. Also PHP has Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * **Flexible** &#8211; PHP is flexible because it allows to almost all databases. MySQL being the de-facto database to connect to because it is open source.
  * Free and open source
  * **PHP is forgiving** &#8211; Why do we say PHP is forgiving? It is forgiving meaning its learning it is not so hard therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Prerequisites for Installing PHP 8.1  {.wp-block-heading}

  * Before you can use PHP you must have **web server installed**. Apache web server is the most widely used with PHP.
  * **Database** must be there for PHP to run. It works well on all databases such Oracle, MySQL, PostgreSQL.
  * **PHP parser** must be there. What is PHP parser if you can ask? PHP Parser is a library that takes a source code written in PHP, passes it through a lexical analyzer and creates its respective syntax tree.

PHP 8.1 is a major update release version. It contains many new features and optimizations e.g

  * It introduced Enumerations feature. Enumerations or Enum feature allow a developer to define a custom type that is limited to one of the discrete number of possible values. This is helpful while defining domain model.
  * Improvement of RFC readonly property version 2. This prevents modification of the property after initialization.
  * Introduction of RFC fibers call action. This seeks to eliminate synchronous and asychronous function call by allowing functions to become interruptible without affecting the entire fuction.
  * Introduction of a non return type. This indicates that a function either exits, throws an exception or doesn&#8217;t terminate.
  * Introduction of first class collable actions. 
  * Introduction of [fsync][1] and [fdatasync][2] functions 
  * Introduction of [array\_is\_list][3] functions
  * Introduction of explicit octal numeral notation

## Related Articles  {.wp-block-heading}

  * [How to Install PHP 8 on Ubuntu 21.04][4]
  * [How to Install Apache, MYSQL, PHP on Ubuntu 20.04][5]

## Table of contents {.wp-block-heading}

  1. Run system update
  2. Add Ondrej sury PPA repository
  3. Install PHP 8.1

## Installing PHP 8.1 on Ubuntu 21.04 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

First, let&#8217;s run system updates on our system with the following command.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y
$ reboot -n</code></pre>

When updates are complete you can probably reboot your system for the changes to take effect, then proceed to the next step.

## 2. Add Ondrej sury PPA repository {.wp-block-heading}

To start installing and using the software package from PPA, you need to tell Ubuntu where to find the PPA. The contents of PPA are not checked or monitored. 

To install this package, run the following command from your terminal.

<pre class="wp-block-code"><code>$ sudo add-apt-repository ppa:ondrej/php</code></pre>

Sample output will look like this:

<pre class="wp-block-code"><code>PPA publishes dbgsym, you may need to include 'main/debug' component
Repository: 'deb http://ppa.launchpad.net/ondrej/php/ubuntu/ hirsute main'
Description:
Co-installable PHP versions: PHP 5.6, PHP 7.x and most requested extensions are included. Only Supported Versions of PHP (http://php.net/supported-versions.php) for Supported Ubuntu Releases (https://wiki.ubuntu.com/Releases) are provided. Don't ask for end-of-life PHP versions or Ubuntu release, they won't be provided.

Debian oldstable and stable packages are provided as well: https://deb.sury.org/#debian-dpa

You can get more information about the packages at https://deb.sury.org

IMPORTANT: The &lt;foo&gt;-backports is now required on older Ubuntu releases.

BUGS&FEATURES: This PPA now has a issue tracker:
https:&#47;&#47;deb.sury.org/#bug-reporting

CAVEATS:
1. If you are using php-gearman, you need to add ppa:ondrej/pkg-gearman
2. If you are using apache2, you are advised to add ppa:ondrej/apache2
3. If you are using nginx, you are advised to add ppa:ondrej/nginx-mainline
   or ppa:ondrej/nginx

PLEASE READ: If you like my work and want to give me a little motivation, please consider donating regularly: https://donate.sury.org/

WARNING: add-apt-repository is broken with non-UTF-8 locales, see
https://github.com/oerdnj/deb.sury.org/issues/56 for workaround:

# LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
More info: https://launchpad.net/~ondrej/+archive/ubuntu/php
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.</code></pre>

Press enter to allow installation on your system. 

Run system update again for the new changes to take effect and restart when necessary.

## 3. Install PHP 8.1 on Ubuntu 21.04 {.wp-block-heading}

When this is complete, move ahead and install PHP 8.1 on Ubuntu 21.04. Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install php8.1</code></pre>

Many dependencies will be installed during this process.

sample output will look like this;

<pre class="wp-block-code"><code>Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following package was automatically installed and is no longer required:
  net-tools
Use 'sudo apt autoremove' to remove it.
The following additional packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils bzip2 libapache2-mod-php8.1 libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.3-0 libsodium23 mailcap mime-support php-common
  php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser bzip2-doc php-pear
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils bzip2 libapache2-mod-php8.1 libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap libjansson4 liblua5.3-0 libsodium23 mailcap mime-support php-common
  php8.1 php8.1-cli php8.1-common php8.1-opcache php8.1-readline ssl-cert
0 upgraded, 22 newly installed, 0 to remove and 0 not upgraded.
Need to get 6841 kB of archives.
After this operation, 30.1 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow the installation to continue.

You can now check the PHP version on your system 

<pre class="wp-block-code"><code>$ php --version
PHP 8.1.0 (cli) (built: Nov 25 2021 20:22:22) (NTS)
Copyright (c) The PHP Group
Zend Engine v4.1.0, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.0, Copyright (c), by Zend Technologies</code></pre>

## 4. Conclusion {.wp-block-heading}

Congratulations, you have successfully installed PHP 8.1 on your Ubuntu 21.04. You can now continue learning more and in case you have a problem you consult us or [PHP documentation][6].

 [1]: https://www.php.net/manual/en/function.fsync.php
 [2]: https://www.php.net/manual/en/function.fdatasync.php
 [3]: https://www.php.net/manual/en/function.array-is-list.php
 [4]: https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/
 [5]: https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/
 [6]: https://www.php.net/archive/2021.php#2021-11-25-1