---
title: How to install PostgreSQL 14 on Ubuntu 20.04 from the source
author: Kipkoech Sang
type: post
date: 2022-05-23T10:28:00+00:00
url: /2022/05/23/how-to-install-postgresql-14-on-ubuntu-20-04-from-the-source/
rank_math_seo_score:
  - 10
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 180
rank_math_primary_category:
  - 7
categories:
  - Database

---
 

In this article, we are going to learn how to install and get PostgreSQL 14 up and running on an Ubuntu 20.04 server. PostgreSQL is a powerful, open-source object-relational database system that uses and extends the SQL language combined with many features that safely store and scale the most complicated data workloads.

## Install PostgreSQL 14 on Ubuntu 20.04 {.wp-block-heading}

Ubuntu comes with a default PosgreSQL in its APT repositories. To check the default in the repositories, we can use the following command.

<pre class="wp-block-code"><code>$ sudo apt-cache search postgresql | grep postgresql

postgresql - object-relational SQL database (supported version)
postgresql-12 - object-relational SQL database, version 12 server
postgresql-client - front-end programs for PostgreSQL (supported version)
postgresql-client-12 - front-end programs for PostgreSQL 12
postgresql-client-common - manager for multiple PostgreSQL client versions
postgresql-common - PostgreSQL database-cluster manager
postgresql-contrib - additional facilities for PostgreSQL (supported version)
postgresql-doc - documentation for the PostgreSQL database management system
postgresql-doc-12 - documentation for the PostgreSQL database management system</code></pre>

As you can see, Ubuntu 20.04 comes with a default PostgreSQL 12.

### Update system repositories  {.wp-block-heading}

Let&#8217;s first update our repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### Install PostgreSQL dependencies  {.wp-block-heading}

We need to have the following dependencies installed first before we can proceed to install from the source.

<pre class="wp-block-code"><code>$ sudo apt install build-essential zlib1g-dev libreadline-dev -y</code></pre>

### Download PostgreSQL 14 from source  {.wp-block-heading}

Head over to <a href="https://www.postgresql.org/ftp/source/" target="_blank" rel="noreferrer noopener">postgresql ftp server</a> to download the latest version

<pre class="wp-block-code"><code>$ wget https://ftp.postgresql.org/pub/source/v14.2/postgresql-14.2.tar.gz</code></pre>

Next, we need to extarct this downloaded content.

<pre class="wp-block-code"><code>$ tar xvfz postgresql-14.2.tar.gz</code></pre>

Then next we are going to cd into it then do configuration and do make and lastly sudo make install.

<pre class="wp-block-code"><code>cd postgresql-14.2
./configure 
make
make install</code></pre>

### Add a Postgres user  {.wp-block-heading}

To allow us run administrative jobs we need to add a postgres user and also because we didnt install from the APT repository, its ncessary to install Postgresql-contrib first.

<pre class="wp-block-code"><code>$ sudo apt install postgresql-contrib</code></pre>

Add a postgres user password with this command.

<pre class="wp-block-code"><code>$ sudo passwd postgres </code></pre>

and to switch to the postgres user use this command 

<pre class="wp-block-code"><code>$ su postgres
$ psql
psql (14.2 (Ubuntu 14.2-1ubuntu1))
Type "help" for help.

postgres=# \l
                              List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges   
-----------+----------+----------+---------+---------+-----------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 | 
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
(3 rows)
                    </code></pre>

To check the version of the PostgreSQL installed use this command.

<pre class="wp-block-code"><code>postgres=# SELECT version();
 version                                                      
 PostgreSQL 14.2 (Ubuntu 14.2-1ubuntu1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 11.2.0-19ubuntu1) 11.2.0, 64-bit
(1 row)

postgres=# </code></pre>

Conclusion 

Congratulations you have installed PostgreSQL 14 from the source.