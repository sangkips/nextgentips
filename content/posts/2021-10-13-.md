---
title: Top Things to do after Installing CentOs 7
author: Kipkoech Sang
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=208
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
categories:
  - Uncategorized

---
Are you stack after installation of CentOS? Worry no more because i am going to guide you on important things to do after fresh installation of CentOS. You need to do this things for you to be able to do your daily routines. 

## Set Server Hostname {.wp-block-heading}

The first thing we can do is change the hostname of our server. 

<pre class="wp-block-code"><code>$ echo $HOSTNAME</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="303" height="58" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-13-18-09-19.png?resize=303%2C58&#038;ssl=1" alt="" class="wp-image-210" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-13-18-09-19.png?w=303&ssl=1 303w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-13-18-09-19.png?resize=300%2C57&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-13-18-09-19.png?resize=24%2C5&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-13-18-09-19.png?resize=36%2C7&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-13-18-09-19.png?resize=48%2C9&ssl=1 48w" sizes="(max-width: 303px) 100vw, 303px" data-recalc-dims="1" /> </figure> 

From the above figure the default hostname is centos. We need to change this to reflect to your needs. Use the following command to edit &#8216;**/etc/hostname&#8217;**

<pre class="wp-block-code"><code># vi /etc/hostname</code></pre>