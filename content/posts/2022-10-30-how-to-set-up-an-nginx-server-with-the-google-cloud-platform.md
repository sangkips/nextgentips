---
title: How to set up an Nginx Server with the Google Cloud Platform
author: Kipkoech Sang
type: post
date: 2022-10-30T19:01:49+00:00
url: /2022/10/30/how-to-set-up-an-nginx-server-with-the-google-cloud-platform/
categories:
  - Linux
  - Uncategorized

---
Have you encountered a problem where you want to host a simple web server but you don&#8217;t know how to go about it, here is a very simple solution to host a simple web server on Google cloud using the free tier. 

In this tutorial, I will take you through the setting up of a server on Google cloud and later install Nginx. From here you can run a simple hello world.

Here I am using a free tier. Make sure you destroy your instance when you are done experimenting to avoid incurring extra charges.

### What is Nginx? {.wp-block-heading}

Nginx pronounced as `<mark style="background-color:#abb8c3" class="has-inline-color">engine x</mark>` according to <a href="https://en.wikipedia.org/wiki/Nginx" target="_blank" rel="noreferrer noopener">Wikipedia</a> is a free and open-source web server that can also be used as a reverse proxy, load balancer, mail proxy, and HTTP cache. Nginx is deployed to serve both static and dynamic content on the network. 

Let&#8217;s dive in to spin up our server on the google cloud platform.

### Set up Google Cloud platform. {.wp-block-heading}

I believe you already have your google cloud account with free tier capability. So to begin, we need to create project. Select create a project and give your project a name and give an organization name if you have one and click create.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="556" height="335" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-10-30.png?resize=556%2C335&#038;ssl=1" alt="nextgentips create project page" class="wp-image-1612" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-10-30.png?w=556&ssl=1 556w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-10-30.png?resize=300%2C181&ssl=1 300w" sizes="(max-width: 556px) 100vw, 556px" data-recalc-dims="1" /> <figcaption>nextgentips create project page</figcaption></figure> 

On the notification click select project so that you will be in a position to create your VM instance from there.

Then the next thing is to enable the Compute Engine API so that you will be in a position to create and run virtual machines on GCP.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="541" height="293" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-14-59.png?resize=541%2C293&#038;ssl=1" alt="Nextgentips: Compute Engine API" class="wp-image-1613" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-14-59.png?w=541&ssl=1 541w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-14-59.png?resize=300%2C162&ssl=1 300w" sizes="(max-width: 541px) 100vw, 541px" data-recalc-dims="1" /> <figcaption>Nextgentips: Compute Engine API</figcaption></figure> 

Inside the Compute Engine page, choose VM instance then create an instance. <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="311" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-19-49.png?resize=810%2C311&#038;ssl=1" alt="Nextgentips: VM instance" class="wp-image-1614" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-19-49.png?w=820&ssl=1 820w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-19-49.png?resize=300%2C115&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-19-49.png?resize=768%2C295&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips: VM instance</figcaption></figure> 

Input the instance name, a region where you want to host your server, choose your desired machine configurations, boot disk such as Debian, Ubuntu etc, on the firewalls allow HTTP traffic so that you will not be blocked from accessing your server, and lastly click create.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="597" height="509" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-25-21.png?resize=597%2C509&#038;ssl=1" alt="Nextgentips: Image creation" class="wp-image-1615" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-25-21.png?w=597&ssl=1 597w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-25-21.png?resize=300%2C256&ssl=1 300w" sizes="(max-width: 597px) 100vw, 597px" data-recalc-dims="1" /> <figcaption>Nextgentips: Image creation</figcaption></figure> 

After the VM has been created, connect to it by using ssh or the google console. To connect using ssh, click on the ssh and you will be taken to a terminal console.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="692" height="280" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-29-03.png?resize=692%2C280&#038;ssl=1" alt="Nextgentips: web-instance " class="wp-image-1616" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-29-03.png?w=692&ssl=1 692w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-29-03.png?resize=300%2C121&ssl=1 300w" sizes="(max-width: 692px) 100vw, 692px" data-recalc-dims="1" /> <figcaption>Nextgentips: web-instance </figcaption></figure> 

Wait for it to validate ssh keys before letting you use the terminal. From here we have pinned up our server ready to do the update and installation of the Nginx server.

### Update the repositories  {.wp-block-heading}

First, let&#8217;s run an update and upgrade of our newly created instance in order to make the repositories up to date.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### Install Nginx  {.wp-block-heading}

After the upgrade is complete we can now install the Nginx server with the following command.

<pre class="wp-block-code"><code>sudo apt install nginx -y</code></pre>

From here you can check if the Nginx is up and running by using the following command.

<pre class="wp-block-code"><code>$ ps auwx | grep nginx
root        1861  0.0  0.3  58420 11984 ?        S    18:35   0:00 nginx: master process /usr/sbin/nginx -g &lt;strong>daemon on; master_process on;&lt;/strong>
www-data    1864  0.0  0.0  58792  3264 ?        S    18:35   0:00 nginx: worker process
sangkip+    1891  0.0  0.0   5136   708 pts/0    S+   18:38   0:00 grep nginx</code></pre>

Everything is up and running. Or you can use `<mark style="background-color:#abb8c3" class="has-inline-color">sudo systemctl status nginx</mark>` if it supports systems or `<mark style="background-color:#abb8c3" class="has-inline-color">sudo service status nginx</mark>` for sysV.

### Verify Nginx  {.wp-block-heading}

To know that the server is working we can do a verification from the browser. Use the IP address of the server `<mark style="background-color:#abb8c3" class="has-inline-color">http://<ip-address></mark>`<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="692" height="280" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-44-18.png?resize=692%2C280&#038;ssl=1" alt="Nextgentips: Nginx welcome page" class="wp-image-1617" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-44-18.png?w=692&ssl=1 692w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-44-18.png?resize=300%2C121&ssl=1 300w" sizes="(max-width: 692px) 100vw, 692px" data-recalc-dims="1" /> <figcaption>Nextgentips: Nginx welcome page</figcaption></figure> 

There you have it, we have installed Nginx successfully, next we need to use an HTML file to display static content on our page.

First, create an index.html page using vim or any code editor. I will be using the vscode.

Create an HTML file with the following content `<mark style="background-color:#abb8c3" class="has-inline-color">Welcome to my website</mark>`

Go back to the VMs terminal and upload the file you have created. It will go to the home directory, so we need to move it to `<mark style="background-color:#abb8c3" class="has-inline-color">/var/www/html</mark>`

<pre class="wp-block-code"><code>sudo mv index.html /var/www/html</code></pre>

Now go back to the browser and refresh your page, this time you will be in a position to see your message.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="488" height="158" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-55-12.png?resize=488%2C158&#038;ssl=1" alt="Nextgentips:our web page" class="wp-image-1618" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-55-12.png?w=488&ssl=1 488w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/10/Screenshot-from-2022-10-30-21-55-12.png?resize=300%2C97&ssl=1 300w" sizes="(max-width: 488px) 100vw, 488px" data-recalc-dims="1" /> <figcaption>Nextgentips:our web page</figcaption></figure> 

Lastly, we need to destroy our server to avoid extra charges. 

Go to where you can see the tilda and click on it, click stop and then delete to remove the server from your project&#8217;s workspace.

And that is all for Nginx on Google Cloud Platform, go ahead and implement the SSL certificate option to remove not secure on the browser.