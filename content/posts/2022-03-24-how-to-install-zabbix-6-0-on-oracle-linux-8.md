---
title: How to install Zabbix 6.0 on Rocky Linux 8
author: Kipkoech Sang
type: post
date: 2022-03-24T11:53:09+00:00
url: /2022/03/24/how-to-install-zabbix-6-0-on-oracle-linux-8/
rank_math_seo_score:
  - 75
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
rank_math_focus_keyword:
  - zabbix 6.0
rank_math_analytic_object_id:
  - 159
categories:
  - Linux

---
<a href="https://www.zabbix.com/" target="_blank" rel="noreferrer noopener">Zabbix 6.0</a> is an open-source tool for monitoring IT infrastructures like networking, servers, cloud services, and virtual machines.

**Zabbix collects and displays basic metrics on a dashboard. It uses flexible notification mechanisms that allow users to configure email-based alerts for all the events that happened. This allows a fast reaction to server problems. Zabbix offers excellent data visualization and reporting using stored data. Zabbix reports and configurations are accessed via a web-based frontend.**

## Key Features of Zabbix 6.0  {.wp-block-heading}

  * Zabbix 6 now supports monitoring of over 100k Zabbix instances, which is now great whenever you want to monitor your instances.
  * You can now in a position to define your user permission on any business service provided.
  * Now you are in a position to define read-write or read-only permissions to your specific business services.
  * You are now in a position to receive alerts and be able to react to any business changes thanks to Zabbix 6 capability.
  * You are now capable of assigning weight to each of your business services
  * Empowering your business service monitoring is now possible with root cause analysis. You can gather all the root cause analysis using the Zabbix API.
  * Its now possible to have out of the box high availability cluster for Zabbix server.
  * They have embedded new way of learning in the context of machine learning and Kubernetes monitoring capability.

## Installing Zabbix 6 on Rocky Linux {.wp-block-heading}

Let&#8217;s begin installing Zabbix 6 on our Fedora 35 server with the steps below.

To begin the installation, begin by updating Fedora repositories with the following command.

<pre class="wp-block-code"><code>sudo dnf update </code></pre>

### 1. Install MariaDB database {.wp-block-heading}

We need to make sure we install MariaDB on our machine before proceeding.

So to install MariaDB on our system run the following command.

But before installing let&#8217;s enable it first

<pre class="wp-block-code"><code>&lt;strong>dnf module list mariadb&lt;/strong>
# output
Fedora Modular 35 - x86_64
Name           Stream        Profiles                                 Summary                                                  
mariadb        10.3          client, devel, galera, server &#91;d]        MariaDB: a very fast and robust SQL database server      
mariadb        10.4          client, devel, galera, server &#91;d]        MariaDB: a very fast and robust SQL database server      
mariadb        10.5          client, devel, galera, server &#91;d]        MariaDB: a very fast and robust SQL database server      

Fedora Modular 35 - x86_64 - Updates
Name           Stream        Profiles                                 Summary                                                  
mariadb        10.3          client, devel, galera, server &#91;d]        MariaDB: a very fast and robust SQL database server      
mariadb        10.4          client, devel, galera, server &#91;d]        MariaDB: a very fast and robust SQL database server      
mariadb        10.5          client, devel, galera, server &#91;d]        MariaDB: a very fast and robust SQL database server      
mariadb        10.6          client, devel, galera, server            MariaDB: a very fast and robust SQL database server      
mariadb        10.7          client, devel, galera, server            MariaDB: a very fast and robust SQL database server      

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

To enable it to use the following command

<pre class="wp-block-code"><code>sudo dnf module enable mariadb:10.7 -y</code></pre>

Then install with the following command.

<pre class="wp-block-code"><code>sudo dnf install mariadb mariadb-server</code></pre>

Check the version of MariaDB with the following command.

<pre class="wp-block-code"><code>rpm -qi maria&lt;strong>db&lt;/strong>
Name        : mariadb
Epoch       : 3
Version     : 10.7.1
Release     : 1.module_f35+13505+f0394ab9
Architecture: x86_64
Install Date: Thu 24 Mar 2022 09:42:50 AM UTC
Group       : Unspecified
Size        : 18813213
License     : GPLv2 and LGPLv2
Signature   : RSA/SHA256, Tue 07 Dec 2021 10:40:15 PM UTC, Key ID db4639719867c58f
Source RPM  : mariadb-10.7.1-1.module_f35+13505+f0394ab9.src.rpm
Build Date  : Tue 07 Dec 2021 01:42:46 PM UTC
Build Host  : buildvm-x86-23.iad2.fedoraproject.org
Packager    : Fedora Project
Vendor      : Fedora Project
URL         : http://mariadb.org
Bug URL     : https://bugz.fedoraproject.org/mariadb
Summary     : A very fast and robust SQL database server
Description :
MariaDB is a community developed fork from MySQL - a multi-user, multi-threaded
SQL database server. It is a client/server implementation consisting of
a server daemon (mariadbd) and many different client programs and libraries.
The base package contains the standard MariaDB/MySQL client programs and
utilities.</code></pre>

Enable MariaDB to start on boot

<pre class="wp-block-code"><code>sudo systemctl enable --now mariadb
sudo systemctl start mariadb
sudo systemctl status mariadb</code></pre>

Make sure MariaDB is up and running like this.

<pre class="wp-block-code"><code>● mariadb.service - MariaDB 10.7 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-03-24 09:48:21 UTC; 33s ago
       Docs: man:mariadbd(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
    Process: 21189 ExecStartPre=/usr/libexec/mariadb-check-socket (code=exited, status=0/SUCCESS)
    Process: 21211 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
    Process: 21306 ExecStartPost=/usr/libexec/mariadb-check-upgrade (code=exited, status=0/SUCCESS)
   Main PID: 21295 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 11 (limit: 1112)
     Memory: 117.5M
        CPU: 1.174s
     CGroup: /system.slice/mariadb.service
             └─21295 /usr/libexec/mariadbd --basedir=/usr</code></pre>

Now we need to secure our MySQL installation with the following command;

<pre class="wp-block-code"><code>mysql_secure_installation</code></pre>

You will get the following output. 

<pre class="wp-block-code"><code>By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!</code></pre>

You can test the database with this command 

<pre class="wp-block-code"><code>mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 11
Server version: 10.7.1-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]&gt; </code></pre>

**Create initial database**

To create a database use MySQL -u root -p to login.

<pre class="wp-block-code"><code>mysql -u root -p
MariaDB &#91;(none)]&gt; create database zabbix character set utf8mb4 collate utf8mb4_bin;
Query OK, 1 row affected (0.001 sec)

MariaDB &#91;(none)]&gt; grant all privileges on zabbix.* to zabbix@localhost identified by 'password';
Query OK, 0 rows affected (0.014 sec)

MariaDB &#91;(none)]&gt; flush privileges;
Query OK, 0 rows affected (0.001 sec)

MariaDB &#91;(none)]&gt; quit
Bye</code></pre>

### 2. Install Apache server {.wp-block-heading}

To install Apache run the following command. Check this article on <a href="https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">how to install an Apache webserver</a> 

<pre class="wp-block-code"><code>sudo dnf install httpd -y</code></pre>

To make sure the server can come up after reboot, we need to enable it and start the service manually first.

<pre class="wp-block-code"><code>systemctl enable httpd
systemctl start httpd
systemctl status httpd</code></pre>

Make sure the status is up and running.

<pre class="wp-block-code"><code>● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor pre&gt;
     Active: active (running) since Thu 2022-03-24 08:21:55 UTC; 8s ago
       Docs: man:httpd.service(8)
   Main PID: 20209 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 177 (limit: 1112)
     Memory: 14.0M
        CPU: 121ms
     CGroup: /system.slice/httpd.service
             ├─20209 /usr/sbin/httpd -DFOREGROUND
             ├─20210 /usr/sbin/httpd -DFOREGROUND
             ├─20211 /usr/sbin/httpd -DFOREGROUND
             ├─20212 /usr/sbin/httpd -DFOREGROUND
             └─20213 /usr/sbin/httpd -DFOREGROUND
</code></pre>

### 3. Install Zabbix 6.0 Repository {.wp-block-heading}

First, we need to add Zabbix 6.0 into our Fedora repository using RPM packages. this will become a bit easy to run and install Zabbix.

<pre class="wp-block-code"><code>rpm -Uvh https://repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/zabbix-release-6.0-1.el8.noarch.rpm</code></pre>

Check the output below

<pre class="wp-block-code"><code>Retrieving https://repo.zabbix.com/zabbix/6.0/rhel/8/x86_64/zabbix-release-6.0-1.el8.noarch.rpm
warning: /var/tmp/rpm-tmp.vvTBxI: Header V4 RSA/SHA512 Signature, key ID a14fe591: NOKEY
Verifying...                          ############################# &#91;100%]
Preparing...                          ############################# &#91;100%]
Updating / installing...
   &lt;strong>1:zabbix-release-6.0-1.el8 &lt;/strong>        ############################# &#91;100%]</code></pre>

### 4. Install Zabbix6.0 frontend, agent and server {.wp-block-heading}

To install frontend, server, and agent, we need a web server such as Apache or Nginx plus a database such as Mysql or PostgreSQL. We will see how we will use all of them.

<pre class="wp-block-code"><code>dnf install zabbix-server-mysql \
zabbix-web-mysql \
zabbix-apache-conf \
zabbix-sql-scripts \
zabbix-selinux-policy \
zabbix-agent</code></pre>

### 5. Import initial databases schema {.wp-block-heading}

Use the following command to import the initial database schema. Make sure you provide the database password you created earlier on.

<pre class="wp-block-code"><code>zcat /usr/share/doc/zabbix-sql-scripts/mysql/server.sql.gz | mysql -u zabbix -p zabbix</code></pre>

### 6. Configure the database for Zabbix 6.0 server {.wp-block-heading}

Use your favorite text editor to edit this configuration file.

<pre class="wp-block-code"><code>sudo vi /etc/zabbix/zabbix_server.conf</code></pre>

Set the password on this configuration file 

<pre class="wp-block-code"><code>DBPassword=password</code></pre>

Save and exit

### 7. Start Zabbix server and agent proceses {.wp-block-heading}

use the following command to start Zabbix server and agent 

<pre class="wp-block-code"><code>systemctl restart zabbix-server zabbix-agent httpd php-fpm
systemctl enable zabbix-server zabbix-agent httpd php-fpm</code></pre>

Make sure the Zabbix is up and running. Check with the following command.

<pre class="wp-block-code"><code>systemctl status zabbix-server
● zabbix-server.service - Zabbix Server with MySQL DB
     Loaded: loaded (/usr/lib/systemd/system/zabbix-server.service; bad; vendor preset: disabled)
     Active: active (running) since Thu 2022-03-24 11:07:59 UTC; 11min ago
   Main PID: 23252 (zabbix_server)
      Tasks: 1 (limit: 1112)
     Memory: 3.0M
        CPU: 51ms
     CGroup: /system.slice/zabbix-server.service
             └─23252 /usr/sbin/zabbix_server -f

Mar 24 11:07:59 fedora systemd&#91;1]: Started Zabbix Server with MySQL DB.</code></pre>

### 8. Configure Zabbix frontend {.wp-block-heading}

To connect to the new Zabbix frontend head over to http://<your\_IP\_address>/zabbix.

You will see the following<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-38.png?resize=810%2C464&#038;ssl=1" alt="zabbix 6.0" class="wp-image-1219" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-38.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-38.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-38.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-38.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Zabbix 6.0</figcaption></figure> <figure class="wp-block-image size-large"><img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-51.png?resize=810%2C464&#038;ssl=1" alt="Zabbix validation" class="wp-image-1220" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-51.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-51.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-51.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-09-51.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>Zabbix validation</figcaption></figure> <figure class="wp-block-image size-large"><img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-11-42.png?resize=810%2C464&#038;ssl=1" alt="Zabbix congrats message" class="wp-image-1221" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-11-42.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-11-42.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-11-42.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-24-14-11-42.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>Zabbix congrats message</figcaption></figure> 

Lastly login with **Admin** as username and **zabbix** as password.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-25-12-35-38.png?resize=810%2C464&#038;ssl=1" alt="" class="wp-image-1226" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-25-12-35-38.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-25-12-35-38.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-25-12-35-38.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/03/Screenshot-from-2022-03-25-12-35-38.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Zabbix 6.0 dashboard</figcaption></figure> 

Congratulations you have installed Zabbix 6.0 on Rocky Linux 8.