---
title: How to install Apache web server on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-04-25T07:16:02+00:00
url: /2022/04/25/how-to-install-apache-web-server-on-ubuntu-22-04/
rank_math_seo_score:
  - 16
rank_math_primary_category:
  - 6
rank_math_focus_keyword:
  - install Apache on Ubuntu 22.04
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 168
categories:
  - Linux

---
Apache HTTP Server is a free and open-source cross-platform web server software. The goal of the Apache project is to provide a secure, efficient, and extensible server that provides HTTP services in sync with the current standards.

The new Apache 2.4.x or newer is needed in order to operate a TLS 1.3 web server with OpenSSL 1.1.1

In this tutorial, we will explain how to install the Apache web server on Ubuntu 22.04.

### 1. Update system repositories  {.wp-block-heading}

The first thing to do is to make sure your system repositories are up to date in order to avoid running into errors at a later stage during the installation.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Install the Apache package.  {.wp-block-heading}

Now that we have updated and upgraded our system, we now need to install Apache2 in our system with the following command:

<pre class="wp-block-code"><code>sudo apt install apache2 </code></pre>

Here apt will install apache2 with its dependencies.

<pre class="wp-block-code"><code>#sample output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  &lt;strong>apache2-bin apache2-data apache2-utils libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap&lt;/strong>
Suggested packages:
  apache2-doc apache2-suexec-pristine | apache2-suexec-custom www-browser
The following NEW packages will be installed:
  apache2 apache2-bin apache2-data apache2-utils libapr1 libaprutil1
  libaprutil1-dbd-sqlite3 libaprutil1-ldap
0 upgraded, 8 newly installed, 0 to remove and 0 not upgraded.
Need to get 1,915 kB of archives.
After this operation, 7,686 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

After the installation is complete, lets check the status of our Apache2 server.

To check the status of Apache2 server use the following command:

<pre class="wp-block-code"><code>sudo systemctl status apache2 
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor prese&gt;
     Active: active (running) since Mon 2022-04-25 09:19:04 EAT; 5min ago
       Docs: https://httpd.apache.org/docs/2.4/
   Main PID: 20151 (apache2)
      Tasks: 55 (limit: 9355)
     Memory: 5.3M
        CPU: 49ms
     CGroup: /system.slice/apache2.service
             ├─20151 /usr/sbin/apache2 -k start
             ├─20152 /usr/sbin/apache2 -k start
             └─20153 /usr/sbin/apache2 -k start

Apr 25 09:19:04 zx-pc systemd&#91;1]: Starting The Apache HTTP Server...
Apr 25 09:19:04 zx-pc apachectl&#91;20147]: AH00558: apache2: Could not reliably de&gt;
Apr 25 09:19:04 zx-pc systemd&#91;1]: Started The Apache HTTP Server.
lines 1-16/16 (END)</code></pre>

From the above output, the Apache web server started out successfully, but the best way to test out is to request a page from Apache itself. 

In order to know the IP address of your server host, you can do the following on your terminal.

<pre class="wp-block-code"><code>hostname -I</code></pre>



Here you will get some addresses to try out.

Another way is to use icanhazip tool which gives us the address also to use in our browser.

<pre class="wp-block-code"><code>curl -4 icanhazip.com</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="790" height="614" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-25-09-39-35.png?resize=790%2C614&#038;ssl=1" alt="Apache2" class="wp-image-1311" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-25-09-39-35.png?w=790&ssl=1 790w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-25-09-39-35.png?resize=300%2C233&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-25-09-39-35.png?resize=768%2C597&ssl=1 768w" sizes="(max-width: 790px) 100vw, 790px" data-recalc-dims="1" /> <figcaption>Apache2</figcaption></figure> 

### 3. Enable firewall  {.wp-block-heading}

For good security practices, we need to enable a firewall in order to block unauthorized ports from accessing the system. Apache register itself with **UFW** upon installation. To list all available ufw profiles use the following command on your terminal.

<pre class="wp-block-code"><code>&lt;strong>sudo ufw app list&lt;/strong>
Available applications:
  Apache
  Apache Full
  Apache Secure
  CUPS</code></pre>

Here we only need to allow traffic on Apache only, that is port 80. To allow so do the following:

<pre class="wp-block-code"><code>sudo ufw allow "Apache"</code></pre>

Lastly, check the status again

<pre class="wp-block-code"><code>sudo ufw status </code></pre>

### 4. Apache processes. {.wp-block-heading}

Whenever you are running apache, take into consideration the following processes, it will help you and save you time.

To start your web server

<pre class="wp-block-code"><code>sudo systemctl start apache2 </code></pre>

To stop web server

<pre class="wp-block-code"><code>sudo systemctl stop apache2</code></pre>

To restart Apache web server

<pre class="wp-block-code"><code>sudo systemctl restart apache2</code></pre>

To reload Apache web server

<pre class="wp-block-code"><code>sudo systemctl reload apache2</code></pre>

To disable Apache web server

<pre class="wp-block-code"><code>sudo systemctl disable apache2</code></pre>

To re-enable Apache after disabling it.

<pre class="wp-block-code"><code>sudo systemctl enable apache2</code></pre>

This will allow Apache2 to start automatically on boot.

## Conclusion {.wp-block-heading}

We have installed Apache2 successfully on Ubuntu 22.04. In case of any problem consult the <a href="https://httpd.apache.org/" target="_blank" rel="noreferrer noopener">Apache documentation</a>.