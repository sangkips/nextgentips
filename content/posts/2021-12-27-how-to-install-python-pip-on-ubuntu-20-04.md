---
title: How to install Python pip on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-27T08:19:14+00:00
url: /2021/12/27/how-to-install-python-pip-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"66.249.69.149";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 49
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
[Pip][1]&nbsp;is a package management system written in Python and is used to install and manage software packages. It connects to an online repository of public packages called the Python Package Index. It can also be configured to connect to other package repositories.

Usually, Pip is automatically installed while working in a virtual environment, and also if you are using Python that has not been modified by the redistributor to remove ensure pip.

Pip is recommended tool for installing Python packages. For you to start using pip, you need to have python installed on your system.

## Related Articles  {.wp-block-heading}

  * [How to install Python pip 21 on Ubuntu 21.10][2]

## Prerequisites  {.wp-block-heading}

  * Have a virtual environment up and running 
  * Make sure you are running Ubuntu 20.04 server/workstation
  * Have a basic knowledge of the terminal.

## Installing pip on Ubuntu 20.04 {.wp-block-heading}

You can install pip either through a virtual environment or while installing Python. It depends on which one you prefer. I will be using a virtual environment to install pip 21 on Ubuntu 20.04.

## 1. Run system updates  {.wp-block-heading}

First, we need to update our repositories to make them up to date by issuing update and upgrade commands on our terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Create a virtual environment  {.wp-block-heading}

A virtual environment is used to manage Python packages for different projects. It creates an isolated environment for pip packages.

Begin by creating a directory where your virtual environment will be located.

<pre class="wp-block-code"><code>$ sudo mkdir nextgentips
$ cd nextgentips</code></pre>

Next, create a virtual environment 

<pre class="wp-block-code"><code>$ python3 -m venv nextvenv</code></pre>

**Nextvenv** is the name given to a virtual environment.

In case you encounter the following error, follow the instruction in order to make necessary changes.

<pre class="wp-block-syntaxhighlighter-code">The virtual environment was not created successfully because ensurepip is not
available.  On Debian/Ubuntu systems, you need to install the python3-venv</pre>

Run the following command to correct the following error.

<pre class="wp-block-syntaxhighlighter-code">$ sudo apt install python3.8-venv</pre>

The following command will install python 3.8 venv.

<pre class="wp-block-code"><code>Output
The following NEW packages will be installed:
  python3.8-venv
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 5444 B of archives.
After this operation, 27.6 kB of additional disk space will be used.
Get:1 http://mirrors.digitalocean.com/ubuntu focal-updates/universe amd64 python3.8-venv amd64 3.8.10-0ubuntu1~20.04.2 &#91;5444 B]
Fetched 5444 B in 0s (108 kB/s)    
Selecting previously unselected package python3.8-venv.
(Reading database ... 95320 files and directories currently installed.)
Preparing to unpack .../python3.8-venv_3.8.10-0ubuntu1~20.04.2_amd64.deb ...
Unpacking python3.8-venv (3.8.10-0ubuntu1~20.04.2) ...
Setting up python3.8-venv (3.8.10-0ubuntu1~20.04.2) ...</code></pre>

Press Y to allow installation to continue.

Go ahead and run this command again&nbsp;**python3 -m venv nextvenv**, this time it will run successfully.

<pre class="wp-block-syntaxhighlighter-code">$ python3 -m venv nextvenv</pre>

To start using the virtual environment, run the following command to activate the virtual environment.

<pre class="wp-block-syntaxhighlighter-code">$ source nextvenv/bin/activate</pre>

When you see something like this **(nextvenv) root@ubuntu:~/nextgentips#**, you know that you have activated the virtual environment successfully.

## 3. Upgrade pip to latest version {.wp-block-heading}

The installation will ask you to upgrade to the latest pip. To run upgrade use the following command;

<pre class="wp-block-code"><code>$ python -m pip install --upgrade pip</code></pre>

<pre class="wp-block-code"><code>Output
Collecting pip
  Downloading pip-21.3.1-py3-none-any.whl (1.7 MB)
     |████████████████████████████████| 1.7 MB 24.8 MB/s 
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 20.0.2
    Uninstalling pip-20.0.2:
      Successfully uninstalled pip-20.0.2
&lt;strong>Successfully installed pip-21.3.1&lt;/strong></code></pre>

## Conclusion {.wp-block-heading}

We have learned how to install pip on Ubuntu 20.04. I am glad you have enjoyed the tutorial. Feel free to comment in case you are faced with a challenge.

 [1]: https://pypi.org/project/pip/
 [2]: https://nextgentips.com/2021/12/07/how-to-install-python-pip-21-on-ubuntu-21-10/