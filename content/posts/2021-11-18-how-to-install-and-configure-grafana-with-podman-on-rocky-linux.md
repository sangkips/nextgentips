---
title: How to install and configure Grafana with Podman on Rocky LInux.
author: Kipkoech Sang
type: post
date: 2021-11-18T12:34:43+00:00
url: /2021/11/18/how-to-install-and-configure-grafana-with-podman-on-rocky-linux/
ss_ss_click_share_count_twitter:
  - 1
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"18.234.30.224";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 97
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
In this tutorial guide we are going to install and configure Grafana with Podman on a Rocky LInux server.

Grafana is a complete observability stack that allows you to monitor and analyze logs, metrics and traces.

Grafana allows you to query, visualize, alert on and understand your data insight. Grafana can create, explore and create beautiful dashboards that can be shared with your teams.

Podman is a daemonless, open source, Linux native tool designed to make it easy to find, run, build, share and deploy applications using Open Container Initiative (OCI) containers and container images. Containers can be run as root or as a regular user.

## Related Content  {.wp-block-heading}

  1. [How to install Grafana 8 on Ubuntu 21.04][1]
  2. [How to install Podman 3 on Debian 11][2]
  3. [How to Install MongoDB with Podman on Rocky Linux 8][3]



## Prerequisites  {.wp-block-heading}

  * Rocky Linux server
  * User with sudo privileges 
  * Have Podman running on your system.

## Table of Contents  {.wp-block-heading}

  1. Update your Rocky Linux repositories
  2. Install Podman
  3. Pull Grafana from the container registry
  4. Create Grafana container 
  5. Run Tests to confirm installation
  6. Run Grafana in web browser
  7. Configuring Grafana web interface
  8. Conclusion.

## 1. Update Rocky Linux Repositories  {.wp-block-heading}

To begin our installation we need to update our Rocky Linux repositories in order to update any changes which might have occured with the operating System. Use the update command below;

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When updates are complete, you can proceed to install podman into our system to enable us create containers.

## 2. Install Podman {.wp-block-heading}

In order for us to create Grafana container, we are using Podman container engine to accomplish this. To install Podman running the following command in your terminal;

<pre class="wp-block-code"><code>$ sudo dnf install podman -y</code></pre>

Sample output will look like this;

<pre class="wp-block-code"><code>Installing:
 podman                           x86_64      3.3.1-9.module+el8.5.0+710+4c471e88           appstream       12 M
Installing dependencies:
 conmon                           x86_64      2:2.0.29-1.module+el8.5.0+710+4c471e88        appstream       51 k
 container-selinux                noarch      2:2.167.0-1.module+el8.5.0+710+4c471e88       appstream       53 k
 containernetworking-plugins      x86_64      1.0.0-1.module+el8.5.0+710+4c471e88           appstream       19 M
 containers-common                noarch      2:1-2.module+el8.5.0+710+4c471e88             appstream       78 k
 criu                             x86_64      3.15-3.module+el8.5.0+710+4c471e88            appstream      517 k
 fuse-common                      x86_64      3.2.1-12.el8                                  baseos          20 k
 fuse-overlayfs                   x86_64      1.7.1-1.module+el8.5.0+710+4c471e88           appstream       71 k
 fuse3                            x86_64      3.2.1-12.el8                                  baseos          49 k
 fuse3-libs                       x86_64      3.2.1-12.el8                                  baseos          93 k
 iptables                         x86_64      1.8.4-20.el8                                  baseos         584 k
 libnet                           x86_64      1.1.6-15.el8                                  appstream       66 k
 libnetfilter_conntrack           x86_64      1.0.6-5.el8                                   baseos          63 k
 libnfnetlink                     x86_64      1.0.1-13.el8                                  baseos          32 k
 libnftnl                         x86_64      1.1.5-4.el8                                   baseos          82 k
 libslirp                         x86_64      4.4.0-1.module+el8.5.0+710+4c471e88           appstream       69 k
 nftables                         x86_64      1:0.9.3-21.el8                                baseos         320 k
 podman-catatonit                 x86_64      3.3.1-9.module+el8.5.0+710+4c471e88           appstream      339 k
 protobuf-c                       x86_64      1.3.0-6.el8                                   appstream       36 k
 runc                             x86_64      1.0.2-1.module+el8.5.0+710+4c471e88           appstream      3.1 M
 slirp4netns                      x86_64      1.1.8-1.module+el8.5.0+710+4c471e88           appstream       50 k
Enabling module streams:
 container-tools                              rhel8                                                             
</code></pre>

To be sure that Podman have been installed, run the following command. It will give you the Podman version you are running;

<pre class="wp-block-code"><code>$ podman -v
podman version 3.3.1</code></pre>

From the time of this writing Podman 3.3.1 is the latest release.

When installation is complete you can proceed to pull Grafana from the Podman container registries.

## 3. Pull Grafana Image {.wp-block-heading}

To pull Grafana we use **podman pull grafana** command, it will ask you where you want to pull the image from as shown below.

<pre class="wp-block-code"><code>$ podman pull grafana</code></pre>

Choose where you would like to pull your image from as shown;

<pre class="wp-block-code"><code>? Please select an image: 
  ▸ registry.fedoraproject.org/grafana:latest
    registry.access.redhat.com/grafana:latest
    registry.centos.org/grafana:latest
    docker.io/library/grafana:latest</code></pre>

For my case I have selected fedora project, you can pull from any of the above. Press enter and you will see the following;

<pre class="wp-block-code"><code># podman pull grafana 
✔ registry.fedoraproject.org/grafana:latest
Trying to pull registry.fedoraproject.org/grafana:latest...
Getting image source signatures
Copying blob d2e5db905089 done  
Copying blob 7e7f471a8c74 done  
Copying config a7b3912622 done  
Writing manifest to image destination
Storing signatures
a7b391262240741224d21f5dce7f50925e53291d42750303ce7cd635e7f2eb5e</code></pre>

From here we have pulled the grafana image, we can check if we have really pull the image with the following command;

<pre class="wp-block-code"><code>$ podman images 
REPOSITORY                          TAG         IMAGE ID      CREATED       SIZE
registry.fedoraproject.org/grafana  latest      a7b391262240  4 months ago  420 MB</code></pre>

## 4. Create Grafana container  {.wp-block-heading}

We can now create our Grafana container after you have successfully pulled the image. To create the container we use the following command but first we need to create a directory where we can store Grafana data so that incase you delete the image by mistake the data will still be available.

<pre class="wp-block-code"><code>$ sudo mkdir grafana_data
$ cd grafana_data</code></pre>

Once inside the directory, we can run run our container with [**podman run**][4] command.

<pre class="wp-block-code"><code>$ podman run --name nextgentipsgrafana -d -p 3000:3000 -v $(pwd):/grafana_data/db:Z grafana 

ed7db43a447ff7b447faf5ca8ff1d5cd335242622613dc2ec72ae97adc5182c6</code></pre>

Let&#8217;s analyse the command in detail;

  * **–name:** describe the name of the project i.e nextgentipsgrafana
  * &#8211;**d:** run your container in detached mode i.e in the background
  * **-p 3000:3000**: map port 3000 of the host to port 3000 on the container.
  * &#8211;**v (volume)**: create persisting data generated and used by podman containers.
  * **$(pwd):/grafana_data/db**: shows the directory to to store your data.
  * **Z**– tells podman that the volume content will be shared between containers

## 5. Run tests {.wp-block-heading}

To check if our container is set up correctly, we can run tests, if your tests can run without any error then the container was created successfully.

<pre class="wp-block-code"><code>$ podman run --rm -it grafana</code></pre>

Sample output from the test will look like this;

<pre class="wp-block-code"><code>INFO&#91;11-18|12:04:39] Registering plugin                       logger=plugins id=pcp-redis-datasource
INFO&#91;11-18|12:04:39] Registering plugin                       logger=plugins id=pcp-vector-datasource
INFO&#91;11-18|12:04:39] Registering plugin                       logger=plugins id=pcp-breadcrumbs-panel
INFO&#91;11-18|12:04:39] Registering plugin                       logger=plugins id=pcp-flamegraph-panel
INFO&#91;11-18|12:04:39] Registering plugin                       logger=plugins id=pcp-notifygraph-panel
INFO&#91;11-18|12:04:39] HTTP Server Listen                       logger=http.server address=&#91;::]:3000 protocol=http subUrl= socket=</code></pre>

## 6. Start Grafana from the browser {.wp-block-heading}

Having successfully created the container, we now need to access our garafana from the browser.

To begin,choose your preferred browser and type the following

<pre class="wp-block-code"><code>http://&lt;your_IP_Address>:3000</code></pre>

3000 is the default port number for grafana. The default **username** and **password** is **admin**. You will have to change the password after your first login.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="442" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-13-53-31-1.png?resize=810%2C442&#038;ssl=1" alt="Grafana interface" class="wp-image-642" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-13-53-31-1.png?resize=1024%2C559&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-13-53-31-1.png?resize=300%2C164&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-13-53-31-1.png?resize=768%2C420&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-13-53-31-1.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Grafana interface</figcaption></figure> 

## 7. Configuring Grafana web interface  {.wp-block-heading}

To do Grafana configuration, create a new dashboard by clicking the plus sign on your left hand side then create panel to create graphical user interface as shown.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="445" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-15-21-07.png?resize=810%2C445&#038;ssl=1" alt="Grafana Dashboard" class="wp-image-643" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-15-21-07.png?resize=1024%2C563&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-15-21-07.png?resize=300%2C165&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-15-21-07.png?resize=768%2C423&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-18-15-21-07.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Grafana Dashboard</figcaption></figure> 

## 8. Conclusion {.wp-block-heading}

We have successfully installed Grafana with Podman on Rocky Linux server. Continue experimenting with the interface until you are conversant with it.

 [1]: https://nextgentips.com/2021/10/30/how-to-install-grafana-8-on-ubuntu-21-04/
 [2]: https://nextgentips.com/2021/10/22/how-to-install-podman-3-on-debian-11/
 [3]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-with-podman-on-rocky-linux-8/
 [4]: https://docs.podman.io/en/latest/markdown/podman-run.1.html