---
title: How to install Python Django in Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-11-04T13:02:21+00:00
url: /2021/11/04/how-to-install-python-django-in-ubuntu-21-10/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:10:"77.88.5.46";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 111
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
Django is Python-based free and open source web framework that allows model-template-view architectural patterns. Django encourages rapid development and clean and pragmatic codes. It takes care of much of the hassle of web development so that you can focus on the code without reinventing the wheel.

  * Django is exceedingly fast as the developers was focusing on quick way to release the project as fast as possible. 
  * Django is scalable. Building apps in Django makes it easy to scale as the traffic increases saving you on the hassle of adding additional server resources to accommodate the surge in traffic.
  * Django takes care of security seriously. It has different ways of authentication which makes it hard for intruders to penetrate.

## Prerequisites  {.wp-block-heading}

  * Python 3
  * Virtualenv 
  * Pip
  * [psycopg2][1] if you are using PostgreSQL
  * &nbsp;[DB API driver][2] like msqlclient if you are planning on MariaDB
  * Database such as Postgresql, MariaDB, SQLite if planning on large scale production code.
  * Basic programming skills

## Related Content  {.wp-block-heading}

  * [How to install PostgreSQL 14 on Ubuntu 20.04][3]
  * [How to install MariaDB 10.7 on Ubuntu 21.04][4]

## Table of Contents {.wp-block-heading}

  * Updating our system packages 
  * Installing Python 3
  * Installing Virtualenv 
      * Installing Pip

## Updating Ubuntu 21.10 packages  {.wp-block-heading}

First we need to update and upgrade our Ubuntu repository so that all packages are up to date I. We can use the following command to do so.

<pre class="wp-block-code"><code>$ sudo apt update
$ sduo apt upgrade -y</code></pre>

When both update and upgrade are complete then we can go ahead and start our installation of Python 3.

## Installing Python 3 {.wp-block-heading}

Django 2 doesn&#8217;t support Python 2 anymore that is why we need to install python 3 in our system. So let&#8217;s install our latest version of Python 3.10.0. Use the following comand to install Python

<pre class="wp-block-code"><code>$ sudo apt install python3.10.0</code></pre>

Sample output

<pre class="wp-block-code"><code>....
The following additional packages will be installed:
  bzip2 libpython3.10-minimal libpython3.10-stdlib mailcap mime-support python3.10-minimal
Suggested packages:
  bzip2-doc python3.10-venv python3.10-doc binfmt-support
The following NEW packages will be installed:
  bzip2 libpython3.10-minimal libpython3.10-stdlib mailcap mime-support python3.10 python3.10-minimal
0 upgraded, 7 newly installed, 0 to remove and 0 not upgraded.
....</code></pre>

Press Y to continue with the installation. After the process is complete, you would have installed Python 3.10 minimal. You can check the version of installed Python with the following command.

<pre class="wp-block-code"><code>$ python3.10 --version
Python 3.10.0</code></pre>

If you happen to check your installed version with **pyhton3 &#8211;version**, you will get the results of the preinstalled version, you don&#8217;t need to remove it from the system, just leave it because it is part of the system properties.

<pre class="wp-block-code"><code># python3 --version
Python 3.9.7</code></pre>

## 2. Install Django Virtualenv  {.wp-block-heading}

It is important to create a virtualenv because each project you create often comes with its installed libraries, so it is important to isolate each so that dependencies would not clash at any time.

For us to create a virtualenv we need to install pip. Pip is a package installer for python. So it is necessary to have this package. Pip has two methods of installation:

  * Using ensurepip
  * Using get-pip.py

## using ensurepip to install pip {.wp-block-heading}

The simpliest way to install pip with ensurepip is to pass the following command which will install pi automatically.

<pre class="wp-block-code"><code>$ python -m ensurepip --upgrade</code></pre>

**&#8211;upgrade**. This ensures installed version of pip is the latest version.

## Using python script to install pip {.wp-block-heading}

Python script is the most convenient way to install pip in our system. It uses bootstrapping logic to install pip. We can do the following. Download the following script from:

<pre class="wp-block-code"><code># wget https://bootstrap.pypa.io/get-pip.py
--2021-11-04 11:33:10--  https://bootstrap.pypa.io/get-pip.py
Resolving bootstrap.pypa.io (bootstrap.pypa.io)... 151.101.208.175, 2a04:4e42:31::175
Connecting to bootstrap.pypa.io (bootstrap.pypa.io)|151.101.208.175|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2159352 (2.1M) &#91;text/x-python]
Saving to: ‘get-pip.py’

get-pip.py                   100%&#91;===========================================&gt;]   2.06M  --.-KB/s    in 0.009s  

2021-11-04 11:33:10 (223 MB/s) - ‘get-pip.py’ saved &#91;2159352/2159352]</code></pre>

When the script is complete then you can now install pip with the following command;

<pre class="wp-block-code"><code># sudo python3.10 get-pip.py
Collecting pip
  Downloading pip-21.3.1-py3-none-any.whl (1.7 MB)
     |████████████████████████████████| 1.7 MB 23.2 MB/s            
Collecting wheel
  Downloading wheel-0.37.0-py2.py3-none-any.whl (35 kB)
Installing collected packages: wheel, pip
Successfully installed pip-21.3.1 wheel-0.37.0
</code></pre>

You can also upgrade pip with the following command:

<pre class="wp-block-code"><code>$ python -m pip install --upgrade pip</code></pre>

It is alaso advisable to check the pip version you are running to avoid conflicts which might arise later. Do the following;

<pre class="wp-block-code"><code># pip --version
pip 21.3.1 from /usr/local/lib/python3.9/dist-packages/pip (python 3.9)</code></pre>

It is now time we install our virtualenv in our system. We can do that with the following:

<pre class="wp-block-code"><code>$ sudo apt install virtualenv</code></pre>

From now on every project you create you do it inside virtualenv. Let us first create a project folder named python_project. Inside this folder we can now install our virtualenv to work on.

<pre class="wp-block-code"><code># mkdir python_project
# cd pyhton_project</code></pre>

Inside the project folder this is where you are going to create your virtualenv. 

<pre class="wp-block-code"><code># virtualenv venv -p python3.10

created virtual environment CPython3.10.0.final.0-64 in 251ms
  creator CPython3Posix(dest=/root/python_project/venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/root/.local/share/virtualenv)
    added seed packages: pip==20.3.4, pkg_resources==0.0.0, setuptools==44.1.1, wheel==0.34.2
  activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator</code></pre>

Having created the virtualenv then we can now focus on building our project but first we need to activate our environment.

<pre class="wp-block-code"><code># source venv/bin/activate</code></pre>

You can know it is working if you can see the **venv** starting from the command line.

<pre class="wp-block-code"><code># source venv/bin/activate
(venv) root@ubuntu-21:~/python_project# </code></pre>

## Installing Django in virtualenv  {.wp-block-heading}

To now install django we can simple invoke install django like in the following code: Make sure you are doing everything inside project folder. If you have exited your virtualenv you need to activate and return to venv environment.

<pre class="wp-block-code"><code># pip install django
Collecting django
  Downloading Django-3.2.9-py3-none-any.whl (7.9 MB)
     |████████████████████████████████| 7.9 MB 29.8 MB/s 
Collecting sqlparse>=0.2.2
  Downloading sqlparse-0.4.2-py3-none-any.whl (42 kB)
     |████████████████████████████████| 42 kB 2.0 MB/s 
Collecting pytz
  Downloading pytz-2021.3-py2.py3-none-any.whl (503 kB)
     |████████████████████████████████| 503 kB 57.5 MB/s 
Collecting asgiref&lt;4,>=3.3.2
  Downloading asgiref-3.4.1-py3-none-any.whl (25 kB)
Ignoring typing-extensions: markers 'python_version &lt; "3.8"' don't match your environment
Installing collected packages: sqlparse, pytz, asgiref, django</code></pre>

## Start Django project {.wp-block-heading}

Set up is now complete. To start a new project we do the following;

<pre class="wp-block-code"><code>$ django-admin startproject python_pproject</code></pre>

## Start Django App {.wp-block-heading}

Lastly to start django apps we do the following:

<pre class="wp-block-code"><code>$ django-admin startapp &lt;app_name></code></pre>

## Conclusion {.wp-block-heading}

In the above tutorial we have successfully installed django and learn how to create project. What is remaining now is for you to practice alot in order to grasp everything django does. In case of a problem contact [Django Documentation.][5]

 [1]: https://www.psycopg.org/
 [2]: https://docs.djangoproject.com/en/3.2/ref/databases/#mysql-db-api-drivers
 [3]: https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/
 [4]: https://nextgentips.com/2021/10/15/how-to-install-mariadb-10-7-on-ubuntu-21-04/
 [5]: https://docs.djangoproject.com/en/3.2/