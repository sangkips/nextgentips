---
title: How to install Docker-ce on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-11-27T15:56:59+00:00
url: /2021/11/27/how-to-install-docker-ce-on-ubuntu-21-10/
ss_ss_click_share_count_facebook:
  - 1
rank_math_description:
  - Docker-ce is a set of platform as a service product that uses OS-level virtualization to deliver software in packages called containers.
rank_math_focus_keyword:
  - docker-ce
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 83
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Docker-ce is a set of platform as a service product that uses OS-level virtualization to deliver software in packages called containers. Containers are usually isolated from one another and bundled their own software libraries and configuration files, they can communicate with each other through well-defined channels.

In this tutorial am going to show you how you can install Docker-ce on Ubuntu 21.10.

Docker makes it possible to get more apps running on the same old servers and also makes it easy to package and ship programs.

## Related Articles  {#related-articles.wp-block-heading}

  * [How to install and use Docker-ce on Fedora 35][1]
  * [How to install Docker on Arch Linux][2]

## Prerequisites {#prerequisites.wp-block-heading}

  * Make sure you have user with sudo privileges
  * Have Ubuntu 21.10 server up and running
  * Have basic kwoledge about running commands on a terminal

## Table of Contents {#table-of-contents.wp-block-heading}

  1. Run system updates
  2. Uninstall old docker version
  3. Install Docker Engine
  4. Enable Docker Engine
  5. Start Docker
  6. Check Docker status
  7. Test Docker 
  8. Conclusion

## 1. Run system updates  {#1-run-system-updates.wp-block-heading}

We need to install recent updates into our system repositories. To do that you need to use the following command in our terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Uninstall old Docker versions  {#2-uninstall-old-docker-versions.wp-block-heading}

If you have ever installed Docker before you need to uninstall them to allow the new Docker-ce to be installed. To do uninstallation, run the following command in your terminal.

<pre class="wp-block-code"><code>$ sudo apt remove docker
Reading package lists... DoneBuilding dependency tree... DoneReading state information... Done
Package 'docker' is not installed, so not removed
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

## 3. Install Docker-ce {#3-install-docker-ce.wp-block-heading}

To begin installing docker-ce, run the following command on your terminal. It will install docker with its dependencies.

<pre class="wp-block-code"><code>$ sudo apt install docker.io</code></pre>

You will get the following sample output on your terminal.

<pre class="wp-block-code"><code>Sample output
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  bridge-utils containerd dns-root-data dnsmasq-base pigz runc ubuntu-fan
Suggested packages:
  ifupdown aufs-tools cgroupfs-mount | cgroup-lite debootstrap docker-doc rinse zfs-fuse | zfsutils
The following NEW packages will be installed:
  bridge-utils containerd dns-root-data dnsmasq-base docker.io pigz runc ubuntu-fan
0 upgraded, 8 newly installed, 0 to remove and 0 not upgraded.
Need to get 72.8 MB of archives.
After this operation, 326 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue.

Check the version of docker installed with the following command.

<pre class="wp-block-code"><code>$ docker --version
Docker version 20.10.7, build 20.10.7-0ubuntu5.1</code></pre>

## 4. Enable Docker-ce {#4-enable-docker-ce.wp-block-heading}

We need to enable docker-ce to start on reboot, in order to avoid starting every time you boot your machine.

<pre class="wp-block-code"><code>$ sudo systemctl enable docker</code></pre>

## 5. Start Docker-ce service  {#5-start-docker-ce-service.wp-block-heading}

You are supposed to start the docker service to run containerd environment. To start, use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl start docker</code></pre>

Lastly, we can check the status of docker to see if it is running.

## 6. Check status of Docker-ce {#6-check-status-of-docker-ce.wp-block-heading}

Check if Docker is running with the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl status docker</code></pre>

If you got status active, then your docker service is running as expected, if not probably you haven&#8217;t started the service.

<pre class="wp-block-code"><code>Output
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2021-11-27 14:55:28 UTC; 17min ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 10660 (dockerd)
      Tasks: 8
     Memory: 50.8M
        CPU: 601ms
     CGroup: /system.slice/docker.service
             └─10660 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.434605109Z" level=info msg="scheme \"unix\" not&gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.434914963Z" level=info msg="ccResolverWrapper: &gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.435531943Z" level=info msg="ClientConn switchin&gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.494687714Z" level=info msg="Loading containers:&gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.736014824Z" level=info msg="Default bridge (doc&gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.864815891Z" level=info msg="Loading containers:&gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.937813828Z" level=info msg="Docker daemon" comm&gt;
Nov 27 14:55:28 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:28.938397056Z" level=info msg="Daemon has complete&gt;
Nov 27 14:55:28 ubuntu systemd&#91;1]: Started Docker Application Container Engine.
Nov 27 14:55:29 ubuntu dockerd&#91;10660]: time="2021-11-27T14:55:29.017010584Z" level=info msg="API listen on /run/&gt;</code></pre>

This one of mine is running as expected.

## 7. Test Docker-ce {#7-test-docker-ce.wp-block-heading}

Let&#8217;s test our docker to see if it is running. Test using hello-world container.

<pre class="wp-block-code"><code>$ docker run hello-world 
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:cc15c5b292d8525effc0f89cb299f1804f3a725c8d05e158653a563f15e4f685
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
</code></pre>

Also, test with the following command;

<pre class="wp-block-code"><code>$ docker run -it ubuntu bash
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
7b1a6ab2e44d: Pull complete 
Digest: sha256:626ffe58f6e7566e00254b638eb7e0f3b11d4da9675088f4781a50ae288f3322
Status: Downloaded newer image for ubuntu:latest</code></pre>

It pulls Ubuntu&#8217;s latest version successfully. I can ls to see what is in that container.

<pre class="wp-block-code"><code>$ ls 
bin   dev  home  lib32  libx32  mnt  proc  run   srv  tmp  var
boot  etc  lib   lib64  media   opt  root  sbin  sys  usr</code></pre>

## 8. Conclusion {#8-conclusion.wp-block-heading}

From here you can see our Docker-CE is running as expected, Learn other docker commands to be well conversant with docker.&nbsp;[Docker Documentation&nbsp;][3]has a tone of information.

 [1]: https://nextgentips.com/2021/11/22/how-to-install-and-use-docker-ce-on-fedora-35/
 [2]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [3]: https://docs.docker.com/engine/install/