---
title: How to install MySQL 8 on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-04-30T07:00:41+00:00
url: /2022/04/30/how-to-install-mysql-8-on-ubuntu-22-04/
rank_math_seo_score:
  - 66
rank_math_focus_keyword:
  - Mysql 8
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 7
rank_math_analytic_object_id:
  - 172
categories:
  - Database

---
MySQL is an open-source relational database management system. Relational databases organize data into one or more data tables in which data types may be related to each other. Relations help structure data. MySQL has stand-alone clients that allow users to interact directly with a MySQL database using SQL.

MySQL is part of the LAMP stack component which stands for Linux, Apache/Nginx, MySQL/MariaDB, and PHP/Python/Perl. In this tutorial, I will show you how to install MySQL 8 on Ubuntu 22.04.

## Installing MySQL 8 on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Ensure that your repositories are up to date in order to avoid running into unwarranted errors during the installation.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Set MySQL repository {.wp-block-heading}

Ubuntu comes with default MySQL packages. So to install the latest version we are going to download the packages with the following command.

<pre class="wp-block-code"><code>curl -LO https://dev.mysql.com/get/mysql-apt-config_0.8.20-1_all.deb</code></pre>

Now that the download is complete, we need to install the .deb file in our system. Use the following command to install. To check the downloaded file, do an **ls** 

<pre class="wp-block-code"><code>sudo dpkg -i mysql-apt-config_0.8.20-1_all.deb</code></pre>

It will open a configuration window where you will be required to choose the version of MySQL to install.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="208" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-16-52.png?resize=810%2C208&#038;ssl=1" alt="Nextgentips-MySQL config window" class="wp-image-1324" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-16-52.png?resize=1024%2C263&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-16-52.png?resize=300%2C77&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-16-52.png?resize=768%2C197&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-16-52.png?w=1226&ssl=1 1226w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips-MySQL config window</figcaption></figure> 

Choose Ubuntu focal and press ok. Next you will see the something like the below screenshot.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="208" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-22-16.png?resize=810%2C208&#038;ssl=1" alt="Nextgentips-MySQL apt config" class="wp-image-1325" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-22-16.png?resize=1024%2C263&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-22-16.png?resize=300%2C77&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-22-16.png?resize=768%2C197&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-30-09-22-16.png?w=1226&ssl=1 1226w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips-MySQL apt config</figcaption></figure> 

Choose MySQL Server and cluster and press enter to move to the next step. The next step choose MySQL 8.0 

We can check if MySQL 8.0 is available with the following command 

<pre class="wp-block-code"><code>$ sudo apt-cache policy mysql-server
mysql-server:
  Installed: (none)
  Candidate: 8.0.28-0ubuntu4
  Version table:
     8.0.28-0ubuntu4 500
        500 http://mirrors.digitalocean.com/ubuntu jammy/main amd64 Packages</code></pre>

### 3. Install MySQL 8.0 on Ubuntu 22.04 {.wp-block-heading}

Let&#8217;s now proceed to install MySQL 8 on our system using the following command.

<pre class="wp-block-code"><code>sudo apt install mysql-server -y</code></pre>

You will see the following sample output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  libcgi-fast-perl libcgi-pm-perl libclone-perl libencode-locale-perl libevent-pthreads-2.1-7 libfcgi-bin libfcgi-perl libfcgi0ldbl
  libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl
  liblwp-mediatypes-perl libmecab2 libprotobuf-lite23 libtimedate-perl liburi-perl mecab-ipadic mecab-ipadic-utf8 mecab-utils
  &lt;strong>mysql-client-8.0 mysql-client-core-8.0 mysql-common mysql-server-8.0 mysql-server-core-8.0&lt;/strong>
Suggested packages:
  libdata-dump-perl libipc-sharedcache-perl libbusiness-isbn-perl libwww-perl mailx tinyca
The following NEW packages will be installed:
  libcgi-fast-perl libcgi-pm-perl libclone-perl libencode-locale-perl libevent-pthreads-2.1-7 libfcgi-bin libfcgi-perl libfcgi0ldbl
  libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl
  liblwp-mediatypes-perl libmecab2 libprotobuf-lite23 libtimedate-perl liburi-perl mecab-ipadic mecab-ipadic-utf8 mecab-utils
  mysql-client-8.0 mysql-client-core-8.0 mysql-common mysql-server mysql-server-8.0 mysql-server-core-8.0
0 upgraded, 28 newly installed, 0 to remove and 0 not upgraded.
Need to get 29.0 MB of archives.</code></pre>

You can check if MySQL have been installed again.

<pre class="wp-block-code"><code>$ apt-cache policy mysql-server
mysql-server:
  &lt;strong>Installed: 8.0.28-0ubuntu4&lt;/strong>
  Candidate: 8.0.28-0ubuntu4
  Version table:
 *** 8.0.28-0ubuntu4 500
        500 http://mirrors.digitalocean.com/ubuntu jammy/main amd64 Packages
        100 /var/lib/dpkg/status</code></pre>

### 4. Secure MySQL installation  {.wp-block-heading}

We need to secure installed MySQL by running the following command.

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation

Securing the MySQL server deployment.

Connecting to MySQL using a blank password.

VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No: y 

There are three levels of password validation policy:

LOW    Length >= 8
MEDIUM Length >= 8, numeric, mixed case, and special characters
STRONG Length >= 8, numeric, mixed case, special characters and dictionary                  file

Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 0
Please set the password for root here.

New password: 

Re-enter new password: 

Estimated strength of the password: 50 
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : y
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y
Success.

By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
Success.

All done!</code></pre>

### 5. Configuring MySQL services {.wp-block-heading}

We can enable MySQL to start automatically whenever we reboot our server. We do so with the following command.

<pre class="wp-block-code"><code>sudo systemctl enable mysql</code></pre>

To check the status of MySQL we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status mysql 
● mysql.service - MySQL Community Server
     Loaded: loaded (/lib/systemd/system/mysql.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2022-04-30 06:36:22 UTC; 18min ago
   Main PID: 2327 (mysqld)
     Status: "Server is operational"
      Tasks: 38 (limit: 1119)
     Memory: 353.1M
        CPU: 2.956s
     CGroup: /system.slice/mysql.service
             └─2327 /usr/sbin/mysqld

Apr 30 06:36:21 ubuntu systemd&#91;1]: Starting MySQL Community Server...
Apr 30 06:36:22 ubuntu systemd&#91;1]: Started MySQL Community Server.</code></pre>

To start MySQL use the following command.

<pre class="wp-block-code"><code>sudo systemctl start mysql</code></pre>

To stop the MySQL service use:

<pre class="wp-block-code"><code>sudo systemctl stop mysql</code></pre>

And to restart MySQL again use.

<pre class="wp-block-code"><code>sudo systemctl restart mysql</code></pre>

To login to MySQL we type the following 

<pre class="wp-block-code"><code>mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.28-0ubuntu4 (Ubuntu)

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> </code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed MySQL 8.0 on Ubuntu 22.04.