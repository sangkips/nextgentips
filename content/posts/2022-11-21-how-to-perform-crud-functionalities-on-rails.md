---
title: How to perform CRUD functionalities on Rails
author: Kipkoech Sang
type: post
date: 2022-11-21T12:22:48+00:00
url: /2022/11/21/how-to-perform-crud-functionalities-on-rails/
categories:
  - Programming

---
Rails CRUD processes is very easy to create, this makes it easy to work on something else. Compare to Django CRUD operation, I can say for Django you need to have sort of a hacking mentality to go through it, but for Rails its just one command and everything is created on a fly.

## Related Articles {.wp-block-heading}

  * <a href="https://nextgentips.com/2022/11/16/how-to-install-ruby-on-manjaro-linux/" target="_blank" rel="noopener" title="">How to install Ruby on Manjaro Linux</a>
  * [How to install Rails 7 on Manjaro Linux][1]

### 1. Creating a Rails project {.wp-block-heading}

To create a new Rails project template, create a directory where you want to store your project. Cd into that project and start the new Rails project with the following command.

<pre class="wp-block-code"><code>rails new &lt;project_name></code></pre>

Wait for it to create a project template for you, if you run into problems its mostly because of the sqlite3 database. One way or the other you will run into problems, follow the error messages to solve the given error.

To know if you have successfully created a Rails project, run the server with `<mark style="background-color:#abb8c3" class="has-inline-color">rails s</mark>` command.

<pre class="wp-block-code"><code>rails s</code></pre>

In your terminal you will be in a position to see the following:

<pre class="wp-block-code"><code>=> Booting Puma
=> Rails 7.0.4 application starting in development 
=> Run `bin/rails server --help` for more startup options
Puma starting in single mode...
* Puma version: 5.6.5 (ruby 3.0.4-p208) ("Birdie's Version")
*  Min threads: 5
*  Max threads: 5
*  Environment: development
*          PID: 66980
* Listening on http://127.0.0.1:3000
* Listening on http://&#91;::1]:3000
Use Ctrl-C to stop</code></pre>

### 2. Creating Rails web page {.wp-block-heading}

To create web pages in Rails, we use Model View Controllers (MVC). The views is what gives the UI,controllers is what determine the pages to be rendered, and models is used to interact with the database. To create a rails webpage we `<mark style="background-color:#abb8c3" class="has-inline-color">rails generator</mark>` command.

<pre class="wp-block-code"><code>rails g controller home index</code></pre>

To check what has been created, start rails server and head to your browser and type the following:

<pre class="wp-block-code"><code>localhost:3000/home/index</code></pre>

You will be greeted with homepage showing `<mark style="background-color:#abb8c3" class="has-inline-color">Home#index</mark>`. For me this homepage route is long, what we should do is whenever we hit localhost:3000, it automatically takes me to the homepage. Let me show you how to do that. 

Go to config folder and look for `<mark style="background-color:#abb8c3" class="has-inline-color">routes.rb</mark>` and add the following and make sure you comment out the default route `<mark style="background-color:#abb8c3" class="has-inline-color">get 'home/index</mark>`.

<pre class="wp-block-code"><code>#get 'home/index
root 'home#index</code></pre>

You are now in a position to hit your homepage directly.

You can also check to see your current routes you have created with `<mark style="background-color:#abb8c3" class="has-inline-color">rails routes</mark>` command. 

<pre class="wp-block-code"><code>rails routes </code></pre>

### 3. Creating CRUD functionalities {.wp-block-heading}

CRUD means create, read, update and delete. Its one of the main functionalities of any website. You need users to view your products, you need to add items to the database, you sometimes want to update the given product or you sometimes wants to remove a certain item from the database. All these happens with CRUD ability. So lets see how to perform crud in rails.

To enable crud, we use `<mark style="background-color:#abb8c3" class="has-inline-color">rails g scaffold</mark>` command. So lets start creating for our project.

<pre class="wp-block-code"><code>rails g scaffold products name:string description: string price:string quantity: string</code></pre>

Once you run the following command, you can see something like this:

<pre class="wp-block-code"><code>....
invoke  active_record
      create    db/migrate/20221121115223_create_products.rb
      create    app/models/product.rb
      invoke    test_unit
      create      test/models/product_test.rb
      create      test/fixtures/products.yml
      invoke  resource_route
       route    resources :products
      invoke  scaffold_controller
      create    app/controllers/products_controller.rb
      invoke    erb
....</code></pre>

After this, we need to create a database schema. To achieve this, we need push our migrations to the database with the following command `<mark style="background-color:#abb8c3" class="has-inline-color">rails db:migrate</mark>` command 

<pre class="wp-block-code"><code>rails db:migrate</code></pre>

Run your server again to see what had been added.

If you head into `home/products` directory on your browser you will see the following:<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="358" height="224" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-21-15-10-51.png?resize=358%2C224&#038;ssl=1" alt="Nextgentips:products" class="wp-image-1662" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-21-15-10-51.png?w=358&ssl=1 358w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-21-15-10-51.png?resize=300%2C188&ssl=1 300w" sizes="(max-width: 358px) 100vw, 358px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips:products</figcaption></figure> 

So now you are in a position to create new products. Hit new product and you will see a nicely build form.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="295" height="474" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-21-15-12-52.png?resize=295%2C474&#038;ssl=1" alt="Nextgentips:Create product" class="wp-image-1663" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-21-15-12-52.png?w=295&ssl=1 295w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-21-15-12-52.png?resize=187%2C300&ssl=1 187w" sizes="(max-width: 295px) 100vw, 295px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips:Create product</figcaption></figure> 

From here now you can test all the CRUD functionalities. I hope you enjoy hacking the rails development. Happy coding

 [1]: https://nextgentips.com/2022/11/17/how-to-install-rails-7-on-manjaro-linux/