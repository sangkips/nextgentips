---
title: How to install Terraform on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-30T17:18:26+00:00
url: /2021/12/30/how-to-install-terraform-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 45
rank_math_internal_links_processed:
  - 1
categories:
  - Automation

---
Terraform is an open-source infrastructure as code software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files. 

In this tutorial, we are going to learn how to install Terraform on Ubuntu 20.04. 

Infrastructure as code (IAC) tools allow you to manage your infrastructure with a configuration file rather than through a graphical interface. IAC allows you to build, change and manage your infrastructure in a safe, consistent, and repeatable way by defining configurations that you can version, reuse and share.

## Advantages of Using Terraform  {.wp-block-heading}

  * Terraform can manage infrastructure on multiple cloud platforms.
  * The human-readable form helps you write infrastructure code quickly.
  * Terraforms state helps you to track resource changes throughout your deployments.
  * You can commit your configurations to version control to safely collaborate on infrastructure.

## Related Articles  {.wp-block-heading}

  * [How to install Terraform on Debian 11][1]

## Prerequisites  {.wp-block-heading}

  * Have Ubuntu 20.04 up and running 
  * Make sure you have basic knowledge of terminal.
  * Have a user with sudo privileges 

## Install Terraform on Ubuntu 20.04 {.wp-block-heading}

Before we can install Terraform, make sure your system is up to date, You have GnuPG, software-properties-common, and curl packages installed. 

## 1. Run system updates {.wp-block-heading}

Begin by updating your system repositories to make them up to date. Use the following command to run system updates. 

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y </code></pre>

## 2. Install Terraform Package dependencies  {.wp-block-heading}

Install the following dependencies, software-properties-common, GnuPG, and curl on your system using the following command.

<pre class="wp-block-code"><code>$ sudo apt install -y gnupg software-properties-common curl</code></pre>

It will not installed any packages because Ubuntu 20.04 comes preinstalled with the following packages. So what you can do is just update them.

## 3. Add Hashicorp GPG key {.wp-block-heading}

Next in line is to add Hashicorp GPG key into our system. We are going to install using the curl command we installed earlier. 

<pre class="wp-block-code"><code>$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -</code></pre>

## 4. Add Hashicorp repository to Ubuntu 20.04 {.wp-block-heading}

Now we need to tell our system where to get installation packages from and that is by adding Hashicorp repository to our system repositories. To add repositories, we need to use the following command.

<pre class="wp-block-code"><code>$ sudo apt-add-repository "deb &#91;arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"</code></pre>

Check what has been added in your system. See below output

<pre class="wp-block-code"><code>Output
&lt;strong>Get:1 https://apt.releases.hashicorp.com focal InRelease &#91;9495 B]&lt;/strong>
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                    
Hit:4 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease                             
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease     
Hit:6 http://security.ubuntu.com/ubuntu focal-security InRelease
&lt;strong>Get:7 https://apt.releases.hashicorp.com focal/main amd64 Packages &#91;41.1 kB]&lt;/strong>
Fetched 50.6 kB in 1s (65.6 kB/s) 
Reading package lists... Done</code></pre>

Lastly is to update our system repositories again for changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

## 5. Install Terraform on Ubuntu 20.04 {.wp-block-heading}

After we have met all the requirements, we can now run terraform installation with ease. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install terraform</code></pre>

Sample output will be like this;

<pre class="wp-block-code"><code>Output
The following NEW packages will be installed:
  &lt;strong>terraform&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 18.7 MB of archives.
After this operation, 62.0 MB of additional disk space will be used.
Get:1 https://apt.releases.hashicorp.com focal/main amd64 terraform amd64 1.1.2 &#91;18.7 MB]
Fetched 18.7 MB in 0s (61.8 MB/s)
Selecting previously unselected package terraform.
(Reading database ... 94647 files and directories currently installed.)
Preparing to unpack .../terraform_1.1.2_amd64.deb ...
Unpacking terraform (1.1.2) ...
Setting up terraform (1.1.2) ...</code></pre>

Now that we have installed Terraform, we can verify the installation with the following command;

<pre class="wp-block-code"><code>$ terraform -help init</code></pre>

Let&#8217;s install a package using Terraform to see if it&#8217;s actually working. 

First let&#8217;s enable tab autocompletion 

<pre class="wp-block-code"><code>$ touch ~/.bashrc</code></pre>

Then install auto-completion package with the following command;

<pre class="wp-block-code"><code>$ terraform -install-autocomplete</code></pre>

Restart your shell for changes to take effect.

## Conclusion {.wp-block-heading}

We have successfully installed Terraform on Ubuntu 20.04. Continue learning more about Terraform using Its rich [documentation.][2]

 [1]: https://nextgentips.com/2021/11/20/how-to-install-terraform-on-debian-11/
 [2]: https://learn.hashicorp.com/collections/terraform/docker-get-started