---
title: How to install Webmin 2 control panel on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-09-02T06:30:58+00:00
url: /2022/09/02/how-to-install-webmin-2-control-panel-on-ubuntu-22-04/
categories:
  - Linux

---
In this tutorial, we will learn how to install the Webmin control panel on Ubuntu 22.04

Webmin is a powerful and flexible web-based server management control panel for UNIX-like users. It allows Webmin to configure operating system internals e.g users, configuration files, disk quotas, etc. Webmin removes the need to edit Unix configuration files manually and lets you manage the system remotely or via console.

### Install Webmin on Ubuntu 22.04 {.wp-block-heading}

Let&#8217;s install Webmin via Ubuntu apt repository. First, update your system in order to make them up to date. 

<pre class="wp-block-code"><code>sudo apt update && apt upgrade</code></pre>

To install Webmin via apt repository, we need to edit the `<mark style="background-color:#abb8c3" class="has-inline-color">/etc/apt/sources.list</mark>` file and add the following line `<mark style="background-color:#abb8c3" class="has-inline-color">deb https://download.webmin.com/download/repository sarge contrib</mark>`

<pre class="wp-block-code"><code>sudo vi /etc/apt/sources.list</code></pre>

Secondly, let&#8217;s add GPG key with which the repository is signed.

<pre class="wp-block-code"><code>wget https://download.webmin.com/jcameron-key.asc
apt-key add jcameron-key.asc</code></pre>

Third, we need to update our system repositories again for the changes to be effected.

<pre class="wp-block-code"><code>sudo apt update </code></pre>

Finally, install the Webmin with the following command.

<pre class="wp-block-code"><code>sudo apt install webmin -y</code></pre>

You should be in a position to see an output like this

<pre class="wp-block-code"><code>The following additional packages will be installed:
  libauthen-pam-perl libio-pty-perl libnet-ssleay-perl perl-openssl-defaults
  unzip
Suggested packages:
  zip
The following NEW packages will be installed:
  libauthen-pam-perl libio-pty-perl libnet-ssleay-perl perl-openssl-defaults
  unzip webmin
0 upgraded, 6 newly installed, 0 to remove and 2 not upgraded.
Need to get 29.0 MB of archives.
After this operation, 305 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

To know that the Webmin was installed correctly, check the version with the following command.

<pre class="wp-block-code"><code>webmin -v
2.000</code></pre>

Having successfully installed Webmin, let&#8217;s now go to the browser and see the Webmin in action.

<pre class="wp-block-code"><code>http://&lt;ip_address>:10000</code></pre>

The username is the **root** and the password is the **root user password**.

You should be in a position to see a dashboard like this.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="416" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-02-08-35-51.png?resize=810%2C416&#038;ssl=1" alt="Nextgentips: Webmin dashboard" class="wp-image-1538" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-02-08-35-51.png?resize=1024%2C526&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-02-08-35-51.png?resize=300%2C154&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-02-08-35-51.png?resize=768%2C394&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-02-08-35-51.png?resize=1170%2C600&ssl=1 1170w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/09/Screenshot-from-2022-09-02-08-35-51.png?w=1282&ssl=1 1282w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips: Webmin dashboard</figcaption></figure>