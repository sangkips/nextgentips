---
title: 'How to install HAProxy 2.5 Load Balancer on  Debian 11'
author: Kipkoech Sang
type: post
date: 2021-12-15T18:07:10+00:00
url: /2021/12/15/how-to-install-haproxy-2-5-load-balancer-on-debian-11/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 61
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
HAProxy is a free, very fast, and reliable reverse proxy offering high availability, load balancing, and proxying for TCP and HTTP-based applications. It is suited for very high-traffic websites and runs the most visited sites.

**What is load balancing?**

Load balancing ensures the availability, uptime, and performance of your servers, websites, and applications during traffic spikes.

In this tutorial, we are going to learn how to install the latest HAProxy on our Debian 11.

## What makes HAProxy so good? {.wp-block-heading}

  * Its&nbsp;**Perfomance&nbsp;**is on another level. HAProxy scales very fast with threads. It can reach 2 million requests over SSL and 100Gpbs for forwarded traffic. This is made possible by event-driven architecture that makes it reacts extremely quickly to any input/output requests.
  * **Reliability**&nbsp;of HAProxy is exceptional. Alot of effort is made in the direction of making HAProxy more reliable. The development process encourages quality with a long term development cycle. User who use HAProxy have faced minimal problems because the team who develops this are maintained for 5 years in the process of development.
  * It has&nbsp;**harden security**. HAProxy employs a number of security mechanism such as, use of chroot, priviledge drops, fork prevention, strict protocol, and detailed traces incase of vialotion detection.

## New Features of HAProxy {.wp-block-heading}

  * It provided better usability around setting variables
  * Has better and descriptive error reporting and logging
  * Has enhanced HTTP and web socket support.
  * The Lua integration now supports an HTTP client for initiating HTTP requests and their is early support for QUIC and HTTP/3
  * HAProxy gives you best in class observability into the traffic that passes throuh it.

## Install HAProxy 2.5 on Debian 11 {.wp-block-heading}

## 1. Run system update and upgrade {.wp-block-heading}

To begin our installation we need to run system update and upgrades in order to make our repositories and packages up to date. Open your terminal and run the following command.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Add HAProxy Repository to the system. {.wp-block-heading}

To install the latest release of HAProxy on Debian 11, we need to add the following repository to our system. After adding the repo, you may need to check your key if it is self-signed. Self-signed keys are authentic and it doesn&#8217;t pose any dangers. Open your terminal and run the following command.

<pre class="wp-block-code"><code>$ curl https://haproxy.debian.net/bernat.debian.org.gpg \
      | gpg --dearmor > /usr/share/keyrings/haproxy.debian.net.gpg</code></pre>

Sample output

<pre class="wp-block-code"><code>Output
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  5604  100  5604    0     0  11675      0 --:--:-- --:--:-- --:--:-- 11650</code></pre>

Use the following command to ensure that the key is self-signed 

<pre class="wp-block-code"><code>$ echo deb "&#91;signed-by=/usr/share/keyrings/haproxy.debian.net.gpg]" \
      http://haproxy.debian.net bullseye-backports-2.5 main \
      > /etc/apt/sources.list.d/haproxy.list</code></pre>

Next, is to run system updates again to ensure that changes made take effect.

<pre class="wp-block-code"><code>$ sudo apt update
&lt;strong>Hit:4 http://deb.debian.org/debian bullseye-backports InRelease&lt;/strong>                                                 
Hit:5 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                   
Get:6 http://haproxy.debian.net bullseye-backports-2.5 InRelease &#91;9198 B] 
Get:7 http://haproxy.debian.net bullseye-backports-2.5/main amd64 Packages &#91;1949 B]</code></pre>

From the above, you can see that backports 2.5 had been added successfully.

## 3. Install HAProxy on Debian  {.wp-block-heading}

To install HAProxy, on your terminal run the following command;

<pre class="wp-block-code"><code>$ sudo apt install haproxy=2.5.\*</code></pre>

The following output will be displayed.

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Selected version '2.5.0-1~bpo11+1' (HAProxy 2.5:bullseye-backports-2.5 &#91;amd64]) for 'haproxy'
The following additional packages will be installed:
  liblua5.3-0
Suggested packages:
  vim-haproxy haproxy-doc
The following NEW packages will be installed:
  haproxy liblua5.3-0
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 2056 kB of archives.
After this operation, 4609 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

From here press Y and enter to allow installation to continue.

We have successfully installed HAProxy but we can confirm by checking the version of installed HAProxy.

<pre class="wp-block-code"><code>$ haproxy -v 
HAProxy version 2.5.0-1~bpo11+1 2021/11/26 - https://haproxy.org/
Status: stable branch - will stop receiving fixes around Q1 2023.
Known bugs: http://www.haproxy.org/bugs/bugs-2.5.0.html
Running on: Linux 5.10.0-7-amd64 #1 SMP Debian 5.10.40-1 (2021-05-28) x86_64</code></pre>

## 4. Conclusion {.wp-block-heading}

We have successfully installed HAProxy on our Debian 11. You can consult [HAProxy documentation][1] for more insights.

You can also check this for Ubuntu installation

  * [How to install HAProxy 2.5 on Ubuntu 20.04][2]

 [1]: http://www.haproxy.org/
 [2]: https://nextgentips.com/2021/12/15/how-to-install-haproxy-2-5-on-ubuntu-20-04/