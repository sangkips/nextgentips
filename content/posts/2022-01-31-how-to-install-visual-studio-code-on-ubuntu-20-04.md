---
title: How to install Visual Studio Code on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-31T14:44:00+00:00
url: /2022/01/31/how-to-install-visual-studio-code-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 14
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this article, we are going to learn how to install Visual Studio code also known as (VScode) on Ubuntu 20.04. 

Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built-in Javascript, Node.js, and Typescript. One can do programming for almost all languages with ease with Visual Studio Code. The languages supported are, Go, PHP, C++, C#, Java, Python, and also .NET.

## Reasons for using VSCode {#reasons-for-using-vscode.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Related Articles  {#related-articles.wp-block-heading}

  * [How to install Visual Studio Code on Ubuntu 21.04][1]
  * [How to install Visual Studio Code on ArchLinux][2]
  * [How to install Visual Studio Code on Ubuntu 21.10][3]

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Ubuntu 20.04 workstation up and running 
  * Have command line basics
  * Have reliable internet access
  * Have sudo priliges or you are root user. 

## Table of Contents {#table-of-contents.wp-block-heading}

  * Update the system
  * Install Vscode via snap
  * Launch Vscode

## Install Visual Studio code {#install-visual-studio-code.wp-block-heading}

VScode can be installed either from the snap store or downloaded directly from the source.

### Install Visual Studio Code via Snap. {#install-visual-studio-code-via-snap.wp-block-heading}

Snap packages can be installed directly from either the command line or through the Ubuntu software repository.

Let us first update our system repository with the following command:

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the update and upgrade are complete, we can install VScode classic into our system.

<pre class="wp-block-code"><code>$ sudo snap install --classic code </code></pre>

<pre class="wp-block-code"><code>Output 
code 899d46d8 from Visual Studio Code (vscode✓) installed</code></pre>

## Installing via APT Repository {#installing-via-apt-repository.wp-block-heading}

First, we need to download the packages using the wget command into our system.

<pre class="wp-block-code"><code>$ wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg</code></pre>

### Add trusted GPG key {#add-trusted-gpg-key.wp-block-heading}

To validate the repository, we need to install trusted GPG keys into our repository in order to self-sign the certificates. Use the following code to do so.

<pre class="wp-block-code"><code>$ sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/</code></pre>

### Create VScode directory {#create-vscode-directory.wp-block-heading}

We need to create a repository where files can be stored. We will do so at /etc/apt/sources.list directory. Run the following code;

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb &#91;arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'</code></pre>

As a bonus thing, we can remove Microsoft gpg packages from the downloads page.

<pre class="wp-block-code"><code>$ rm -f packages.microsoft.gpg</code></pre>

Then we need to update our repository cache in order for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

<pre class="wp-block-code"><code># output
Hit:1 http://mirrors.digitalocean.com/ubuntu focal InRelease
Hit:2 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                    
Hit:3 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease                             
Hit:4 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease                                 
Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease                                       
&lt;strong>Get:6 https://packages.microsoft.com/repos/code stable InRelease &#91;10.4 kB]      
Get:7 https://packages.microsoft.com/repos/code stable/main armhf Packages &#91;67.3 kB]
Get:8 https://packages.microsoft.com/repos/code stable/main amd64 Packages &#91;66.7 kB]
Get:9 https://packages.microsoft.com/repos/code stable/main arm64 Packages &lt;/strong>&#91;67.4 kB]</code></pre>

### Install VSCode on Ubuntu 20.04 {#install-vscode-on-ubuntu-20-04.wp-block-heading}

Lastly, we need t install VSCode with the following command;

<pre class="wp-block-code"><code>$ sudo apt install code</code></pre>

Sample output will look like this;

<pre class="wp-block-code"><code>The following additional packages will be installed:
  adwaita-icon-theme at-spi2-core fontconfig fontconfig-config fonts-dejavu-core gtk-update-icon-cache hicolor-icon-theme
  humanity-icon-theme libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3 libavahi-common-data
  libavahi-common3 libcairo-gobject2 libcairo2 libcolord2 libcups2 libdatrie1 libepoxy0 libfontconfig1 libgbm1
  libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgraphite2-3 libgtk-3-0 libgtk-3-bin libgtk-3-common
  libharfbuzz0b libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0
  libpixman-1-0 librest-0.7-0 librsvg2-2 librsvg2-common libsecret-1-0 libsecret-common libsoup-gnome2.4-1 libthai-data
  libthai0 libtiff5 libwayland-client0 libwayland-cursor0 libwayland-egl1 libwayland-server0 libwebp6 libxcb-render0
  libxcb-shm0 libxcomposite1 libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxkbfile1 libxrandr2
  libxrender1 libxss1 libxtst6 ubuntu-mono x11-common
Suggested packages:
  colord cups-common gvfs liblcms2-utils librsvg2-bin
The following NEW packages will be installed:
  adwaita-icon-theme at-spi2-core code fontconfig fontconfig-config fonts-dejavu-core gtk-update-icon-cache
  hicolor-icon-theme humanity-icon-theme libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 libavahi-client3
  libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcolord2 libcups2 libdatrie1 libepoxy0 libfontconfig1
  libgbm1 libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgraphite2-3 libgtk-3-0 libgtk-3-bin
  libgtk-3-common libharfbuzz0b libjbig0 libjpeg-turbo8 libjpeg8 liblcms2-2 libpango-1.0-0 libpangocairo-1.0-0
  libpangoft2-1.0-0 libpixman-1-0 librest-0.7-0 librsvg2-2 librsvg2-common libsecret-1-0 libsecret-common libsoup-gnome2.4-1
  libthai-data libthai0 libtiff5 libwayland-client0 libwayland-cursor0 libwayland-egl1 libwayland-server0 libwebp6
  libxcb-render0 libxcb-shm0 libxcomposite1 libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 libxkbcommon0 libxkbfile1
  libxrandr2 libxrender1 libxss1 libxtst6 ubuntu-mono x11-common
0 upgraded, 70 newly installed, 0 to remove and 0 not upgraded.
Need to get 96.5 MB of archives.
After this operation, 411 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue.

We can start Visual Studio Code with the following command:

<pre class="wp-block-code"><code>$ code </code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have installed Visual Studio Code in our Ubuntu 20.04 workstation. Go ahead and configure to suit your programming needs. Consult Visual studio code documentation if you face any problems. I hope you have enjoyed and learn something new.

 [1]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
 [2]: https://nextgentips.com/2021/11/30/how-to-install-visual-studio-code-6-2-on-archlinux/
 [3]: https://nextgentips.com/2021/11/06/how-to-install-visual-studio-code-on-ubuntu-21-10/