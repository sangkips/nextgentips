---
title: How to install Vivaldi browser on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-05-21T07:58:13+00:00
url: /2022/05/21/how-to-install-vivaldi-browser-on-fedora-36/
rank_math_seo_score:
  - 13
rank_math_primary_category:
  - 6
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 178
categories:
  - Linux

---
Vivaldi browser is a freeware, cross-platform web browser developed by Vivaldi Technologies. It has a minimalistic user interface with basic icons and fonts and, an optionally color scheme that changes based on the background and design of the web page being visited.

## Features of Vivaldi browser {.wp-block-heading}

  * It has a built-in ad blocker, pop-up blocker, and tracker blocker. It helps block intrusive ads and makes the browser much faster which many users want.
  * It comes with a built-in email client with IMAP and POP3 support.
  * The browser can be used as a feed reader to save RSS and Atom feeds.
  * It has the ability to stack and tile tabs, annotate web pages, and add notes to bookmarks.
  * It supports numerous mouse gestures for actions like tab switching and keyboard activation
  * It supports hibernation for both individual tabs and for tab stacks, hence freeing resources while the user does not actively use those tabs.
  * It has translate features that can translate any web page with a single click with a built-in, private translation tool.

## Prerequisites {.wp-block-heading}

  * Have a user with sudo privileges.
  * Have Fedora 36 up and running.
  * Have basic knowledge of terminal

## Install Vivaldi browser on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories {.wp-block-heading}

To start installing Vivaldi, we can first make our repositories up to date by doing an update and upgrade where necessary.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

### 2. Add Vivaldi Repository to Fedora 36 {.wp-block-heading}

To install Vivaldi, there is an RPM repository provided for this, so what we need to do is to add this repository to our system using the following.

<pre class="wp-block-code"><code>$ sudo dnf install dnf-utils</code></pre>

You will see the following output

<pre class="wp-block-code"><code>Dependencies resolved.
=============================================================================================================================================
 Package                           Architecture                   Version                              Repository                       Size
=============================================================================================================================================
Installing:
 &lt;strong>dnf-utils &lt;/strong>                        noarch                         4.2.0-1.fc36                         updates                          37 k

Transaction Summary
=============================================================================================================================================
Install  1 Package

Total download size: 37 k
Installed size: 23 k
Is this ok &#91;y/N]: y
Downloading Packages:
dnf-utils-4.2.0-1.fc36.noarch.rpm                                                                            109 kB/s |  37 kB     00:00    
---------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                         42 kB/s |  37 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                     1/1 
  Installing       : dnf-utils-4.2.0-1.fc36.noarch                                                                                       1/1 
  Running scriptlet: dnf-utils-4.2.0-1.fc36.noarch                                                                                       1/1 
  Verifying        : dnf-utils-4.2.0-1.fc36.noarch                                                                                       1/1 

Installed:
  dnf-utils-4.2.0-1.fc36.noarch                                                                                                              

Complete!</code></pre>

Then we need to add the repository with this command.

<pre class="wp-block-code"><code>$ sudo dnf config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo</code></pre>

Once added we can then proceed to install the Vivaldi browser stable version with the following command.

<pre class="wp-block-code"><code>$ sudo dnf install vivaldi-stable -y</code></pre>

You will have to allow the key generation process during installation.

<pre class="wp-block-code"><code>vivaldi                                                                                                       23 kB/s | 3.1 kB     00:00    
Importing GPG key 0xC27AA466:
 Userid     : "Vivaldi Package Composer KEY07 &lt;packager@vivaldi.com>"
 Fingerprint: CB63 144F 1BA3 1BC3 9E27 79A8 FEB6 023D C27A A466
 From       : https://repo.vivaldi.com/archive/linux_signing_key.pub
Is this ok &#91;y/N]: y</code></pre>

Then the installation will proceed to completion. You will get the following 

<pre class="wp-block-code"><code> Package                                        Architecture       Version                                         Repository           Size
Installing:
&lt;strong> vivaldi-stable &lt;/strong>                                x86_64             5.2.2623.48-1                                   vivaldi              83 M
Installing dependencies:
 adwaita-cursor-theme                           noarch             42.0-1.fc36                                     fedora              622 k
 adwaita-icon-theme                             noarch             42.0-1.fc36                                     fedora              4.2 M
 alsa-lib                                       x86_64             1.2.6.1-4.fc36                                  fedora              499 k
 at-spi2-atk                                    x86_64             2.38.0-4.fc36                                   fedora               86 k
 at-spi2-core                                   x86_64             2.44.1-1.fc36                                   updates             178 k
 atk                                            x86_64             2.38.0-1.fc36                                   fedora              272 k
 avahi-glib                                     x86_64             0.8-15.fc36                                     fedora               15 k
 avahi-libs                                     x86_64             0.8-15.fc36                                     fedora               68 k
 bluez-libs                                     x86_64             5.64-1.fc36                                     fedora               84 k
 cairo                                          x86_64             1.17.6-1.fc36                                   fedora              675 k
 cairo-gobject                                  x86_64             1.17.6-1.fc36                                   fedora               18 k
 cdparanoia-libs                                x86_64             10.2-39.fc36                                    fedora               54 k
 colord-libs                                    x86_64             1.4.6-1.fc36                                    fedora              233 k
 cups-libs        </code></pre>

## Conclusion {.wp-block-heading}

And we have successfully installed the Vivaldi browser on Fedora 36. Enjoy the greatest features you can get on this awesome browser. For more information check <a href="https://vivaldi.com" target="_blank" rel="noreferrer noopener">Viladi documentation</a>.