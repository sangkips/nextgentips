---
title: How to Upgrade Linux Kernel to 5.17 Release on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-03-21T18:22:22+00:00
url: /2022/03/21/how-to-upgrade-linux-kernel-to-5-17-release-on-ubuntu-20-04/
rank_math_seo_score:
  - 63
rank_math_focus_keyword:
  - upgrade linux kernel to 5.17
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 6
mtm_data:
  - 'a:2:{i:0;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}i:1;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}}'
rank_math_analytic_object_id:
  - 156
categories:
  - Linux

---
In this tutorial, we are going to learn how to upgrade Linux Kernel to 5.17 mainline release on Ubuntu 20.04.

<a href="https://www.kernel.org/" target="_blank" rel="noreferrer noopener">Linux Kernel</a> is a **free and open-source, monolithic, modular, multitasking Unix-like operating system**. **It is the main component of a Linux operating system and is the core interface between the computer’s hardware and its processes. It makes communication possible between computer hardware and processes running on it and it manages resources effectively.**

Linux 5.17 mainline was released recently by **Linux Torvalds** with better new features to try out. The mainline tree is maintained by Linus Torvalds and It is where all new features are added and releases always come from.

## Notable features on Linux Kernel 5.17 {.wp-block-heading}

  * Introduction of new AMD P-state subsystem for future AMD CPUs that provide performance boost.
  * Introduction of page-table check feature to better protect the GNU/Linux system from threats.
  * It introduces boot time memtest memory tester to the mk68k architecture.
  * introduces an alternative way to implement loops in BPF programs .
  * It adds support for tracking forced-idle time to the core scheduling feature and adds support to offloading traffic-control actions on network devices.

## Table of Contents {#table-of-contents.wp-block-heading}

  1. Run updates for your system
  2. Check the current version of Linux kernel you are running
  3. Download Linux kernel headers from Ubuntu Mainline
  4. Download Linux Kernel image
  5. Download modules required to build the kernel
  6. Install new kernel
  7. Reboot the system
  8. Conclusion

## Upgrade Linux Kernel to 5.17 release {#upgrade-linux-kernel-to-5-16-release.wp-block-heading}

### 1. Run system update {#1-run-system-update.wp-block-heading}

The first thing to do is to run system updates on our Ubuntu 20.04 server. Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When upgrades and updates are complete, we can now begin to download headers, modules, and images.

Before we can proceed lets check the Linux kernel we are having with the following command.

<pre class="wp-block-code"><code>uname -r
5.4.0-97-generic</code></pre>

### 2. Download Linux kernel Headers. {#2-download-linux-kernel-headers.wp-block-heading}

Linux kernel headers is a package providing the Linux kernel headers. These are part of the Kernel even though shipped separately. The headers act as an interface between internal kernel components and also between userspace and the kernel.

To download this package header, head over to the <a href="https://kernel.ubuntu.com/~kernel-ppa/mainline" target="_blank" rel="noreferrer noopener">Ubuntu PPA mainline repository</a> and make downloads for your amd64 system. We are going to download the following header files.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.17/amd64/linux-headers-5.17.0-051700_5.17.0-051700.202203202130_all.deb</code></pre>

Another header file to download is this one. Download the generic one here.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.17/amd64/linux-headers-5.17.0-051700-generic_5.17.0-051700.202203202130_amd64.deb</code></pre>

Sample output will look like this:

<pre class="wp-block-code"><code>#output
linux-headers-5.17.0-051700 100%&#91;=========================================>]   2.82M  --.-KB/s    in 0.08s   

2022-03-21 18:01:21 (35.4 MB/s) - ‘linux-headers-5.17.0-051700-generic_5.17.0-051700.202203202130_amd64.deb’ saved &#91;2957866/2957866]</code></pre>

Note the difference between those headers

### 3. Download Linux kernel Modules {#2-download-linux-kernel-modules.wp-block-heading}

Linux kernel are pieces of code that can be loaded and unloaded into the kernel upon demand. They extend the functionality of the kernel without the need to reboot the system. A module can be configured as built-in or loadable. To dynamically load or remove a module, it has to be configured as a loadable module in the kernel configuration.

To download the Linux Kernel module run the following command on your terminal.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.17/amd64/linux-modules-5.17.0-051700-generic_5.17.0-051700.202203202130_amd64.deb</code></pre>

Next is to download the image.

## 3. Download Linux Kernel Image {#3-download-linux-kernel-image.wp-block-heading}

Linux kernel image is a snapshot of the Linux kernel that is able to run by itself after giving the control to it. For example, whenever you need to boot the system up, it will bootload an image from the hard disk.

To download the Linux Kernel image 5.17 run the following command on your terminal.

<pre class="wp-block-code"><code>wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.17/amd64/linux-image-unsigned-5.17.0-051700-generic_5.17.0-051700.202203202130_amd64.deb</code></pre>

Make sure you are seeing the image from the downloads if you do an ls.

Now that we have finished downloading **images, modules,** and **headers** it is now time to install them.

### 4. Install Linux Kernel on Ubuntu 20.04 {#4-install-linux-kernel-on-ubuntu-20-04.wp-block-heading}

To install Linux Kernel 5.17, let’s run the following command;

<pre class="wp-block-code"><code>$ sudo dpkg -i *.deb</code></pre>

Wait for the process to complete before restarting the system.

<pre class="wp-block-code"><code>reboot -n</code></pre>

After you have restarted the system, check the Linux Kernel release installed.

<pre class="wp-block-code"><code>$ uname -r</code></pre>

The output you will now get is like this;

<pre class="wp-block-code"><code>5.17.0-051700-generic</code></pre>

## Conclusion {#conclusion.wp-block-heading}

As you can see we have successfully upgraded from 5.4.0-97-generic to 5.17.0-051700-generic, the latter being the latest release.