---
title: Kubernetes Architecture
author: Kipkoech Sang
type: post
date: 2021-12-10T11:37:42+00:00
url: /2021/12/10/kubernetes-architecture/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 69
rank_math_internal_links_processed:
  - 1
categories:
  - Kubernetes

---
In this tutorial, I will walk you through Kubernetes architecture, control plane, and worker node components.

Control plane makes global decisions i.e scheduling tasks and also maintains cluster details in an etcd component. 

## Control Plane {.wp-block-heading}

The Control plane is a global decision-maker, it is responsible for scheduling events. Containerized apps are scheduled on worker nodes based on the memory allocated on the YAML file. The Control plane starts new pods and terminates old ones when replica set values are not met.

## Control plane components {.wp-block-heading}

  * **Kube-apiserver**. This is the frontend that exposes Kubernetes API. It validates and configures data for API objects, e.g pods, services, deployments, and replication controllers. 
  * **etcd (data store).** It is used as kubernetes backing store for all cluster data. It is the Kubernetes backend and it uses etcd to monitor states i.e desired state and actual state. 
  * **Kube-scheduler.** It assign unscheduled pods to different nodes based on its memory usage. 
  * **Kube-controller-manager.** This runs control processes. A control process is a loop that focusses on making desired state equal to the current state. Some of the controller manager include node, job, endpoint and service account and token controllers. 

## Worker-node components {.wp-block-heading}

A worker node runs containerized applications and continuously reports to the control plane about the health of the cluster.

  * **Kubelet.** This an agent that runs on each node in a Kubernetes cluster. It ensures that the containers inside the pods are in an healthy state. 
  * **Kube-proxy.** This is a network proxy that runs on each node in a kubernetes cluster. It maintains network rules on all nodes allowing easy communication between the pods elements.
  * **Container runtime.** It is a software responsible for running containers inside the cluster nodes.