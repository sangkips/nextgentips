---
title: How to install and configure QEMU 7 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-04-20T20:06:21+00:00
url: /2022/04/20/how-to-install-and-configure-qemu-7-on-ubuntu-20-04/
rank_math_seo_score:
  - 11
rank_math_internal_links_processed:
  - 1
rank_math_analytic_object_id:
  - 166
rank_math_primary_category:
  - 6
categories:
  - Linux

---
In this tutorial, we are going to learn how to install and configure QEMU 7 on Ubuntu 20.04.

QEMU is a free and open-source hypervisor, it emulates the machine’s processor through dynamic binary translation and provides a set of different hardware and device models for the machine, enabling it to run a variety of guest operating systems.

QEMU is capable of emulating a complete machine in software without the need for hardware virtualization support. It is also capable of providing userspace API virtualization for Linux and BSD kernel services. It is commonly invoked indirectly via libvirt library when using open source applications such as oVirt, OpenStack, and virt-manager.

## Install QEMU on Ubuntu 20.04 {.wp-block-heading}

### 1. Run system updates {.wp-block-heading}

To begin with, we need to update our repositories in order to make them up to date, Use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When Updates are complete, now we can install QEMU.

### 2. Install QEMU 7 on Ubuntu 20.04 {.wp-block-heading}

QEMU is available from Ubuntu repositories but in this tutorial, I will show you how to build from the source. 

First, we are going to download the tar Qemu.

<pre class="wp-block-code"><code>wget https://download.qemu.org/qemu-7.0.0.tar.xz</code></pre>

Second, We are going to extract the archive into our system.

<pre class="wp-block-code"><code>tar xvJf qemu-7.0.0.tar.xz</code></pre>

The sample output will look like this below.

<pre class="wp-block-code"><code>#output
qemu-7.0.0/target/i386/nvmm/nvmm-accel-ops.h
qemu-7.0.0/target/i386/nvmm/nvmm-all.c
qemu-7.0.0/target/i386/nvmm/nvmm-accel-ops.c
qemu-7.0.0/target/i386/machine.c
qemu-7.0.0/target/i386/helper.h
qemu-7.0.0/target/i386/cpu.c
qemu-7.0.0/target/i386/cpu-dump.c
qemu-7.0.0/target/i386/whpx/
qemu-7.0.0/target/i386/whpx/meson.build
qemu-7.0.0/target/i386/whpx/whpx-internal.h
qemu-7.0.0/target/i386/whpx/whpx-apic.c
qemu-7.0.0/target/i386/whpx/whpx-all.c
qemu-7.0.0/target/i386/whpx/whpx-accel-ops.h
qemu-7.0.0/target/i386/whpx/whpx-accel-ops.c
qemu-7.0.0/.dir-locals.el
qemu-7.0.0/block.c
qemu-7.0.0/.exrc
qemu-7.0.0/subprojects/
qemu-7.0.0/subprojects/libvhost-user/
qemu-7.0.0/subprojects/libvhost-user/libvhost-user.c
qemu-7.0.0/subprojects/libvhost-user/meson.build
qemu-7.0.0/subprojects/libvhost-user/libvhost-user-glib.c
qemu-7.0.0/subprojects/libvhost-user/link-test.c
qemu-7.0.0/subprojects/libvhost-user/standard-headers/
qemu-7.0.0/subprojects/libvhost-user/standard-headers/linux
qemu-7.0.0/subprojects/libvhost-user/libvhost-user.h
qemu-7.0.0/subprojects/libvhost-user/include/
qemu-7.0.0/subprojects/libvhost-user/include/atomic.h
qemu-7.0.0/subprojects/libvhost-user/libvhost-user-glib.h
qemu-7.0.0/qemu.nsi</code></pre>

Give it time to extract the contents into your system.

Third, cd into qemu 7.0.0 directory before we can do the configuration.

<pre class="wp-block-code"><code>cd qemu-7.0.0</code></pre>

If you ls into the qemu directory, you will see the following content.

<pre class="wp-block-code"><code>COPYING       blockdev-nbd.c  docs                  job-qmp.c          os-win32.c            qemu-nbd.c       storage-daemon
COPYING.LIB   blockdev.c      dtc                   job.c              page-vary-common.c    qemu-options.hx  stubs
Kconfig       blockjob.c      dump                  libdecnumber       page-vary.c           qemu.nsi         subprojects
Kconfig.host  bsd-user        ebpf                  linux-headers      pc-bios               qemu.sasl        target
LICENSE       capstone        fpu                   linux-user         plugins               qga              tcg
MAINTAINERS   chardev         fsdev                 memory_ldst.c.inc  po                    qobject          tests
Makefile      common-user     gdb-xml               meson              python                qom              tools
README.rst    configs         gdbstub.c             meson.build        qapi                  replay           trace
VERSION       configure       gitdm.config          meson_options.txt  qemu-bridge-helper.c  replication.c    trace-events
accel         contrib         hmp-commands-info.hx  migration          qemu-edid.c           roms             ui
audio         cpu.c           hmp-commands.hx       module-common.c    qemu-img-cmds.hx      scripts          util
authz         cpus-common.c   hw                    monitor            qemu-img.c            scsi             version.rc
backends      crypto          include               nbd                qemu-io-cmds.c        semihosting
block         disas           io                    net                qemu-io.c             slirp
block.c       disas.c         iothread.c            os-posix.c         qemu-keymap.c         softmmu</code></pre>

When you are inside the qemu directory, then it&#8217;s time you run the configuration file.

<pre class="wp-block-code"><code>./configure</code></pre>

If you are getting any errors, please make sure you have to make installed in your system.

<pre class="wp-block-code"><code>sudo apt install make -y
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Suggested packages:
  make-doc
The following NEW packages will be installed:
  make
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 162 kB of archives.
After this operation, 393 kB of additional disk space will be used.
Get:1 http://mirrors.digitalocean.com/ubuntu focal/main amd64 make amd64 4.2.1-1.2 &#91;162 kB]
Fetched 162 kB in 0s (3628 kB/s)
Selecting previously unselected package make.
(Reading database ... 94907 files and directories currently installed.)
Preparing to unpack .../make_4.2.1-1.2_amd64.deb ...
Unpacking make (4.2.1-1.2) ...
Setting up make (4.2.1-1.2) ...
Processing triggers for man-db (2.9.1-1) ...</code></pre>

If you are still getting the error ninja not installed, please install ninja-build with the following command.

<pre class="wp-block-code"><code>#&lt;strong>sudo apt install ninja-build -y&lt;/strong>
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following NEW packages will be installed:
  ninja-build
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 107 kB of archives.
After this operation, 338 kB of additional disk space will be used.
Get:1 http://mirrors.digitalocean.com/ubuntu focal/universe amd64 ninja-build amd64 1.10.0-1build1 &#91;107 kB]
Fetched 107 kB in 0s (1872 kB/s)
Selecting previously unselected package ninja-build.
(Reading database ... 100528 files and directories currently installed.)
Preparing to unpack .../ninja-build_1.10.0-1build1_amd64.deb ...
Unpacking ninja-build (1.10.0-1build1) ...
Setting up ninja-build (1.10.0-1build1) ...
Processing triggers for man-db (2.9.1-1) ...</code></pre>

Also make sure **libpixman-1-dev, glib2** are installed before running **./configure** 

Run ./configure again in your terminal and wait for it to finish. It will take time.

Sample output will look like this below.

<pre class="wp-block-code"><code>Executing subproject libvhost-user 

libvhost-user| Project name: libvhost-user
libvhost-user| Project version: undefined
libvhost-user| C compiler for the host machine: cc -m64 -mcx16 (gcc 9.4.0 "cc (Ubuntu 9.4.0-1ubuntu1~20.04.1) 9.4.0")
libvhost-user| C linker for the host machine: cc -m64 -mcx16 ld.bfd 2.34
libvhost-user| Dependency threads found: YES unknown (cached)
libvhost-user| Dependency glib-2.0 found: YES 2.64.6 (overridden)
libvhost-user| Build targets in project: 12
libvhost-user| Subproject libvhost-user finished.

Program cat found: YES (/usr/bin/cat)
Program scripts/decodetree.py found: YES (/usr/bin/python3 /root/qemu-7.0.0/scripts/decodetree.py)
Program ../scripts/modules/module_block.py found: YES (/usr/bin/python3 /root/qemu-7.0.0/block/../scripts/modules/module_block.py)
Program ../scripts/block-coroutine-wrapper.py found: YES (/usr/bin/python3 /root/qemu-7.0.0/block/../scripts/block-coroutine-wrapper.py)
Program scripts/modinfo-collect.py found: YES (/root/qemu-7.0.0/scripts/modinfo-collect.py)
Program scripts/modinfo-generate.py found: YES (/root/qemu-7.0.0/scripts/modinfo-generate.py)
Program nm found: YES
Program scripts/undefsym.py found: YES (/usr/bin/python3 /root/qemu-7.0.0/scripts/undefsym.py)
Program scripts/feature_to_c.sh found: YES (/bin/sh /root/qemu-7.0.0/scripts/feature_to_c.sh)
Configuring 50-edk2-i386-secure.json using configuration
Configuring 50-edk2-x86_64-secure.json using configuration
Configuring 60-edk2-aarch64.json using configuration
Configuring 60-edk2-arm.json using configuration
Configuring 60-edk2-i386.json using configuration
Configuring 60-edk2-x86_64.json using configuration
Program qemu-keymap found: NO
Program cp found: YES (/usr/bin/cp)
Program sphinx-build-3 sphinx-build found: NO
Program python3 found: YES (/usr/bin/python3)
Program diff found: YES (/usr/bin/diff)
Program dbus-daemon found: YES (/usr/bin/dbus-daemon)
Program /usr/bin/gdbus-codegen found: YES (/usr/bin/gdbus-codegen)
Program initrd-stress.sh found: YES (/root/qemu-7.0.0/tests/migration/initrd-stress.sh)
Build targets in project: 721

qemu 7.0.0</code></pre>

Lastly, we need to run **make** command in order to complete our installation.

<pre class="wp-block-code"><code>make</code></pre>

Give a few minutes for the make command to complete execution.

To open QEMU using GUI, just type&nbsp;**_virt-manager_**&nbsp;on terminal

<pre class="wp-block-code"><code>$ virt-manager</code></pre>

Check the version using the following command;

<pre class="wp-block-code"><code>$ sudo apt show qemu-system-x86</code></pre>

The following output will be displayed.

<pre class="wp-block-code"><code></code></pre>

To uninstall Qemu do the following

<pre class="wp-block-code"><code>$ sudo apt purge "qemu*"
$ sudo apt autoremove</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed QEMU 7 on our Ubuntu 20.04. To learn more you can consult&nbsp;<a href="https://planet.virt-tools.org/" target="_blank" rel="noreferrer noopener">QEMU documentation</a>.