---
title: How to install Odoo 15 on Debian 11
author: Kipkoech Sang
type: post
date: 2022-04-13T10:23:21+00:00
url: /2022/04/13/how-to-install-odoo-15-on-debian-11/
rank_math_seo_score:
  - 16
rank_math_internal_links_processed:
  - 1
rank_math_focus_keyword:
  - Install Odoo on Debian 11
rank_math_primary_category:
  - 6
rank_math_analytic_object_id:
  - 165
categories:
  - Linux

---
In this tutorial, I will take you through the installation steps for Odoo 15 on Debian 11 server. So let&#8217;s dive right in.

Odoo is a popular CRM (Customer Relationship Management) software solution that is used by businesses of all sizes. **Odoo is easy to use, has a wide range of features, and is backed by a team of experts. Odoo also has a robust plugin system that makes it easy to add new features and functionality to your CRM. Odoo is available in a number of languages.**

Odoo allows users to manage their business data and processes in a centralized location, making it a valuable tool for small businesses and enterprise organizations. Odoo also offers a variety of features that make it an attractive choice for online marketing and sales operations.

Odoo is very user-friendly and it allows you to manage your business with ease. It has useful features such as a calendar for easy management of date functions, project management features for easy time tracking on any task, invoicing and tracking feature, and also has reports which make it easy to use and useful software for small and big businesses.

## Prerequisites {.wp-block-heading}

  * You need to have Debian 11 server up and running 
  * Have PostgreSQL installed 
  * Have a non-root user with sudo privileges if you are not root

We have two versions of Odoo, the community version which is free and open-source, and the enterprise version available for enterprise customers only. We are going to install the free version i.e the community version.

## Install Odoo 15 on Debian 11 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

The first thing to do is to update your system repositories in order to make them up to date. Use the following command to do so.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y </code></pre>

### 2. Install the PostgreSQL database  {.wp-block-heading}

Odoo needs the PostgreSQL server to run smoothly. So we need to install it onto our system with the following command.

<pre class="wp-block-code"><code>sudo apt install -y postgresql</code></pre>

Sample output from the installation of Postgresql

<pre class="wp-block-code"><code>#output
Success. You can now start the database server using:

    pg_ctlcluster 12 main start

Ver Cluster Port Status Owner    Data directory              Log file
12  main    5432 down   postgres /var/lib/postgresql/12/main /var/log/postgresql/postgresql-12-main.log
update-alternatives: using /usr/share/postgresql/12/man/man1/postmaster.1.gz to provide /usr/share/man/man1/postmaster.1.gz (postmaster.1.gz) in auto mode
Setting up sysstat (12.2.0-2ubuntu0.1) ...

Creating config file /etc/default/sysstat with new version
update-alternatives: using /usr/bin/sar.sysstat to provide /usr/bin/sar (sar) in auto mode
Created symlink /etc/systemd/system/multi-user.target.wants/sysstat.service → /lib/systemd/system/sysstat.service.
Setting up postgresql (12+214ubuntu0.1) ...
Processing triggers for systemd (245.4-4ubuntu3.15) ...
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.7) ...</code></pre>

Now that we have installed PostgreSQL, we need to start the server cluster with the following command.

<pre class="wp-block-code"><code>pg_ctlcluster 12 main start</code></pre>

Then you can check the cluster if it is up and running with the following command.

<pre class="wp-block-code"><code>sudo systemctl status postgresql
● postgresql.service - PostgreSQL RDBMS
     Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
     Active: active (exited) since Tue 2022-04-05 18:45:46 UTC; 3min 14s ago
   Main PID: 2321 (code=exited, status=0/SUCCESS)
      Tasks: 0 (limit: 1131)
     Memory: 0B
     CGroup: /system.slice/postgresql.service

Apr 05 18:45:46 ubuntu systemd&#91;1]: Starting PostgreSQL RDBMS...
Apr 05 18:45:46 ubuntu systemd&#91;1]: Finished PostgreSQL RDBMS.</code></pre>

You can read more on the installation of PostgreSQL 14 in this article. <a href="https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install PostgreSQL 14 on Ubuntu 20.04|21.10|22.04</a>

### 3. Add Odoo package repository for Debian 11 {.wp-block-heading}

Odoo provides a package repository for Ubuntu that will allow us to add nightly keys to our system repositories hence allowing us to install Odoo with ease.

So the first thing is to add keys to our system with the following command.

<pre class="wp-block-code"><code>wget -O - https://nightly.odoo.com/odoo.key | apt-key add -</code></pre>

Then after that is complete, add Odoo nightly 15 to our sources.listd.d which will in turn allows us to make some required configuring to Odoo once installed.

<pre class="wp-block-code"><code>echo "deb http://nightly.odoo.com/15.0/nightly/deb/ ./" &gt;&gt; /etc/apt/sources.list.d/odoo.list</code></pre>

Lastly, we need to run system-wide updates again and then install Odoo.

<pre class="wp-block-code"><code>sudo apt update </code></pre>

### 4. Install Odoo 15 on Ubuntu 20.04 {.wp-block-heading}

To install Odoo run the following command.

<pre class="wp-block-code"><code>sudo apt install odoo -y</code></pre>

<pre class="wp-block-code"><code>Hit:1 http://security.debian.org/debian-security bullseye-security InRelease
Hit:2 http://deb.debian.org/debian bullseye InRelease                                                                         
Hit:3 http://deb.debian.org/debian bullseye-updates InRelease                                                                 
Hit:4 http://deb.debian.org/debian bullseye-backports InRelease                                                               
Ign:5 http://nightly.odoo.com/15.0/nightly/deb ./ InRelease                                                                   
Get:6 http://nightly.odoo.com/15.0/nightly/deb ./ Release &#91;1188 B]                                                            
Get:7 http://nightly.odoo.com/15.0/nightly/deb ./ Release.gpg &#91;833 B]
Hit:8 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease
Get:9 http://nightly.odoo.com/15.0/nightly/deb ./ Packages &#91;2098 B]
Fetched 4119 B in 1s (3465 B/s)
Reading package lists... Done
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done</code></pre>

Now we can check the status of Odoo to confirm if it is running or not.

<pre class="wp-block-code"><code>systemctl status odoo
● odoo.service - Odoo Open Source ERP and CRM
     Loaded: loaded (/lib/systemd/system/odoo.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-04-13 05:40:30 UTC; 10min ago
   Main PID: 22778 (odoo)
      Tasks: 4 (limit: 1132)
     Memory: 79.0M
        CPU: 1.143s
     CGroup: /system.slice/odoo.service
             └─22778 /usr/bin/python3 /usr/bin/odoo --config /etc/odoo/odoo.conf --logfile /var/log/odoo/odoo-server.log

Apr 13 05:40:30 debian systemd&#91;1]: Started Odoo Open Source ERP and CRM.</code></pre>

We need to enable Odoo to start automatically every time we boot up.

<pre class="wp-block-code"><code>systemctl enable odoo --now
Synchronizing state of odoo.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable odoo</code></pre>

Odoo listens on TCP port 8069. We can confirm that with the following command.

<pre class="wp-block-code"><code>ss -tunelp | grep 8069
tcp   LISTEN 0      128          0.0.0.0:8069      0.0.0.0:*    users:(("odoo",pid=22778,fd=5)) uid:109 ino:38497 sk:5 cgroup:/system.slice/odoo.service &lt;-&gt;       </code></pre>

To login into Odoo use the following on your browser.

<pre class="wp-block-code"><code>http:&#47;&#47;&lt;IP-address&gt;:8069</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="556" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-05-21.png?resize=810%2C556&#038;ssl=1" alt="" class="wp-image-1293" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-05-21.png?w=883&ssl=1 883w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-05-21.png?resize=300%2C206&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-05-21.png?resize=768%2C527&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Odoo Login</figcaption></figure> <figure class="wp-block-image size-full"><img decoding="async" loading="lazy" width="810" height="438" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-12-52.png?resize=810%2C438&#038;ssl=1" alt="" class="wp-image-1294" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-12-52.png?w=1004&ssl=1 1004w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-12-52.png?resize=300%2C162&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/04/Screenshot-from-2022-04-13-09-12-52.png?resize=768%2C415&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /><figcaption>Odoo Dashboard</figcaption></figure> 

When you login to Odoo for the first time you will find that the connection is not secure. What we will do is install an SSL certificate in order to make our connection secure.

## Conclusion {.wp-block-heading}

We have successfully installed Odoo on Debian 11, what remains is the SSL certificate for production-ready sites. I will tackle that next.