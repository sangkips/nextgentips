---
title: How to install and configure Git on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-08T16:15:42+00:00
url: /2021/12/08/how-to-install-and-configure-git-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 72
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
[Git][1] is software for tracking changes in any set of files, usually used for coordinating work among programmers collaboratively developing source code during software development. It is a distributed version control system. This means a local clone of the project is a complete version control repository.

In this tutorial, I will show you how to install and work with git on Ubuntu 20.04. Git being free and open-source version control system is designed to handle everything from small to large projects with speed and efficiency. 

## Git Features that make it stand out. {.wp-block-heading}

  * **Branching and marging.** Git allows one to have multiple local branches that are entirely independent of each other. Creation, merging, and deletion takes few seconds to take effect. When you push to a remote repository, you dont have to push all your branches. Choose the one you would like to push giving you freedom to choose which one to work on.
  * Git is small and fast. With git, nearly all operations are done locally, giving a huge speed advantage on centralized systems that had to communicate with a server. 
  * Git is distributed. For git, instead of doing a checkout of the current tip of the source code, you do a clone of the entire repository.
  * Data assurance. The data model that git uses ensures the cryptographic integrity of every bit of your project. 
  * Staging area. This is an immediate area where commits can be formatted and revied before completing the commit.
  * Git is free and open source 

## Basic Git commands {.wp-block-heading}

Let&#8217;s begin by exploring some basic git commands used:

**Git init** creates a new local git repository. Use the following command to initialize your repository

<pre class="wp-block-syntaxhighlighter-code">$ git init</pre>

**Git clone** is used to copy a repository either from a remote server or on a local repository. You can use the following command to clone the repository. let&#8217;s start with cloning a repository from a local repository.

<pre class="wp-block-syntaxhighlighter-code">git clone </pre>

**Git add**. This adds new or changed files in your working directory to the git staging area. 

<pre class="wp-block-code"><code>$ git add &lt;filename&gt;</code></pre>

**Git add <filename>** selects that file and moves it to the staging area marking it for inclusion in the next commit. 

**Git branch**. Git branches are effectively a pointer to a snapshot of your changes. A branch represents an independent line of development. Branches act as an abstraction for the edit, stage, and commit process. Git branch command lets you create, list, rename and delete branches.

**Git commit**. git commit creates a commit that is like a snapshot of your repository. Commits are snapshots of your entire repository at a specific time. Commits are the building blocks of save points within git&#8217;s version control. 

<pre class="wp-block-code"><code>$ git commit -m "commit message"</code></pre>

The above command starts the commit process and allows you to include the commit message. -m lets you write a commit message on the terminal.

**Git remote**. git remote manages sets of remote that you are tracking with your local repository. 

  * git remote -v: List the current remotes associated with the local repository.
  * git remote add \[name\] \[url\]: add a remote
  * git remote remove [name]: remove a remote

**Git status**. This shows the current state of your git working directory and staging area. Whenever you are in doubt run **git status** command.

**Git pull:** This updates your current working branch and all of the remote-tracking branches. It is always good to run **git pull** on branches you are working on locally. 

**Git push**: It uploads all local branch commits to the corresponding remote branch. Git push updates the remote branch with local commit. 

## Prerequisites {.wp-block-heading}

<ul id="block-52482f66-edbe-4912-8b66-be9f8ad81481">
  <li>
    Have an Ubuntu 20.04 server up and running
  </li>
  <li>
    Have a user with non-root access or with sudo privileges
  </li>
  <li>
    Have basic knowlege of terminal.
  </li>
</ul>

## Table of Contents {.wp-block-heading}

  1. Run system updates 
  2. install git from the source 
  3. Configuring git
  4. Conclusion

## 1. Run system updates {.wp-block-heading}

The first step is to run system updates in order to make all the repositories up to date. On your terminal run the following command. 

<pre class="wp-block-code"><code>$ sudo apt install update && apt upgrade -y</code></pre>

When both updates and upgrades are complete, you can begin installing git.

## 2. Install git from the source {.wp-block-heading}

To install git from the source we will have to compile the package. the good thing about compiling from the source is that we get to install the latest version of git and allow you to be in control of whatever you wish to include in the process.

Let&#8217;s begin by installing git software dependencies. They are available from the software repositories. Run the following command to install git software dependencies.

<pre class="wp-block-code"><code>$ sudo apt install gcc cmake libz-dev libssl-dev libcurl4-gnutls-dev libexpat1-dev gettext</code></pre>

Sample output.

<pre class="wp-block-code"><code>output
Reading state information... Done
Note, selecting 'zlib1g-dev' instead of 'libz-dev'
The following additional packages will be installed:
  binutils binutils-common binutils-x86-64-linux-gnu cmake-data cpp cpp-9 gcc-9 gcc-9-base libasan5 libatomic1
  libbinutils libc-dev-bin libc6-dev libcc1-0 libcroco3 libcrypt-dev libctf-nobfd0 libctf0 libgcc-9-dev
  libgomp1 libisl22 libitm1 libjsoncpp1 liblsan0 libmpc3 libquadmath0 librhash0 libtsan0 libubsan1
  linux-libc-dev make manpages-dev
Suggested packages:
  binutils-doc cmake-doc ninja-build cpp-doc gcc-9-locales gcc-multilib autoconf automake libtool flex bison
  gdb gcc-doc gcc-9-multilib gcc-9-doc gettext-doc autopoint libasprintf-dev libgettextpo-dev glibc-doc
  libcurl4-doc libgnutls28-dev libidn11-dev libkrb5-dev libldap2-dev librtmp-dev libssh2-1-dev pkg-config
  libssl-doc make-doc
The following NEW packages will be installed:
  binutils binutils-common binutils-x86-64-linux-gnu cmake cmake-data cpp cpp-9 gcc gcc-9 gcc-9-base gettext
  libasan5 libatomic1 libbinutils libc-dev-bin libc6-dev libcc1-0 libcroco3 libcrypt-dev libctf-nobfd0 libctf0
  libcurl4-gnutls-dev libexpat1-dev libgcc-9-dev libgomp1 libisl22 libitm1 libjsoncpp1 liblsan0 libmpc3
  libquadmath0 librhash0 libssl-dev libtsan0 libubsan1 linux-libc-dev make manpages-dev zlib1g-dev
0 upgraded, 39 newly installed, 0 to remove and 0 not upgraded.
Need to get 40.4 MB of archives.
After this operation, 180 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press enter to allow installation to continue.

Next, we need to download git tarball into our system. Navigate to the git website and select the tarball version to download. You can use curl in the process or download the tarball into your specified folder before extracting 

<pre class="wp-block-code"><code>$ curl -o git.tar.gz https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.34.1.tar.gz</code></pre>

Move ahead and extract the contents. To unpack the compressed tarball use the following command.

<pre class="wp-block-code"><code>$ tar -zxf git.tar.gz</code></pre>

Cd into the git directory

<pre class="wp-block-code"><code>$ cd git-*</code></pre>

After extraction is complete, make the package and install with the following commands.

<pre class="wp-block-code"><code>$ make prefix=/usr/local all
$ sudo make prefix=/usr/local install </code></pre>

wait for make process to complete

We need to replace the shell process so that the git installed will be used.

<pre class="wp-block-code"><code>$ exec bash</code></pre>

That&#8217;s all for installation. You can now check the version of the installed git with the following command.

<pre class="wp-block-code"><code>$ git --version
git version 2.34.1</code></pre>

## 3. Configuring git {.wp-block-heading}

Git can be configured with **git config** command. Run the following command to run basic configurations.

<pre class="wp-block-code"><code>$ git config --global user.name "Your_Name"
$ git config --global user.email "Your_email" </code></pre>

Display all the configurations you have implemented with the following command.

<pre class="wp-block-code"><code>$ git config --list 
user.name=nextgentips
user.email=nextgentips@gmail.com</code></pre>

## 4. Conclusion {.wp-block-heading}

That is it for now on how to install and configure basic git usage on ubuntu 20.04. Go ahead and learn how to implement those commands we have learned. Happy coding.

 [1]: https://git-scm.com/