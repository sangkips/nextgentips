---
title: How to install Wireguard on Debian 11/Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-25T13:53:20+00:00
url: /2021/10/25/how-to-install-wireguard-on-debian-11-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"203.221.110.208";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 124
rank_math_internal_links_processed:
  - 1
categories:
  - Security

---
In this tutorial we are going to learn how to install wireguard VPN on Debian 11. Wireguard is a communication protocol which is free and open source software that implements encrypted [virtual private networks][1](VPN) and was designed with some goals, to provide ease of use, high speed performance and low attack space.

Wireguard aims for better performance and more power than [IPsec][2] and [OpenVPN][3]. Wireguard protocol passes traffic over [UDP][4]. Let us define all these terms we have used so far. It utilizes state-of-the art cryptography.

**Virtual Private Network** (VPN)- A VPN is an encrypted connection over internet from a device to a network. The encrypted connection ensures that data is safely transmitted. It prevents unauthorized people from eavesdropping on the traffic and allows the user to conduct work remotely. 

**IPSec** is a group of protocols that are used together to set up encrypted connections between devices. It helps keep data sent over private network secure from intruders. It works by encrypting IP packets along with authenticating the source where it came from.

**OpenVPN** is a VPN system that implements techniques to create secure point-to-point or site-to-site in routed configurations and remote access facilities. OpenVPN allows peers to authenticate each other using pre-shared secret keys, password or Usernames. 

## Characteristics of Wireguard {.wp-block-heading}

  * It is simple and easy to use. Wireguard aims to be as easy as to configure and deploy as SSH. It is capable of roaming between IP addresses. It provides an extremely basic yet powerful interface.
  * Wireguard has minimal attack service. It has been designed with ease of implementation and simplicity in mind. It is meant to be easily in very few lines of code as compared to others such as OpenVPN which is implemented with very large code bases hence can be audited easily by security experts.
  * Wireguard has high performance. Wireguard lives under Linux Kernel which means secure networking can be very high speed. It is both suitable for both small embedded devices such as smart phones and fully loaded backbone routers.

## Install Wireguard from the source. {.wp-block-heading}

Wiregard can be built from the source by compiling the kernel module from the source. You will need gcc version 4.7 or greater and your kernel headers in the right location for compilation.

## 1. Install Wireguard toolchain  {.wp-block-heading}

Use the following command to install the correct tool chain.

<pre class="wp-block-code"><code>$ sudo apt-get install libelf-dev linux-headers-$(uname -r) build-essential pkg-config</code></pre>

Press Yes and enter to continue with installation.

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
linux-headers-5.11.0-18-generic is already the newest version (5.11.0-18.19).
linux-headers-5.11.0-18-generic set to manually installed.
The following additional packages will be installed:
  bzip2 cpp cpp-10 dpkg-dev fakeroot fontconfig-config fonts-dejavu-core g++ g++-10 gcc gcc-10 gcc-10-base
  libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan6 libatomic1 libc-dev-bin
  libc-devtools libc6-dev libcc1-0 libcrypt-dev libdeflate0 libdpkg-perl libfakeroot libfile-fcntllock-perl
  libfontconfig1 libgcc-10-dev libgd3 libgomp1 libisl23 libitm1 libjbig0 libjpeg-turbo8 libjpeg8 liblsan0
  libmpc3 libnsl-dev libquadmath0 libstdc++-10-dev libtiff5 libtirpc-dev libtsan0 libubsan1 libwebp6 libxpm4
  linux-libc-dev lto-disabled-list make manpages-dev rpcsvc-proto zlib1g-dev
Suggested packages:
  bzip2-doc cpp-doc gcc-10-locales debian-keyring g++-multilib g++-10-multilib gcc-10-doc gcc-multilib autoconf
  automake libtool flex bison gdb gcc-doc gcc-10-multilib glibc-doc bzr libgd-tools libstdc++-10-doc make-doc
The following NEW packages will be installed:
  build-essential bzip2 cpp cpp-10 dpkg-dev fakeroot fontconfig-config fonts-dejavu-core g++ g++-10 gcc gcc-10
  gcc-10-base libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan6 libatomic1
  libc-dev-bin libc-devtools libc6-dev libcc1-0 libcrypt-dev libdeflate0 libdpkg-perl libelf-dev libfakeroot
  libfile-fcntllock-perl libfontconfig1 libgcc-10-dev libgd3 libgomp1 libisl23 libitm1 libjbig0 libjpeg-turbo8
  libjpeg8 liblsan0 libmpc3 libnsl-dev libquadmath0 libstdc++-10-dev libtiff5 libtirpc-dev libtsan0 libubsan1
  libwebp6 libxpm4 linux-libc-dev lto-disabled-list make manpages-dev pkg-config rpcsvc-proto zlib1g-dev
0 upgraded, 55 newly installed, 0 to remove and 4 not upgraded.
Need to get 56.9 MB of archives.
After this operation, 212 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

## 2. Get the code {.wp-block-heading}

Let us clone the git to get the code for building wireguard with the following code.

<pre class="wp-block-code"><code>$ git clone https://git.zx2c4.com/wireguard-linux-compat</code></pre>

<pre class="wp-block-code"><code>Output
#  git clone https://git.zx2c4.com/wireguard-linux-compat
Cloning into 'wireguard-linux-compat'...
remote: Enumerating objects: 8763, done.
remote: Counting objects: 100% (8763/8763), done.
remote: Compressing objects: 100% (2622/2622), done.
remote: Total 8763 (delta 6653), reused 7759 (delta 5928), pack-reused 0
Receiving objects: 100% (8763/8763), 2.03 MiB | 13.59 MiB/s, done.
Resolving deltas: 100% (6653/6653), done.</code></pre>

Then get the wireguard tools with the following command.

<pre class="wp-block-code"><code>$ git clone https://git.zx2c4.com/wireguard-tools</code></pre>

<pre class="wp-block-code"><code>Output
# git clone https://git.zx2c4.com/wireguard-tools
Cloning into 'wireguard-tools'...
remote: Enumerating objects: 661, done.
remote: Counting objects: 100% (661/661), done.
remote: Compressing objects: 100% (636/636), done.
remote: Total 3017 (delta 389), reused 9 (delta 9), pack-reused 2356
Receiving objects: 100% (3017/3017), 639.38 KiB | 5.66 MiB/s, done.
Resolving deltas: 100% (2012/2012), done.</code></pre>

## 3. Compile and install the module. {.wp-block-heading}

Let us compile our clone github repository with the following command:

<pre class="wp-block-code"><code>$ make -C wireguard-linux-compat/src -j$(nproc)

$ sudo make -C wireguard-linux-compat/src install</code></pre>

## 4. Compile and install the wg (8) tool {.wp-block-heading}

**wg**&nbsp;is the configuration utility for getting and setting the configuration of WireGuard tunnel interfaces. Use the following command install wg.

<pre class="wp-block-code"><code>$ make -C wireguard-tools/src -j$(nproc)</code></pre>

<pre class="wp-block-code"><code>Output
$ make -C wireguard-tools/src -j$(nproc)
make: Entering directory '/home/nextgentips/wireguard-tools/src'
  CC      wg.o
  CC      config.o
  CC      curve25519.o
  CC      encoding.o
  CC      genkey.o
  CC      ipc.o
  CC      pubkey.o
  CC      set.o
  CC      setconf.o
  CC      show.o
  CC      showconf.o
  CC      terminal.o
  LD      wg
make: Leaving directory '/home/nextgentips/wireguard-tools/src'</code></pre>

<pre class="wp-block-code"><code>$ sudo make -C wireguard-tools/src install</code></pre>

<pre class="wp-block-code"><code>Sample ouput
$ sudo make -C wireguard-tools/src install
make: Entering directory '/home/nextgentips/wireguard-tools/src'
'wg' -&gt; '/usr/bin/wg'
'man/wg.8' -&gt; '/usr/share/man/man8/wg.8'
'completion/wg.bash-completion' -&gt; '/usr/share/bash-completion/completions/wg'
'wg-quick/linux.bash' -&gt; '/usr/bin/wg-quick'
install: creating directory '/etc/wireguard'
'man/wg-quick.8' -&gt; '/usr/share/man/man8/wg-quick.8'
'completion/wg-quick.bash-completion' -&gt; '/usr/share/bash-completion/completions/wg-quick'
'systemd/wg-quick.target' -&gt; '/lib/systemd/system/wg-quick.target'
'systemd/wg-quick@.service' -&gt; '/lib/systemd/system/wg-quick@.service'
make: Leaving directory '/home/nextgentips/wireguard-tools/src'</code></pre>

## Conclusion {.wp-block-heading}

We have installed wireguard on our Ubuntu 21.04/ Debian 11 through compilation from the source. Incase you face any challenges you can always consult the wireguard [documentation][5]

 [1]: https://www.cisco.com/c/en/us/products/security/vpn-endpoint-security-clients/what-is-vpn.html
 [2]: https://www.cloudflare.com/learning/network-layer/what-is-ipsec/#:~:text=IPsec%20is%20a%20group%20of%20protocols%20that%20are%20used%20together,where%20the%20packets%20come%20from.
 [3]: https://openvpn.net/what-is-a-vpn/
 [4]: https://en.wikipedia.org/wiki/User_Datagram_Protocol
 [5]: https://www.wireguard.com/