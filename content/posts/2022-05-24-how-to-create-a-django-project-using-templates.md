---
title: How to create Django Templates
author: Kipkoech Sang
type: post
date: 2022-05-24T19:11:06+00:00
url: /2022/05/24/how-to-create-a-django-project-using-templates/
rank_math_seo_score:
  - 14
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 36
rank_math_analytic_object_id:
  - 181
categories:
  - Programming

---
In this tutorial, we will learn how to create Django models. We are going to use Django 4.0. In the previous tutorial, we saw how to <a href="https://nextgentips.com/2022/05/03/how-to-set-up-a-python-django-application-using-django-4-0/" target="_blank" rel="noreferrer noopener">create your first project</a>. We displayed hello world in our browser.

As a recap of what we saw in the previous lesson, let&#8217;s create a new project once more.

On your terminal, we can create a new directory for our project first. Let&#8217;s call **new_project**

<pre class="wp-block-code"><code>$ cd Desktop
$ sudo mkdir new_project
$ cd new_project</code></pre>

When you are inside the new_project folder, now is the time to install Django. As a good practice, I always advise creating a **requirements.txt** file so that you can store all your installed applications. So to create a requirements.txt file, go to the terminal. Remember we are at the new_project directory. 

<pre class="wp-block-code"><code>$ sudo touch requirements.txt</code></pre>

Now that we have the requiremets.txt file, we can now use nano or vim to write into it. I am going to use vim text editor, you can use any you prefer.

<pre class="wp-block-code"><code>sudo vi requirements.txt</code></pre>

Inside django inside this file.

<pre class="wp-block-code"><code>django~=4.0</code></pre>

## Create a Virtual Environment {.wp-block-heading}

Let&#8217;s begin by creating a virtual environment. Use the following code to accomplish that.

<pre class="wp-block-code"><code>$ sudo python3 -m venv myvenv</code></pre>

Myvenv here is the name of our virtual environment.

Then we need to activate our virtual environment before we can begin our project.

<pre class="wp-block-code"><code>source myvenv/bin/activate</code></pre>

Please make sure you see your terminal being prefixed by the name of your terminal like this.

<pre class="wp-block-code"><code>(myvenv) nextgen@zx-pc:~/Desktop/new_project$ </code></pre>

## Install Django {.wp-block-heading}

Let&#8217;s now install Django using the pip command inside the virtual environment. Remember the requirements.txt we created earlier, it&#8217;s now time we use it to install Django.

<pre class="wp-block-code"><code>$ &lt;strong>pip install -r requirements.txt&lt;/strong>
Collecting django
  Using cached Django-4.0.4-py3-none-any.whl (8.0 MB)
Collecting asgiref&lt;4,&gt;=3.4.1
  Using cached asgiref-3.5.2-py3-none-any.whl (22 kB)
Collecting sqlparse&gt;=0.2.2
  Using cached sqlparse-0.4.2-py3-none-any.whl (42 kB)
Installing collected packages: sqlparse, asgiref, django
Successfully installed asgiref-3.5.2 django-4.0.4 sqlparse-0.4.2</code></pre>

Now that we have installed Django successfully, we can proceed to create a project.

## Start Django Project {.wp-block-heading}

To start a Django project, use the following command inside the virtual environment.

<pre class="wp-block-code"><code>$ django-admin startproject main .</code></pre>

The name of our project is **main** and please don&#8217;t forget the period in the end. It shows that we are creating a project in the current directory.

After we have created the project, we can now move ahead and create Django app 

<pre class="wp-block-code"><code>python manage.py startapp products </code></pre>

The name of the app we are creating is **products** 

## Create Django views {.wp-block-heading}

A view function is a Python function that takes the web request and returns a web function. Let&#8217;s see with an example what we mean.

<pre class="wp-block-code"><code>from django.shortcuts import render

from django.http import HttpResponse


def home_view(request):
    return HttpResponse('&lt;h2&gt;Hello world&lt;h2&gt;')</code></pre>

Inside the products app, we need to create a urls.py file where the routing logic will occur. Add this file and proceed.

<pre class="wp-block-code"><code>from django.urls import path
from . views import home_view

urlpatterns = &#91;
    path('', home_view, name='home'),
]</code></pre>

Go to main/urls.py file and create the following 

<pre class="wp-block-code"><code>from django.contrib import admin
from django.urls import path, include

urlpatterns = &#91;
    path('admin/', admin.site.urls),
    path('', include('products.urls')),
]</code></pre>

The next thing we need to do is to add our product name inside the settings.py file. Under installed apps, we need to add products app there like this.

<pre class="wp-block-code"><code>INSTALLED_APPS = &#91;
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'products.apps.ProductsConfig',# add this file 
]</code></pre>

To run our app use the following command inside the root of your django project. Here root is where you are having manage.py file.

<pre class="wp-block-code"><code>$ python manage.py runserver</code></pre>

This is what we are going to see.

<pre class="wp-block-code"><code>System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
May 24, 2022 - 18:02:56
Django version 4.0.4, using settings 'main.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
</code></pre>

Don&#8217;t worry about unapplied migrations we will deal with that in a while. Open your browser and use this **http://127.0.0.1:8000/** to open our development server.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="404" height="192" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-05-05.png?resize=404%2C192&#038;ssl=1" alt="django view " class="wp-image-1360" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-05-05.png?w=404&ssl=1 404w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-05-05.png?resize=300%2C143&ssl=1 300w" sizes="(max-width: 404px) 100vw, 404px" data-recalc-dims="1" /> <figcaption>django view</figcaption></figure> 

Hurray! We have run our first Django program. If you are getting what you can see above, then we can proceed.

## Creating Templates  {.wp-block-heading}

From what we have created so far lies one big problem, here we are not in a position to render templates meaning we are not in a position to scale our project. What we ought to do is make use of templates. Here templates I mean make use of HTML files. Let&#8217;s see how we can implement this.

From the views.py file, we have what we have created above, I want us to delete **return HttpResponse(&#8216;<h1> Hello world </h1>&#8217;)** and use **return render(request, &#8216;html&#8217;)**

<pre class="wp-block-code"><code>from django.http import HttpResponse
from django.shortcuts import render


def home_view(request):
    return render(request, 'products/index.html')# add this file </code></pre>

We need first to create a template file inside our products app. To do that, go to the products folder and create another folder and call it **templates**, make sure you spelled it correctly. Inside templates create an index.html file.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="313" height="181" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-18-41.png?resize=313%2C181&#038;ssl=1" alt="templates " class="wp-image-1361" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-18-41.png?w=313&ssl=1 313w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-18-41.png?resize=300%2C173&ssl=1 300w" sizes="(max-width: 313px) 100vw, 313px" data-recalc-dims="1" /> <figcaption>templates </figcaption></figure> 

Before we can write anything inside the index.html file, we need first to move to the settings.py file and add our template path

<pre class="wp-block-code"><code>TEMPLATES = &#91;
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': &#91;
            BASE_DIR / 'templates' # add this file 
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': &#91;
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]</code></pre>

First, let&#8217;s apply those unapplied migrations with the following command. This will populate our database.

<pre class="wp-block-code"><code>$ python manage.py migrate
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK</code></pre>

Run python manage.py runserver again and open your browser.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="395" height="181" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-57-41.png?resize=395%2C181&#038;ssl=1" alt="Django using template" class="wp-image-1363" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-57-41.png?w=395&ssl=1 395w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-24-21-57-41.png?resize=300%2C137&ssl=1 300w" sizes="(max-width: 395px) 100vw, 395px" data-recalc-dims="1" /> <figcaption>Django using template</figcaption></figure> 

We have arrived at the same thing but this time using template form.

In the next lesson, we are going to see how to perform CRUD operations using Django.