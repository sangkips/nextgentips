---
title: How to install InfluxDB 2 on Debian 11
author: Kipkoech Sang
type: post
date: 2022-02-09T18:31:39+00:00
url: /2022/02/09/how-to-install-influxdb-2-on-debian-11/
rank_math_primary_category:
  - 7
rank_math_description:
  - InfluxDB is an open-source time-series database. It is used for the storage and retrieval of time series data. Lets Install InfluxDB
rank_math_focus_keyword:
  - install influxdb
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 7
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
InfluxDB is an open-source time-series database. It is used for storage and retrieval of time series data in fields such as operation monitoring, operations metrics, internet of things sensor data, and real-time analytics. In this tutorial, we are going to learn how to install InfluxDB on Debian 11.

By default, InfluxDB uses the following network ports for communication.

  * **TCP port 8086**&nbsp;for client-server communication.
  * **TCP port 8088**&nbsp;for RPC service to perform backup and restore operations.

## Why InfluxDB? {#why-influxdb.wp-block-heading}

  * It provides deep insight and analytics
  * InfluxDB is optimized for developer productivity
  * It has best UI which features dashboard, explorer and script editor.
  * It has faster time to market. That is production is faster while usin influxDB.
  * It is easy to build and easy to share templates.

## Prerequisites {#prerequisites.wp-block-heading}

  * You need to be root to perform tasks
  * Have basic knowledge of terminal.
  * Have Debian 11 server up and running.

## Related Articles  {#related-articles.wp-block-heading}

  * [How to install InfluxDB on Ubuntu 20.04][1]

## Installing InfluxDB on Debian 11 {#installing-influxdb-on-debian-11.wp-block-heading}

### 1. Update system repositories  {#1-update-system-repositories.wp-block-heading}

The first thing to do is to make sure that repositories are up to date. Updating repositories will enable you to avoid running into errors later.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Add InfluxDB repository to Ubuntu Repository {#2-add-influxdb-repository-to-ubuntu-repository.wp-block-heading}

To install the stable latest version using apt, run the following command using curl.

**Add key**

<pre class="wp-block-code"><code>$ wget -qO- https://repos.influxdata.com/influxdb.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdb.gpg &gt; /dev/null</code></pre>

<pre class="wp-block-code"><code>$ export DISTRIB_ID=$(lsb_release -si); export DISTRIB_CODENAME=$(lsb_release -sc)</code></pre>

### 3. Add Repository {#3-add-repository.wp-block-heading}

<pre class="wp-block-code"><code>$ echo "deb &#91;signed-by=/etc/apt/trusted.gpg.d/influxdb.gpg] https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list &gt; /dev/null</code></pre>

Then update the system repositories again

<pre class="wp-block-code"><code>$ sudo apt update
&lt;strong>Get:5 https://repos.influxdata.com/debian bullseye InRelease &#91;4739 B]&lt;/strong>                                                         
Hit:6 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                       
Get:7 http://deb.debian.org/debian bullseye-backports/main Sources.diff/Index &#91;63.3 kB]
Get:8 http://deb.debian.org/debian bullseye-backports/main amd64 Packages.diff/Index &#91;63.3 kB]
Get:9 http://deb.debian.org/debian bullseye-backports/main Translation-en.diff/Index &#91;60.0 kB]</code></pre>

### 4. Install influxDB on Ubuntu 20.04 {#4-install-influxdb-on-ubuntu-20-04.wp-block-heading}

To install the latest stable version of InfluxDB we use the following command:

<pre class="wp-block-code"><code>$ sudo apt install influxdb2</code></pre>

The following is the output you will get from the terminal.

<pre class="wp-block-code"><code>Output.
The following NEW packages will be installed:
  influxdb2 influxdb2-cli
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 114 MB of archives.
After this operation, 195 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y
Get:1 https://repos.influxdata.com/debian bullseye/stable amd64 influxdb2 amd64 2.1.1 &#91;108 MB]
Get:2 https://repos.influxdata.com/debian bullseye/stable amd64 influxdb2-cli amd64 2.2.1 &#91;5325 kB]
Fetched 114 MB in 2s (68.9 MB/s)    
Selecting previously unselected package influxdb2.
(Reading database ... 25056 files and directories currently installed.)
Preparing to unpack .../influxdb2_2.1.1_amd64.deb ...
Unpacking influxdb2 (2.1.1) ...
Selecting previously unselected package influxdb2-cli.
Preparing to unpack .../influxdb2-cli_2.2.1_amd64.deb ...
Unpacking influxdb2-cli (2.2.1) ...
Setting up influxdb2 (2.1.1) ...
Created symlink /etc/systemd/system/influxd.service → /lib/systemd/system/influxdb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/influxdb.service → /lib/systemd/system/influxdb.service.
Setting up influxdb2-cli (2.2.1) ...</code></pre>

### 5. Configuring InfluxDB {#5-configuring-influxdb.wp-block-heading}

In order to start using InfluxDB, we need to start and enable it.

<pre class="wp-block-code"><code># start InfluxDB
$ sudo systemctl start influxdb
# enable
$ sudo systemctl enable --now influxdb
# status
$ sudo systemctl status influxdb</code></pre>

<pre class="wp-block-code"><code># Status 
● influxdb.service - InfluxDB is an open-source, distributed, time series database
     Loaded: loaded (/lib/systemd/system/influxdb.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-02-09 17:52:45 UTC; 43s ago
       Docs: https://docs.influxdata.com/influxdb/
    Process: 550 ExecStart=/usr/lib/influxdb/scripts/influxd-systemd-start.sh (code=exited, status=0/SUCCESS)
   Main PID: 555 (influxd)
      Tasks: 7 (limit: 1132)
     Memory: 111.2M
        CPU: 735ms
     CGroup: /system.slice/influxdb.service
             └─555 /usr/bin/influxd

Feb 09 17:52:44 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:44.761167Z lvl=info msg="Open store (end)" log_id=0Z&gt;
Feb 09 17:52:44 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:44.761210Z lvl=info msg="Starting retention policy e&gt;
Feb 09 17:52:44 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:44.761247Z lvl=info msg="Starting precreation servic&gt;
Feb 09 17:52:44 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:44.761304Z lvl=info msg="Starting query controller" &gt;
Feb 09 17:52:44 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:44.766154Z lvl=info msg="Configuring InfluxQL statem&gt;
Feb 09 17:52:44 debian influxd-systemd-start.sh&#91;550]: InfluxDB API at http://localhost:8086/ready unavailable after 1 attempts&gt;
Feb 09 17:52:45 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:45.084183Z lvl=info msg=Listening log_id=0ZZuh_gW000&gt;
Feb 09 17:52:45 debian influxd-systemd-start.sh&#91;555]: ts=2022-02-09T17:52:45.085334Z lvl=info msg=Starting log_id=0ZZuh_gW000 &gt;
Feb 09 17:52:45 debian influxd-systemd-start.sh&#91;550]: InfluxDB started
Feb 09 17:52:45 debian systemd&#91;1]: Started InfluxDB is an open-source, distributed, time series database.
lines 1-22/22 (END)</code></pre>

Enable –now InfluxDB will ensure that InfluxDB starts every time the server is reloaded. We don’t need to reenable every time.

### 6. Enable Firewall settings {#6-enable-firewall-settings.wp-block-heading}

To ensure that we can access InfluxDB from outside, we need to enable TCP 8086 on the firewall.

To know more about how to enable firewall, read this article&nbsp;[How to setup Ubuntu 20.04 Server for the first time][2]

<pre class="wp-block-code"><code>$ sudo ufw allow 8086/tcp</code></pre>

To check the status we use&nbsp;**ufw enable and status**&nbsp;command

<pre class="wp-block-code"><code>$ sudo ufw enable
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
8086/tcp                   ALLOW       Anywhere                  
8086/tcp (v6)              ALLOW       Anywhere (v6)  </code></pre>

We can verify the version installed with the following command.

<pre class="wp-block-code"><code>$ influx version
Influx CLI 2.2.1 (git: 31ac783) build_date: 2021-11-09T21:24:22Z</code></pre>

### 7. Setting up InfluxDB  {#7-setting-up-influxdb.wp-block-heading}

Initial InfluxDB setup entails creating a default organization, user, bucket, and operator API token.

### Setup InfluxDB through UI {#setup-influxdb-through-ui.wp-block-heading}

  * With InfluxDB running visit **localhost:8086**.
  * Click get started

**Set up initial user**

  * Enter a Username for your initial user.
  * Enter a Password and Confirm Password for your user.
  * Enter your initial Organization Name.
  * Enter your initial Bucket Name.
  * Click Continue.

### Set up InfluxDB through Influx CLI  {#set-up-influxdb-through-influx-cli.wp-block-heading}

Begin CLI setup with the following command. 

<pre class="wp-block-code"><code>$ influx setup</code></pre>

  * Enter a primary username.
  * Enter a password for your user.
  * Confirm your password by entering it again.
  * Enter a name for your primary organization.
  * Enter a name for your primary bucket.
  * Enter a retention period for your primary bucket—valid units are nanoseconds (ns), microseconds (us or µs), milliseconds (ms), seconds (s), minutes (m), hours (h), days (d), and weeks (w). Enter nothing for an infinite retention period.
  * Confirm the details for your primary user, organization, and bucket.

<pre class="wp-block-code"><code>> Welcome to InfluxDB 2.0!
? Please type your primary username nextgentips
? Please type your password **********
? Please type your password again **********
? Please type your primary organization name nextgen
? Please type your primary bucket name example
? Please type your retention period in hours, or 0 for infinite 0
? Setup with these parameters?
  Username:          nextgentips
  Organization:      nextgen
  Bucket:            example
  Retention Period:  infinite
 Yes
User            Organization    Bucket
nextgentips     nextgen         example</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have successfully installed InfluxDB on Debian 11. In case of any issue head over to&nbsp;<a href="https://docs.influxdata.com/influxdb/v1.8/introduction/install/?t=curl" target="_blank" rel="noreferrer noopener">InfluxDB documentation</a>&nbsp;for clarification.

 [1]: https://nextgentips.com/2022/02/08/how-to-install-influxdb-on-ubuntu-20-04/
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/