---
title: How to install R and Rstudio on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-11-20T10:19:54+00:00
url: /2021/11/20/how-to-install-r-and-rstudio-on-ubuntu-20-04/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 94
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial, we are going to learn how to install R and Rstudio on Ubuntu 20.04.

R is a free software environment for statistical computing and graphics. It compiles and runs on different platforms including Windows, Linux, macOS. 

Rstudio provides free and open-source tools for R. Rstudio is an application like Microsoft word, except that instead of helping you write in English, Rstudio helps you write in R.

Even if you want to run on Rstudio completely, you still need to have R on your system because Rstudio helps you use the version of R that lives on your computer, but it doesn&#8217;t come with a version of R on its own. 

## Prerequisites  {.wp-block-heading}

To install R and Rstudio you need to consider the following:

  1. Have atleast 1 GB of RAM on your system
  2. Have basic understanding of command line 
  3. Have user with sudo privileges if not root user
  4. Have access to internet.

## Table of Contents {.wp-block-heading}

  1. Run system updates to make the repositories current 
  2. Install helper packages 
  3. Add GPG key
  4. Add R packages to Repository
  5. Install R
  6. install Rstudio
  7. Conclusion.

## 1. Run system Updates {.wp-block-heading}

We begin every installation by updating our system repositories in order to make them up to date. You can use the following command;

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After you have completed all the updates and upgrades you can now install helper packages.

## 2. Install Helper packages  {.wp-block-heading}

Helper functions produce a summary or table of the passed variable. So to install these packages we need to run the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo apt install --no-install-recommends software-properties-common dirmngr</code></pre>

It looks like we are not adding anything, for now, everything comes as a default on Ubuntu 20.04.

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
dirmngr is already the newest version (2.2.19-3ubuntu2.1).
dirmngr set to manually installed.
software-properties-common is already the newest version (0.99.9.8).
software-properties-common set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

## 3. Add GPG key to the system  {.wp-block-heading}

Add the following key by Michael Rutter to the repository.

<pre class="wp-block-code"><code>$ wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc</code></pre>

It will add a whole public key block to the system. After that, you need to verify your key block.

<pre class="wp-block-code"><code>$ gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc </code></pre>

You will get the following output

<pre class="wp-block-code"><code>output
gpg: directory '/root/.gnupg' created
gpg: keybox '/root/.gnupg/pubring.kbx' created
pub   rsa2048 2010-10-19 &#91;SCA] &#91;expires: 2027-09-30]
      E298A3A825C0D65DFD57CBB651716619E084DAB9
uid                      Michael Rutter &lt;marutter@gmail.com>
sub   rsa2048 2010-10-19 &#91;E] &#91;expires: 2027-09-30]</code></pre>

We have successfully added the GPG key so now we can proceed to add the R 4.1 repository from [CRAN][1].

## 4. Add the R 4.1 Repository from CRAN {.wp-block-heading}

CRAN is a network of FTP and web servers from around the world that stores identical, up to date, versions of code and documentation for R. Use the following command to add 

<pre class="wp-block-code"><code>$ sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'</code></pre>

You will get the following sample output.

<pre class="wp-block-code"><code>Output
Get:1 http://security.ubuntu.com/ubuntu focal-security InRelease &#91;114 kB]
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease
Hit:4 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Get:6 https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/ InRelease &#91;3622 B]
Get:7 https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/ Packages &#91;45.1 kB]
Fetched 163 kB in 1s (201 kB/s)
Reading package lists... Done</code></pre>

## 5. Install R and its dependecies {.wp-block-heading}

Run the following command in order to install R and its dependencies into the system.

<pre class="wp-block-code"><code>$ sudo apt install --no-install-recommends r-base -y</code></pre>

This will install R into our system. You can check to verify that R was installed by typing **R** into the terminal. You will get the following;

<pre class="wp-block-code"><code>output
R version 4.1.2 (2021-11-01) -- "Bird Hippie"
Copyright (C) 2021 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> q()</code></pre>

Type **q()** to quit the console. 

## 6. Install Rstudio {.wp-block-heading}

To install Rstudio we need to download it from the Rstudio download center. 

<pre class="wp-block-code"><code>$ wget -qO- https://download1.rstudio.org/desktop/bionic/amd64/rstudio-2021.09.1-372-amd64-debian.tar.gz</code></pre>

After the install is complete we can **ls** to see our downloaded Rstudio

<pre class="wp-block-code"><code>$ ls
rstudio-2021.09.1-372-amd64.deb  snap  wget-log</code></pre>

Now you can run the installer with the following command:

<pre class="wp-block-code"><code>$ sudo dpkg -i rstudio-2021.09.1-372-amd64.deb</code></pre>

This will run the installation on our system. For my case I ran into the following errors;

<pre class="wp-block-code"><code>output
Selecting previously unselected package rstudio.
(Reading database ... 97536 files and directories currently installed.)
Preparing to unpack rstudio-2021.09.1-372-amd64.deb ...
Unpacking rstudio (2021.09.1+372) ...
dpkg: dependency problems prevent configuration of rstudio:
 rstudio depends on libclang-dev; however:
  Package libclang-dev is not installed.
 rstudio depends on libxkbcommon-x11-0; however:
  Package libxkbcommon-x11-0 is not installed.
 rstudio depends on libpq5; however:
  Package libpq5 is not installed.

dpkg: error processing package rstudio (--install):
 dependency problems - leaving unconfigured
Processing triggers for mime-support (3.64ubuntu1) ...
Processing triggers for shared-mime-info (1.15-1) ...
Errors were encountered while processing:
 rstudio</code></pre>

We need to install the following dependencies causing errors. We can do the following to fix the errors;

<pre class="wp-block-code"><code>$ sudo apt install -f -y</code></pre>

Run the installer again, this time around it will be successful

<pre class="wp-block-code"><code>$ sudo dpkg -i rstudio-2021.09.1-372-amd64.deb</code></pre>

<pre class="wp-block-code"><code>output
(Reading database ... 105864 files and directories currently installed.)
Preparing to unpack rstudio-2021.09.1-372-amd64.deb ...
Unpacking rstudio (2021.09.1+372) over (2021.09.1+372) ...
Setting up rstudio (2021.09.1+372) ...
Processing triggers for mime-support (3.64ubuntu1) ...
Processing triggers for shared-mime-info (1.15-1) ...</code></pre>

As you can see we have successfully installed Rstudio.

To open Rstudio go to Ubuntu Desktop and open apps, you will see the Rstudio logo. Click on that and begin exploring Rstudio.

## 7. Conclusion. {.wp-block-heading}

We have successfully installed R and Rstudio on our Ubuntu 20.04. Go ahead and explore more.

 [1]: https://cran.r-project.org/