---
title: How to install Nagios Core on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-12T09:58:30+00:00
url: /2021/10/12/how-to-install-nagios-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 139
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
In this article, we are going to install Nagios Core on Ubuntu 20.04 server. Nagios core formally known as Nagios is a free and open-source software application that monitors systems, networks, and infrastructure. Nagios offers monitoring and alerting services for servers, switches, applications, and services. It alerts users when something goes wrong and alerts again when it has been rectified.

## Here is what you will end up with {.wp-block-heading}

  * Nagios and Nagios plugins will be installed underneath /usr/local/nagios.
  * Nagios will be installed to monitor few aspects of your system (cpu load, disk usage, etc.)
  * The nagios web interface will be accesible at http://localhost/nagios/

### Required packages  {.wp-block-heading}

  * PHP
  * Apache2
  * GCC compiler and development libraries 
  * GD development libraries 

Check out this article on how to [install Apache 2 and PHP on Ubuntu 20.04 Ubuntu server][1]

## Step 1: Update Ubuntu 20.04 server {.wp-block-heading}

Login to your server using the following command.

<pre class="wp-block-code"><code>$ ssh username@IP_Address</code></pre>

Once logged in issue the following commands to update the package list

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

Next, we can install the packages necessary for building Nagios 4 and Nagios plugins. 

<pre class="wp-block-code"><code>$ sudo apt install nagios4 nagios-plugins-contrib nagios-nrpe-plugin</code></pre>

<pre class="wp-block-code"><code>Sample output

Setting up libmonitoring-plugin-perl (0.40-1) ...
Setting up ruby (1:2.7+1) ...
Setting up rake (13.0.1-4) ...
Setting up liblwp-protocol-https-perl (6.07-2ubuntu2) ...
Setting up libwww-perl (6.43-1) ...
Setting up libruby2.7:amd64 (2.7.0-5ubuntu1.5) ...
Setting up liblwp-useragent-determined-perl (1.07-1) ...
Setting up libxml-parser-perl (2.46-1) ...
Setting up ruby2.7 (2.7.0-5ubuntu1.5) ...
Setting up libwebinject-perl (1.94-1) ...
Setting up libxml-sax-expat-perl (0.51-1) ...
update-perl-sax-parsers: Registering Perl SAX parser XML::SAX::Expat with priority 50...
update-perl-sax-parsers: Updating overall Perl SAX parser modules info file...
Replacing config file /etc/perl/XML/SAX/ParserDetails.ini with new version
Processing triggers for install-info (6.7.0.dfsg.2-5) ...
Processing triggers for mime-support (3.64ubuntu1) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
Processing triggers for rsyslog (8.2001.0-1ubuntu1.1) ...
Processing triggers for ufw (0.36-6ubuntu1) ...
Processing triggers for systemd (245.4-4ubuntu3.13) ...
Processing triggers for man-db (2.9.1-1) ...
Processing triggers for php7.4-cli (7.4.3-4ubuntu2.7) ...
Processing triggers for libapache2-mod-php7.4 (7.4.3-4ubuntu2.7) ...</code></pre>

The above command will install a bunch of commands including Apache2, postfix mail servers if you require, Nagios core, Nagios plugins.

We have an Apache configuration file that comes with Nagios depending on mod\_authz\_groupfile and mod\_auth\_digest modules, which are not enabled by default. 

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
mod\_authz\_groupfile module provides authorization capabilities so that authenticated users can be allowed or denied access to portions of the website by group membership.
 **mod\_auth\_digest** implements HTTP digest authentication and provides an alternative to **[mod\_auth\_basic][2]** where the password is not transmitted as cleartext. 

The following command enables the mod\_authz\_groupfile and mod\_auth\_digest modules on our system

<pre class="wp-block-code"><code>$ sudo a2enmod authz_groupfile auth_digest</code></pre>

<pre class="wp-block-code"><code>Sample output
Considering dependency authz_core for authz_groupfile:
Module authz_core already enabled
Enabling module authz_groupfile.
Considering dependency authn_core for auth_digest:
Module authn_core already enabled
Enabling module auth_digest.
To activate the new configuration, you need to run:
  systemctl restart apache2</code></pre>

Apache default configuration allows Nagios access from the localhost and private IPs only. Now we need to change the configuration so that only authenticated users can view the interface and issue commands.

Open your preferred text editor to make the necessary changes 

<pre class="wp-block-code"><code>$ sudo nano /etc/apache2/conf-enabled/nagios4-cgi.conf</code></pre>

<pre class="wp-block-code"><code>Sample output
#Require ip  ::1/128 fc00::/7 fe80::/10 10.0.0.0/8 127.0.0.0/8 169.254.0.0/16 172.16.0.0/12 192.168.0.0/16
    &lt;Files "cmd.cgi">
        AuthDigestDomain "Nagios4"
        AuthDigestProvider file
        AuthUserFile    "/etc/nagios4/htdigest.users"
        AuthGroupFile   "/etc/group"
        AuthName        "Nagios4"
        AuthType        Digest
        #Require all     granted
        Require        valid-user
    &lt;/Files>
&lt;/DirectoryMatch>

&lt;Directory /usr/share/nagios4/htdocs>
    Options     +ExecCGI
&lt;/Directory></code></pre>

From the above comment outline starting with **Require IP,** **<Files &#8220;cmd.cgi&#8221;>** and Require all granted. Uncomment the line containing **Require valid-user**. Save the file and exit nano.

The final results will look like the above

Now restart Apache for changes to take effect

<pre class="wp-block-code"><code>$ sudo systemctl restart apache2</code></pre>

You can now verify that both Apache and Nagios4 are running by using the following commands:

<pre class="wp-block-code"><code>$ sudo systemctl status apache2
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2021-11-20 13:14:03 UTC; 16s ago
       Docs: https://httpd.apache.org/docs/2.4/
    Process: 31569 ExecStart=/usr/sbin/apachectl start (code=exited, status=0/SUCCESS)
   Main PID: 31590 (apache2)
      Tasks: 6 (limit: 2344)
     Memory: 10.1M
     CGroup: /system.slice/apache2.service
             ├─31590 /usr/sbin/apache2 -k start
             ├─31591 /usr/sbin/apache2 -k start
             ├─31592 /usr/sbin/apache2 -k start
             ├─31593 /usr/sbin/apache2 -k start
             ├─31594 /usr/sbin/apache2 -k start
             └─31595 /usr/sbin/apache2 -k start

Nov 20 13:14:03 ubuntu-20 systemd&#91;1]: Starting The Apache HTTP Server...
Nov 20 13:14:03 ubuntu-20 apachectl&#91;31587]: AH00558: apache2: Could not reliably determine the server's fully qu>
Nov 20 13:14:03 ubuntu-20 systemd&#91;1]: Started The Apache HTTP Server.</code></pre>

<pre class="wp-block-code"><code>$ sudo systemctl status nagios4
● nagios4.service - LSB: nagios host/service/network monitoring and management system
     Loaded: loaded (/etc/init.d/nagios4; generated)
     Active: active (running) since Sat 2021-11-20 13:04:30 UTC; 11min ago
       Docs: man:systemd-sysv-generator(8)
      Tasks: 6 (limit: 2344)
     Memory: 3.7M
     CGroup: /system.slice/nagios4.service
             ├─26535 /usr/sbin/nagios4 -d /etc/nagios4/nagios.cfg
             ├─26536 /usr/sbin/nagios4 --worker /var/lib/nagios4/rw/nagios.qh
             ├─26537 /usr/sbin/nagios4 --worker /var/lib/nagios4/rw/nagios.qh
             ├─26538 /usr/sbin/nagios4 --worker /var/lib/nagios4/rw/nagios.qh
             ├─26539 /usr/sbin/nagios4 --worker /var/lib/nagios4/rw/nagios.qh
             └─26561 /usr/sbin/nagios4 -d /etc/nagios4/nagios.cfg

Nov 20 13:09:52 ubuntu-20 nagios4&#91;26535]: SERVICE ALERT: localhost;Swap Usage;CRITICAL;SOFT;2;SWAP CRITICAL - 0%>
Nov 20 13:10:52 ubuntu-20 nagios4&#91;26535]: SERVICE ALERT: localhost;Swap Usage;CRITICAL;SOFT;3;SWAP CRITICAL - 0%>
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: SERVICE ALERT: localhost;Swap Usage;CRITICAL;HARD;4;SWAP CRITICAL - 0%>
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: SERVICE NOTIFICATION: nagiosadmin;localhost;Swap Usage;CRITICAL;notify>
Nov 20 13:11:52 ubuntu-20 postfix/sendmail&#91;31551]: fatal: open /etc/postfix/main.cf: No such file or directory
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: wproc: NOTIFY job 4 from worker Core Worker 26539 is a non-check helpe>
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: wproc:   host=localhost; service=Swap Usage; contact=nagiosadmin
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: wproc:   early_timeout=0; exited_ok=1; wait_status=256; error_code=0;
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: wproc:   stderr line 01: send-mail: fatal: open /etc/postfix/main.cf: >
Nov 20 13:11:52 ubuntu-20 nagios4&#91;26535]: wproc:   stderr line 02: Can't send mail: sendmail process failed with></code></pre>

Postfix is not running because I did not want to send emails but you can allow that when prompted during installation.

## Creating a User Account {.wp-block-heading}

Nagios is configured by default to grant administrative privileges to the user called **[nagiosadmin][3]**. We use **[htdigest][4]** to create users.

Use the following command to create a new user:

<pre class="wp-block-code"><code>$ sudo htdigest -c /etc/nagios4/htdigest.users Nagios4 nagiosadmin
Adding password for nagiosadmin in realm Nagios4.
New password: &lt;password>
Re-type new password: &lt;password></code></pre>

Restart Apache service for effects to take place 

## Configure Firewall {.wp-block-heading}

Configure a firewall with UFW configuration tool and allow HTTP and HTTPS to pass through to allow Apache to be reachable.

<pre class="wp-block-code"><code>$ sudo ufw allow Apache
Rules updated
Rules updated (v6)</code></pre>

## Accessing Nagios Web Interface  {.wp-block-heading}

To access Nagios from the web interface use the following command:

<pre class="wp-block-code"><code>$ http://&lt;IP_Address&gt;/nagios4</code></pre>

## Conclusion {.wp-block-heading}

From the above tutorial you are now in a position to install Nagios in your server to help you monitor your services. You can read more by reading Nagios Documentation

 [1]: https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/
 [2]: https://httpd.apache.org/docs/2.4/mod/mod_auth_basic.html
 [3]: https://www.ibm.com/docs/en/power8?topic=POWER8/p8ef9/p8ef9_ppim_nagios_userid.htm
 [4]: https://httpd.apache.org/docs/2.4/programs/htdigest.html