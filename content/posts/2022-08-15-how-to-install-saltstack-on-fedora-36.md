---
title: How to install SaltStack on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-08-15T09:49:45+00:00
url: /2022/08/15/how-to-install-saltstack-on-fedora-36/
categories:
  - Automation

---
In this guide, we will walk you through the installation of SaltStack on Fedora 36.

SaltStack is a Python-based, open-source for event-driven It automation, remote task execution, and configuration management. Salt was designed to be highly modular and easily extensible, to make it easy to mold to diverse IT enterprise use cases.

Salt is capable of maintaining remote nodes in defined states that is it can ensure specific packages are installed and that specific services are running. Salt can query and execute commands either on individual nodes or by using arbitrary selection criteria.

## The core functions of Salt {.wp-block-heading}

  * Salt enables commands on the remote systems to be called in parallel rather than serially.
  * Salt provides a simple programming interface.
  * It uses the smallest and fastest network payload possible
  * Use a secure and encrypted protocol.

## Salt Architecture  {.wp-block-heading}

Salt uses the master client model. Master issues command to the client and the client executes the commands. **Salt master** is the server that is running the **salt-master** service. **Salt minions** are the servers running the **salt-minion** services.

**A target** is a group of minions across one or many masters that a job&#8217;s salt command applies to.

**Grains** interface derives information about the underlying system. Grains are collected for the operating system, domain name, IP addresses, kernel os, etc. 

## Installing salt on Fedora 36 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Before we begin, we need to update our system repositories in order to make them up to date.

<pre class="wp-block-code"><code>sudo dnf update </code></pre>

### 2. Install Salt on Fedora 36 {.wp-block-heading}

Salt is readily available on Fedora repositories and the EPEL. So it is easy to run and install Salt using the dnf or the yum package.

To install both Salt master and minion on your system, use the following command and remember that you need to install a master on the control machine and minions on the servers you want the salt to run on.

#### Install Salt master {.wp-block-heading}

<pre class="wp-block-code"><code>$ sudo dnf install salt-master</code></pre>

The following is the output from the above command.

<pre class="wp-block-code"><code>Package                    Arch        Version              Repository    Size
================================================================================
Installing:
 salt-master                noarch      3004.2-1.fc36        updates      3.1 M
Installing dependencies:
 dnf-utils                  noarch      4.2.1-1.fc36         updates       37 k
 libtomcrypt                x86_64      1.18.2-14.fc36       fedora       389 k
 libtommath                 x86_64      1.2.0-7.fc36         fedora        63 k
 libunwind                  x86_64      1.6.2-2.fc36         fedora        67 k
 openpgm                    x86_64      5.2.122-28.fc36      fedora       177 k
 python3-msgpack            x86_64      1.0.3-2.fc36         fedora        86 k
 python3-pycryptodomex      x86_64      3.15.0-1.fc36        updates      1.1 M
 python3-pycurl             x86_64      7.45.1-1.fc36        fedora       186 k
 python3-zmq                x86_64      22.3.0-2.fc36        fedora       403 k
 salt                       noarch      3004.2-1.fc36        updates      9.3 M
 zeromq                     x86_64      4.3.4-3.fc36         fedora       440 k

Transaction Summary
================================================================================
Install  12 Packages

Total download size: 15 M
Installed size: 58 M
Is this ok &#91;y/N]: y</code></pre>

To install Salt minion we use the following command.

<pre class="wp-block-code"><code>sudo dnf install salt-minion</code></pre>

Whenever you want to connect to minions, you need to have proper permission and for this we need to have Salt ssh.

<pre class="wp-block-code"><code>sudo dnf install salt-ssh</code></pre>

In order to install Salt API we use the following command.

<pre class="wp-block-code"><code>sudo dnf install salt-api</code></pre>

### 3. Post installation tasks  {.wp-block-heading}

After the installation is complete, you can run post installations tasks on both master and minions. 

To enable Salt master start automatically on boot use the following command.

<pre class="wp-block-code"><code>sudo systemctl enable salt-master</code></pre>

To start Salt master, we use the following command 

<pre class="wp-block-code"><code>sudo systemctl start salt-master</code></pre>

To enable Salt minion start automatically on boot we use the this command.

<pre class="wp-block-code"><code>sudo systemctl enable salt-minion</code></pre>

To start Salt minion, we use the following command.

<pre class="wp-block-code"><code>sudo systemctl start salt-minion</code></pre>

To check the status of both Salt master and Salt minion, we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status salt-master // for salt master
$ sudo systemctl status salt-minion // for salt minion
● salt-master.service - The Salt Master Server
     Loaded: loaded (/usr/lib/systemd/system/salt-master.service; enabled; ven&gt;
     Active: &lt;strong>active (running)&lt;/strong> since Sat 2022-08-13 06:36:03 UTC; 11s ago
       Docs: man:salt-master(1)
             file:///usr/share/doc/salt/html/contents.html
             https:&#47;&#47;docs.saltproject.io/en/latest/contents.html
   Main PID: 10739 (salt-master)
      Tasks: 32 (limit: 1113)
     Memory: 199.1M
        CPU: 6.621s
     CGroup: /system.slice/salt-master.service
             ├─ 10739 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10743 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10746 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10747 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10750 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10751 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10752 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10759 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10760 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10762 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10766 /usr/bin/python3 /usr/bin/salt-master
             ├─ 10768 /usr/bin/python3 /usr/bin/salt-master
</code></pre>

### 4. Configuration of the Saltstack {.wp-block-heading}

To start off, we need to configure the master so that it will know of the existence of the minions. All the configuration files are found on `<mark style="background-color:#abb8c3" class="has-inline-color">/etc/salt/</mark>`.The minions or the master must be findable for it to establish the communication.

To configure Salt master go to `<mark style="background-color:#abb8c3" class="has-inline-color">/etc/salt/master</mark>` and add your master IP address with Salt-master IP address.

<pre class="wp-block-code"><code>$ sudo vi /etc/salt/master</code></pre>

Uncomment interface and add the salt-master IP address.

Restart Salt-master for the changes to take effect.

<pre class="wp-block-code"><code>sudo systemctl restart salt-master</code></pre>

Next we need to configure Salt-minion, go to `<mark style="background-color:#abb8c3" class="has-inline-color">/etc/salt/minion</mark>` and replace master: salt with a master IP address.

<pre class="wp-block-code"><code>sudo /etc/salt/minion</code></pre>

Uncomment master: salt and remove salt and replace with your master IP address.

We need to restart the server again for the changes to take effect.

<pre class="wp-block-code"><code>sudo systemctl restart salt-minion </code></pre>

### 5. Test configuration {.wp-block-heading}

To see the configuration use the following commands:

<pre class="wp-block-code"><code>sudo salt-key -L</code></pre>

From the above command you be in a position to see the following:

<pre class="wp-block-code"><code>Accepted Keys:
Denied Keys:
Unaccepted Keys:
Rejected Keys:</code></pre>

To accept all keys use the following command.

<pre class="wp-block-code"><code>$ salt-key -A</code></pre>

## Conclusion {.wp-block-heading}

Congratulations we have installed Salt-master and Salt-Minion on Fedora 36.