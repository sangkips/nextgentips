---
title: How to install Minikube on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-23T16:59:12+00:00
url: /2022/01/23/how-to-install-minikube-on-ubuntu-20-04/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:13:"105.167.31.21";i:1;s:15:"110.249.202.113";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 25
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 38
rank_math_seo_score:
  - 21
categories:
  - Kubernetes

---
In his tutorial, we are going to learn how to install Minikube on Ubuntu 20.04. I am going to use Docker as a containerization platform. You can either VirtualBox, Podman, KVM, etc.

[Minikube][1]&nbsp;is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your local machine. Kubernetes quickly set up a local Kubernetes cluster on Linux, Windows, and macOS.

It is always advisable before diving into Kubernetes, you will need a minimal Kubernetes setup. Such a setup should spin up fast and integrate well with other tools.

Minikube is the best fit because of the following:

  * It runs on Windows, Linux and MacOS.
  * It supports the latest Kubernetes release
  * It supports multiple container runtimes i.e Containerd, KVM, Docker, Podman etc
  * It has supports for adbvanced features such as Load balancing, Featuregates and filesystems mounts.
  * It has support for Addons. Addons being a marketplace for developers to share configurations for running services on Minikube.
  * It supports CI environments

## Prerequisites {.wp-block-heading}

To run Kubernetes effectively you need to allocate the following to Minikube on your system.

  * 2 CPUs or more
  * 2 GB of memory
  * 20 GB or more of free disk space
  * Reliable internet connections.
  * Conatiner or virtual machine manager such as Docker, KVM, Podman, Virtual Box etc.

## Related Articles  {.wp-block-heading}

  * [How to install and configure Minikube on Ubuntu 21.10][2]
  * [How to install and use Minikube on Fedora 35][3]

## Install Minikube on Ubuntu 20.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

The first thing we need to do is to update our system repositories in order to make them up to date. Run the following command on your terminal to update our system.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Docker on Ubuntu 20.04 {.wp-block-heading}

As I said from the beginning, we are going to use Docker. Check this article on [how to install Docker CE on Ubuntu 20.04.][4]

You can install Docker using the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install docker.io</code></pre>

You need to enable and start docker so that it will start every time you boot your system. 

<pre class="wp-block-code"><code># Enable docker
$ sudo systemctl enable docker
# Start docker
$sudo systemctl start docker 
# Check docker status
$ sudo systemctl status docker  </code></pre>

<pre class="wp-block-code"><code># Docker Status 
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2022-01-23 15:32:26 UTC; 5min ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 8858 (dockerd)
      Tasks: 10
     Memory: 41.8M
     CGroup: /system.slice/docker.service
             └─8858 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock</code></pre>

Once Docker is installed, we need to add a user into our system because Docker runs in rootless mode.

**Add a user to Docker** with the following command.

<pre class="wp-block-code"><code># add user
$ useradd --create-home nextgentips
# give permission
$ sudo usermod -aG docker nextgentips</code></pre>

When Docker installation is complete then it&#8217;s time to install Kubectl.

### 3. Install Kubectl on Ubuntu 20.04 {.wp-block-heading}

Lets install kubectl using binary install. We are going to use the curl command.

<pre class="wp-block-code"><code>$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"</code></pre>

Download the following checksum to validate our install 

<pre class="wp-block-code"><code>$ curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"</code></pre>

Then when installation is complete validate with the following command.

<pre class="wp-block-code"><code>$ echo "$(&lt;kubectl.sha256)  kubectl" | sha256sum --check</code></pre>

If valid you will get **kubectl ok** as the output.

**Install Kubectl**

<pre class="wp-block-code"><code>$ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl</code></pre>

If you ls you will notice that kubectl changes color meaning it&#8217;s now executable.

Check if t=Kubectl is installed properly.

<pre class="wp-block-code"><code>$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.2", GitCommit:"9d142434e3af351a628bffee3939e64c681afa4d", GitTreeState:"clean", BuildDate:"2022-01-19T17:35:46Z", GoVersion:"go1.17.5", Compiler:"gc", Platform:"linux/amd64"}</code></pre>

### 4. Install Minikube on Ubuntu 20.04 {.wp-block-heading}

In order to install the latest version release of Minikube, we are going to download it using the curl command.

<pre class="wp-block-code"><code>$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb</code></pre>

Then we need to install using the following command.

<pre class="wp-block-code"><code>$ sudo dpkg -i minikube_latest_amd64.deb</code></pre>

<pre class="wp-block-code"><code># output
Selecting previously unselected package minikube.
(Reading database ... 95004 files and directories currently installed.)
Preparing to unpack minikube_latest_amd64.deb ...
Unpacking minikube (1.25.1-0) ...
Setting up minikube (1.25.1-0) ...</code></pre>

Check the **Minikube version** with the following command

<pre class="wp-block-code"><code>$ minikube version
minikube version: v1.25.1
commit: 3e64b11ed75e56e4898ea85f96b2e4af0301f43d</code></pre>

### 5. Start Minikube  {.wp-block-heading}

To start using Minikube we need to start it first with the following command:

<pre class="wp-block-code"><code>$ minikube start</code></pre>

You will see the following output

<pre class="wp-block-code"><code># output
* minikube v1.25.1 on Ubuntu 20.04 (kvm/amd64)
* Automatically selected the docker driver

X The requested memory allocation of 1983MiB does not leave room for system overhead (total system memory: 1983MiB). You may face stability issues.
* Suggestion: Start minikube with less memory allocated: 'minikube start --memory=1983mb'

* Starting control plane node minikube in cluster minikube
* Pulling base image ...
* Downloading Kubernetes v1.23.1 preload ...
    &gt; preloaded-images-k8s-v16-v1...: 504.42 MiB / 504.42 MiB  100.00% 231.43 M
    &gt; gcr.io/k8s-minikube/kicbase: 378.98 MiB / 378.98 MiB  100.00% 32.06 MiB p
* Creating docker container (CPUs=2, Memory=1983MB) ...
* Preparing Kubernetes v1.23.1 on Docker 20.10.12 ...
  - kubelet.housekeeping-interval=5m
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Verifying Kubernetes components...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Enabled addons: default-storageclass, storage-provisioner
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default</code></pre>

Check **kubectl cluster** information with the following command;

<pre class="wp-block-code"><code>$ kubectl cluster-info</code></pre>

If its working correctly then we will see a URL being shown 

<pre class="wp-block-code"><code># output
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.</code></pre>

To check configuration use **kubectl config view**

<pre class="wp-block-code"><code>$ kubectl config view </code></pre>

You will get a very nice json file like the one shown below.

<pre class="wp-block-code"><code># output
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/nextgentips/.minikube/ca.crt
    extensions:
    - extension:
        last-update: Sun, 23 Jan 2022 16:16:06 UTC
        provider: minikube.sigs.k8s.io
        version: v1.25.1
      name: cluster_info
    server: https://192.168.49.2:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    extensions:
    - extension:
        last-update: Sun, 23 Jan 2022 16:16:06 UTC
        provider: minikube.sigs.k8s.io
        version: v1.25.1
      name: context_info
    namespace: default
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/nextgentips/.minikube/profiles/minikube/client.crt
    client-key: /home/nextgentips/.minikube/profiles/minikube/client.key</code></pre>

### 6. Enable Kubernetes Dashboard {.wp-block-heading}

Kubernetes comes with a dashboard where you will manage your clusters. To enable dashboard, use the following command:

<pre class="wp-block-code"><code>$ minikube dashboard</code></pre>

Running the following command will get us the following output.

<pre class="wp-block-code"><code>#output
* Enabling dashboard ...
  - Using image kubernetesui/dashboard:v2.3.1
  - Using image kubernetesui/metrics-scraper:v1.0.7
* Verifying dashboard health ...
* Launching proxy ...
* Verifying proxy health ...
* Opening http://127.0.0.1:32855/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
  http:&#47;&#47;127.0.0.1:32855/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Minikube on Ubuntu 20.04. If you face any challenge do give us a comment and we will be happy to assist. 

If you want to learn more about Kubernetes check out the following good books, It will teach you how to pass your CKA exams

  * [Kubernetes Administration I: A Hands-On Guide to Certified Kubernetes Administrator Exam][5]

 [1]: https://minikube.sigs.k8s.io/docs/
 [2]: https://nextgentips.com/2021/12/24/how-to-install-and-configure-minikube-on-ubuntu-21-10/
 [3]: https://nextgentips.com/2021/12/18/how-to-install-and-configure-minikube-on-fedora-35/
 [4]: https://nextgentips.com/2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/ "how to install Docker CE on Ubuntu 20.04."
 [5]: https://www.amazon.com/gp/product/B07DQRHXT8/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07DQRHXT8&linkCode=as2&tag=nextgentips-20&linkId=c4b437459743a85abebaa37ea5af6e4c "Kubernetes Administration I: A Hands-On Guide to Certified Kubernetes"