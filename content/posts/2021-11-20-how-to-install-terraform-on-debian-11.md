---
title: How to install Terraform on Debian 11
author: Kipkoech Sang
type: post
date: 2021-11-20T20:54:12+00:00
url: /2021/11/20/how-to-install-terraform-on-debian-11/
ss_ss_click_share_count_facebook:
  - 1
ss_ss_click_share_count_twitter:
  - 1
ss_ss_click_share_count_linkedin:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 93
rank_math_internal_links_processed:
  - 1
categories:
  - Automation

---
In this tutorial, we are going to learn how to install Terraform on Debian 11.

Terraform is an open-source infrastructure as code (IaC) software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files.

Terraform Infrastructure as code (IaC) tools allow you to manage infrastructure with configuration files rather than through a graphical user interface. 

## Reasons for using Terraform {.wp-block-heading}

The advent of Terraform has played an important part in the IT world because of the following:

  * Terraform manages infrastructure across clouds. It provision infrastructure across large tone of public clouds and services using single workflow.
  * Terraform codify your application infrastructure hence reducing human error and increase automation by provisioning infrastructure as code.
  * Terraform create reproducible infrastructure hence providing consistent testing, staging and production environments with the same configuration.

## Prerequisites  {.wp-block-heading}

  1. Debian 11 system
  2. User with sudo privileges 
  3. Internet connection

## Table of Contents {.wp-block-heading}

  1. Update Debian system repositories 
  2. Add HashiCorp GPG key
  3. Install all required dependencies 
  4. Add official HashCorp Linux repository
  5. Install Terraform
  6. Verify installation
  7. Conclusion

## 1. Update Debian 11 repositories  {.wp-block-heading}

The first thing we need to do is to update our system repositories to make them up to date. You can use the following command to run systems updates;

<pre class="wp-block-code"><code>$ sudo apt update 

# Run upgrade where necessary
$ sudo apt upgrade </code></pre>

When both upgrades and updates are complete we can now add the HashiCorp GPG key into our system. We can use the following command to add our HashiCorp key.

<pre class="wp-block-code"><code>$ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -</code></pre>

If you got an ok message then you are good to go to the next step.

## 2. Install Terraform required dependencies  {.wp-block-heading}

Terraform has dependencies that need to be met before installing it. These dependencies include;

  * gnupg
  * curl
  * software properties common.

To install these dependencies use the following command to do so;

<pre class="wp-block-code"><code>$ sudo apt-get install -y gnupg software-properties-common curl</code></pre>

Sample output will look like this;

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
curl is already the newest version (7.74.0-1.3+b1).
gnupg is already the newest version (2.2.27-2).
gnupg set to manually installed.
The following additional packages will be installed:
  gir1.2-glib-2.0 gir1.2-packagekitglib-1.0 libappstream4 libdw1 libgirepository-1.0-1 libglib2.0-bin
  libglib2.0-data libgstreamer1.0-0 libpackagekit-glib2-18 libpolkit-agent-1-0 libpolkit-gobject-1-0
  libstemmer0d libunwind8 packagekit packagekit-tools policykit-1 python3-gi python3-software-properties
Suggested packages:
  gstreamer1.0-tools appstream
The following NEW packages will be installed:
  gir1.2-glib-2.0 gir1.2-packagekitglib-1.0 libappstream4 libdw1 libgirepository-1.0-1 libglib2.0-bin
  libglib2.0-data libgstreamer1.0-0 libpackagekit-glib2-18 libpolkit-agent-1-0 libpolkit-gobject-1-0
  libstemmer0d libunwind8 packagekit packagekit-tools policykit-1 python3-gi python3-software-properties
  software-properties-common
0 upgraded, 19 newly installed, 0 to remove and 0 not upgraded.</code></pre>

Move ahead to add the HashiCorp Linux repository

## 4. Add official HashiCorp Linux repository  {.wp-block-heading}

This repository will help us during the installation of HashiCorp later. To install this repository, use the following command to accomplish this;

<pre class="wp-block-code"><code>$ sudo apt-add-repository "deb &#91;arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"</code></pre>

Now we need to update our repositories for the changes to take effect. **Run sudo apt update**

<pre class="wp-block-code"><code>$ sudo apt update 
Get:6 https://apt.releases.hashicorp.com bullseye InRelease &#91;9501 B]                                       
Get:7 https://apt.releases.hashicorp.com bullseye/main amd64 Packages &#91;37.2 kB]
Fetched 46.7 kB in 1s (68.2 kB/s)   
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.</code></pre>

From the above, we can see that harshicorp.com has been added to our bullseye release. We can now proceed to install Terraform.

## 5. Install Terraform CLI {.wp-block-heading}

To install Terraform CLI we need to run the following command;

<pre class="wp-block-code"><code>$ sudo apt-get install terraform</code></pre>

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
  terraform
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 18.1 MB of archives.
After this operation, 59.1 MB of additional disk space will be used.
Get:1 https://apt.releases.hashicorp.com bullseye/main amd64 terraform amd64 1.0.11 &#91;18.1 MB]
Fetched 18.1 MB in 1s (14.2 MB/s)
Selecting previously unselected package terraform.
(Reading database ... 30451 files and directories currently installed.)
Preparing to unpack .../terraform_1.0.11_amd64.deb ...
Unpacking terraform (1.0.11) ...
Setting up terraform (1.0.11) ...</code></pre>

From the above, you can see that Terraform was installed successfully.

You can check the version of Terraform installed with the following;

<pre class="wp-block-code"><code>$ terraform version
Terraform v1.0.11
on linux_amd64</code></pre>

## Conclusion. {.wp-block-heading}

We have successfully installed Terraform on Debian 11, you can move ahead and start using Terraform. You can consult Terraform [documentation][1] in case of any problem.

 [1]: https://learn.hashicorp.com/tutorials/terraform/install-cli