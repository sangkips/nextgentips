---
title: How to install Portainer with Docker
author: Kipkoech Sang
type: post
date: 2022-01-15T12:11:11+00:00
url: /2022/01/15/how-to-install-portainer-with-docker/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 32
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Portainer is a free and open-source lightweight service delivery platform for containerized applications that can be used to manage Docker, Kubernetes, Docker swarm, etc. The application is simple to deploy and use. The application allows you to manage all your container services via smart GUIs or an extensive API, this makes the developers&#8217; work easier.

Portainer gives developers a chance to deploy, manage, and troubleshoot containerized applications without needing to deeply have experience with Kubernetes. This is awesome in my view.

In this tutorial we are going to learn how to install Portainer inside a docker container, also we will learn the **uses of Portainer**, what are **Portainer agents**. Also, we need to understand about **Portainer ports** i.e which ports do Portainer uses to communicate with the world. So let&#8217;s dive in

We have two editions of Portainer, the Portainer community edition which is free to use, and the Portainer Business Edition which requires one to purchase the license fee to use and it has more features compared to the community edition.

## Why you need Portainer? {.wp-block-heading}

The reason why we need to use Portainer in our case is because of the following reasons:

  * Portainers streamline the operations of container management so that enterprise can focus on other pressing needs.
  * Portainer removes the complexity associated with deploying and managing containers 
  * Portainer is the best tool to offer security to every enterprise giving good governance to any institution.
  * Portainer works well with Docker, Kubernetes, Docker swarm, meaning you have tone of applications to deploy with incase you are not well familiar with one, you can switched to another.
  * Portainer can manage networks for you within the GUI, os your work is to add, remove, or edit network endpoints configurations with ease.
  * Portainers easily manages containers for you easily.

## What are Portainer Agents? {.wp-block-heading}

Portainer has two elements, the Portainer server, and the Portainer agent. Think of a Portainer agent as the helper, it gets the information on behalf of the server, then goes ahead to negotiate how that info will be conveyed to the intended recipient. This is also how a Portainer agent works, Portainer agents are deployed to each node in your cluster and configured to report back to the Portainer server container. 

A single Portainer server can accept connections from many Portainer agents. 

### Portainer edge agent  {.wp-block-heading}

Think of a situation where you are working outside of your organization network, here the Portainer agents cant communicate with the Portainer server as intended because you are on a separate network. In this situation Poratiner edge agents come in, the remote environment only needs to access the Portainer server rather than the Portainer server needing access to the remote environment. This communication is managed by the Portainer edge agent. 

### Portainer ports  {.wp-block-heading}

Every application which needs to communicate with the outside world requires some ports to be open so that it allows full access to the website resources. This is the same case with the Portainer, it requires certain ports for communication.

Portainer Server listens on port 9443 for UI and API and exposes a TCP tunnel server on port 8000.

Portainer agents listen on port 9001.

## Prerequisites  {.wp-block-heading}

  * You need to have Docker version 20 + for you to run Portainer 2 and above.
  * Have basic knowlege of commanline 
  * 20 + GB diskspace
  * Have sudo access

## Install Portainer CE with Docker  {.wp-block-heading}

We are going to install the Portainer community edition because it&#8217;s free. I am also going to run docker on Ubuntu 20.04

### Run system updates  {.wp-block-heading}

To start off, we need to run system update in order to make our repositories up to date. 

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade
# you can reboot your system afterwards</code></pre>

## Install Docker on Ubuntu 20.04 {.wp-block-heading}

We need to first have Docker up and running before we can install Portainer. Check out this article on [how to install Docker on Ubuntu 20.04][1]. Definitely, you can use other distributions as you like.

So to install follow these steps 

Remove installed docker if you had installed another version

<pre class="wp-block-code"><code>$ sudo apt remove docker </code></pre>

Then install Docker 

<pre class="wp-block-code"><code>$ sudo apt install docker.io -y</code></pre>

You can check the version of installed Docker with:

<pre class="wp-block-code"><code>$ docker --version
Docker version 20.10.7, build 20.10.7-0ubuntu5~20.04.2</code></pre>

We have successfully installed Docker version 20.10.7 suitable for Portainer installation

You need to enable and start docker 

<pre class="wp-block-code"><code># enable docker 
$ sudo systemctl enable docker 
# start docker 
$ sudo systemctl start docker 
# check status 
$ sudo systemctl status docker </code></pre>

<pre class="wp-block-code"><code># docker start output
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2022-01-15 11:13:48 UTC; 3min 4s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 17244 (dockerd)
      Tasks: 8
     Memory: 50.9M
     CGroup: /system.slice/docker.service
             └─17244 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Jan 15 11:13:48 ubuntu dockerd&#91;17244]: time="2022-01-15T11:13:48.719137739Z" level=warning msg="Your kernel does></code></pre>

## Install Portainer with Docker  {.wp-block-heading}

Let&#8217;s dive into the installation of Portainer with Docker. Begin by creating a Portainer volume that will store Portainer server data

<pre class="wp-block-code"><code>$ docker volume create nextgen_data</code></pre>

You will get nextgen_data as output 

The next thing is to download and install the Portainer server

<pre class="wp-block-code"><code>$ docker run -d -p 8000:8000 -p 9443:9443 --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v nextgen_data:/data \
    portainer/portainer-ce</code></pre>

This will be the sample output

<pre class="wp-block-code"><code>Unable to find image 'portainer/portainer-ce:latest' locally
latest: Pulling from portainer/portainer-ce
0ea73420e2bb: Pull complete 
c367f59be2e1: Pull complete 
b71b88d796e2: Pull complete 
Digest: sha256:4f126c5114b63e9d1bceb4b368944d14323329a9a0d4e7bb7eb53c9b7435d498
Status: Downloaded newer image for portainer/portainer-ce:latest
abc0e25a561112a640a829f64cc15bd593847a619fab9553f33b0754a876e965</code></pre>

-d means running a container in a detached mode

-p shows the port number to use 

-v represents the volume 

To check if Portainer has been installed successfully run the **docker ps** command 

<pre class="wp-block-code"><code>$ docker ps</code></pre>

If you get the following output then you are good to continue 

<pre class="wp-block-code"><code>CONTAINER ID   IMAGE                    COMMAND        CREATED              STATUS              PORTS                                                                                            NAMES
abc0e25a5611   portainer/portainer-ce   "/portainer"   About a minute ago   Up About a minute   0.0.0.0:8000->8000/tcp, :::8000->8000/tcp, 0.0.0.0:9443->9443/tcp, :::9443->9443/tcp, 9000/tcp   portainer</code></pre>

Now that we have installed Portainer, the next thing is to access its GUI interface via the web browser. 

<pre class="wp-block-code"><code>https://&lt;your_IP_address>:9443</code></pre>

You will be greeted with the following interface.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="465" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-36-31.png?resize=810%2C465&#038;ssl=1" alt="Portainer Login page " class="wp-image-983" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-36-31.png?w=905&ssl=1 905w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-36-31.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-36-31.png?resize=768%2C441&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Portainer Login page </figcaption></figure> 

Choose a strong password and login<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="377" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-55-24.png?resize=810%2C377&#038;ssl=1" alt="Portainer Dashboard" class="wp-image-984" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-55-24.png?resize=1024%2C476&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-55-24.png?resize=300%2C140&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-55-24.png?resize=768%2C357&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-55-24.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Portainer Dashboard</figcaption></figure> 

Use Docker as the local environment. You will get the following screen<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-59-17.png?resize=810%2C464&#038;ssl=1" alt="Portainer interface" class="wp-image-985" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-59-17.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-59-17.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-59-17.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/01/Screenshot-from-2022-01-15-14-59-17.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Portainer interface </figcaption></figure> 

You can continue experimenting with different properties Portainer has.

## Conclusion {.wp-block-heading}

Congratulations, you have successfully installed Portainer with docker. If you got any issue, always consult the [Portainer documentation][2].

 [1]: https://nextgentips.com/2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/ "how to install Docker on Ubuntu 20.04"
 [2]: https://docs.portainer.io/v/ce-2.11/start/install/server/docker/linux "Portainer documentation"