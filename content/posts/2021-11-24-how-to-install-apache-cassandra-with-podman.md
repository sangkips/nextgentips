---
title: How to install Apache Cassandra with Podman
author: Kipkoech Sang
type: post
date: 2021-11-24T15:59:54+00:00
url: /2021/11/24/how-to-install-apache-cassandra-with-podman/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 86
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
Apache Cassandra is a free and open-source, distributed, wide-column store, NoSQL database management system designed to handle large amounts of data across many commodity servers, providing high availability with no single point of failure.

Linear scalability and proven fault-tolerance on cloud infrastructure make it the perfect platform for mission-critical data.

NoSQL databases are lightweight, open-source, non-relational, and largely distributed. It has the following strengths:

  * It can scale horizontally. Cassandra allows for seamless addition of nodes. A node represent an instance of Cassandra. This nodes do communicate with each other through a protocol called&nbsp;[gossip][1]. Gossip is a peer-to-peer communication protocol in which nodes periodically exchanges communication about themselves and other nodes they know about.
  * It contain distributed architecture. Distributed means that Cassandra can run on multiple machines while appearing to users as a unified whole. Cassandra databases easily scale when an application is under stress, this prevents data loss from any given datacenter’s hardware failure.
  * It has flexible approach to schema definition. A schema once defined its columns for a table while inserting data in every row, all columns must at least filled with null value but for Cassandra column families are defined the columns are not. You can add any column to any column family at any time.

Apache Cassandra is suitable for high-volume data types. With the advent of Big data, Cassandra has become the solution to issues that SQL databases were not able to solve.

## Related Content  {.wp-block-heading}

  * [How to Install Apache Cassandra 4 on Ubuntu 21.10][2]
  * [How to install Podman 3 on Debian 11][3]
  * [How to Install MongoDB with Podman on Rocky Linux 8][4]

## Prerequisites  {.wp-block-heading}

  * Have user with sudo priviliges 
  * Have basic terminal knowlege.

## Table of Content {.wp-block-heading}

  1. Run system updates 
  2. Install podman
  3. Pull Apache Cassandra image
  4. Start Apache Cassandra
  5. Container shell access.
  6. Conclusion

## 1. Run system updates  {.wp-block-heading}

To begin our installation we need to update our repositories accordingly. Use the following command to run the updates.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

After the update is complete we can now install Podman into our system.

## 2. Install Podman  {.wp-block-heading}

Podman is a daemon-less, open-source, Linux native tool designed to make it easy to find, run, build, share and deploy applications using Open Container Initiative (OCI) containers and container images. Containers can be run as root or as a regular users.

Podman manages the entire container ecosystem including pods, container images, container volumes using the&nbsp;[libpod][5]&nbsp;library.

To install Podman, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf install podman </code></pre>

Press Y to continue the installation

<pre class="wp-block-code"><code>Sample output
Dependencies resolved.
=================================================================================================================
 Package                          Arch        Version                                       Repository      Size
=================================================================================================================
Installing:
 &lt;strong>podman                           x86_64      3.3.1-9.module+el8.5.0+710+4c471e88           appstream       12 M&lt;/strong>
Installing dependencies:
 conmon                           x86_64      2:2.0.29-1.module+el8.5.0+710+4c471e88        appstream       51 k
 container-selinux                noarch      2:2.167.0-1.module+el8.5.0+710+4c471e88       appstream       53 k
 containernetworking-plugins      x86_64      1.0.0-1.module+el8.5.0+710+4c471e88           appstream       19 M
 containers-common                noarch      2:1-2.module+el8.5.0+710+4c471e88             appstream       78 k
 criu                             x86_64      3.15-3.module+el8.5.0+710+4c471e88            appstream      517 k
 fuse-common                      x86_64      3.2.1-12.el8                                  baseos          20 k
 fuse-overlayfs                   x86_64      1.7.1-1.module+el8.5.0+710+4c471e88           appstream       71 k
 fuse3                            x86_64      3.2.1-12.el8                                  baseos          49 k
 fuse3-libs                       x86_64      3.2.1-12.el8                                  baseos          93 k
 iptables                         x86_64      1.8.4-20.el8                                  baseos         584 k
 libnet                           x86_64      1.1.6-15.el8                                  appstream       66 k
 libnetfilter_conntrack           x86_64      1.0.6-5.el8                                   baseos          63 k
 libnfnetlink                     x86_64      1.0.1-13.el8                                  baseos          32 k
 libnftnl                         x86_64      1.1.5-4.el8                                   baseos          82 k
 libslirp                         x86_64      4.4.0-1.module+el8.5.0+710+4c471e88           appstream       69 k
 nftables                         x86_64      1:0.9.3-21.el8                                baseos         320 k
 podman-catatonit                 x86_64      3.3.1-9.module+el8.5.0+710+4c471e88           appstream      339 k
 protobuf-c                       x86_64      1.3.0-6.el8                                   appstream       36 k
 runc                             x86_64      1.0.2-1.module+el8.5.0+710+4c471e88           appstream      3.1 M
 slirp4netns                      x86_64      1.1.8-1.module+el8.5.0+710+4c471e88           appstream       50 k
Enabling module streams:
 container-tools                              rhel8                                                             

Transaction Summary
=================================================================================================================
Install  21 Packages

Total download size: 37 M
Installed size: 127 M
Is this ok &#91;y/N]: Y</code></pre>

It will install Podman with its dependencies.

Check the version with the following command;

<pre class="wp-block-code"><code>podman -v
podman version 3.3.1</code></pre>

## 3. Pull Apache Cassandra image {.wp-block-heading}

Now we can pull the Apache Cassandra image. We can use Podman pull Cassandra

<pre class="wp-block-code"><code>$ sudo podman pull cassandra </code></pre>

Choose an image to download then press enter to continue

<pre class="wp-block-code"><code>Sample output
? Please select an image: 
  ▸ registry.fedoraproject.org/cassandra:latest
    registry.access.redhat.com/cassandra:latest
    registry.centos.org/cassandra:latest
    docker.io/library/cassandra:latest
✔ docker.io/library/cassandra:latest
Trying to pull docker.io/library/cassandra:latest...
Getting image source signatures
Copying blob 237daeb1ae28 done  
Copying blob 8186acb88df2 done  
Copying blob f18b0b4b5df2 done  
Copying blob 7b1a6ab2e44d done  
Copying blob 3df82a898b2e done  
Copying blob e4604cccdbce done  
Copying blob cbc774b0759a done  
Copying blob 53f10cf9421a done  
Copying blob e0209de75848 done  
Copying config b082d7bbd1 done  
Writing manifest to image destination
Storing signatures
&lt;strong>b082d7bbd165&lt;/strong>6596e4d84fca9cdf3183f206683688c2099c7478b090e602e279</code></pre>

We can check the image with **Podman images** command 

<pre class="wp-block-code"><code>$ podman images
REPOSITORY                   TAG         IMAGE ID      CREATED      SIZE
docker.io/library/cassandra  latest      b082d7bbd165  2 weeks ago  347 MB</code></pre>

The image hash is the same, so we are certain we are having the correct image.

## 4. Start Cassandra {.wp-block-heading}

Let&#8217;s first create a directory where we will store our data.

<pre class="wp-block-code"><code>$ mkdir data
$ cd data </code></pre>

When you are inside the data directory, we can now run our Cassandra container with the following command;

<pre class="wp-block-code"><code>$ podman run --name mycassandra -d -p 7000:7000 -v $(pwd):/data/db:Z cassandra </code></pre>

Let&#8217;s check if the container is running with the **podman ps** command

<pre class="wp-block-code"><code>$ podman ps 
CONTAINER ID  IMAGE                               COMMAND       CREATED        STATUS            PORTS                   NAMES
bc86028310f3  docker.io/library/cassandra:latest  cassandra -f  2 minutes ago  Up 2 minutes ago  0.0.0.0:7000-&gt;7000/tcp  mycassandra</code></pre>

## 5. Container shell access and viewing cassandra logs {.wp-block-heading}

You can access the Cassandra shell with the following command;

<pre class="wp-block-code"><code>$ podman exec -it mycassandra bash
root@bc86028310f3:/# </code></pre>

You can access the logs with the following:

<pre class="wp-block-code"><code>$ podman logs mycassandra</code></pre>

## 6. Conclusion {.wp-block-heading}

We have successfully created a Cassandra container using Podman. Continue exploring more. Podman has so much to offer.

 [1]: https://docs.datastax.com/en/cassandra-oss/3.x/cassandra/architecture/archGossipAbout.html
 [2]: https://nextgentips.com/2021/10/28/how-to-install-apache-cassandra-4-on-ubuntu-21-10/
 [3]: https://nextgentips.com/2021/10/22/how-to-install-podman-3-on-debian-11/
 [4]: https://nextgentips.com/2021/11/17/how-to-install-mongodb-with-podman-on-rocky-linux-8/
 [5]: https://github.com/dankohn/libpod