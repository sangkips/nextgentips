---
title: How to install Flask on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-12-13T18:13:55+00:00
url: /2021/12/13/how-to-install-flask-on-ubuntu-21-10/
ss_ss_click_share_count_linkedin:
  - 1
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"114.119.132.171";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 65
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this tutorial guide, we are going to explore how to install the Flask framework on Ubuntu 21.10.

Flask is a microweb framework written in Python. It is classified as a microframework because it doesn&#8217;t require particular libraries or tools. It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions.

Flask depends on [jinja][1] template engine and the Werkzeug [WSGI][2] toolkit. 

## Prerequisites  {.wp-block-heading}

  * Flask need Python 3.6 and newer in order to run
  * You need to have virtual environment up an running 
  * Ubuntu 21.10 server up and running.
  * Have a user with sudo privileges.

## Dependencies  {.wp-block-heading}

Flask requires the following dependencies.

  * Werkzeug. This implements WSGI which is the standard Python interface between applications and servers.
  * Jinja. It is a template language that renders the pages to your application data.
  * MarkupSafe. It escapes untrusted input when rendering templates to avoid injection attacks. 
  * ItsDangerous. It securely signs data to ensure data integrity
  * Click. This is a framework for writing command line applications 

## Install Flask on Ubuntu 21.10 {.wp-block-heading}

## 1. Run system updates  {.wp-block-heading}

To install Flask we need to have a virtual environment. To create a virtual environment, let&#8217;s first run system updates in order to make all the repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When both updates and upgrades are complete, we can now proceed to create a virtual environment for our project. Check this article on how to create a virtual environment. 

[How to install Python pip 21 on Ubuntu 21.10][3]

To learn how to install the latest Python, follow this link

[How to install Python 3.11 on Ubuntu 20.04][4]

## 2. Create a virtual environment  {.wp-block-heading}

To start with let&#8217;s first create a project directory.

<pre class="wp-block-code"><code>$ sudo mkdir nextgen
$ cd nextgen 
$ python3 -m venv nextvenv</code></pre>

I know many of you will encounter this error message &#8220;The virtual environment was not created successfully because ensurepip is not available. On Debian/Ubuntu systems, you need to install the python3-venv&#8221;

What you need to do is to install a virtual environment first. Use the following command to create venv 

<pre class="wp-block-code"><code>$ sudo apt install python3.9-venv</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  python-pip-whl
The following NEW packages will be installed:
  python-pip-whl python3.9-venv
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 1902 kB of archives.
After this operation, 2320 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

Press Y and enter to allow installation to continue. When this installation is complete create your virtual environment 

## 3. Activate virtual environment  {.wp-block-heading}

Before you can use the virtual environment, make sure you activate the virtual environment. Activate with the following command.

<pre class="wp-block-code"><code>$ source nextvenv/bin/activate</code></pre>

Output must look like this 

<pre class="wp-block-code"><code>output
(nextvenv) root@ubuntu:~/nextgen# </code></pre>

## 4. Install Flask {.wp-block-heading}

Inside the activated virtual environment, you can now install the Flask framework. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ pip install flask</code></pre>

The output will look like this

<pre class="wp-block-code"><code>Collecting flask
  Downloading Flask-2.0.2-py3-none-any.whl (95 kB)
     |████████████████████████████████| 95 kB 7.3 MB/s 
Collecting click>=7.1.2
  Downloading click-8.0.3-py3-none-any.whl (97 kB)
     |████████████████████████████████| 97 kB 15.0 MB/s 
Collecting itsdangerous>=2.0
  Downloading itsdangerous-2.0.1-py3-none-any.whl (18 kB)
Collecting Jinja2>=3.0
  Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
     |████████████████████████████████| 133 kB 40.0 MB/s 
Collecting Werkzeug>=2.0
  Downloading Werkzeug-2.0.2-py3-none-any.whl (288 kB)
     |████████████████████████████████| 288 kB 56.1 MB/s 
Collecting MarkupSafe>=2.0
  Downloading MarkupSafe-2.0.1-cp39-cp39-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_12_x86_64.manylinux2010_x86_64.whl (30 kB)
Installing collected packages: MarkupSafe, Werkzeug, Jinja2, itsdangerous, click, flask
Successfully installed Jinja2-3.0.3 MarkupSafe-2.0.1 Werkzeug-2.0.2 click-8.0.3 flask-2.0.2 itsdangerous-2.0.1</code></pre>

You can check the version of Flask you are running with the following command.

<pre class="wp-block-code"><code>$ flask --version
Python 3.9.7
Flask 2.0.2
Werkzeug 2.0.2</code></pre>

I am running Python 3.9.7 which comes preinstalled with Ubuntu 21.10, you don&#8217;t need to install it.

## 5. Conclusion {.wp-block-heading}

Congratulations! You have installed Flask and now you are ready to jump into creating awsome projects with Flask. To get started go to [Flask getting started page][5] to start writing your Flask project. Happy that you have learned something today. Follow along for more tutorials.

 [1]: https://jinja.palletsprojects.com/en/3.0.x/
 [2]: https://werkzeug.palletsprojects.com/en/2.0.x/
 [3]: https://nextgentips.com/2021/12/07/how-to-install-python-pip-21-on-ubuntu-21-10/
 [4]: https://nextgentips.com/2021/11/23/how-to-install-python-3-11-on-ubuntu-20-04/
 [5]: https://flask.palletsprojects.com/en/2.0.x/quickstart/