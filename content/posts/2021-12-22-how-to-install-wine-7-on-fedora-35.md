---
title: How to install Wine 7 on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-12-22T18:40:34+00:00
url: /2021/12/22/how-to-install-wine-7-on-fedora-35/
entry_views:
  - 2
view_ip:
  - 'a:2:{i:0;s:32:"2804:388:e061:7979:0:4c:e630:401";i:1;s:13:"54.36.148.141";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 57
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to install Wine 7 on Fedora 35.

Wine is a compatibility layer capable of running Windows applications on several [POSIX][1]-compliant operating systems i.e Linux, macOS, and BSD. Instead of simulating internal Windows logic like a virtual machine or emulator, Wine translates Windows API calls into POSIX calls instantly eliminating the performance and memory penalties of other methods and allowing you to integrate Windows applications in your desktop.

## What&#8217;s new in Wine 7 release {.wp-block-heading}

  * Reimplimentation of winMM joystick driver
  * All unix libraries converted to the syscall-based libraries

## Benefits of Using Wine {.wp-block-heading}

  * Unix has made it possible to write powerful scripts, Wine makes it possible to call windows applications from scripts that can also laverage Unix environment.
  * Wine makes it possible to access Windows applications remotely.
  * Wine makes it possible to use thin clients i.e install Wine and you will easily connect to windows applications using x-terminal.
  * Wine is open-source so you can easily extend to suite your needs.
  * Wine can be used to make existing windows applications available on the web by using VNC and its Java/HTML5 client.

## Related Articles  {.wp-block-heading}

  * [How to install and configure Wine on Ubuntu 21.10/21.04][2]

## Installing Wine on Fedora 35 {.wp-block-heading}

## 1. Run System Updates  {.wp-block-heading}

To begin with we need to update our repositories to make them up to date. Open your terminal and type the following command;

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When updates are complete, move ahead to install WineHQ packages.

## 2. Install WineHQ repository on fedora 35 {.wp-block-heading}

Add the following WineHQ repository to Fedora 35. This will enable us to run wine with ease on our Fedora 35 workstation/server.

<pre class="wp-block-code"><code>$ sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/35/winehq.repo</code></pre>

Sample output will look like this;

<pre class="wp-block-code"><code>Output
Adding repo from: https://dl.winehq.org/wine-builds/fedora/35/winehq.repo</code></pre>

## 3. Install Wine 7 on Fedora 35 {.wp-block-heading}

We can now begin installing Wine 7 after completing the addition of the WineHQ repository to our system. We are going to install the Wine development branch. Run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf install winehq-devel</code></pre>

You will see the following output.

<pre class="wp-block-code"><code>Output
Installing:
&lt;strong> winehq-devel                         x86_64       1:7.0.rc2-1.1                             WineHQ         76 k&lt;/strong>
Installing dependencies:
 SDL2                                 i686         2.0.18-2.fc35                             updates       632 k
 SDL2                                 x86_64       2.0.18-2.fc35                             updates       602 k
 alsa-lib                             i686         1.2.6.1-3.fc35                            updates       529 k
 alsa-lib                             x86_64       1.2.6.1-3.fc35                            updates       497 k
 avahi-libs                           i686         0.8-14.fc35                               fedora         72 k
 avahi-libs                           x86_64       0.8-14.fc35                               fedora         68 k
 bzip2-libs                           i686         1.0.8-9.fc35                              fedora         40 k
 cairo                                i686         1.17.4-4.fc35                             fedora        716 k
 cairo                                x86_64       1.17.4-4.fc35                             fedora        664 k</code></pre>

To allow installation to continue press Y and enter command.

This command will install WineHQ development version and its many dependent packages. 

Press Y again to allow WineHQ key to be added also. It will look like this;

<pre class="wp-block-code"><code>Output
$ WineHQ packages                                                                  114 kB/s | 3.1 kB     00:00    
Importing GPG key 0xF987672F:
 Userid     : "WineHQ packages &lt;wine-devel@winehq.org>"
 Fingerprint: D43F 6401 4536 9C51 D786 DDEA 76F1 A20F F987 672F
 From       : https://dl.winehq.org/wine-builds/winehq.key
Is this ok &#91;y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.</code></pre>

Lastly, we can check the version of installed Wine on Fedora 35 with the following command;

<pre class="wp-block-code"><code>$ wine --version
wine-7.0-rc2</code></pre>

I also want to show you how to install both **stable** and **staging branches** 

### Install Wine Stable branch on Fedora 35. {.wp-block-heading}

This is the recommended install because it has passed all tests. To install this branch run the following command on your command line;

<pre class="wp-block-code"><code>$ sudo dnf install winehq-stable</code></pre>

### Install wine Staging branch {.wp-block-heading}

To install this staging branch, use the following command.

<pre class="wp-block-code"><code>$ sudo dnf install winehq-staging </code></pre>

Now that we have completed the installation of Wine, we need to run the Wine program in order to start using it. To run the program on our terminal type the following;

<pre class="wp-block-code"><code>$ winecfg </code></pre>

## 4. Conclusion {.wp-block-heading}

Congratulations we have successfully installed Wine on our Fedora 35 Linux. If you faced any issues please feel free to contact us for assistance. Or you can consult [WineHQ documentation][3].

 [1]: https://www.gnu.org/software/libc/manual/html_node/POSIX.html
 [2]: https://nextgentips.com/2021/12/12/how-to-install-and-configure-wine-on-ubuntu-21-10-21-04/
 [3]: https://www.winehq.org/