---
title: How to install Ansible in Ubuntu 20.04.
author: Kipkoech Sang
type: post
date: 2021-12-23T10:01:17+00:00
url: /2021/12/23/how-to-install-ansible-in-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"66.249.64.140";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 56
rank_math_internal_links_processed:
  - 1
categories:
  - Automation

---
Welcome to today&#8217;s tutorial, we are going to talk about the easier way to install Ansible inside Ubuntu 20.04 distro. 

[Ansible][1] is an open-source software provisioning, configuration management, and application deployment tool enabling infrastructure as code. It runs both on Unix-like systems and on Windows platforms. 

Ansible is a radically simple IT automation engine that automates cloud provisioning, configuration management, application deployment, intra-service orchestration, etc.

Ansible uses no agents and no additional custom security infrastructure. It is easy to deploy because it uses YAML file systems that allow you to describe your deployment environment in plain English. 

To install the released version of Ansible, pip or packet manager is used. 

## Prerequisites  {.wp-block-heading}

  * Have Python 3.8 and above
  * Have pip installed on your system
  * Have Ubuntu 20.04 server up and running 

In order to install the latest pip and Python3, follow this tutorial. 

  * [How to install Python pip 21 on Ubuntu 21.10][2]
  * [How to install Python 3.9 interpreter on Ubuntu 20.04][3]
  * [How to install Python 3.11 on Ubuntu 20.04][4]

## Installing Ansible on Ubuntu 20.04 {.wp-block-heading}

## Run system updates  {.wp-block-heading}

To begin with we need to run system updates in order to make our repositories up to date. Open your terminal and type in the following code.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates are complete, move forward to install pip.

## Install Pip {.wp-block-heading}

If pip is not already installed, we can do install with the following command;

<pre class="wp-block-code"><code>$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py</code></pre>

After you have finished bootstrapping pip then run the following;

<pre class="wp-block-code"><code>$ python3 get-pip.py --user</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Output
Collecting pip
  Downloading pip-21.3.1-py3-none-any.whl (1.7 MB)
     |████████████████████████████████| 1.7 MB 356 kB/s            
Installing collected packages: pip
Successfully installed pip-21.3.1
</code></pre>

Once pip is installed then it&#8217;s time for us to install Ansible.

## 1. Installing Ansible with pip {.wp-block-heading}

Install Ansible with the following command;

<pre class="wp-block-code"><code>$ sudo python3 -m pip install --user ansible</code></pre>

Sample output you will get.

<pre class="wp-block-code"><code>Output
Collecting ansible
  Downloading ansible-5.1.0.tar.gz (37.7 MB)
     |████████████████████████████████| 37.7 MB 359 kB/s 
Collecting ansible-core~=2.12.1
  Downloading ansible-core-2.12.1.tar.gz (7.4 MB)
     |████████████████████████████████| 7.4 MB 350 kB/s 
Requirement already satisfied: PyYAML in /usr/lib/python3/dist-packages (from ansible-core~=2.12.1-&gt;ansible) (5.3.1)
Requirement already satisfied: cryptography in /usr/lib/python3/dist-packages (from ansible-core~=2.12.1-&gt;ansible) (2.8)
Collecting jinja2
  Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
     |████████████████████████████████| 133 kB 304 kB/s 
Collecting packaging
  Downloading packaging-21.3-py3-none-any.whl (40 kB)
     |████████████████████████████████| 40 kB 406 kB/s 
Collecting resolvelib&lt;0.6.0,&gt;=0.5.3
  Downloading resolvelib-0.5.4-py2.py3-none-any.whl (12 kB)
Collecting MarkupSafe&gt;=2.0
  Downloading MarkupSafe-2.0.1-cp38-cp38-manylinux2010_x86_64.whl (30 kB)
Collecting pyparsing!=3.0.5,&gt;=2.0.2
  Downloading pyparsing-3.0.6-py3-none-any.whl (97 kB)
     |████████████████████████████████| 97 kB 160 kB/s 
Building wheels for collected packages: ansible, ansible-core
  Building wheel for ansible (setup.py) ... done
  Created wheel for ansible: filename=ansible-5.1.0-py3-none-any.whl size=62784809 sha256=dd95119b08282936f2870c50c4f8d00313c330b6084d85be1a663841d9f64323
  Stored in directory: /root/.cache/pip/wheels/4b/1c/fe/a0053a3e7165b4ae498cc938caea0f848bb1f3e5ae532a8257
  Building wheel for ansible-core (setup.py) ... done
  Created wheel for ansible-core: filename=ansible_core-2.12.1-py3-none-any.whl size=2073412 sha256=257cd9c13ddf5797f68b3d45acbfca0c18cc5a8d7cea5059229c170e5ba9f5bc
  Stored in directory: /root/.cache/pip/wheels/34/bd/63/4f3348987a1079c559b4f10f5a8460784d8ac803d46e762d87
Successfully built ansible ansible-core
Installing collected packages: MarkupSafe, jinja2, pyparsing, packaging, resolvelib, ansible-core, ansible
Successfully installed MarkupSafe-2.0.1 ansible-5.1.0 ansible-core-2.12.1 jinja2-3.0.3 packaging-21.3 pyparsing-3.0.6 resolvelib-0.5.4</code></pre>

## Installing Ansible via PPA packages  {.wp-block-heading}

Ubuntu builds are available in PPA repositories.

First Run system updates on your system.

<pre class="wp-block-code"><code>$ sudo apt update </code></pre>

## Install prerequisites  {.wp-block-heading}

We need to install Ansible prerequisites to aid in the installation of Ansible. Run the following command;

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common</code></pre>

Sample output

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
software-properties-common is already the newest version (0.99.9.8).
software-properties-common set to manually installed.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

## 2. Add PPA Repository to Ubuntu 20.04 {.wp-block-heading}

To add the Ubuntu PPA repository to our system, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo add-apt-repository --yes --update ppa:ansible/ansible</code></pre>

Then run updates again 

## Install Ansible on Ubuntu 20.04 {.wp-block-heading}

Run the following command on your terminal to install Ansible.

<pre class="wp-block-code"><code>$ sudo apt install ansible</code></pre>

Sample output

<pre class="wp-block-code"><code>Output
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  ansible-core python3-bcrypt python3-packaging python3-paramiko python3-pyparsing python3-resolvelib sshpass
Suggested packages:
  python3-gssapi python-pyparsing-doc
The following NEW packages will be installed:
  ansible ansible-core python3-bcrypt python3-packaging python3-paramiko python3-pyparsing python3-resolvelib
  sshpass
0 upgraded, 8 newly installed, 0 to remove and 0 not upgraded.
Need to get 21.4 MB of archives.
After this operation, 286 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue, dependencies will be installed in the process.

We can check the version with the following command;

<pre class="wp-block-code"><code>output
ansible &#91;core 2.11.7] 
  config file = /etc/ansible/ansible.cfg
  configured module search path = &#91;'/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible
  python version = 3.8.10 (default, Nov 26 2021, 20:14:08) &#91;GCC 9.3.0]
  jinja version = 2.10.1
  libyaml = True</code></pre>

## Uninstall Ansible {.wp-block-heading}

If you will like to remove ansible use the following command. 

<pre class="wp-block-code"><code>$ sudo apt remove ansible</code></pre>

It will remove Ansible with its all dependencies.

## Conclusion  {.wp-block-heading}

We have successfully installed Ansible in our Ubuntu 20.04. Continue learning more with [Ansible documentation.][5]

 [1]: https://www.redhat.com/en/technologies/management/ansible
 [2]: https://nextgentips.com/2021/12/07/how-to-install-python-pip-21-on-ubuntu-21-10/
 [3]: https://nextgentips.com/2021/10/04/install-python-3-on-ubuntu-20-04/
 [4]: https://nextgentips.com/2021/11/23/how-to-install-python-3-11-on-ubuntu-20-04/
 [5]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#prerequisites