---
title: How to perform CRUD functionalities on FastAPI App
author: Kipkoech Sang
type: post
date: 2022-11-30T13:58:51+00:00
url: /2022/11/30/how-to-perform-crud-functionalities-in-fastapi/
categories:
  - Programming

---
In this tutorial we are going to learn how to perform Create, Read, Update and Delete operations in a FastAPI application. In our <a href="https://nextgentips.com/2022/11/29/how-to-install-and-use-fastapi/" target="_blank" rel="noopener" title="">previous post</a>, we saw how to install and create the first application using RESTful FastAPI.

In this tutorial we will create an API to fetch your favorite songs from the database. We will be implementing the database using the MYSQL. 

### Run CRUD operations {.wp-block-heading}

To begin with, lets first create a project folder. Inside this project lets install fastapi and uvicorn server.

<pre class="wp-block-code"><code>mkdir songs
cd songs
pip install fastapi
pip install "uvicorn&#91;standard]"</code></pre>

Open the songs folder with your favorite code editor and create `<mark style="background-color:#abb8c3" class="has-inline-color">main.py</mark>` file. From here lets roll.

<pre class="wp-block-code"><code>from fastapi import FastAPI

app = FastAPI()

# Project root endpoint
@app.get('/')
async def root():
    return {'Message': 'Welcome to my Songs Library'}</code></pre>

### Fetch all songs(GET) {.wp-block-heading}

I want to get all the movies from the database, therefore before we can implement fully fletched database, we will use a dummy database.

<pre class="wp-block-code"><code>from fastapi import FastAPI

app = FastAPI()
#Dummy database
songs = &#91; {"title":"", "Artist":"", "year":0},#empty dictionary for easy logic implemetation in retrieving songs from db
    {"title": "Good bye", "Artist": "Cellin dion", "year": 2000},
    {"title": "Ali maccaine", "Artist": "mike robson", "year": 2008},
    {"title": "God yo are good", "Artist": "Jesus alive ministries", "year": 2020},
    {"title": "Chop that shit", "Artist": "Black eyepees", "year": 2022},
    {"title": "come home", "Artist": "Ricky", "year": 2023},
]


# Project root endpoint
@app.get('/')
async def root():
    return {'Message': 'Welcome to my Songs Library'}

@app.get('/songs')
def get_ll_Songs():
    return songs</code></pre>

In the above code, we have populated a database with fake figures to help us work on the project before we can implement the MYSQL database. Now go to the swagger docs to test your first endpoint.

<pre class="wp-block-code"><code>http:&#47;&#47;127.0.0.1:8000/docs</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="225" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-03-03.png?resize=810%2C225&#038;ssl=1" alt="Nextgentips: List all songs" class="wp-image-1684" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-03-03.png?resize=1024%2C285&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-03-03.png?resize=300%2C83&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-03-03.png?resize=768%2C214&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-03-03.png?w=1320&ssl=1 1320w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: List all songs</figcaption></figure> 

If you click on the drop down menu and click on execute you shall be in a position to see our dummy data.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="371" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-05-46.png?resize=810%2C371&#038;ssl=1" alt="Nextgentips: All db data" class="wp-image-1685" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-05-46.png?resize=1024%2C469&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-05-46.png?resize=300%2C137&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-05-46.png?resize=768%2C352&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-13-05-46.png?w=1254&ssl=1 1254w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: All db data</figcaption></figure> 

### Retrieve one song(GET) {.wp-block-heading}

Whenever you want to retrieve a single song from the database, you need to implement code like below example.

<pre class="wp-block-code"><code># Retrieve a single song
@app.get('/song/{song_id}')
def get_one_song(song_id:int):
    return songs&#91;song_id]
    </code></pre>

### Create a song(POST) {.wp-block-heading}

The next thing is to create a song into the database. Create is the first letter from CRUD operations. Use the following code to create a record into the database.

<pre class="wp-block-code"><code># Create a song
@app.post('/song')
def create_song(song:dict): # pass the dictionary from the dummy data
    songs.append(song) # append the created song to the end of the array
    return {'message': 'Song successfully created'}</code></pre>

You will be in a position to see your return message from the swagger UI or go to list all songs to confirm if the data was created.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="546" height="246" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-15-31-54.png?resize=546%2C246&#038;ssl=1" alt="Nextgentips: Create success message" class="wp-image-1689" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-15-31-54.png?w=546&ssl=1 546w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-15-31-54.png?resize=300%2C135&ssl=1 300w" sizes="(max-width: 546px) 100vw, 546px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: Create success message</figcaption></figure> 

### Updating a song(PUT) {.wp-block-heading}

Whenever we want to update a song, we can use PUT to accomplish that, sometimes we want to update a bit of the data, then we can use PATCH. For this example I will still use POST request then when I have implemented the full database then I will choose to use PUT.

<pre class="wp-block-code"><code># Update a song
@app.post('/update_song')
def update_song(song_id:int, song:dict):
    updated_song = songs&#91;song_id]# get a song from array of songs
    updated_song&#91;'title'] = song&#91;'title']
    updated_song&#91;'Artist'] = song&#91;'Artist']
    updated_song&#91;'year'] = song&#91;'year']
    songs&#91;song_id] = updated_song
    return  updated_song, {'message': "The song has updated successfully"}</code></pre>

Success message after doing the update.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="551" height="211" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-16-32-35.png?resize=551%2C211&#038;ssl=1" alt="Nextgentips: Update success message" class="wp-image-1690" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-16-32-35.png?w=551&ssl=1 551w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-16-32-35.png?resize=300%2C115&ssl=1 300w" sizes="(max-width: 551px) 100vw, 551px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: Update success message</figcaption></figure> 

### Delete a song(DELETE) {.wp-block-heading}

Deleting an item from the database only requires a id of what you want to delete. To delete the song from our array, use the following code:

<pre class="wp-block-code"><code>@app.delete('/song/{song_id}')
def delete_song(song_id:int):
    songs.pop(song_id)# python pop method to remove an item
    return {'message': "The song has been deleted successfully"}</code></pre>

The following is the success message <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="621" height="253" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-16-52-14.png?resize=621%2C253&#038;ssl=1" alt="Nextgentips: Delete success message" class="wp-image-1691" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-16-52-14.png?w=621&ssl=1 621w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/11/Screenshot-from-2022-11-30-16-52-14.png?resize=300%2C122&ssl=1 300w" sizes="(max-width: 621px) 100vw, 621px" data-recalc-dims="1" /> <figcaption class="wp-element-caption">Nextgentips: Delete success message</figcaption></figure> 

## Conclusion {.wp-block-heading}

We have successfully implemented the CRUD functionalities using the dummy array data. next we will see how to implement this using data from the real database with the use of <a href="https://www.sqlalchemy.org/" target="_blank" rel="noopener" title="">SQLAlchemy</a>.