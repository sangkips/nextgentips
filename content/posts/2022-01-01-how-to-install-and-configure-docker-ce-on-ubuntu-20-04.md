---
title: How to install and configure Docker-CE on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-01T08:38:02+00:00
url: /2022/01/01/how-to-install-and-configure-docker-ce-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 44
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial am going to show you how you can install Docker-CE on Ubuntu 20.04.

Docker is a set of platform as a service product that uses OS-level virtualization to deliver software in packages called containers. Containers are usually isolated from one another and bundled their own software libraries and configuration files, they can communicate with each other through well-defined channels.

Docker makes it possible to get more apps running on the same old servers and also makes it easy to package and ship programs.

## Related Articles {.wp-block-heading}

  * [How to install and use Docker-ce on Fedora 35][1]
  * [How to install Docker on Arch Linux][2]
  * [How to install Docker-ce on Ubuntu 21.10][3]

## Prerequisites {.wp-block-heading}

  * Make sure you have user with sudo privileges
  * Have Ubuntu 20.04 server up and running
  * Have basic kwoledge about running commands on a terminal

## Table of Contents {.wp-block-heading}

  1. Run system updates
  2. Uninstall old docker version
  3. Install Docker Engine
  4. Enable Docker Engine
  5. Start Docker
  6. Check Docker status
  7. Test Docker
  8. Conclusion

## 1. Run system updates {.wp-block-heading}

We need to install recent updates into our system repositories. To do that you need to use the following command in our terminal.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

## 2. Uninstall old Docker versions {.wp-block-heading}

If you have ever installed Docker before you need to uninstall them to allow the new Docker-CE to be installed. To do uninstallation, run the following command in your terminal.

<pre class="wp-block-code"><code>$ sudo apt remove docker</code></pre>

## 3. Install Docker-ce on Ubuntu 20.04 {.wp-block-heading}

To begin installing docker-ce, run the following command on your terminal. It will install docker with its dependencies.

<pre class="wp-block-code"><code>$ sudo apt install docker.io</code></pre>

You will get the following sample output on your terminal.

<pre class="wp-block-code"><code>Output
The following additional packages will be installed:
  bridge-utils containerd dns-root-data dnsmasq-base libidn11 pigz runc ubuntu-fan
Suggested packages:
  ifupdown aufs-tools cgroupfs-mount | cgroup-lite debootstrap docker-doc rinse zfs-fuse | zfsutils
The following NEW packages will be installed:
  bridge-utils containerd dns-root-data dnsmasq-base docker.io libidn11 pigz runc ubuntu-fan
0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.
Need to get 74.5 MB of archives.
After this operation, 361 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

Press Y to allow installation to continue.

Check the version of docker installed with the following command.

<pre class="wp-block-code"><code>$ docker version
Client:
 Version:           20.10.7
 API version:       1.41
 Go version:        go1.13.8
 Git commit:        20.10.7-0ubuntu5~20.04.2
 Built:             Mon Nov  1 00:34:17 2021
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      true

Server:
 Engine:
  Version:          20.10.7
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.13.8
  Git commit:       20.10.7-0ubuntu5~20.04.2
  Built:            Fri Oct 22 00:45:53 2021
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.5.5-0ubuntu3~20.04.1
  GitCommit:        
 runc:
  Version:          1.0.1-0ubuntu2~20.04.1
  GitCommit:        
 docker-init:
  Version:          0.19.0
  GitCommit:        </code></pre>

## 4. Enable Docker-ce {.wp-block-heading}

We need to enable docker-ce to start on reboot, in order to avoid starting every time you boot your machine.

<pre class="wp-block-code"><code>$ sudo systemctl enable docker</code></pre>

## 5. Start Docker-ce service {.wp-block-heading}

You are supposed to start the docker service to run containerd environment. To start, use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl start docker</code></pre>

Lastly, we can check the status of docker to see if it is running.

## 6. Check status of Docker-ce {.wp-block-heading}

Check if Docker is running with the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo systemctl status docker</code></pre>

If you got status active, then your docker service is running as expected, if not probably you haven’t started the service.

<pre class="wp-block-code"><code>Output
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2022-01-01 08:25:22 UTC; 2min 0s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 15639 (dockerd)
      Tasks: 8
     Memory: 50.2M
     CGroup: /system.slice/docker.service
             └─15639 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Jan 01 08:25:21 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:21.900771603Z" level=warning msg="Your kernel does>
Jan 01 08:25:21 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:21.900788697Z" level=warning msg="Your kernel does>
Jan 01 08:25:21 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:21.900798067Z" level=warning msg="Your kernel does>
Jan 01 08:25:21 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:21.901620999Z" level=info msg="Loading containers:>
Jan 01 08:25:22 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:22.031254578Z" level=info msg="Default bridge (doc>
Jan 01 08:25:22 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:22.117928719Z" level=info msg="Loading containers:>
Jan 01 08:25:22 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:22.188902438Z" level=info msg="Docker daemon" comm>
Jan 01 08:25:22 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:22.189323611Z" level=info msg="Daemon has complete>
Jan 01 08:25:22 ubuntu systemd&#91;1]: Started Docker Application Container Engine.
Jan 01 08:25:22 ubuntu dockerd&#91;15639]: time="2022-01-01T08:25:22.235122738Z" level=info msg="API listen on /run/>
lines 1-21/21 (END)</code></pre>

## 7. Test Docker-ce {.wp-block-heading}

Let’s test our docker to see if it is running. Test using hello-world container.

<pre class="wp-block-code"><code>$ docker run hello-world</code></pre>

<pre class="wp-block-code"><code>Output
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:2498fce14358aa50ead0cc6c19990fc6ff866ce72aeb5546e1d59caac3d0d60f
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https:&#47;&#47;hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/</code></pre>

You can also search docker images available on the Docker hub. Let&#8217;s search for Ubuntu images available for example.

<pre class="wp-block-code"><code>$ docker search ubuntu</code></pre>

You will get the following output

<pre class="wp-block-code"><code>ubuntu                                                    Ubuntu is a Debian-based Linux operating sys…   13421     &#91;OK]       
dorowu/ubuntu-desktop-lxde-vnc                            Docker image to provide HTML5 VNC interface …   598                  &#91;OK]
websphere-liberty                                         WebSphere Liberty multi-architecture images …   282       &#91;OK]       
rastasheep/ubuntu-sshd                                    Dockerized SSH service, built on top of offi…   256                  &#91;OK]
consol/ubuntu-xfce-vnc                                    Ubuntu container with "headless" VNC session…   243                  &#91;OK]
ubuntu-upstart                                            DEPRECATED, as is Upstart (find other proces…   112       &#91;OK]       
ansible/ubuntu14.04-ansible                               Ubuntu 14.04 LTS with ansible                   98                   &#91;OK]
neurodebian                                               NeuroDebian provides neuroscience research s…   88        &#91;OK]       
1and1internet/ubuntu-16-nginx-php-phpmyadmin-mysql-5      ubuntu-16-nginx-php-phpmyadmin-mysql-5          50                   &#91;OK]
open-liberty                                              Open Liberty multi-architecture images based…   48        &#91;OK]       
ubuntu-debootstrap                                        DEPRECATED; use "ubuntu" instead                45        &#91;OK]       
i386/ubuntu                                               Ubuntu is a Debian-based Linux operating sys…   28                   
nuagebec/ubuntu                                           Simple always updated Ubuntu docker images w…   24                   &#91;OK]
1and1internet/ubuntu-16-apache-php-5.6                    ubuntu-16-apache-php-5.6                        14                   &#91;OK]
1and1internet/ubuntu-16-apache-php-7.0                    ubuntu-16-apache-php-7.0                        13                   &#91;OK]
1and1internet/ubuntu-16-nginx-php-phpmyadmin-mariadb-10   ubuntu-16-nginx-php-phpmyadmin-mariadb-10       11                   &#91;OK]
1and1internet/ubuntu-16-nginx-php-5.6-wordpress-4         ubuntu-16-nginx-php-5.6-wordpress-4             9                    &#91;OK]
darksheer/ubuntu                                          Base Ubuntu Image -- Updated hourly             5                    &#91;OK]
1and1internet/ubuntu-16-nginx-php-7.0                     ubuntu-16-nginx-php-7.0                         4                    &#91;OK]
owncloud/ubuntu                                           ownCloud Ubuntu base image                      3                    
1and1internet/ubuntu-16-nginx-php-7.1-wordpress-4         ubuntu-16-nginx-php-7.1-wordpress-4             3                    &#91;OK]
1and1internet/ubuntu-16-sshd                              ubuntu-16-sshd                                  1                    &#91;OK]
1and1internet/ubuntu-16-php-7.1                           ubuntu-16-php-7.1                               1                    &#91;OK]
smartentry/ubuntu                                         ubuntu with smartentry                          1                    &#91;OK]
1and1internet/ubuntu-16-rspec                             ubuntu-16-rspec                                 0                    &#91;OK]</code></pre>

## Running a Docker Container  {.wp-block-heading}

Lets ru a container with the following command;

<pre class="wp-block-code"><code>$ docker run -it ubuntu</code></pre>

**-it** gives interactive shell access to the container 

<pre class="wp-block-code"><code>$ docker run -it fedora </code></pre>

It will pull the fedora image from the Docker Hub.

<pre class="wp-block-code"><code>Output
Unable to find image 'fedora:latest' locally
latest: Pulling from library/fedora
edad61c68e67: Pull complete 
Digest: sha256:40ba585f0e25c096a08c30ab2f70ef3820b8ea5a4bdd16da0edbfc0a6952fa57
Status: Downloaded newer image for fedora:latest</code></pre>

From here you can do anything with Fedora distro. As an example lets do an update inside the container.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

## 8. Conclusion {.wp-block-heading}

From here you can see our Docker-CE is running as expected, Learn other docker commands to be well conversant with docker.&nbsp;[Docker Documentation&nbsp;][4]has a tone of information.

 [1]: https://nextgentips.com/2021/11/22/how-to-install-and-use-docker-ce-on-fedora-35/
 [2]: https://nextgentips.com/2021/09/30/how-to-install-docker-on-arch-linux/
 [3]: https://nextgentips.com/2021/11/27/how-to-install-docker-ce-on-ubuntu-21-10/
 [4]: https://docs.docker.com/engine/install/