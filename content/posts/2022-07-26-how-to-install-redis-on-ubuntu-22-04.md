---
title: How to install Redis on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-26T09:05:20+00:00
url: /2022/07/26/how-to-install-redis-on-ubuntu-22-04/
categories:
  - Database

---
Welcome to this tutorial where we will learn how to install the Redis server o Ubuntu 22.04.

Redis server is an open source in-memory data store used by many developers as a database, cache, streaming engine, and message broker.

**Redis as a real-time data store** means Redis&#8217; versatile in-memory data structures let you build data infrastructure for real-time applications requiring low latency and high throughput 

**Caching and session storage**. Redis speed is ideal for caching database queries, complex computations, API calls, and session states.

**Streaming and messaging**. Streamed data type enables high-speed data ingestion, messaging, event sourcing, and notifications. 

## Install Redis on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Before we begin the installation, we need to update our system repositories in order to make system repositories up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Add Redis to the apt repository {.wp-block-heading}

To install the stable version of Redis we need to install it from the official Redis packages and to do so we need to add an apt repository .

<pre class="wp-block-code"><code>$ curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg</code></pre>

Add the downloaded package to the repository by using echo.

<pre class="wp-block-code"><code>$ echo "deb &#91;signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list</code></pre>

What follows is to update the system repositories once more for the changes to take effect.

<pre class="wp-block-code"><code>sudo apt update</code></pre>

Lastly is to install Redis with the following command.

<pre class="wp-block-code"><code>sudo apt install redis </code></pre>

You should get the output something like this.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  redis-server redis-tools
Suggested packages:
  &lt;strong>ruby-redis&lt;/strong>
The following NEW packages will be installed:
  redis redis-server redis-tools
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 1642 kB of archives.
After this operation, 9437 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] 
</code></pre>

### 3. Testing Redis {.wp-block-heading}

If your installation was successful, you can test Redis with the following command.

<pre class="wp-block-code"><code>sudo systemctl status redis-server </code></pre>

You must see the following output.

<pre class="wp-block-code"><code>● redis-server.service - Advanced key-value store
     Loaded: loaded (/lib/systemd/system/redis-server.service; disabled; vendor&gt;
     Active: active (running) since Tue 2022-07-26 07:41:00 UTC; 7min ago
       Docs: http://redis.io/documentation,
             man:redis-server(1)
   Main PID: 24426 (redis-server)
     Status: "Ready to accept connections"
      Tasks: 5 (limit: 1033)
     Memory: 2.8M
        CPU: 385ms
     CGroup: /system.slice/redis-server.service
             └─24426 "/usr/bin/redis-server 127.0.0.1:6379" "" "" "" "" "" "" ""

Jul 26 07:41:00 localhost systemd&#91;1]: Starting Advanced key-value store...
Jul 26 07:41:00 localhost systemd&#91;1]: Started Advanced key-value store.
lines 1-15/15 (END)
</code></pre>

We can also leverage Redis-cli whenever we want to test whether Redis is working correctly. To use Redis-cli use the following command to ping the connection. 

<pre class="wp-block-code"><code>$ redis-cli

root@localhost:~# redis-cli
127.0.0.1:6379&gt; ping
PONG
127.0.0.1:6379&gt; ping
PONG
127.0.0.1:6379&gt; </code></pre>

To know that Redis is establishing communication, you will get an output as <mark style="background-color:#abb8c3" class="has-inline-color">`pong`</mark> 

### 4. Configuring Redis  {.wp-block-heading}

By default Redis is accessible from the `<mark style="background-color:#abb8c3" class="has-inline-color">localhost</mark>`. it doesn&#8217;t allow remote connections. So to enable Redis to accept remote connections, we need to edit the following configuration file.

<pre class="wp-block-code"><code>$ sudo vi /etc/redis/redis.conf</code></pre>

Check the line that begins with `<mark style="background-color:#abb8c3" class="has-inline-color">bind 127.0.0.1 : : 1</mark>` and uncomment it.

<pre class="wp-block-code"><code>bind 127.0.0.1 ::1</code></pre>

Save and exit and make sure you restart the Redis server again for the changes to take effect.

<pre class="wp-block-code"><code>sudo systemctl restart redis-server</code></pre>

To check that the service has actually taken effect, let&#8217;s test it with the<mark style="background-color:#abb8c3" class="has-inline-color"> `netstat`</mark> command 

<pre class="wp-block-code"><code>$ sudo netstat -lnp | grep redis</code></pre>

If you find an error that netstat is not installed you can do an install with the following command.

<pre class="wp-block-code"><code>sudo apt install net-tools </code></pre>

You will see the output like this.

<pre class="wp-block-code"><code>tcp        0      0 127.0.0.1:6379          0.0.0.0:*               LISTEN      24735/redis-server  
tcp6       0      0 ::1:6379                :::*                    LISTEN      24735/redis-server </code></pre>

### 5. Securing Redis {.wp-block-heading}

By default, Redis binds to all interfaces and has no authentication systems in place. To make Redis more secure, use the following steps to harden your Redis.

  * Make sure the port Redis uses to listen for connections (by default 6379 and additionally 16379 if you run Redis in cluster mode, plus 26379 for Sentinel) is firewalled so that it is not possible to contact Redis from the outside world.
  * Use the `<strong>requirepass</strong>` option in order to add an additional layer of security so that clients will require to authenticate using the **`AUTH`** command.
  * Use a configuration file where the bind directive is set in order to guarantee that Redis listens on only the network interfaces you are using.

In Redis password is configured directly by changing the following configuration file.

<pre class="wp-block-code"><code>$ sudo vi /etc/redis/redis.conf</code></pre>

Scroll down to the security section and change the following and uncomment it and place a very strong password 

<pre class="wp-block-code"><code># requirepass foobared</code></pre>

Save the file and exit and make sure you restart the Redis server again.

<pre class="wp-block-code"><code>sudo systemctl restart redis-server</code></pre>

## Conclusion {.wp-block-heading}

Congratulations, you have installed and configured the Redis server accordingly. For more information, check its rich <a href="https://redis.io/docs/getting-started/" target="_blank" rel="noreferrer noopener">documentation</a>.