---
title: How to install Terraform on Fedora 36
author: Kipkoech Sang
type: post
date: 2022-06-18T11:07:39+00:00
url: /2022/06/18/how-to-install-terraform-on-fedora-36/
rank_math_internal_links_processed:
  - 1
rank_math_seo_score:
  - 62
rank_math_primary_category:
  - 37
rank_math_analytic_object_id:
  - 184
rank_math_focus_keyword:
  - install terraform on fedora
categories:
  - Automation

---
Terraform is an open-source infrastructure as code software tool that provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files.

In this tutorial, we are going to learn how to install Terraform on Fedora 36.

**Infrastructure as code (IAC) tools allow you to manage your infrastructure with a configuration file rather than through a graphical interface. IAC allows you to build, change and manage your infrastructure in a safe, consistent, and repeatable way by defining configurations that you can version, reuse and share.**

## Where do we use Terraform? {.wp-block-heading}

Terraform is used in many use cases such as:

  * Infrastructure as code. We use Terraform to automate and provision our infrastructure such as servers, databases, firewalls, etc. This helps to accelerate cloud adoption because manual provisioning is always slow and cumbersome.
  * Manage network infrastructure. Terraform is used to automate key networking tasks such as updating load balancer member pools.
  * For multi-cloud deployments. Terraform helps in deploying serverless functions such as AWS Lambda.

## Advantages of Using Terraform {.wp-block-heading}

  * Terraform can manage infrastructure on multiple cloud platforms.
  * The human-readable form helps you write infrastructure code quickly.
  * Terraforms state helps you to track resource changes throughout your deployments.
  * You can commit your configurations to version control to safely collaborate on infrastructure.

To deploy infrastructure with Terraform you need to understand the following aspects:

  * **Scope**: You need to identify the infrastructure for your project in advance.
  * **Author**: Write the configuration manifest for your infrastructure.
  * **Initialize**: Install all the plugins needed by Terraform to manage your infrastructure.
  * **Plan**: Preview the changes Terraform will make to match your configurations.
  * **Apply**: Make the planned changes to suit your configs

**What is good about Terraform is that it keeps track of your infrastructure in a state file. Terraform uses the state file to determine the changes to make in your infrastructure in order to match your configurations.**

## Install Terraform on Fedora 36 {.wp-block-heading}

### 1. Update system repositories. {.wp-block-heading}

Before we begin the installation of Terraform, we need to update system repositories in order to make them up to date.

<pre class="wp-block-code"><code>sudo dnf update -y</code></pre>

### 2. Install dnf-config manager  {.wp-block-heading}

Dnf-config manager is the means of managing rpm repositories available to dnf. It is used for managing installed but not enabled by default repositories. Add dnf-config with the following command.

<pre class="wp-block-code"><code>sudo dnf install dnf-plugins-core -y</code></pre>

After installation of dnf-config manager is done, we now need to use it to add official Hashicorp Linux repository to our Fedora 36.

<pre class="wp-block-code"><code>sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo</code></pre>

After you have added to Fedora 36 repository, you can now proceed to install Terraform.

### Install Terraform on Fedora 36 {.wp-block-heading}

To install Terraform on Fedora 36, use the following command.

<pre class="wp-block-code"><code>sudo dnf install terraform -y</code></pre>

The sample output will look like this.

<pre class="wp-block-code"><code>#output
=============================================================================
 Package                           Architecture                   Version                                                   
=============================================================================
Installing:
 &lt;strong>terraform                         x86_64                         1.2.3-1                            hashicorp&lt;/strong>                         

Transaction Summary
=============================================================================
Install  1 Package

Total download size: 13 M
Installed size: 60 M
Downloading Packages:
terraform-1.2.3-1.x86_64.rpm                                                                                  42 MB/s |  13 MB     00:00    
---------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                         41 MB/s |  13 MB     00:00     
Hashicorp Stable - x86_64                                                                                     48 kB/s | 3.1 kB     00:00    
Importing GPG key 0xA3219F7B:
 Userid     : "HashiCorp Security (HashiCorp Package Signing) &lt;security+packaging@hashicorp.com&gt;"
 Fingerprint: E8A0 32E0 94D8 EB4E A189 D270 DA41 8C88 A321 9F7B
 From       : https://rpm.releases.hashicorp.com/gpg
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                     1/1 
  Installing       : terraform-1.2.3-1.x86_64                                                                                            1/1 
  Verifying        : terraform-1.2.3-1.x86_64                                                                                            1/1 

Installed:
  terraform-1.2.3-1.x86_64                                                                                                                   

Complete!</code></pre>

In order to verify the installation use `<strong>terraform -help</strong>` command

<pre class="wp-block-code"><code>&lt;strong>$ terraform -help&lt;/strong>
Usage: terraform &#91;global options] &lt;subcommand&gt; &#91;args]

The available commands for execution are listed below.
The primary workflow commands are given first, followed by
less common or more advanced commands.

Main commands:
  init          Prepare your working directory for other commands
  validate      Check whether the configuration is valid
  plan          Show changes required by the current configuration
  apply         Create or update infrastructure
  destroy       Destroy previously-created infrastructure
</code></pre>

Check the version of installed Terraform with `<strong>terraform -v</strong>` command.

<pre class="wp-block-code"><code>$ terraform -v 
Terraform v1.2.3</code></pre>

## Conclusion  {.wp-block-heading}

We have successfully installed Terraform on Fedora 36. You can proceed to experiment with it now.