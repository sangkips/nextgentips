---
title: Finding your way on a Linux System part 2
author: Kipkoech Sang
type: post
date: 2021-10-08T08:48:15+00:00
url: /2021/10/08/finding-your-way-on-a-linux-system-part-2/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 6
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 143
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
## Using Directories and Listing Files  {.wp-block-heading}

## Files and Directories  {.wp-block-heading}

In this article, we will explore directories and the listing of files in Linux systems. Files contain data used by the computer system. Directories are used to create organization within a file system. Directories can contain files and other directories.

For example, we can use this command to check the directories and files you have.

<pre class="wp-block-code"><code>$ tree</code></pre>

If you are on Ubuntu you can install the tree with the following command:

<pre class="wp-block-code"><code>$ sudo apt install tree

$ sudo snap install tree</code></pre>

<pre class="wp-block-code"><code>Sample output
├── --background
├── cockroach-data &#91;error opening dir]
├── cockroach-latest.linux-amd64.tgz
├── cockroach-v21.1.11
│   └── src
│       └── github.com
│           └── cockroachdb
│               └── cockroach
│                   ├── AUTHORS
│                   ├── _bazel
│                   │   ├── bin
│                   │   ├── cockroach
│                   │   ├── out
│                   │   └── testlogs
│                   ├── build
│                   │   ├── archive
│                   │   │   ├── contents
│                   │   │   │   ├── Makefile
│                   │   │   │   └── README
│                   │   │   └── README.md
│                   │   ├── bazelbuilder
│                   │   │   ├── bazelbuild.sh
│                   │   │   ├── bazeltest.sh
│                   │   │   └── Dockerfile
│                   │   ├── bazelutil
│                   │   │   └── bazel-generate.sh
</code></pre>

From the above, we have 492 directories and 2847 files.

## File and Directory Names  {.wp-block-heading}

In Linux, It is not advisable to use special characters or use spaces when naming files or directories. Spaces for example need escape character **\** to be entered correctly. For example 

<pre class="wp-block-code"><code>$ cd Report \ schedule</code></pre>

Take note of the escape character \. 

## Navigating the Filesystem {.wp-block-heading}

## Getting current location {.wp-block-heading}

In the Linux command line menu, we use **pwd** to know the current location. For example 

<pre class="wp-block-code"><code>$ pwd</code></pre>

Let&#8217;s say we are in the Documents directory and inside documents, we have a WordPress subdirectory. We can know where we are by using pwd command.

The relationship of directories is represented with a forward slash (/). **WordPress** is a subdirectory of **documents** which is a subdirectory of **sang** which is located in a directory called **home** which has a parent directory **root.** 

## Listing Directory content  {.wp-block-heading}

Listing contents of the current directory is done with the **ls command**. The **ls** command only sees the content of the current directory.

<pre class="wp-block-code"><code>$ ls
--background
 cockroach-data
 cockroach-latest.linux-amd64.tgz
 cockroach-v21.1.11
 cockroach-v21.1.11.linux-amd64
 Desktop
 Documents
 Downloads
 GNS3
 google-chrome-stable_current_amd64.deb
'--http-addr=localhost:8080'
 --insecure
'--join=localhost:26257,localhost:26258,localhost:26259'
'--listen-addr=localhost:26257'
 Music
 Pictures
 Postman
 Public
</code></pre>

## Changing Current Directory {.wp-block-heading}

Navigation in Linux is done with the **changes directory command (cd)**. 

We can still use **cd** to go back to the home directory.

## Conclusion {.wp-block-heading}

We have learned how to go about Linux by using special commands such as ls, cd, and pwd. Make sure you understand how to use it because it&#8217;s necessary when navigating through Linux. Next, we will explore Creating, moving, and deleting files in Linux.