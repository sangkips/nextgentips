---
title: How to install Apache CouchDB 3.2 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-17T08:16:46+00:00
url: /2021/10/17/how-to-install-apache-couchdb-3-2-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 132
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this guide, we are going to explore how to install CouchDB on Ubuntu 20.04.

Apache CouchDB is a free and open-source NoSQL database that focuses on data storage in&nbsp;**JSON**&nbsp;format.

CouchDB comes with a suite of features, such as on-the-fly document transformation and real-time change notifications that make web development a breeze. It comes with an easy-to-use web administration console, served directly out of CouchDB. CouchDB is highly available and partition tolerant but is also [eventually consistent][1]. CouchDB has a fault-tolerant storage engine that puts the safety of your data first.

The first thing we do is to make sure that Ubuntu operating system packages are up to date. Issue the following command to update your system.

<pre class="wp-block-code"><code>$ sudo apt update && sudo apt upgrade -y</code></pre>

Next, we can install the required dependencies with the following command:

<pre class="wp-block-code"><code>$ sudo apt install curl software-properties-common apt-transport-https gnupg2 -y</code></pre>

After you have installed all the required dependencies, the next thing is now to install CouchDB:

## Install CouchDB on Ubuntu 20.04 {.wp-block-heading}

CouchDB by default is not available in the Ubuntu repository, so what we have to do is to add CouchDB into our system with the following command: 

We will first add the **[GPG key][2]** to our system with the following command:

<pre class="wp-block-code"><code>$ curl -L https://couchdb.apache.org/repo/bintray-pubkey.asc | sudo apt-key add -</code></pre>

The output will be as follows:

<pre class="wp-block-code"><code>Output
 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3100  100  3100    0     0  43055      0 --:--:-- --:--:-- --:--:-- 43055
OK</code></pre>

The next thing is to add the CouchDB repository to the APT

<pre class="wp-block-code"><code>$ echo "deb https://apache.bintray.com/couchdb-deb focal main" |
sudo tee -a /etc/apt/sources.list</code></pre>

Next, is to update our repository again for changes to take effect and install CouchDB with the following command:

<pre class="wp-block-code"><code>$ sudo apt update -y
$ sudo apt install couchdb -y</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="583" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-51-43.png?resize=810%2C583&#038;ssl=1" alt="CouchDB Standalone" class="wp-image-750" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-51-43.png?resize=1024%2C737&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-51-43.png?resize=300%2C216&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-51-43.png?resize=768%2C553&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-51-43.png?w=1054&ssl=1 1054w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>CouchDB Standalone</figcaption></figure> 

You will be prompted to configure either **standalone**&nbsp;or&nbsp;**clustered**&nbsp;mode. Since we are installing on a single server, we will opt for the single-server&nbsp;**standalone**&nbsp;option.

In the next prompt, you are supposed to configure the network interface to which the **CouchDB** will bind to. In **standalone** server mode, the default is **127.0.0.1**.

Set the admin password next.

Confirm the password and finalize your installation.

## Verify CouchDB Installation {.wp-block-heading}

In order to check if your installation went on smoothly use the following command to confirm

<pre class="wp-block-code"><code>$ ss -antpl | grep 5984$ </code></pre>

The **CouchDB** server listens on port **5984** by default.



You can now verify that the installation was successful by running the curl command. If you get a JSON command you are sure it is working. 

<pre class="wp-block-code"><code>$ curl http://127.0.0.1:5984/</code></pre>

## Access CouchDB Web Interface  {.wp-block-heading}

The web user interface can be accessed from our browser by typing the following command on your browser, **http://127.0.0.1:5984/_utils/**.

Type admin and input your password to login<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-57-30.png?resize=810%2C464&#038;ssl=1" alt="CouchDB Dashboard" class="wp-image-751" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-57-30.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-57-30.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-57-30.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-28-20-57-30.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>CouchDB Dashboard</figcaption></figure> 

## Conclusion {.wp-block-heading}

Congratulation you are now able to install CouchDB on Ubuntu 20.04. Hopefully, you have found this tutorial helpful. Check [Apache Documentation][3] in case you have difficulty.

You may find the following article useful:

  * [Databases you can use for free][4]

 [1]: https://docs.couchdb.org/en/main/intro/consistency.html#intro-consistency
 [2]: https://gnupg.org/
 [3]: https://docs.couchdb.org/en/stable/install/unix.html#enabling-the-apache-couchdb-package-repository
 [4]: https://nextgentips.com/2021/10/02/databases-you-can-use-for-free/