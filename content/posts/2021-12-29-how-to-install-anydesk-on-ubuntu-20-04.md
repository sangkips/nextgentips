---
title: How to install Anydesk on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-12-29T18:11:18+00:00
url: /2021/12/29/how-to-install-anydesk-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"114.119.132.170";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 47
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial guide, we are going to learn how to install Anydesk on Ubuntu 20.04.

Anydesk is a closed source remote desktop application, it provides platform independent remote access to personal computers and other devices running the host application. It is both suitable for personal use and company-wise.

## Why Anydesk? {.wp-block-heading}

  * Anydesk is well known for superior security. It uses military grade&nbsp;[TLS technology][1]&nbsp;to ensure your device is not accessed from unauthorized sources. Anydesk encrypt every connection with Asymmetric RSA 2048 key exchange. They use salted password Hashing to protect passwords.
  * Anydesk runs on all operating system platforms without any extra cost. This is enough reason for its popularity
  * Andesk has superior performance when it comes to remote connection. Anydesk has what we call DeskRT codec which help communication while having low latency or low bandwidth.

## What do Anydesk offers {.wp-block-heading}

Anydesk is a very powerful remote access tool that offers the following:

  * Easy and stable operations 
  * Offers powerful Linux-based remote connectivity
  * Smooth and seamless remote access to any computer
  * Offers continuous connection to any operating system.
  * Offers simple and user-friendly and set up and administration tools.

## Related Articles  {.wp-block-heading}

  * [How to Install Anydesk on Ubuntu 21.04][2]

## Installing Anydesk on Ubuntu 20.04 {.wp-block-heading}

## 1. Run System updates  {.wp-block-heading}

To begin our installation, we first need to make sure that our system repositories are up to date by doing a system-wide update. On your terminal type the following command;

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After both updates and upgrades are over, you can restart your system for changes to take effect.

## 2. Add Repository key  {.wp-block-heading}

We need to add the repository key to our trusted software providers. To add the key use the following command on your terminal.

<pre class="wp-block-code"><code>$ wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -</code></pre>

It will show ok as the output meaning the key had been added successfully into your system.

## 3. Add Anydesk repository {.wp-block-heading}

To update our sources.list, we need to add the Anydesk repository so that in case you want to access it, the system will know where to find our Anydesk. So add the following command;

<pre class="wp-block-code"><code>$ echo "deb http://deb.anydesk.com/ all main" > /etc/apt/sources.list.d/anydesk-stable.list</code></pre>

After you have added make sure you run system updates again in order to update our repositories.

<pre class="wp-block-code"><code>$ sudo apt update
&lt;strong>Get:1 http://deb.anydesk.com all InRelease &#91;5588 B]&lt;/strong>
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                   
Hit:4 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease                             
Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease
Hit:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
&lt;strong>Get:7 http://deb.anydesk.com all/main amd64 Packages &#91;639 B]&lt;/strong></code></pre>

From the above highlighted, Anydesk has been added to our repository successfully. Next is to only install it into our system.

## 4. Install Anydesk on Ubuntu 20.04 {.wp-block-heading}

It&#8217;s now time we install Anydesk. To run the installation, use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install anydesk -y</code></pre>

Anydesk needs many packages to run smoothly, this is evidence by the below sample output.

<pre class="wp-block-code"><code>Sample output
The following additional packages will be installed:
  adwaita-icon-theme cpp cpp-9 fontconfig fontconfig-config fonts-dejavu-core gcc-9-base gtk-update-icon-cache
  hicolor-icon-theme humanity-icon-theme libatk1.0-0 libatk1.0-data libauthen-sasl-perl libavahi-client3
  libavahi-common-data libavahi-common3 libcairo-gobject2 libcairo2 libcups2 libdata-dump-perl libdatrie1
  libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libencode-locale-perl libfile-basedir-perl
  libfile-desktopentry-perl libfile-listing-perl libfile-mimeinfo-perl libfont-afm-perl libfontconfig1
  libfontenc1 libgail-common libgail18 libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgl1
  libgl1-mesa-dri libglapi-mesa libglu1-mesa libglvnd0 libglx-mesa0 libglx0 libgraphite2-3 libgtk2.0-0
  libgtk2.0-bin libgtk2.0-common libgtkglext1 libharfbuzz0b libhtml-form-perl libhtml-format-perl
  libhtml-parser-perl libhtml-tagset-perl libhtml-tree-perl libhttp-cookies-perl libhttp-daemon-perl
  libhttp-date-perl libhttp-message-perl libhttp-negotiate-perl libice6 libio-html-perl libio-socket-ssl-perl
  libio-stringy-perl libipc-system-simple-perl libisl22 libjbig0 libjpeg-turbo8 libjpeg8 libllvm12
  liblwp-mediatypes-perl liblwp-protocol-https-perl libmailtools-perl libminizip1 libmpc3 libnet-dbus-perl
  libnet-http-perl libnet-smtp-ssl-perl libnet-ssleay-perl libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0
  libpangox-1.0-0 libpciaccess0 libpixman-1-0 librsvg2-2 librsvg2-common libsensors-config libsensors5 libsm6
  libthai-data libthai0 libtie-ixhash-perl libtiff5 libtimedate-perl libtry-tiny-perl liburi-perl libvulkan1
  libwayland-client0 libwebp6 libwww-perl libwww-robotrules-perl libx11-protocol-perl libx11-xcb1 libxaw7
  libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-present0 libxcb-randr0 libxcb-render0 libxcb-shape0
  libxcb-shm0 libxcb-sync1 libxcb-xfixes0 libxcomposite1 libxcursor1 libxdamage1 libxfixes3 libxft2 libxi6
  libxinerama1 libxkbfile1 libxml-parser-perl libxml-twig-perl libxml-xpathengine-perl libxmu6 libxpm4
  libxrandr2 libxrender1 libxshmfence1 libxt6 libxtst6 libxv1 libxxf86dga1 libxxf86vm1 mesa-vulkan-drivers
  perl-openssl-defaults ubuntu-mono x11-common x11-utils x11-xserver-utils xdg-utils</code></pre>

Press Y to allow for the installation to continue. 

## Conclusion {.wp-block-heading}

In this tutorial, we have learned how to install Anydesk on Ubuntu 20.04. I am happy you have learned something new. In case you are faced with any difficulty do comment and we will be happy to help.

 [1]: https://www.cloudflare.com/learning/ssl/transport-layer-security-tls/
 [2]: https://nextgentips.com/2021/10/23/how-to-install-anydesk-on-ubuntu-21-04/