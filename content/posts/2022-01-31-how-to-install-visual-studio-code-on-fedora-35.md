---
title: How to install Visual Studio Code on Fedora 35
author: Kipkoech Sang
type: post
date: 2022-01-31T17:43:59+00:00
url: /2022/01/31/how-to-install-visual-studio-code-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 13
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this article, we are going to learn how to install Visual Studio code also known as (VScode) on Fedora 35.

Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built-in Javascript, Node.js, and Typescript. One can do programming for almost all languages with ease with Visual Studio Code. The languages supported are, Go, PHP, C++, C#, Java, Python, and also .NET.

## Reasons for using VSCode {#reasons-for-using-vscode.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem.
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Related Articles {#related-articles.wp-block-heading}

  * [How to install Visual Studio Code on Ubuntu 21.04][1]
  * [How to install Visual Studio Code on ArchLinux][2]
  * [How to install Visual Studio Code on Ubuntu 21.10][3]
  * [How to install Visual Studio Code on Ubuntu 20.04][4]

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Fedora 35 workstation up and running
  * Have command line basics
  * Have reliable internet access
  * Have sudo priliges or you are root user.

## Install Visual Studio Code on Fedora 35 {#install-visual-studio-code-on-fedora-35.wp-block-heading}

### 1. Update system repositories  {#1-update-system-repositories.wp-block-heading}

To begin the process of installing VisualStudio Code on Fedora 35, we need to update our repositories in order to make them up to date, this will enable us not to run into errors later during the installation process.

<pre class="wp-block-code"><code>$ sudo dnf update -y </code></pre>

### 2. Add Microsoft GPG keys {#2-add-microsoft-gpg-keys.wp-block-heading}

Currently Fedora 35 ships the stable 64-bit version VS code in the yum repository. So the first thing is to add GPG keys into our repository. use the following command;

<pre class="wp-block-code"><code>$ sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc</code></pre>

### 3. Create VS code repository  {#3-create-vs-code-repository.wp-block-heading}

We need to create a repository where we will store our VS code files. Use the following code to accomplish this;

<pre class="wp-block-code"><code>$ sudo sh -c 'echo -e "&#91;code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'</code></pre>

After adding the repository, run system updates again.

<pre class="wp-block-code"><code>$ sudo dnf update </code></pre>

<pre class="wp-block-code"><code># output
&lt;strong>Visual Studio Code &lt;/strong>                                                                             17 MB/s |  22 MB     00:01    
Dependencies resolved.
Nothing to do.
Complete!</code></pre>

### 4. Install Visual Studio Code in Fedora 35 {#4-install-visual-studio-code-in-fedora-35.wp-block-heading}

After we have added all the requirements, we can now install VS code with the following code;

<pre class="wp-block-code"><code>$ sudo dnf install code </code></pre>

Sample output will look like this

<pre class="wp-block-code"><code>Installing:
 &lt;strong>code                                     x86_64              1.63.2-1639562596.el7                 code                 103 M&lt;/strong>
Installing dependencies:
 adwaita-cursor-theme                     noarch              41.0-1.fc35                           fedora               625 k
 adwaita-icon-theme                       noarch              41.0-1.fc35                           fedora                11 M
 alsa-lib                                 x86_64              1.2.6.1-3.fc35                        updates              497 k
 at-spi2-atk                              x86_64              2.38.0-3.fc35                         fedora                86 k
 at-spi2-core                             x86_64              2.42.0-1.fc35                         fedora               176 k
 atk                                      x86_64              2.36.0-4.fc35                         fedora               269 k
 avahi-libs                               x86_64              0.8-14.fc35                           fedora                68 k
 cairo                                    x86_64              1.17.4-4.fc35                         fedora               664 k
 cairo-gobject                            x86_64              1.17.4-4.fc35                         fedora                18 k
 cdparanoia-libs                          x86_64              10.2-38.fc35                          fedora                53 k
 colord-libs                              x86_64              1.4.5-3.fc35                          fedora               232 k
 cups-libs                                x86_64              1:2.3.3op2-13.fc35                    updates              265 k</code></pre>

This installation will install many dependencies in the process.

We can start Visual Studio Code with the following command:

<pre class="wp-block-code"><code>$ code </code></pre>

Make sure you are not root while running **code**. Be a regular user.

## Conclusion {#conclusion.wp-block-heading}

We have installed Visual Studio Code in Fedora 35 workstation. Go ahead and configure to suit your programming needs. <a href="https://code.visualstudio.com/docs" target="_blank" rel="noreferrer noopener" title="Consult Visual studio code documentation">Consult Visual studio code documentation</a> if you face any problems. I hope you have enjoyed and learned something new.

 [1]: https://nextgentips.com/2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
 [2]: https://nextgentips.com/2021/11/30/how-to-install-visual-studio-code-6-2-on-archlinux/
 [3]: https://nextgentips.com/2021/11/06/how-to-install-visual-studio-code-on-ubuntu-21-10/
 [4]: https://nextgentips.com/2022/01/31/how-to-install-visual-studio-code-on-ubuntu-20-04/