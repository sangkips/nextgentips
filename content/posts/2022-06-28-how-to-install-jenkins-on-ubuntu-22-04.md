---
title: How to install Jenkins on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-06-28T15:06:15+00:00
url: /2022/06/28/how-to-install-jenkins-on-ubuntu-22-04/
mtm_data:
  - 'a:1:{i:0;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}}'
categories:
  - Automation

---
<a href="https://www.jenkins.io/doc/" target="_blank" rel="noreferrer noopener">Jenkins</a> is a continuous integration tool that allows continuous development, testing, and deployment of newly created codes.

Jenkins can be installed through native package systems, Docker, or can be run as a standalone on a machine with a Java Runtime Environment (JRE).

## Features of Jenkins  {.wp-block-heading}

  * Jenkins is easy to install on any platform
  * It has great plugins 
  * It is easy to configure to any desired state.
  * It is very extensible through plugins 
  * It has a highly distributed architecture. Jenkins works on master-slave architecture.

## Jenkins Pipeline {.wp-block-heading}

Jenkins Pipeline is suite of plugins which support implementing and integrating continuous delivery pipelines. The following are the stages of Jenkins Pipeline 

  * Code commit. This is saving your code to github using git.
  * Build stage
  * Testing stage
  * Release stage
  * Deploy/Deliver stage.

## Prerequisites  {.wp-block-heading}

  * 256 MB of RAM
  * 1 GB of disk space 
  * Make sure Java is installed 

## Installing Jenkins on Ubuntu 22.04 {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

Before we can begin the installation, we need to make sure that our systems are up to date.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Java (JRE) {.wp-block-heading}

Jenkins requires Java to run, so we will require to install openJDK on our machine first. To install java lets first check if we have java installed on our system.

<pre class="wp-block-code"><code>$ java version</code></pre>

If you got not found, then we need to install it before installing Jenkins.

<pre class="wp-block-code"><code>$ sudo apt install openjdk-11-jre -y</code></pre>

The sample output you will get will look like this.

<pre class="wp-block-code"><code>The following additional packages will be installed:
  ca-certificates-java fonts-dejavu-extra java-common libatk-wrapper-java
  libatk-wrapper-java-jni openjdk-11-jre-headless
Suggested packages:
  default-jre fonts-ipafont-gothic fonts-ipafont-mincho fonts-wqy-microhei
  | fonts-wqy-zenhei
The following NEW packages will be installed:
  ca-certificates-java fonts-dejavu-extra java-common libatk-wrapper-java
  libatk-wrapper-java-jni openjdk-11-jre openjdk-11-jre-headless
0 upgraded, 7 newly installed, 0 to remove and 184 not upgraded.
Need to get 43.8 MB of archives.
After this operation, 180 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] </code></pre>

We can check if openjdk 11 is installed once again 

<pre class="wp-block-code"><code>$ java -version
openjdk version "11.0.15" 2022-04-19
OpenJDK Runtime Environment (build 11.0.15+10-Ubuntu-0ubuntu0.22.04.1)
OpenJDK 64-Bit Server VM (build 11.0.15+10-Ubuntu-0ubuntu0.22.04.1, mixed mode, sharing)
</code></pre>

We are good to start installation of Jenkins 

### 3. Install Jenkins on Ubuntu 22.04 {.wp-block-heading}

To install jenkins we are going to add it into apt repository, first we are going to add signing key to our repositories.

<pre class="wp-block-code"><code>$ sudo curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null</code></pre>

When you have successfully added the keyring, add Jenkins apt repository using the following command.

<pre class="wp-block-code"><code>$ sudo echo deb &#91;signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null</code></pre>

The next thing is to update our repositories in order for the changes to take effect.

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

When updates are complete, we can now install Jenkins using the following command.

<pre class="wp-block-code"><code>sudo apt install jenkins -y</code></pre>

The following will be the sample output you will get

<pre class="wp-block-code"><code>The following additional packages will be installed:
  net-tools
The following NEW packages will be installed:
  jenkins net-tools
0 upgraded, 2 newly installed, 0 to remove and 184 not upgraded.
Need to get 87.9 MB of archives.
After this operation, 92.1 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n]</code></pre>

### 4. Configuring Jenkins  {.wp-block-heading}

After you have successfully installed Jenkins, then the next thing to do is to configure it. 

Lets start with enabling Jenkins on our system.

<pre class="wp-block-code"><code>$ sudo systemctl enable jenkins </code></pre>

Start Jenkins with the following command.

<pre class="wp-block-code"><code>$ sudo systemctl start jenkins </code></pre>

To check the status of Jenkins we use the following command.

<pre class="wp-block-code"><code>$ sudo systemctl status jenkins </code></pre>

You should be in a position to see the following output.

<pre class="wp-block-code"><code>● jenkins.service - Jenkins Continuous Integration Server
     Loaded: loaded (/lib/systemd/system/jenkins.service; enabled; vendor prese>
     Active: active (running) since Tue 2022-06-28 16:38:44 EAT; 5min ago
   Main PID: 28316 (java)
      Tasks: 43 (limit: 9355)
     Memory: 2.1G
        CPU: 1min 16.555s
     CGroup: /system.slice/jenkins.service
             └─28316 /usr/bin/java -Djava.awt.headless=true -jar /usr/share/jav>

Jun 28 16:38:15 zx-pc jenkins&#91;28316]: This may also be found at: /var/lib/jenki>
Jun 28 16:38:15 zx-pc jenkins&#91;28316]: *****************************************>
Jun 28 16:38:15 zx-pc jenkins&#91;28316]: *****************************************>
Jun 28 16:38:15 zx-pc jenkins&#91;28316]: *****************************************>
Jun 28 16:38:44 zx-pc jenkins&#91;28316]: 2022-06-28 13:38:44.413+0000 &#91;id=30]     >
Jun 28 16:38:44 zx-pc jenkins&#91;28316]: 2022-06-28 13:38:44.441+0000 &#91;id=23]     >
Jun 28 16:38:44 zx-pc systemd&#91;1]: Started Jenkins Continuous Integration Server.
Jun 28 16:38:45 zx-pc jenkins&#91;28316]: 2022-06-28 13:38:45.653+0000 &#91;id=50]     >
Jun 28 16:38:45 zx-pc jenkins&#91;28316]: 2022-06-28 13:38:45.655+0000 &#91;id=50]     >
Jun 28 16:38:45 zx-pc jenkins&#91;28316]: 2022-06-28 13:38:45.663+0000 &#91;id=50]  </code></pre>

### 5. Unlocking Jenkins  {.wp-block-heading}

To start configuring Jenkins for the first time, go to your browser and type `<strong>http://localhost:80</strong>`**80**. Wait until Jenkins starts<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="495" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-51-00.png?resize=810%2C495&#038;ssl=1" alt="Jenkins start page " class="wp-image-1414" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-51-00.png?w=994&ssl=1 994w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-51-00.png?resize=300%2C184&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-51-00.png?resize=768%2C470&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-51-00.png?resize=400%2C245&ssl=1 400w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Jenkins start page</figcaption></figure> 

To get the Administrator password use the following command.

<pre class="wp-block-code"><code>$ sudo cat /var/lib/jenkins/secrets/initialAdminPassword</code></pre>

Copy the password and use it. Press next to get the plugins to install.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="495" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-57-57.png?resize=810%2C495&#038;ssl=1" alt="Jenkins plugins " class="wp-image-1415" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-57-57.png?w=994&ssl=1 994w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-57-57.png?resize=300%2C184&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-57-57.png?resize=768%2C470&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-16-57-57.png?resize=400%2C245&ssl=1 400w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Jenkins plugins</figcaption></figure> 

Click on `install suggested plugins` for convenience.

Once the plugins have been installed, its now time o set the user <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="495" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-38-43.png?resize=810%2C495&#038;ssl=1" alt="Jenkins Add user. Nextgentips" class="wp-image-1416" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-38-43.png?w=994&ssl=1 994w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-38-43.png?resize=300%2C184&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-38-43.png?resize=768%2C470&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-38-43.png?resize=400%2C245&ssl=1 400w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Jenkins Add user. Nextgentips</figcaption></figure> 

Input your credentials and click save and continue.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="411" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-58-44.png?resize=810%2C411&#038;ssl=1" alt="Jenkins Dashboard. Nextgentips" class="wp-image-1417" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-58-44.png?resize=1024%2C519&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-58-44.png?resize=300%2C152&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-58-44.png?resize=768%2C389&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/06/Screenshot-from-2022-06-28-17-58-44.png?w=1282&ssl=1 1282w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Jenkins Dashboard. Nextgentips</figcaption></figure> 

We have successfully installed Jenkins, proceed to create your new project.