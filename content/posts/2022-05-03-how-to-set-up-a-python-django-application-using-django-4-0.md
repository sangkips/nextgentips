---
title: How to set up a Python Django Application using Django 4.0
author: Kipkoech Sang
type: post
date: 2022-05-03T19:17:01+00:00
url: /2022/05/03/how-to-set-up-a-python-django-application-using-django-4-0/
rank_math_seo_score:
  - 16
rank_math_internal_links_processed:
  - 1
rank_math_primary_category:
  - 36
rank_math_analytic_object_id:
  - 173
categories:
  - Programming
tags:
  - django
  - pyhton3.11

---
Django is a backend development language, it is a language for perfectionists. This is called so because it gives you many components out of blue. Whenever you want to develop an application, Django is the go-to language. 

To start using Django, you need to be well conversant with Python language first. Begin by knowing all Python-related stuff such as variables, how to do looping in python, tuples, and dictionaries because this is what you encounter every time while working with Django.

In this tutorial, I will take you through setting the Python Django project. The easiest way to do what I usually use is using Pycharm text editor.

## Steps to follow: {.wp-block-heading}

## Step 1: Open Pycharm text editor. {.wp-block-heading}

On the upper left corner click on **file->New project->Name of the project->create**

<pre class="wp-block-code"><code>file-&gt;New project-&gt;Name of the project-&gt;create</code></pre>

Here Pycharm automatically creates a project for you.

Alternatively, you can create a virtual environment on your terminal using this process: <a href="https://nextgentips.com/2022/01/22/how-to-install-python-django-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install Python Django on Ubuntu 20.04</a>

## Step 2: Creating a Project {.wp-block-heading}

Creating a project is the easiest part, first install Django using the following command on the build-in terminal on Pycharm.

<pre class="wp-block-code"><code>&lt;strong>$pip install django~=4.0&lt;/strong>
Collecting django
  Using cached Django-4.0.4-py3-none-any.whl (8.0 MB)
Collecting asgiref&lt;4,&gt;=3.4.1
  Using cached asgiref-3.5.1-py3-none-any.whl (22 kB)
Collecting sqlparse&gt;=0.2.2
  Using cached sqlparse-0.4.2-py3-none-any.whl (42 kB)
Installing collected packages: sqlparse, asgiref, django
Successfully installed asgiref-3.5.1 django-4.0.4 sqlparse-0.4.2</code></pre>

You can check the version of Django using this command.

<pre class="wp-block-code"><code>$ python -m django --version
4.0.4</code></pre>

Next is to start your project.

On your command line use the following to start a new project.

<pre class="wp-block-code"><code>django-admin startproject myproject .</code></pre>

Make use of the period at the end, it is necessary because it&#8217;s instructing that the project be created in the current directory.

If you do an ls inside the project directory, you should see the following:

<pre class="wp-block-code"><code>$ls
manage.py  myproject</code></pre>

Inside my project, this should be what you can find inside

<pre class="wp-block-code"><code>asgi.py  __init__.py  settings.py  urls.py  wsgi.py</code></pre>

## Step 3: Start development server {.wp-block-heading}

When you are satisfied with your setup, start the development server while inside the root of the Django project. The root of the Django project is the directory where manage.py is located. in our case its inside myproject folder.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="331" height="325" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-33-49.png?resize=331%2C325&#038;ssl=1" alt="" class="wp-image-1331" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-33-49.png?w=331&ssl=1 331w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-33-49.png?resize=300%2C295&ssl=1 300w" sizes="(max-width: 331px) 100vw, 331px" data-recalc-dims="1" /> <figcaption>nextgentips-Root of Django project</figcaption></figure> 

Use the following command to run the development server.

<pre class="wp-block-code"><code>$ python manage.py runserver</code></pre>

This will start the development server and you will the following from your terminal.

<pre class="wp-block-code"><code>
System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
May 03, 2022 - 18:39:21
Django version 4.0.4, using settings 'myproject.settings'
Starting development server at &lt;strong>http://127.0.0.1:8000/&lt;/strong>
</code></pre>

Don&#8217;t worry about the 18 unapplied migrations, we will fix those in a moment. Look the development server will run at port 8000 on our localhost. Copy that URL and paste it into your browser. This is what you will get.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="810" height="568" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-42-16.png?resize=810%2C568&#038;ssl=1" alt="Nextgentips-development server" class="wp-image-1332" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-42-16.png?w=991&ssl=1 991w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-42-16.png?resize=300%2C210&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-42-16.png?resize=768%2C539&ssl=1 768w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Nextgentips-development server</figcaption></figure> 

## Step 4: Creating an App {.wp-block-heading}

Whenever you are creating a project, there are modules that contain the element you want to implement. Let&#8217;s say you want to create an e-commerce website, you will want to divide your project into tiny modules called apps. For example, you will create a products app, cart app, customer app, etc. This will make your project more convenient and elegant.

So to create an app use the following command inside the root of your Django project.

<pre class="wp-block-code"><code>python manage.py startapp product</code></pre>

Product is the name of our app in this example.

Inside the created project folder, this is what you will see.

<pre class="wp-block-code"><code>admin.py  apps.py  __init__.py  migrations  models.py  tests.py  views.py</code></pre>

This will be the directory structure <figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="319" height="468" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-55-08.png?resize=319%2C468&#038;ssl=1" alt="nextgentips-directory structure" class="wp-image-1333" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-55-08.png?w=319&ssl=1 319w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-21-55-08.png?resize=204%2C300&ssl=1 204w" sizes="(max-width: 319px) 100vw, 319px" data-recalc-dims="1" /> <figcaption>nextgentips-directory structure</figcaption></figure> 

## Step 5: Creating your first view {.wp-block-heading}

As you can see from the screenshot above, we have views.py. Click on it, this is where you will write your logic. Inside your view insert the following code.

<pre class="wp-block-code"><code>from django.shortcuts import render

from django.http import HttpResponse


def home_view(request):
    return HttpResponse('&lt;h2&gt;Hello world&lt;h2&gt;')
</code></pre>

I want you to create another file called URLs inside your app, then you will need to insert the following in that urls.py 

product/urls.py

<pre class="wp-block-code"><code>from django.urls import path


from .views import home_view

urlpatterns = &#91;
    path('', home_view, name='home')
]
</code></pre>

Then on the myproject/urls.py insert the following:

<pre class="wp-block-code"><code>from django.contrib import admin
from django.urls import path, include 

urlpatterns = &#91;
    path('', include('product.urls')),
    path('admin/', admin.site.urls),
]
</code></pre>

**On the settings.py** you need to register your app. Go to settings.py->installed_apps->AddYour app name in our case its product.

<pre class="wp-block-code"><code>Go to settings.py-&gt;installed_apps-&gt;'product.apps.ProductConfig'</code></pre>

Refresh your browser and you will see **Hello world** printed on your screen.<figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="422" height="153" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-22-12-56.png?resize=422%2C153&#038;ssl=1" alt="Hello world " class="wp-image-1334" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-22-12-56.png?w=422&ssl=1 422w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2022/05/Screenshot-from-2022-05-03-22-12-56.png?resize=300%2C109&ssl=1 300w" sizes="(max-width: 422px) 100vw, 422px" data-recalc-dims="1" /> <figcaption>Hello world </figcaption></figure> 

In the next lesson, we will see how to create a project using <a href="https://nextgentips.com/2022/05/24/how-to-create-a-django-project-using-templates/" target="_blank" rel="noreferrer noopener">templates</a> and how to do our first database migration.