---
title: How to Install Apache Cassandra 4 on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-10-28T04:02:53+00:00
url: /2021/10/28/how-to-install-apache-cassandra-4-on-ubuntu-21-10/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
ss_ss_click_share_count_twitter:
  - 1
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 120
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In this tutorial we are going to explore how to install Apache Cassandra 4 on our Ubuntu 21.10 server. Apache Cassandra is an open-source, free, NoSQL database used to store large data.

NoSQL databases are light weight, open source, non-relational and largely distributed. It has the following strengths:

  * It has horizontal scalability. Cassandra allows for seamless addition of nodes. a node represent an instance of Cassandra. This nodes do communicate with each other through a protocol called [gossip][1]. Gossip is a peer-to-peer communication protocol in which nodes periodically exchanges communication about themselves and other nodes they know about.
  * It contain distributed architecture. Distributed means that Cassandra can run on multiple machines while appearing to users as a unified whole. Cassandra databases easily scale when an application is under stress, this prevents data loss from any given datacenter&#8217;s hardware failure. 
  * It has flexible approach to schema definition. A schema once defined its columns for a table while inserting data in every row, all columns must at least filled with null value but for Cassandra column families are defined the columns are not. You can add any column to any column family at any time.

Apache Cassandra is suitable for high volume data types. With the advent of Big data, Cassandra have become the solution to issues which SQL databases was not able to solve.

## 1. Prerequisites  {.wp-block-heading}

  * Up and running Ubuntu server 21.10
  * Strong internet connection
  * Server is running java 8 or OpenJDK 8

## 2. Installing OpenJDK 8 for Apache Cassandra 4.0 {.wp-block-heading}

Before we can begin installation, the first thing to do is to ensure java 8 or above is installed. So let us check if we have java in our system with the following command on our terminal.

<pre class="wp-block-code"><code>$ java version</code></pre>

<pre class="wp-block-code"><code># java -version
Command 'java' not found, but can be installed with:
apt install default-jre              # version 2:1.11-72, or
apt install openjdk-11-jre-headless  # version 11.0.12+4-0ubuntu2
apt install openjdk-15-jre-headless  # version 15.0.3+3-1
apt install openjdk-16-jre-headless  # version 16.0.1+9-1
apt install openjdk-17-jre-headless  # version 17~24-1ubuntu1
apt install openjdk-8-jre-headless   # version 8u292-b10-0ubuntu1</code></pre>

If Java 8 is not installed in your system, Let&#8217;s go ahaed and install it. The following command can install Java 8 on your system.

<pre class="wp-block-code"><code>$ apt install openjdk-8-jre-headless</code></pre>

It will start installing many dependencies as you can from the output shown below

<pre class="wp-block-code"><code># apt install openjdk-8-jre-headless -y
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  ca-certificates-java fontconfig-config fonts-dejavu-core java-common libavahi-client3 libavahi-common-data
  libavahi-common3 libcups2 libfontconfig1 libjpeg-turbo8 libjpeg8 liblcms2-2 libpcsclite1 libxi6 libxrender1
  libxtst6 x11-common
Suggested packages:
  default-jre cups-common liblcms2-utils pcscd libnss-mdns fonts-dejavu-extra fonts-ipafont-gothic
  fonts-ipafont-mincho fonts-wqy-microhei fonts-wqy-zenhei fonts-indic
The following NEW packages will be installed:
  ca-certificates-java fontconfig-config fonts-dejavu-core java-common libavahi-client3 libavahi-common-data
  libavahi-common3 libcups2 libfontconfig1 libjpeg-turbo8 libjpeg8 liblcms2-2 libpcsclite1 libxi6 libxrender1
  libxtst6 openjdk-8-jre-headless x11-common</code></pre>

Now we can check if Java 8 was installed successfully installed.

<pre class="wp-block-code"><code>$ java -version</code></pre>

sample output 

<pre class="wp-block-code"><code># java -version
openjdk version "1.8.0_302"
OpenJDK Runtime Environment (build 1.8.0_302-8u302-b08-0ubuntu2-b08)
OpenJDK 64-Bit Server VM (build 25.302-b08, mixed mode)</code></pre>

## 3. Install Apache Cassandra. {.wp-block-heading}

To ensure packages are accessed via HTTPS, apt-transport-https Package should be installed. This APT transport allows the use of repositories accessed via HTTP Secure protocol. Use the following command to do so

<pre class="wp-block-code"><code>$ sudo apt install apt-transport-https</code></pre>

From the ouput below apt-transport-https is there by default on Ubuntu 21.10 server.

<pre class="wp-block-code"><code># sudo apt install apt-transport-https
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
apt-transport-https is already the newest version (2.3.9).</code></pre>

## 4. Add Public GPG key  {.wp-block-heading}

To communicate with other protocols you must exchange public keys. Add the Apache Cassandra repository keys to the list of trusted keys on the server:

<pre class="wp-block-code"><code>$ wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -</code></pre>

<pre class="wp-block-code"><code># wget -q -O - https://www.apache.org/dist/cassandra/KEYS | apt-key add -
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
OK</code></pre>

## 5. Add Cassandra repository to Cassandra.sources.list file {.wp-block-heading}

We can now go ahead and add Cassandra repositorty to Cassandra.sources.list files with the following command. You will realize we are using **40x**. This is a corresponding distribution name.

<pre class="wp-block-code"><code>$ sudo sh -c 'echo "deb http://www.apache.org/dist/cassandra/debian 40x main" &gt; /etc/apt/sources.list.d/cassandra.list'</code></pre>

Before we can do the install of Apache Cassandra, run an update to update all the repositories first.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt install cassandra</code></pre>

Sample output

<pre class="wp-block-code"><code>....
Preparing to unpack .../cassandra_4.0.1_all.deb ...
Unpacking cassandra (4.0.1) ...
Setting up cassandra (4.0.1) ...
Adding group `cassandra' (GID 120) ...
Done.
vm.max_map_count = 1048575
net.ipv4.tcp_keepalive_time = 300
update-rc.d: warning: start and stop actions are no longer supported; falling back to defaults
......</code></pre>

We can verify the status of Cassandra with the following command:

<pre class="wp-block-code"><code>$ sudo systemctl status </code></pre>

<pre class="wp-block-code"><code># sudo systemctl status cassandra
● cassandra.service - LSB: distributed storage system for structured data
     Loaded: loaded (/etc/init.d/cassandra; generated)
     Active: active (running) since Thu 2021-10-28 03:40:14 UTC; 3min 35s ago
       Docs: man:systemd-sysv-generator(8)
    Process: 6867 ExecStart=/etc/init.d/cassandra start (code=exited, status=0/SUCCESS)
      Tasks: 41 (limit: 2338)
     Memory: 1.2G
        CPU: 14.487s
     CGroup: /system.slice/cassandra.service
             └─6967 /usr/bin/java -ea -da:net.openhft... -XX:+UseThreadPriorities -XX:+HeapDumpOnOutOfMemoryErro&gt;

Oct 28 03:40:14 ubuntu-21 systemd&#91;1]: Starting LSB: distributed storage system for structured data...
Oct 28 03:40:14 ubuntu-21 systemd&#91;1]: Started LSB: distributed storage system for structured data.</code></pre>

You can also use this command to check the status of Cassandra

<pre class="wp-block-code"><code>$ nodetool status</code></pre>

Sample output

<pre class="wp-block-code"><code># nodetool status
Datacenter: datacenter1
=======================
Status=Up/Down
|/ State=Normal/Leaving/Joining/Moving
--  Address    Load       Tokens  Owns (effective)  Host ID                               Rack 
UN  127.0.0.1  69.08 KiB  16      100.0%            4d60e0d8-af8a-4e67-9f37-a7bf2728a01b  rack1</code></pre>

Connect to the database with the following command:

<pre class="wp-block-code"><code>cqlsh</code></pre>

Sample Output

<pre class="wp-block-code"><code># cqlsh
Connected to Test Cluster at 127.0.0.1:9042
&#91;cqlsh 6.0.0 | Cassandra 4.0.1 | CQL spec 3.4.5 | Native protocol v5]
Use HELP for help.
cqlsh&gt; exit</code></pre>

You can now go ahead and start Apache Cassandra operations.

## Conclusion {.wp-block-heading}

In this tutorial you have learn how to install Apache Cassandra on Ubuntu 21.10 server. It is now time for you to learn more on the operations of Cassandra in order to become more familiar. In case of difficulty consult the [Apache Cassandra Documentation][2] 

Check out this article for additional information

  * [Databases you can use for free][3]

 [1]: https://docs.datastax.com/en/cassandra-oss/3.x/cassandra/architecture/archGossipAbout.html
 [2]: https://cassandra.apache.org/doc/latest/cassandra/getting_started/installing.html#installing-the-debian-packages
 [3]: https://nextgentips.com/2021/10/02/databases-you-can-use-for-free/