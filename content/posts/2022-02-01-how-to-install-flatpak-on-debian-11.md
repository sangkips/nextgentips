---
title: How to install Flatpak on Debian 11
author: Kipkoech Sang
type: post
date: 2022-02-01T12:24:30+00:00
url: /2022/02/01/how-to-install-flatpak-on-debian-11/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 11
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Flatpak on Debian 11.

Flatpak is a utility for software deployment and package management for Linux. Flatpak offers a sandbox environment in which users can run application software in isolation from the rest of the system.

Flatpak can be used by all kinds of desktop environments and aims to be agnostic as possible regarding how applications are built.

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Debian 11 desktop up and running
  * User with sudo privileges
  * Have basic knowlege of terminal commands

## Related Articles  {#related-articles.wp-block-heading}

  * <a href="https://nextgentips.com/2022/02/01/how-to-install-flatpak-on-fedora-35/" target="_blank" rel="noreferrer noopener">How to install Flatpak on Fedora 35</a>
  * <a href="https://nextgentips.com/2021/11/22/how-to-install-and-configure-flatpak-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener" title="How to install and configure Flatpak on Ubuntu 20.04">How to install and configure Flatpak on Ubuntu 20.04</a>

### 1. Run system updates {#1-run-system-updates.wp-block-heading}

To begin our installation, we need to run system-wide updates, Updates are necessary for every system in order to make the system packages up to date.

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the updates are complete we can proceed to install the repository necessary for Flatpak installation.

### 2. Install Flatpak {#2-install-flatpak.wp-block-heading}

Debian 10 and above have Flatpak package in its system repositories. So to install you just use apt install Flatpak.

<pre class="wp-block-code"><code>$ sudo apt install flatpak</code></pre>

The following is a sample output 

<pre class="wp-block-code"><code>The following additional packages will be installed:
  adwaita-icon-theme alsa-topology-conf alsa-ucm-conf at-spi2-core bubblewrap dbus-user-session dconf-gsettings-backend
  dconf-service desktop-file-utils fontconfig fontconfig-config fonts-dejavu-core fuse glib-networking glib-networking-common
  glib-networking-services gnome-desktop3-data gsettings-desktop-schemas gtk-update-icon-cache hicolor-icon-theme
  libappstream-glib8 libarchive13 libasound2 libasound2-data libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0
  libavahi-client3 libavahi-common-data libavahi-common3 libavahi-glib1 libcairo-gobject2 libcairo2 libcolord2 libcups2
  libdatrie1 libdconf1 libdeflate0 libepoxy0 libflac8 libfontconfig1 libfribidi0 libgdk-pixbuf-2.0-0 libgdk-pixbuf-xlib-2.0-0
  libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgnome-desktop-3-19 libgpgme11 libgraphite2-3 libgtk-3-0
  libgtk-3-bin libgtk-3-common libharfbuzz0b libjbig0 libjpeg62-turbo libjson-glib-1.0-0 libjson-glib-1.0-common liblcms2-2
  libmalcontent-0-0 libogg0 libopus0 libostree-1-1 libpango-1.0-0 libpangocairo-1.0-0 libpangoft2-1.0-0 libpipewire-0.3-0
  libpipewire-0.3-modules libpixman-1-0 libpolkit-agent-1-0 libpolkit-gobject-1-0 libproxy1v5 librest-0.7-0 librsvg2-2
  librsvg2-common libsndfile1 libsoup-gnome2.4-1 libsoup2.4-1 libspa-0.2-modules libstemmer0d libthai-data libthai0 libtiff5
  libvorbis0a libvorbisenc2 libwayland-client0 libwayland-cursor0 libwayland-egl1 libwebp6 libx11-6 libx11-data libxau6
  libxcb-render0 libxcb-shm0 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxdmcp6 libxext6 libxfixes3 libxi6 libxinerama1
  libxkbcommon0 libxkbregistry0 libxrandr2 libxrender1 libxtst6 p11-kit p11-kit-modules pipewire pipewire-bin policykit-1
  shared-mime-info x11-common xdg-dbus-proxy xdg-desktop-portal xdg-desktop-portal-gtk xkb-data</code></pre>

### 3. Install the software Flatpak plugin  {#3-install-the-software-flatpak-plugin.wp-block-heading}

If you are running GNOME, it&#8217;s necessary for you to install the Flatpak plugin for GNOME. Run the following command.

<pre class="wp-block-code"><code>$ sudo apt install gnome-software-plugin-flatpak</code></pre>

### 4. Add Flathub repository {#4-add-flathub-repository.wp-block-heading}

We need to enable remote access to the apps using the Flathub repository. To do that use the following command: 

<pre class="wp-block-code"><code>$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo</code></pre>

Then restart your system for the changes to take effect.

Check the version of installed Flatpak.

<pre class="wp-block-code"><code>$ flatpak --version
Flatpak 1.10.7</code></pre>

### 5. Test Flatpak  {#5-test-flatpak.wp-block-heading}

Let&#8217;s now test to see Flatpak in action. We can install Zoom client.

To **Install zoom** run the following command;

<pre class="wp-block-code"><code>$ flatpak install flathub us.zoom.Zoom</code></pre>

You will get the following output 

<pre class="wp-block-code"><code>Looking for matches…
Required runtime for us.zoom.Zoom/x86_64/stable (runtime/org.freedesktop.Platform/x86_64/21.08) found in remote flathub
Do you want to install it? &#91;Y/n]: y

us.zoom.Zoom permissions:
    ipc                    network                  pulseaudio       x11       devices       file access &#91;1]
    dbus access &#91;2]        bus ownership &#91;3]        tags &#91;4]

    &#91;1] xdg-run/pipewire-0, ~/.zoom:create, ~/Documents/Zoom:create
    &#91;2] org.freedesktop.ScreenSaver, org.gnome.Shell
    &#91;3] org.kde.*
    &#91;4] proprietary


        ID                                             Branch            Op           Remote            Download
        ID                                             Branch            Op           Remote            Download
 1. &#91;✓] org.freedesktop.Platform.GL.default            21.08             i            flathub           130.9 MB / 131.2 MB
 2. &#91;✓] org.freedesktop.Platform.Locale                21.08             i            flathub           142.9 MB / 325.8 MB
 3. &#91;✓] org.freedesktop.Platform.openh264              2.0               i            flathub             1.5 MB / 1.5 MB
 4. &#91;✓] org.freedesktop.Platform                       21.08             i            flathub           152.8 MB / 199.6 MB
 5. &#91;✓] us.zoom.Zoom                                   stable            i            flathub            57.4 MB / 57.4 MB

Installing 5/5… ████████████████████ 100%  57.4 MB/s</code></pre>

To run zoom client, use the following command.

<pre class="wp-block-code"><code>$ flatpak run us.zoom.Zoom </code></pre>

It will open zoom client and from there you need to do the necessary configuration.

## Conclusion {#conclusion.wp-block-heading}

We have successfully installed Flatpak on Fedora 35. To learn more about Flatpak, use its **<a href="https://docs.flatpak.org/en/latest/getting-started.html" target="_blank" rel="noreferrer noopener" title="Flatpak documentation">Flatpak documentation</a>**. I hope you enjoyed and learned something new.