---
title: How To install Zoom-Client 5.8 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-16T18:00:41+00:00
url: /2021/10/16/how-to-install-zoom-client-5-8-on-ubuntu-20-04-03-lts/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 133
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Zoom is a cloud-based video communications app that allows you to set up virtual video and audio conferencing, webinars, live chats, screen-sharing, and other collaborative capabilities. The platform is compatible with Mac, Windows, Linux, iOS, and Android, meaning nearly anyone can access it.

The bigger advantage why many people use Zoom is because of its simplicity. Its easy to use, lightweight, and the interface is relatively intuitive to use with popular features like Gallery View a mode that allows you to see every person on the call at once built right into the app.

Before we can install Zoom client make sure that the camera and microphone are working.

If everything is good then we can begin our installation.

## 1. Install zoom using Graphical Interface. {.wp-block-heading}

Using graphical is suitable for Linux novices those who are not conversant with the command line interface. Use the following command to do so:

<pre class="wp-block-code"><code>$ sudo apt install gdebi</code></pre>

Gdebi is a tiny little app to help in installation of deb files by handling dependencies more effectively. 

<pre class="wp-block-code"><code>Output
$ sudo apt install gdebi
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following packages were automatically installed and are no longer required:
  libfprint-2-tod1 libllvm9 shim
Use 'sudo apt autoremove' to remove them.
The following additional packages will be installed:
  binutils binutils-common binutils-x86-64-linux-gnu build-essential diffstat
  dpkg-dev fakeroot g++ g++-9 gcc gcc-9 gdebi-core gettext gnome-icon-theme
  intltool-debian libalgorithm-diff-perl libalgorithm-diff-xs-perl
  libalgorithm-merge-perl libapt-pkg-perl libarchive-zip-perl libasan5
  libasync-mergepoint-perl libatomic1 libb-hooks-endofscope-perl
  libb-hooks-op-check-perl libbinutils libc-dev-bin libc6-dev
  libcapture-tiny-perl libclass-method-modifiers-perl libclass-xsaccessor-perl
  libclone-perl libcpanel-json-xs-perl libcroco3 libcrypt-dev libctf-nobfd0
  libctf0 libdevel-callchecker-perl libdevel-size-perl
  libdigest-bubblebabble-perl libdigest-hmac-perl libdynaloader-functions-perl
  libemail-valid-perl libexporter-tiny-perl libfakeroot libfile-find-rule-perl
  libfont-ttf-perl libfuture-perl libgcc-9-dev libgtk2-perl
  libimport-into-perl libio-async-loop-epoll-perl libio-async-perl
  libio-pty-perl libio-string-perl libipc-run-perl libitm1
  libjson-maybexs-perl liblinux-epoll-perl liblist-compare-perl
  liblist-moreutils-perl liblsan0 libmodule-implementation-perl
  libmodule-runtime-perl libmoo-perl libmoox-aliases-perl
  libnamespace-clean-perl libnet-dns-perl libnet-dns-sec-perl
  libnet-domain-tld-perl libnet-ip-perl libnet-libidn-perl
  libnumber-compare-perl libpackage-stash-perl libpackage-stash-xs-perl
  libpango-perl libparams-classify-perl libpath-tiny-perl
  libperl4-corelibs-perl libperlio-gzip-perl libquadmath0 libreadonly-perl
  libref-util-perl libref-util-xs-perl librole-tiny-perl
  libsereal-decoder-perl libsereal-encoder-perl libsereal-perl libstdc++-9-dev
  libstrictures-perl libstruct-dumb-perl libsub-exporter-progressive-perl
  libsub-identify-perl libsub-name-perl libsub-quote-perl libtest-fatal-perl
  libtest-refcount-perl libtext-glob-perl libtext-levenshtein-perl libtsan0
  libtype-tiny-perl libtype-tiny-xs-perl libubsan1 libunicode-utf8-perl
  libvariable-magic-perl libxml-libxml-perl libxml-namespacesupport-perl
  libxml-sax-base-perl libxml-sax-expat-perl libxml-sax-perl
  libxml-writer-perl libyaml-libyaml-perl lintian linux-libc-dev make
  manpages-dev patchutils t1utils

</code></pre>

Next head to [Download center][1] to download DEB installer.

After the install is complete head over to downloads page and double click the Zoom package to open with Gdebi<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="464" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-22-21-18-13-1024x586.png?resize=810%2C464&#038;ssl=1" alt="" class="wp-image-354" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-22-21-18-13.png?resize=1024%2C586&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-22-21-18-13.png?resize=300%2C172&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-22-21-18-13.png?resize=768%2C440&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-22-21-18-13.png?w=1294&ssl=1 1294w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> </figure> 

Click install to continue with installation. Enter your admin password to continue.

After install is complete head over to your accessibility area and search for **zoom**, double click to open zoom.

## 2. Installing Zoom via Snap {.wp-block-heading}

First begin by updating our repository with the following command.

<pre class="wp-block-code"><code>$ sudo apt update

$ sudo apt upgrade</code></pre>

After the update and upgrade are complete, you can install Zoom client via [snap package][2]. Type the following command into your terminal.

<pre class="wp-block-code"><code>$ sudo snap install zoom-client</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="570" height="55" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-32-59.png?resize=570%2C55&#038;ssl=1" alt="" class="wp-image-258" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-32-59.png?w=570&ssl=1 570w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-32-59.png?resize=300%2C29&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-32-59.png?resize=24%2C2&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-32-59.png?resize=36%2C3&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-32-59.png?resize=48%2C5&ssl=1 48w" sizes="(max-width: 570px) 100vw, 570px" data-recalc-dims="1" /> </figure> 

## Launch Zoom-client {.wp-block-heading}

On your terminal type **zoom-client** go to your Application launcher and search for zoom.

<pre class="wp-block-code"><code>$ zoom-client</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="660" height="535" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-40-05.png?resize=660%2C535&#038;ssl=1" alt="" class="wp-image-259" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-40-05.png?w=660&ssl=1 660w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-40-05.png?resize=300%2C243&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-40-05.png?resize=24%2C19&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-40-05.png?resize=36%2C29&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-20-40-05.png?resize=48%2C39&ssl=1 48w" sizes="(max-width: 660px) 100vw, 660px" data-recalc-dims="1" /> </figure> 

## 3. Installing zoom via Terminal {.wp-block-heading}

First download DEB installer from the following [Download Center][1].

<pre class="wp-block-code"><code>$ cd ~/Downloads/</code></pre>

Check the zoom file downloaded. 

install the file using the following command 

<pre class="wp-block-code"><code>$ sudo apt install ./zoom_amd64.deb</code></pre>

If you are facing any challenge make sure the following dependencies are installed first.

<pre class="wp-block-code"><code># libglib2.0-0 \
libgstreamer-plugins-base0.10-0 \
libxcb-shape0 \
libxcb-shm0 \
libxcb-xfixes0 \
libxcb-randr0 \
libxcb-image0 \
libfontconfig1 \
libgl1-mesa-glx \
libxi6 \
libsm6 \
libxrender1 \
libpulse0 \
libxcomposite1 \  
libxslt1.1 \
libsqlite3-0 \ 
libxcb-keysyms1 \ 
libxcb-xtest0 \
ibus \</code></pre>

When everything goes as planned, you can launch your zoom using the following command:

<pre class="wp-block-code"><code>$ zoom client</code></pre>

## Uninstalling Zoom client  {.wp-block-heading}

You can do zoom uninstall using the following command:

<pre class="wp-block-code"><code>$ sudo apt remove zoom</code></pre>

## Getting started with Zoom Meeting  {.wp-block-heading}

Once you have installed and test that zoom is working, you are ready to go. If you just want to join the meeting, you can do so like this:

  * Use the zoom meeting link provided by the host.
  * Click join on zoom homepage and enter meeting ID manually.
  * Lastly you can use dial-in calling if you don&#8217;t have access to a mobile app.

If you do have an account and want to&nbsp;schedule your first zoom meeting all you need to do is head to either the app, or your account page on the website, where you can click the &#8220;Schedule&#8221; option. From there, follow the prompts.

## Conclusion {.wp-block-heading}

Congratulation you have install Zoom client on your Ubuntu 20.04. It is now time for you to schedule your meetings with ease.

 [1]: https://zoom.us/download?os=linux
 [2]: https://snapcraft.io/docs/package-repositories