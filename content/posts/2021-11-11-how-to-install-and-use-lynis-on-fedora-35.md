---
title: How to install and Use Lynis on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-11T18:31:34+00:00
url: /2021/11/11/how-to-install-and-use-lynis-on-fedora-35/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
apvc_active_counter:
  - Yes
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 105
rank_math_internal_links_processed:
  - 1
categories:
  - Security

---
In this guide we are going to learn how to install and use Lynis on Fedora 35.

Lynis is an open-source, battle-tested security tool for systems running Linux, MacOS and Unix-based operating system. It performs an extensive health scan of your system in order to support hardening and compliance testing.

Lynis gives complete information about the current operating system, current operating system version, hardware running on the Linux machine, firmware information etc. 

## **Uses of Lynis Software** {.wp-block-heading}

  * It is used for auditing systems entirely
  * It is used for Penetration testing purposes 
  * It is used for system hardening
  * It is used for checking any vulnerability issues in the system.
  * It is used for checking compliance in the system.

## Prerequisites  {.wp-block-heading}

To follow along make sure you have the following:

  * Fedora 35 server up and running 
  * user account with sudo privileges
  * Internet connection
  * Conversant with Linux terminal 

## Table of Contents {.wp-block-heading}

  1. make sure that the server is up to date.
  2. Install Lynis 
  3. Run System Audit

## 1. Update Fedora 35 server {.wp-block-heading}

First we need to ensure taht our server is up to date. To do that we need to run the following command in in our terminal **dnf update -y**

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When the update is complete, then we can move on to install Lynis software. 

## 2. Download Lynis software {.wp-block-heading}

Download Lynis software from [Lynis download][1] page. We can use **wget** command to download directly from the terminal. type the following command into your terminal.

<pre class="wp-block-code"><code>$ wget https://cisofy.com/files/lynis-3.0.6.tar.gz</code></pre>

If you are using a fresh install server like me you will probably get an error message &#8220;wget not found&#8221;, what you nedd to do is to install wget with the following command:

<pre class="wp-block-code"><code>$ sudo dnf install wget</code></pre>

Press y to allow the installation to continue.

Sample output 

<pre class="wp-block-code"><code># sudo dnf install wget
DigitalOcean Droplet Agent                                                        29 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                     Architecture           Version                        Repository               Size
=================================================================================================================
Installing:
 wget                        x86_64                 1.21.2-2.fc35                  updates                 805 k
Installing dependencies:
 libmetalink                 x86_64                 0.1.3-15.fc35                  fedora                   31 k

Transaction Summary
=================================================================================================================
Install  2 Packages

Total download size: 835 k
Installed size: 3.3 M
Is this ok &#91;y/N]: y
Downloading Packages:
(1/2): libmetalink-0.1.3-15.fc35.x86_64.rpm                                      1.3 MB/s |  31 kB     00:00    
(2/2): wget-1.21.2-2.fc35.x86_64.rpm                                             9.4 MB/s | 805 kB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                            3.5 MB/s | 835 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : libmetalink-0.1.3-15.fc35.x86_64                                                        1/2 
  Installing       : wget-1.21.2-2.fc35.x86_64                                                               2/2 
  Running scriptlet: wget-1.21.2-2.fc35.x86_64                                                               2/2 
  Verifying        : libmetalink-0.1.3-15.fc35.x86_64                                                        1/2 
  Verifying        : wget-1.21.2-2.fc35.x86_64                                                               2/2 

Installed:
  libmetalink-0.1.3-15.fc35.x86_64                           wget-1.21.2-2.fc35.x86_64                          

Complete!</code></pre>

Now you can run the download again, this time round it will go through because we now have wget in our system.

<pre class="wp-block-code"><code>$ wget https://cisofy.com/files/lynis-3.0.6.tar.gz</code></pre>

We now need to extract our tarball into the folder you have chosen your file to be downloaded to. For my case I have downloaded to the default download folder. So I will extract it here. You can check the repository where you are with the following command:

<pre class="wp-block-code"><code>$ pwd
$ ls</code></pre>

Do an **ls** to confirm if indeed the download goes through.

<pre class="wp-block-code"><code># ls
lynis-3.0.6.tar.gz</code></pre>

As you can see the file exist in my root folder. Now we can extract to the root folder still.

<pre class="wp-block-code"><code>$ tar xfvz lynis-3.0.6.tar.gz</code></pre>

After the extraction is complete, it is now time for us to run our Lynis to give us the state of our system.

To audit the system we can run the following command, but you need to cd into lynis directory first

<pre class="wp-block-code"><code>$ cd lynis</code></pre>

Run this command now 

<pre class="wp-block-code"><code>$ ./lynis audit system</code></pre>

You will get the following results, It is is a long list

<pre class="wp-block-code"><code># ./lynis audit system

&#91; Lynis 3.0.6 ]

################################################################################
  Lynis comes with ABSOLUTELY NO WARRANTY. This is free software, and you are
  welcome to redistribute it under the terms of the GNU General Public License.
  See the LICENSE file for details about using this software.

  2007-2021, CISOfy - https://cisofy.com/lynis/
  Enterprise support available (compliance, plugins, interface and tools)
################################################################################


&#91;+] Initializing program
------------------------------------
  - Detecting OS...                                           &#91; DONE ]
  - Checking profiles...                                      &#91; DONE ]

  ---------------------------------------------------
  Program version:           3.0.6
  Operating system:          Linux
  Operating system name:     Fedora Linux
  Operating system version:  35
  Kernel version:            5.14.10
  Hardware platform:         x86_64
  Hostname:                  fedora-35
  ---------------------------------------------------
  Profiles:                  /root/lynis/default.prf
  Log file:                  /var/log/lynis.log
  Report file:               /var/log/lynis-report.dat
  Report version:            1.0
  Plugin directory:          ./plugins
  ---------------------------------------------------
  Auditor:                   &#91;Not Specified]
  Language:                  en
  Test category:             all
  Test group:                all
  ---------------------------------------------------
  - Program update status...                                  &#91; SKIPPED ]

&#91;+] Plugins (phase 1)
------------------------------------
 Note: plugins have more extensive tests and may take several minutes to complete
  
  - Plugins enabled                                           &#91; NONE ]

figuration file                  &#91; FOUND ]
  - Checking default I/O kernel scheduler                     &#91; NOT FOUND ]
  - Checking core dumps configuration
    - configuration in systemd conf files                     &#91; DEFAULT ]
    - configuration in etc/profile                            &#91; DEFAULT ]
    - 'hard' configuration in security/limits.conf            &#91; DEFAULT ]
    - 'soft' configuration in security/limits.conf            &#91; DEFAULT ]
    - Checking setuid core dumps configuration                &#91; PROTECTED ]
  - Check if reboot is needed                                 &#91; YES ]




&#91;+] Storage
------------------------------------
  - Checking firewire ohci driver (modprobe config)           &#91; NOT DISABLED ]

&#91;+] Networking
------------------------------------
  - Checking IPv6 configuration                               &#91; ENABLED ]
      Configuration method                                    &#91; AUTO ]
      IPv6 only                                               &#91; NO ]
  - Checking configured nameservers
    - Testing nameservers
      Nameserver: 67.207.67.2                                 &#91; SKIPPED ]
      Nameserver: 67.207.67.3                                 &#91; SKIPPED ]
    - Minimal of 2 responsive nameservers                     &#91; SKIPPED ]
    - DNSSEC supported (systemd-resolved)                     &#91; NO ]
  - Checking default gateway                                  &#91; DONE ]
  - Getting listening ports (TCP/UDP)                         &#91; DONE ]
  - Checking promiscuous interfaces                           &#91; OK ]
  - Checking waiting connections                              &#91; OK ]
  - Checking status DHCP client
  - Checking for ARP monitoring software                      &#91; NOT FOUND ]
  - Uncommon network protocols                                &#91; 0 ]

&#91;+] Printers and Spools
------------------------------------
  - Checking cups daemon                                      &#91; NOT FOUND ]
  - Checking lp daemon                                        &#91; NOT RUNNING ]

&#91;+] Software: firewalls
------------------------------------
  - Checking iptables kernel module                           &#91; FOUND ]
  - Checking host based firewall                              &#91; ACTIVE ]

&#91;+] LDAP Services
------------------------------------
  - Checking OpenLDAP instance                                &#91; NOT FOUND ]

&#91;+] Logging and files
------------------------------------
  - Checking for a running log daemon                         &#91; OK ]
    - Checking Syslog-NG status                               &#91; NOT FOUND ]
    - Checking systemd journal status                         &#91; FOUND ]
    - Checking Metalog status                                 &#91; NOT FOUND ]
    - Checking RSyslog status                                 &#91; NOT FOUND ]
    - Checking RFC 3195 daemon status                         &#91; NOT FOUND ]
    - Checking minilogd instances                             &#91; NOT FOUND ]
  - Checking logrotate presence                               &#91; OK ]
  - Checking remote logging                                   &#91; NOT ENABLED ]
  - Checking log directories (static list)                    &#91; DONE ]
  - Checking open log files                                   &#91; SKIPPED ]

&#91;+] Home directories
------------------------------------
  - Permissions of home directories                           &#91; OK ]
  - Ownership of home directories                             &#91; OK ]
  - Checking shell history files                              &#91; OK ]


&#91;+] Hardening
------------------------------------
    - Installed compiler(s)                                   &#91; NOT FOUND ]
    - Installed malware scanner                               &#91; NOT FOUND ]
    - Non-native binary formats                               &#91; NOT FOUND ]

================================================================================

  -&#91; Lynis 3.0.6 Results ]-

  Warnings (1):
  ----------------------------
  ! Reboot of system is most likely needed &#91;KRNL-5830] 
    - Solution : reboot
      https:&#47;&#47;cisofy.com/lynis/controls/KRNL-5830/

  Suggestions (36):
  ----------------------------
  * Consider hardening system services &#91;BOOT-5264] 
    - Details  : Run '/usr/bin/systemd-analyze security SERVICE' for each service
      https://cisofy.com/lynis/controls/BOOT-5264/

  * If not required, consider explicit disabling of core dump in /etc/security/limits.conf file &#91;KRNL-5820] 
      https://cisofy.com/lynis/controls/KRNL-5820/

  * Configure password hashing rounds in /etc/login.defs &#91;AUTH-9230] 
      https://cisofy.com/lynis/controls/AUTH-9230/

  * Configure minimum password age in /etc/login.defs &#91;AUTH-9286] 
      https://cisofy.com/lynis/controls/AUTH-9286/

  * Configure maximum password age in /etc/login.defs &#91;AUTH-9286] 
      https://cisofy.com/lynis/controls/AUTH-9286/

  * Default umask in /etc/login.defs could be more strict like 027 &#91;AUTH-9328] 
      https://cisofy.com/lynis/controls/AUTH-9328/

  * To decrease the impact of a full /var file system, place /var on a separate partition &#91;FILE-6310] 
      https://cisofy.com/lynis/controls/FILE-6310/

  * Disable drivers like USB storage when not used, to prevent unauthorized storage or data theft &#91;USB-1000] 
      https://cisofy.com/lynis/controls/USB-1000/

  * Disable drivers like firewire storage when not used, to prevent unauthorized storage or data theft &#91;STRG-1846] 
      https://cisofy.com/lynis/controls/STRG-1846/

  * Check DNS configuration for the dns domain name &#91;NAME-4028] 
      https://cisofy.com/lynis/controls/NAME-4028/

  * Split resolving between localhost and the hostname of the system &#91;NAME-4406] 
      https://cisofy.com/lynis/controls/NAME-4406/

  * Consider using a tool to automatically apply upgrades &#91;PKGS-7420] 
      https://cisofy.com/lynis/controls/PKGS-7420/

  * Determine if protocol 'dccp' is really needed on this system &#91;NETW-3200] 
      https://cisofy.com/lynis/controls/NETW-3200/

  * Determine if protocol 'sctp' is really needed on this system &#91;NETW-3200] 
      https://cisofy.com/lynis/controls/NETW-3200/

  * Determine if protocol 'rds' is really needed on this system &#91;NETW-3200] 
      https://cisofy.com/lynis/controls/NETW-3200/

  * Determine if protocol 'tipc' is really needed on this system &#91;NETW-3200] 
      https://cisofy.com/lynis/controls/NETW-3200/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : AllowTcpForwarding (set YES to NO)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : ClientAliveCountMax (set 3 to 2)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : Compression (set YES to NO)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : LogLevel (set INFO to VERBOSE)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : MaxAuthTries (set 6 to 3)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : MaxSessions (set 10 to 2)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : PermitRootLogin (set YES to (FORCED-COMMANDS-ONLY|NO|PROHIBIT-PASSWORD|WITHOUT-PASSWORD))
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : Port (set 22 to )
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : TCPKeepAlive (set YES to NO)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : X11Forwarding (set YES to NO)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Consider hardening SSH configuration &#91;SSH-7408] 
    - Details  : AllowAgentForwarding (set YES to NO)
      https://cisofy.com/lynis/controls/SSH-7408/

  * Enable logging to an external logging host for archiving purposes and additional protection &#91;LOGG-2154] 
      https://cisofy.com/lynis/controls/LOGG-2154/

  * Add a legal banner to /etc/issue, to warn unauthorized users &#91;BANN-7126] 
      https://cisofy.com/lynis/controls/BANN-7126/

  * Add legal banner to /etc/issue.net, to warn unauthorized users &#91;BANN-7130] 
      https://cisofy.com/lynis/controls/BANN-7130/

  * Enable process accounting &#91;ACCT-9622] 
      https://cisofy.com/lynis/controls/ACCT-9622/

  * Enable sysstat to collect accounting (no results) &#91;ACCT-9626] 
      https://cisofy.com/lynis/controls/ACCT-9626/

  * Determine if automation tools are present for system management &#91;TOOL-5002] 
      https://cisofy.com/lynis/controls/TOOL-5002/

  * Consider restricting file permissions &#91;FILE-7524] 
    - Details  : See screen output or log file
    - Solution : Use chmod to change file permissions
      https://cisofy.com/lynis/controls/FILE-7524/

  * One or more sysctl values differ from the scan profile and could be tweaked &#91;KRNL-6000] 
    - Solution : Change sysctl value or disable test (skip-test=KRNL-6000:&lt;sysctl-key>)
      https://cisofy.com/lynis/controls/KRNL-6000/

  * Harden the system by installing at least one malware scanner, to perform periodic file system scans &#91;HRDN-7230] 
    - Solution : Install a tool like rkhunter, chkrootkit, OSSEC
      https://cisofy.com/lynis/controls/HRDN-7230/

  Follow-up:
  ----------------------------
  - Show details of a test (lynis show details TEST-ID)
  - Check the logfile for all details (less /var/log/lynis.log)
  - Read security controls texts (https://cisofy.com)
  - Use --upload to upload data to central system (Lynis Enterprise users)

================================================================================

  Lynis security scan details:

  Hardening index : 64 &#91;############        ]
  Tests performed : 231
  Plugins enabled : 0

  Components:
  - Firewall               &#91;V]
  - Malware scanner        &#91;X]

  Scan mode:
  Normal &#91;V]  Forensics &#91; ]  Integration &#91; ]  Pentest &#91; ]

  Lynis modules:
  - Compliance status      &#91;?]
  - Security audit         &#91;V]
  - Vulnerability scan     &#91;V]

  Files:
  - Test and debug information      : /var/log/lynis.log
  - Report data                     : /var/log/lynis-report.dat

================================================================================

  Lynis 3.0.6

  Auditing, system hardening, and compliance for UNIX-based systems
  (Linux, macOS, BSD, and others)

  2007-2021, CISOfy - https://cisofy.com/lynis/
  Enterprise support available (compliance, plugins, interface and tools)

================================================================================

  &#91;TIP]: Enhance Lynis audits by adding your settings to custom.prf (see /root/lynis/default.prf for all settings)
</code></pre>

## Commands commonly used with Lynis {.wp-block-heading}

**Audit system**

Pass it in order to run the audit on the entire system

<pre class="wp-block-code"><code>$ ./lynis audit system</code></pre>

**show commands** : 

When you want to check the available lynis commands you can run show commands 

<pre class="wp-block-code"><code>$ ./lynis show commands 

Commands:
lynis audit
lynis configure
lynis generate
lynis show
lynis update
lynis upload-only</code></pre>

**show help**: 

If you want to get any help from command line pass show help and it will launch an help screen

<pre class="wp-block-code"><code>$ ./lynis show help
Lynis 3.0.6 - Help
==========================

Commands:
audit
configure
generate
show
update
upload-only</code></pre>

**show profiles**: 

It is used to show discovered profiles from the system

<pre class="wp-block-code"><code>$ ./lynis show profiles 
/root/lynis/default.prf</code></pre>

**show settings**: 

It is used to show lynis discovered settings 

<pre class="wp-block-code"><code>$ ./lynis show settings</code></pre>

**show version**

It is used to display the version of lynis 

<pre class="wp-block-code"><code>$ ./lynis show version
3.0.6</code></pre>

## Conclusion {.wp-block-heading}

Congratulations you have learn how to install and use Lynis audit software tool.

 [1]: https://cisofy.com/downloads/lynis/