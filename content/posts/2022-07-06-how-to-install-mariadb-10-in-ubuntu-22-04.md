---
title: How to install MariaDB 10 in Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-06T13:22:15+00:00
url: /2022/07/06/how-to-install-mariadb-10-in-ubuntu-22-04/
categories:
  - Database

---
In this guide we are going to install MariaDB 10 development version, but first what is MariaDB?

MariaDB Server is one of the most popular open-source relational databases. It’s made by the original developers of MySQL and guaranteed to stay open source. It is part of most cloud offerings and the default in most Linux distributions.

It is built upon the values of performance, stability, and openness, and MariaDB Foundation ensures contributions will be accepted on technical merit. Recent&nbsp;**new functionality**&nbsp;includes&nbsp;**advanced clustering with&nbsp;[Galera Cluster 4][1]**,&nbsp;**compatibility features with Oracle Database**&nbsp;and&nbsp;**Temporal Data Tables**, allowing one to query the data as it stood at any point in the past.

You need to have Ubuntu 22.04 server with non-root administrative privileges and a firewall configured with UFW. Check out this for [initial server setup guide for Ubuntu 20.04.][2]

## Related articles {#related-articles.wp-block-heading}

  * [How to Install and Configure MariaDB 10.7 on Fedora 35][3]

## Installing MariaDB 10 {#installing-mariadb-10-7.wp-block-heading}

### 1. Run system updates {#1-run-system-updates.wp-block-heading}

Begin by updating your apt repository with the following command on your terminal

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Install Mariadb 10 on Ubuntu 22.04 {#3-install-mariadb-10-7-on-ubuntu-20-04.wp-block-heading}

Install MariaDB with the following command

<pre class="wp-block-code"><code>$ sudo apt install mariadb-server mariadb-client</code></pre>

The output will look like this

<pre class="wp-block-code"><code>The following additional packages will be installed:
  galera-4 libcgi-fast-perl libcgi-pm-perl libclone-perl
  libconfig-inifiles-perl libdaxctl1 libdbd-mysql-perl libdbi-perl
  libencode-locale-perl libfcgi-bin libfcgi-perl libfcgi0ldbl
  libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl
  libhttp-date-perl libhttp-message-perl libio-html-perl
  liblwp-mediatypes-perl libmariadb3 libmysqlclient21 libndctl6 libpmem1
  libsnappy1v5 libtimedate-perl liburi-perl liburing2 mariadb-client-10.6
  mariadb-client-core-10.6 mariadb-common mariadb-server-10.6
  mariadb-server-core-10.6 mysql-common socat
Suggested packages:
  libmldbm-perl libnet-daemon-perl libsql-statement-perl libdata-dump-perl
  libipc-sharedcache-perl libbusiness-isbn-perl libwww-perl mailx mariadb-test
The following NEW packages will be installed:
  galera-4 libcgi-fast-perl libcgi-pm-perl libclone-perl
  libconfig-inifiles-perl libdaxctl1 libdbd-mysql-perl libdbi-perl
  libencode-locale-perl libfcgi-bin libfcgi-perl libfcgi0ldbl
  libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl
  libhttp-date-perl libhttp-message-perl libio-html-perl
  liblwp-mediatypes-perl libmariadb3 libmysqlclient21 libndctl6 libpmem1
  libsnappy1v5 libtimedate-perl liburi-perl liburing2 mariadb-client
  mariadb-client-10.6 mariadb-client-core-10.6 mariadb-common mariadb-server
  mariadb-server-10.6 mariadb-server-core-10.6 mysql-common socat
0 upgraded, 36 newly installed, 0 to remove and 7 not upgraded.
Need to get 16.8 MB of archives.
After this operation, 104 MB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

You check the version of installed MariaDB with the following command:

<pre class="wp-block-code"><code>$ mariadb --version
mariadb  Ver 15.1 Distrib 10.6.7-MariaDB, for debian-linux-gnu (x86_64) using  EditLine wrapper</code></pre>

### 3. Initial MariaDB configuration {#3-initial-mariadb-configuration.wp-block-heading}

The installation process will not ask you to set a password and this leaves MariaDB insecure. We will use a script that the MariaDB-server package provides strict access to the server and remove unused accounts. Read more about the security script&nbsp;[here][4].

To ensure that our database is secure we need to run the&nbsp;**mysql\_secure\_installation**&nbsp;command to remove all remote root login, remove anonymous users, etc.

<pre class="wp-block-code"><code>$ sudo mysql_secure_installation</code></pre>

The following sample output will be displayed.

<pre class="wp-block-code"><code>NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication &#91;Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? &#91;Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? &#91;Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? &#91;Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? &#91;Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? &#91;Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!</code></pre>

Next is to **enable MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl enable mariadb</code></pre>

**Start MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl start mariadb</code></pre>

Check the&nbsp;**status of the MariaDB database**

<pre class="wp-block-code"><code>$ sudo systemctl status mariadb</code></pre>

<pre class="wp-block-code"><code>● mariadb.service - MariaDB 10.6.7 database server
     Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor prese>
     Active: active (running) since Wed 2022-07-06 13:03:40 UTC; 8min ago
       Docs: man:mariadbd(8)
             https:&#47;&#47;mariadb.com/kb/en/library/systemd/
   Main PID: 2941 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 8 (limit: 1033)
     Memory: 57.1M
        CPU: 336ms
     CGroup: /system.slice/mariadb.service
             └─2941 /usr/sbin/mariadbd</code></pre>

## Test MariaDB 10 {#test-mariadb-10-7.wp-block-heading}

To test MariaDB we can create a database. Login to MariaDB console with the following command.

<pre class="wp-block-code"><code>$ mysql -u root -p</code></pre>

<pre class="wp-block-code"><code>Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 37
Server version: 10.6.7-MariaDB-2ubuntu1 Ubuntu 22.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB &#91;(none)]> </code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have learned how to install MariaDB on Ubuntu 22.04 server and secured it using the `mysql_secure_installation` script. You can now practice running SQL queries to learn more. I hope you enjoyed it and you have learned something new.

 [1]: https://galeracluster.com/library/whats-new.html
 [2]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [3]: https://nextgentips.com/2021/12/25/how-to-install-and-configure-mariadb-10-7-on-fedora-35/
 [4]: https://mariadb.com/kb/en/mysql_secure_installation/