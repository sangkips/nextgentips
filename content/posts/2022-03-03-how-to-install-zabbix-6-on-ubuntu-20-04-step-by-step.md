---
title: How to install Zabbix 6 on Ubuntu 20.04 step by step
author: Kipkoech Sang
type: post
date: 2022-03-03T11:26:18+00:00
url: /2022/03/03/how-to-install-zabbix-6-on-ubuntu-20-04-step-by-step/
rank_math_primary_category:
  - 9
rank_math_description:
  - Zabbix is an open-source tool for monitoring IT infrastructure like networking, servers, cloud services and virtual machines.
rank_math_focus_keyword:
  - install zabbix 6
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_internal_links_processed:
  - 1
categories:
  - Monitoring

---
In this tutorial I will be showing you how to install Zabbix 6 step by step on Ubuntu 20.04.

Zabbix is an open-source tool for monitoring IT infrastructure like networking, servers, cloud services and virtual machines. 

**Zabbix collects and display basic metrics on a dashboard. It uses a flexible notification mechanisms that allows users to configure email based alerts for all the events that happened. This allows fast reaction to server problems. Zabbix offers excellent data visualization and reporting using stored data. Zabbix reports and configurations are accessed via a web based frontend.**

## Features of Zabbix 6 {.wp-block-heading}

  * Zabbix 6 now supports monitoring of over 100k Zabbix instances, which is now great whenever you want to monitor your instances.
  * You can now in a position to define your user permission on any business service provided.
  * Now you are in a position to define read-write or read-only permissions to your specific business services.
  * You are now in a position to receive alerts and be able to react to any business changes thanks to Zabbix 6 capability.
  * You are now capable of assigning weight to each of your business services 
  * Empowering your business service monitoring is now possible with root cause analysis. You can gather all the root cause analysis using the Zabbix API.
  * Its now possible to have out of the box high availability cluster for Zabbix server.
  * They have embedded new way of learning in the context of machine learning and Kubernetes monitoring capability. 

## Zabbix Architecture {.wp-block-heading}

What are the components of Zabbix?

**Server**: It is the central component which agents reports the availability and integrity of the information.

**Storage:** All information are stored in a database mostly MySQL.

**Web interface:** Its where you monitor your Zabbix server activities 

**Zabbix proxy:** It helps to distribute the load of any Zabbix server 

**Agents:** They are deployed on monitoring targets to actively monitor local resources and reports gathered data to Zabbix server.

## Installing Zabbix 6 from packages. {.wp-block-heading}

There a few ways you can install Zabbix, i.e. installing from the sources, from packages and installing from containers. We will be installing from the Ubuntu packages.

So this is how we are going to achieve this

  1. OS distribution will be Ubuntu 20.04
  2. Database will be PostgreSQL
  3. Web server will be NGINX

Lets begin our installation.

### 1. Update system repositories. {.wp-block-heading}

The first step is to update system repositories, this will make our system repositories up to date and also avoid running into errors.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade</code></pre>

### 2. Install Zabbix repository. {.wp-block-heading}

We are going to download Zabbix .deb packages and later install it. Make sure you run system wide updates to enable the changes take effect.

<pre class="wp-block-code"><code>wget https://repo.zabbix.com/zabbix/6.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.0-1+ubuntu20.04_all.deb</code></pre>

After the download is complete we need to install it using the following command.

<pre class="wp-block-code"><code>dpkg -i zabbix-release_6.0-1+ubuntu20.04_all.deb</code></pre>

Then we need to update our repositories again before installing the frontend, server and the agent.

<pre class="wp-block-code"><code>sudo apt update</code></pre>

### 3. Install Zabbix server, Frontend and agent {.wp-block-heading}

Let&#8217;s now proceed to install Zabbix server, frontend and agent. Use the following command.

<pre class="wp-block-code"><code>apt install zabbix-server-pgsql zabbix-frontend-php php7.4-pgsql zabbix-nginx-conf zabbix-sql-scripts zabbix-agent</code></pre>

When installation is complete, we can verify the Zabbix server installed using the following command.

<pre class="wp-block-code"><code>apt-cache policy zabbix-server-pgsql</code></pre>

### 4. Install Postgresql database {.wp-block-heading}

To install Postgresql check out this article <a href="https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install PostgreSQL 14 on Ubuntu 20.04|21.10</a>

Create a file repository for Postgresql

<pre class="wp-block-code"><code>sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" &gt; /etc/apt/sources.list.d/pgdg.list'</code></pre>

Next is to import GPG key

<pre class="wp-block-code"><code>wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -</code></pre>

After this, we need to update our system again.

<pre class="wp-block-code"><code>apt update</code></pre>

And lastly install Postgresql 14 with the following command;

<pre class="wp-block-code"><code>sudo apt install postgresql-14 -y</code></pre>

Once installed login into Postgresql shell;

<pre class="wp-block-code"><code>sudo -i -u postgres</code></pre>

We now need to create a database user with permissions like this:

<pre class="wp-block-code"><code>sudo -u postgres createuser --pwprompt zabbix</code></pre>

Setup the database Zabbix with previously created user.

<pre class="wp-block-code"><code>sudo -u postgres createdb -O zabbix -E Unicode -T template0 zabbix</code></pre>

After this we now need to import initial schema and data. Enter your initial created password when prompted.

<pre class="wp-block-code"><code>zcat /usr/share/doc/zabbix-sql-scripts/postgresql/server.sql.gz | sudo -u zabbix psql zabbix</code></pre>

### 5. Configure database for Zabbix server {.wp-block-heading}

Open your preferred text editor and edit the following **/etc/zabbix/zabbix_server.conf**. I am using vim as text editor.

<pre class="wp-block-code"><code>sudo vi /etc/zabbix/zabbix_server.conf</code></pre>

Add this 

<pre class="wp-block-code"><code>DBPassword=password</code></pre>

### 6. Configure PHP for Zabbix frontend  {.wp-block-heading}

To configure PHP we need to edit **/etc/zabbix/nginx.conf**, uncomment and set listen and server_name directives.

<pre class="wp-block-code"><code>sudo vi /etc/zabbix/nginx.conf</code></pre>

Make sure listen is in port 80 and server_name set to your ip address

Lastly you need to start Zabbix server and agent

<pre class="wp-block-code"><code>#restart
systemctl restart zabbix-server zabbix-agent nginx php7.4-fpm
# enable
systemctl enable zabbix-server zabbix-agent nginx php7.4-fpm</code></pre>

### 7. Configure Zabbix frontend {.wp-block-heading}

To configure frontend head over to http://<server ip/domain> and follow the instructions.

## Conclusion {.wp-block-heading}

I hope this tutorial have gave you more insight about Zabbix. Head over to the <a href="https://www.zabbix.com/documentation/6.0/en/manual/installation/install_from_packages/debian_ubuntu" target="_blank" rel="noreferrer noopener">Zabbix documentation</a> for more information.