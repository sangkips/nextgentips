---
title: How to install Python pip 21 on Ubuntu 21.10
author: Kipkoech Sang
type: post
date: 2021-12-07T14:27:48+00:00
url: /2021/12/07/how-to-install-python-pip-21-on-ubuntu-21-10/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:38:"2601:58b:4200:bb00:d535:14bd:713d:736a";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 73
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
[Pip][1] is a package management system written in Python and is used to install and manage software packages. It connects to an online repository of public packages called the Python Package Index. It can also be configured to connect to other package repositories.

Usually, Pip is automatically installed while working in a virtual environment, and also if you are using Python that has not been modified by the redistributor to remove ensure pip.

Pip is recommended tool for installing Python packages. For you to start using pip, you need to have python installed on your system. 

## Ways of installing pip {#ways-of-installing-pip.wp-block-heading}

  * Using virtual environment method
  * Using get-pip.py script 

## Using Virtual environment method {#using-virtual-environment-method.wp-block-heading}

This invocation will install pip if it is not already installed. In order to ensure that the latest pip installed is the current one pass the &#8211;upgrade parameter to the above command.

<pre class="wp-block-code"><code>$ python -m pip install --upgrade pip</code></pre>

This method requires you have a virtual environment set up first.

Let me show you how to set up a virtual environment.

First, let&#8217;s create a directory called nextgentips

<pre class="wp-block-code"><code>$ sudo mkdir nextgentips
$ cd nextgentips</code></pre>

We will create a virtual environment called **nextvenv**

<pre class="wp-block-code"><code>$ python3 -m venv nextvenv</code></pre>

In case you encounter the following error, follow the instruction in order to make necessary changes.

<pre class="wp-block-code"><code>The virtual environment was not created successfully because ensurepip is not
available.  On Debian/Ubuntu systems, you need to install the python3-venv</code></pre>

Run the following command to correct the following error.

<pre class="wp-block-code"><code>$ sudo apt install python3.9-venv</code></pre>

<pre class="wp-block-code"><code>Output
The following additional packages will be installed:
  python-pip-whl
The following NEW packages will be installed:
  python-pip-whl python3.9-venv
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 1902 kB of archives.
After this operation, 2320 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

Press Y to allow the installation to continue. 

Go ahead and run this command again **python3 -m venv nextvenv**, this time it will run successfully.

<pre class="wp-block-code"><code>$ python3 -m venv nextvenv</code></pre>

To start using the virtual environment, run the following command to activate the virtual environment.

<pre class="wp-block-code"><code>$ source nextvenv/bin/activate</code></pre>

When you see something like this **(nextvenv) root@ubuntu:~/nextgentips#**, you know that you have activated the virtual environment successfully.

Run the following command to install the latest pip.

<pre class="wp-block-code"><code>$ python -m pip install --upgrade pip</code></pre>

Sample output will look like this:

<pre class="wp-block-code"><code>output
Requirement already satisfied: pip in ./nextvenv/lib/python3.9/site-packages (20.3.4)
Collecting pip
  Downloading pip-21.3.1-py3-none-any.whl (1.7 MB)
     |████████████████████████████████| 1.7 MB 25.0 MB/s 
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 20.3.4
    Uninstalling pip-20.3.4:
      Successfully uninstalled pip-20.3.4
&lt;strong>Successfully installed pip-21.3.1&lt;/strong></code></pre>

## Installing pip using bash script {#installing-pip-using-bash-script.wp-block-heading}

First, you will need to add the universe repo. Use the following command to do so.

<pre class="wp-block-code"><code>$ sudo add-apt-repository universe</code></pre>

<pre class="wp-block-code"><code>output
Adding component(s) 'universe' to all repositories.
Press &#91;ENTER] to continue or Ctrl-c to cancel.
Hit:1 http://mirrors.digitalocean.com/ubuntu impish InRelease
Hit:2 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                    
Hit:3 http://mirrors.digitalocean.com/ubuntu impish-updates InRelease                            
Hit:4 http://security.ubuntu.com/ubuntu impish-security InRelease
Hit:5 http://mirrors.digitalocean.com/ubuntu impish-backports InRelease
Reading package lists... Done</code></pre>

Now let&#8217;s get installation script from <https://bootstrap.pypa.io/get-pip.py> using curl 

<pre class="wp-block-code"><code>$ curl https://bootstrap.pypa.io/pip/3.9/get-pip.py --output get-pip.py</code></pre>

When the download is complete, move ahead to install the script with the following command;

<pre class="wp-block-code"><code>$ sudo python3 get-pip.py</code></pre>

You will get the following sample output.

<pre class="wp-block-code"><code>Output
Collecting pip&lt;21.0
  Downloading pip-20.3.4-py2.py3-none-any.whl (1.5 MB)
     |████████████████████████████████| 1.5 MB 33.1 MB/s 
Collecting wheel
  Downloading wheel-0.37.0-py2.py3-none-any.whl (35 kB)
Installing collected packages: wheel, pip
Successfully installed pip-20.3.4 wheel-0.37.0
WARNING: You are using pip version 20.3.4; however, version 21.3.1 is available.
You should consider upgrading via the '/usr/bin/python3 -m pip install --upgrade pip' command.</code></pre>

You can upgrade to the latest version using

<pre class="wp-block-code"><code>$ python3 -m pip install --upgrade pip</code></pre>

## Conclusion {#conclusion.wp-block-heading}

I am glad you have learned how to install pip on Ubuntu 21.10. Continue visiting our page for more good content.

 [1]: https://pypi.org/project/pip/