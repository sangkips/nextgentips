---
title: How to Check Disk Space Utilization in Linux
author: Kipkoech Sang
type: post
date: 2021-10-14T13:38:47+00:00
url: /2021/10/14/top-linux-commands-to-check-disk-space-utilization/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 137
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this article, we are going to explore Linux inbuilt commands to check disk space utilization.

  * du command
  * fdisk-l command 
  * df command 
  * stat command 
  * ls-al command 

## du Command  {.wp-block-heading}

The du command is a standard Linux/Unix command that allows a user to gain disk usage information quickly. Its best applied to specific directories and allows many variations for customizing the output to meet your needs.

<pre class="wp-block-code"><code>du &#91;options] &#91;directories or files]</code></pre>

<pre class="wp-block-code"><code>$ du
4	./.mozilla/firefox/lmfttvz6.default-release/minidumps
1360	./.mozilla/firefox/lmfttvz6.default-release/gmp-gmpopenh264/1.8.1.1
1364	./.mozilla/firefox/lmfttvz6.default-release/gmp-gmpopenh264
16	./.mozilla/firefox/lmfttvz6.default-release/sessionstore-backups
8	./.mozilla/firefox/lmfttvz6.default-release/bookmarkbackups
52	./.mozilla/firefox/lmfttvz6.default-release/features/{a01fe044-8a58-48c9-a1fc-d81a42054f12}
56	./.mozilla/firefox/lmfttvz6.default-release/features
41056	./.mozilla/firefox/lmfttvz6.default-release
41108	./.mozilla/firefox
41120	./.mozilla
1865812	.
</code></pre>

### Commonly used options with du command  {.wp-block-heading}

### -h, &#8211;human-readable  {.wp-block-heading}

**du -h** displays the information in human-readable format. The `-h` flag prints size outputs, such as the ones above, in a human-readable format. This format provides a unit of measure (Bytes).

<pre class="wp-block-code"><code>$ du -h
384K	./.mozilla/firefox/lmfttvz6.default-release/datareporting/archived
420K	./.mozilla/firefox/lmfttvz6.default-release/datareporting
4.0K	./.mozilla/firefox/lmfttvz6.default-release/extensions
4.0K	./.mozilla/firefox/lmfttvz6.default-release/minidumps
1.4M	./.mozilla/firefox/lmfttvz6.default-release/gmp-gmpopenh264/1.8.1.1
1.4M	./.mozilla/firefox/lmfttvz6.default-release/gmp-gmpopenh264
16K	./.mozilla/firefox/lmfttvz6.default-release/sessionstore-backups
8.0K	./.mozilla/firefox/lmfttvz6.default-release/bookmarkbackups
52K	./.mozilla/firefox/lmfttvz6.default-release/features/{a01fe044-8a58-48c9-a1fc-d81a42054f12}
56K	./.mozilla/firefox/lmfttvz6.default-release/features
41M	./.mozilla/firefox/lmfttvz6.default-release
41M	./.mozilla/firefox
41M	./.mozilla
1.8G	.
</code></pre>

### -s &#8211;summarize  {.wp-block-heading}

The `-s` flag is added to the `-h` flag on occasion. With their powers combined, they do not become eco-friendly. Instead, they allow us to get a summary of the directory&#8217;s usage in a human-readable format.

### -a &#8211;all {.wp-block-heading}

This option is used to view all files and not just directories. Adding **-h** option will provide more readability and display disk space information in a better format. It lists the sizes of all files and directories in the given file path.&nbsp;

### &#8211;time {.wp-block-heading}

&nbsp;It shows the time of the last modification to any file in the directory or sub-directory that you run it against. It can be use with the&nbsp;`-ah`&nbsp;flags to find the directory last modified.

<pre class="wp-block-code"><code>$ du -ah --time
4.0K	2021-10-16 16:01	./.mozilla/firefox/lmfttvz6.default-release/pkcs11.txt
96K	2021-11-09 17:01	./.mozilla/firefox/lmfttvz6.default-release/permissions.sqlite
4.0K	2021-10-16 16:01	./.mozilla/firefox/lmfttvz6.default-release/containers.json
56K	2021-11-09 17:09	./.mozilla/firefox/lmfttvz6.default-release/extensions.json
41M	2021-11-09 17:16	./.mozilla/firefox/lmfttvz6.default-release
4.0K	2021-10-16 16:01	./.mozilla/firefox/profiles.ini
41M	2021-11-09 17:16	./.mozilla/firefox
41M	2021-11-09 17:16	./.mozilla
1.8G	2021-11-20 15:15	.
</code></pre>

### -c, &#8211;total {.wp-block-heading}

The&nbsp;`-c`&nbsp;flag adds a line to the bottom of the output that gives you a grand total of all of the disk usage for the file path given.

<pre class="wp-block-code"><code>$ du -ch
8.0K	./.mozilla/firefox/lmfttvz6.default-release/bookmarkbackups
52K	./.mozilla/firefox/lmfttvz6.default-release/features/{a01fe044-8a58-48c9-a1fc-d81a42054f12}
56K	./.mozilla/firefox/lmfttvz6.default-release/features
41M	./.mozilla/firefox/lmfttvz6.default-release
41M	./.mozilla/firefox
41M	./.mozilla
1.8G	.
1.8G	total</code></pre>

You can check everything with man pages 

<pre class="wp-block-code"><code>$ man du
DU(1)                           User Commands                           DU(1)

NAME
       du - estimate file space usage

SYNOPSIS
       du &#91;OPTION]... &#91;FILE]...
       du &#91;OPTION]... --files0-from=F

DESCRIPTION
       Summarize disk usage of the set of FILEs, recursively for directories.

       Mandatory  arguments  to  long options are mandatory for short options
       too.

       -0, --null
              end each output line with NUL, not newline

       -a, --all
              write counts for all files, not just directories

       --apparent-size
              print apparent sizes, rather than disk usage; although the  ap‐
 Manual page du(1) line 1 (press h for help or q to quit)
</code></pre>

## fdisk -l command  {.wp-block-heading}

The&nbsp;_fdisk -l_&nbsp;command displays the disk size along with disk partitioning details. The&nbsp;fdisk&nbsp;utility lets you create and manage partitions on a hard disk. The partition information, which is kept in the disk&#8217;s first physical block.

You can run&nbsp;fdisk&nbsp;only if you&#8217;re&nbsp;root&nbsp;or have read/write permissions for the block-special file concerned.

<pre class="wp-block-code"><code>$ sudo fdisk -l
Disk /dev/loop14: 32.45 MiB, 34017280 bytes, 66440 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop15: 246.85 MiB, 258822144 bytes, 505512 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop16: 240.82 MiB, 252493824 bytes, 493152 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes</code></pre>

## df command  {.wp-block-heading}

**df** command is used to display the&nbsp;**disk space used in the file system**. The &#8216;df&#8217; stands for&nbsp;**&#8220;disk filesystem**.&#8221; It defines the number of blocks used, the number of blocks available, and the directory where the file system is mounted.

<pre class="wp-block-code"><code># df &#91;OPTION]... &#91;FILE]...</code></pre>

### Commonly used options with df comamnd  {.wp-block-heading}

#### -a, &#8211;all {.wp-block-heading}

It is used to include pseudo, duplicate, remote file systems.

<pre class="wp-block-code"><code>$ df -a
/dev/loop1           128      128         0 100% /snap/bare/5
/dev/loop6        224256   224256         0 100% /snap/gnome-3-34-1804/72
/dev/loop7         52224    52224         0 100% /snap/snap-store/547
/dev/loop8         63616    63616         0 100% /snap/gtk-common-themes/1506
/dev/loop9         63360    63360         0 100% /snap/core20/1242
/dev/loop10        66816    66816         0 100% /snap/gtk-common-themes/1519
/dev/loop11       253056   253056         0 100% /snap/zoom-client/163
/dev/loop12        51072    51072         0 100% /snap/snap-store/433
/dev/loop13        33280    33280         0 100% /snap/snapd/13270
/dev/loop14        33280    33280         0 100% /snap/snapd/13640
/dev/loop15       252800   252800         0 100% /snap/zoom-client/164
/dev/loop16       246656   246656         0 100% /snap/gnome-3-34-1804/24
/dev/sda1         523248     5356    517892   2% /boot/efi
tmpfs             805584       32    805552   1% /run/user/1000
gvfsd-fuse             0        0         0    - /run/user/1000/gvfs
/dev/fuse              0        0         0    - /run/user/1000/doc
tmpfs             805588     1980    803608   1% /run/snapd/ns
nsfs                   0        0         0    - /run/snapd/ns/snap-store.mnt
</code></pre>

## df -h {.wp-block-heading}

It prints the results in a human-readable format.

<pre class="wp-block-code"><code>$ df -h
/dev/sda2       117G   16G   96G  14% /
tmpfs           3.9G   58M  3.8G   2% /dev/shm
tmpfs           5.0M  4.0K  5.0M   1% /run/lock
tmpfs           3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/loop2      100M  100M     0 100% /snap/core/11993
/dev/loop0      254M  254M     0 100% /snap/brave/137
/dev/loop3       56M   56M     0 100% /snap/core18/2128
/dev/loop4       56M   56M     0 100% /snap/core18/2246
/dev/loop5       62M   62M     0 100% /snap/core20/1169
/dev/loop1      128K  128K     0 100% /snap/bare/5
/dev/loop6      219M  219M     0 100% /snap/gnome-3-34-1804/72
/dev/loop7       51M   51M     0 100% /snap/snap-store/547
/dev/loop8       63M   63M     0 100% /snap/gtk-common-themes/1506
/dev/loop9       62M   62M     0 100% /snap/core20/1242
/dev/loop10      66M   66M     0 100% /snap/gtk-common-themes/1519
/dev/loop11     248M  248M     0 100% /snap/zoom-client/163
/dev/loop12      50M   50M     0 100% /snap/snap-store/433
/dev/loop13      33M   33M     0 100% /snap/snapd/13270
/dev/loop14      33M   33M     0 100% /snap/snapd/13640
/dev/loop15     247M  247M     0 100% /snap/zoom-client/164
/dev/loop16     241M  241M     0 100% /snap/gnome-3-34-1804/24
/dev/sda1       511M  5.3M  506M   2% /boot/efi
tmpfs           787M   32K  787M   1% /run/user/1000
</code></pre>

## df -m {.wp-block-heading}

It shows the output in a one-megabyte block size. Similarly, it can also be shown in a one-gigabyte block by using the _-g_ option. 

<pre class="wp-block-code"><code>$ df -m
/dev/sda2         119171 15557     97519  14% /
tmpfs               3934    59      3875   2% /dev/shm
tmpfs                  5     1         5   1% /run/lock
tmpfs               3934     0      3934   0% /sys/fs/cgroup
/dev/loop2           100   100         0 100% /snap/core/11993
/dev/loop0           254   254         0 100% /snap/brave/137
/dev/loop3            56    56         0 100% /snap/core18/2128
/dev/loop4            56    56         0 100% /snap/core18/2246
/dev/loop5            62    62         0 100% /snap/core20/1169
/dev/loop1             1     1         0 100% /snap/bare/5
/dev/loop6           219   219         0 100% /snap/gnome-3-34-1804/72
/dev/loop7            51    51         0 100% /snap/snap-store/547
/dev/loop8            63    63         0 100% /snap/gtk-common-themes/1506
/dev/loop9            62    62         0 100% /snap/core20/1242
/dev/loop10           66    66         0 100% /snap/gtk-common-themes/1519
/dev/loop11          248   248         0 100% /snap/zoom-client/163
/dev/loop12           50    50         0 100% /snap/snap-store/433
/dev/loop13           33    33         0 100% /snap/snapd/13270
/dev/loop14           33    33         0 100% /snap/snapd/13640
/dev/loop15          247   247         0 100% /snap/zoom-client/164
/dev/loop16          241   241         0 100% /snap/gnome-3-34-1804/24
/dev/sda1            511     6       506   2% /boot/efi
tmpfs  </code></pre>

## df -i {.wp-block-heading}

It displays disk information in numbers of inodes. The inode is used by Linux filesystems to describe files and to store metadata about the files.

<pre class="wp-block-code"><code>$ df -i 
/dev/sda2      7782400 397586 7384814    6% /
tmpfs          1006983     96 1006887    1% /dev/shm
tmpfs          1006983      8 1006975    1% /run/lock
tmpfs          1006983     18 1006965    1% /sys/fs/cgroup
/dev/loop2       12841  12841       0  100% /snap/core/11993
/dev/loop0        9816   9816       0  100% /snap/brave/137
/dev/loop3       10803  10803       0  100% /snap/core18/2128
/dev/loop4       10833  10833       0  100% /snap/core18/2246
/dev/loop5       11732  11732       0  100% /snap/core20/1169
/dev/loop1          29     29       0  100% /snap/bare/5
/dev/loop6       18500  18500       0  100% /snap/gnome-3-34-1804/72
/dev/loop7       15841  15841       0  100% /snap/snap-store/547
/dev/loop8       62342  62342       0  100% /snap/gtk-common-themes/1506
/dev/loop9       11736  11736       0  100% /snap/core20/1242
/dev/loop10      65095  65095       0  100% /snap/gtk-common-themes/1519
/dev/loop11      13544  13544       0  100% /snap/zoom-client/163
/dev/loop12      15827  15827       0  100% /snap/snap-store/433
/dev/loop13        474    474       0  100% /snap/snapd/13270
/dev/loop14        479    479       0  100% /snap/snapd/13640
/dev/loop15      13540  13540       0  100% /snap/zoom-client/164
/dev/loop16      25913  25913       0  100% /snap/gnome-3-34-1804/24
/dev/sda1            0      0       0     - /boot/efi
tmpfs          1006983     93 1006890    1% /run/user/1000
</code></pre>

## df -x {.wp-block-heading}

It shows information of all files excluding the type of filesystem selected in the command. In the example,_&nbsp;df -x tmpfs_&nbsp;command lists all but excludes tmpfs filesystems.

<pre class="wp-block-code"><code>$ df -x tempfs 
/dev/sda2      122030736 15930096  99858800  14% /
tmpfs            4027932    58952   3968980   2% /dev/shm
tmpfs               5120        4      5116   1% /run/lock
tmpfs            4027932        0   4027932   0% /sys/fs/cgroup
/dev/loop2        101888   101888         0 100% /snap/core/11993
/dev/loop0        259328   259328         0 100% /snap/brave/137
/dev/loop3         56832    56832         0 100% /snap/core18/2128
/dev/loop4         56832    56832         0 100% /snap/core18/2246
/dev/loop5         63360    63360         0 100% /snap/core20/1169
/dev/loop1           128      128         0 100% /snap/bare/5
/dev/loop6        224256   224256         0 100% /snap/gnome-3-34-1804/72
/dev/loop7         52224    52224         0 100% /snap/snap-store/547
/dev/loop8         63616    63616         0 100% /snap/gtk-common-themes/1506
/dev/loop9         63360    63360         0 100% /snap/core20/1242
/dev/loop10        66816    66816         0 100% /snap/gtk-common-themes/1519
/dev/loop11       253056   253056         0 100% /snap/zoom-client/163
/dev/loop12        51072    51072         0 100% /snap/snap-store/433
/dev/loop13        33280    33280         0 100% /snap/snapd/13270
/dev/loop14        33280    33280         0 100% /snap/snapd/13640
/dev/loop15       252800   252800         0 100% /snap/zoom-client/164
/dev/loop16       246656   246656         0 100% /snap/gnome-3-34-1804/24
/dev/sda1         523248     5356    517892   2% /boot/efi
tmpfs             805584       32    805552   1% /run/user/1000
</code></pre>

## Stat command  {.wp-block-heading}

Stat command gives information about the file and filesystem. It gives information such as the size of the file, access permissions and the user ID and group ID, birth time access time of the file

Syntax for stat

<pre class="wp-block-code"><code># stat --options filenames</code></pre>

<pre class="wp-block-code"><code>$ stat /etc/resolv.conf
 File: /etc/resolv.conf -> ../run/systemd/resolve/stub-resolv.conf
  Size: 39        	Blocks: 0          IO Block: 4096   symbolic link
Device: 802h/2050d	Inode: 3932365     Links: 1
Access: (0777/lrwxrwxrwx)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2021-11-20 08:30:18.308716022 +0300
Modify: 2021-10-16 15:51:11.339578003 +0300
Change: 2021-10-16 15:51:11.339578003 +0300
 Birth: -
</code></pre>

## ls -al command  {.wp-block-heading}

The&nbsp;_ls -al_&nbsp;command displays the entire contents of a directory along with its size.

<pre class="wp-block-code"><code>$ ls -al
-rw-r--r--  1 nextgentips nextgentips      310 Oct 16 16:00  .pam_environment
drwxr-xr-x  2 nextgentips nextgentips     4096 Nov 19 13:51  Pictures
drwx------  3 nextgentips nextgentips     4096 Oct 16 16:19  .pki
drwxrwxr-x  3 nextgentips nextgentips     4096 Oct 20 13:41  Postman
-rw-r--r--  1 nextgentips nextgentips      807 Oct 16 15:51  .profile
drwxr-xr-x  2 nextgentips nextgentips     4096 Oct 16 15:57  Public
-rw-rw-r--  1 nextgentips nextgentips      533 Nov 19 18:23  sang
drwx------  6 nextgentips nextgentips     4096 Nov 19 14:14  snap
drwx------  2 nextgentips nextgentips     4096 Nov 10 10:18  .ssh
-rw-rw-r--  1 nextgentips nextgentips        0 Oct 30 17:36 '--store=node1'
-rw-r--r--  1 nextgentips nextgentips        0 Oct 16 16:18  .sudo_as_admin_successful
drwxr-xr-x  2 nextgentips nextgentips     4096 Oct 16 15:57  Templates
drwx------  6 nextgentips nextgentips     4096 Oct 31 23:11  .thunderbird
drwxr-xr-x  2 nextgentips nextgentips     4096 Oct 16 15:57  Videos
-rw-------  1 nextgentips nextgentips      824 Nov 17 13:16  .viminfo
drwxrwxr-x  3 nextgentips nextgentips     4096 Oct 29 16:39 'VirtualBox VMs'
drwxrwxr-x  3 nextgentips nextgentips     4096 Oct 24 18:25  .vscode
-rw-rw-r--  1 nextgentips nextgentips      177 Oct 24 18:51  .wget-hsts
drwxrwxr-x  5 nextgentips nextgentips     4096 Oct 25 16:30  wireguard-linux-compat
drwxrwxr-x  5 nextgentips nextgentips     4096 Oct 25 16:31  wireguard-tools
drwx------  7 nextgentips nextgentips     4096 Oct 22 21:57  .zoom
</code></pre>

## Conclusion {.wp-block-heading}

We have learned how to use du, fdisk -l, df, df stat and ls-all command.