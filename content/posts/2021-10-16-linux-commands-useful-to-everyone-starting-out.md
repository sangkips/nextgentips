---
title: Linux Commands Useful to Everyone starting out
author: Kipkoech Sang
type: post
date: 2021-10-16T11:39:28+00:00
url: /2021/10/16/linux-commands-useful-to-everyone-starting-out/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 134
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
This Linux commands are useful for everyone starting out. It is useful for day to day operations when doing Linux stuff. We are going to explore a number of them during this tutorial. So stay tuned.

## File Permission Commands {.wp-block-heading}

Let us first understand permission groups available. 

Every file and directory has three user based permission groups.

  * **owner** – The Owner permissions apply only the owner of the file or directory, they will not impact the actions of other users.
  * **group** – The Group permissions apply only to the group that has been assigned to the file or directory, they will not effect the actions of other users.
  * **all users** – The All Users permissions apply to all other users on the system, this is the permission group that you want to watch the most.

### Permission Types  {.wp-block-heading}

We have the following permission types:

  * **read** (r)– The Read permission refers to a user’s capability to read the contents of the file.
  * **write** (w)– The Write permissions refer to a user’s capability to write or modify a file or directory.
  * **execute** (x) – The Execute permission affects a user’s capability to execute a file or view the contents of a directory.

use the following command to see the permissions.

<pre class="wp-block-code"><code>$ ls-l</code></pre><figure class="wp-block-image size-full">

<img decoding="async" loading="lazy" width="610" height="65" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-12-36-37.png?resize=610%2C65&#038;ssl=1" alt="" class="wp-image-252" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-12-36-37.png?w=610&ssl=1 610w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-12-36-37.png?resize=300%2C32&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-12-36-37.png?resize=24%2C3&ssl=1 24w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-12-36-37.png?resize=36%2C4&ssl=1 36w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-16-12-36-37.png?resize=48%2C5&ssl=1 48w" sizes="(max-width: 610px) 100vw, 610px" data-recalc-dims="1" /> </figure> 

From the above you can see that i only have read and write permission.

## Linux Directory Commands {.wp-block-heading}

  * **pwd** is used to display the location of the current working directory.
  * **mkdir** is used to create a new directory under any directory.
  * **rmdir** is used to delete a directory.
  * **ls** is used to display a list of content of a directory.
  * **cd** is used to change the current directory.

## Linux File commands {.wp-block-heading}

  * **touch** is used to create empty files. We can create multiple empty files by executing it once.
  * **cat** is a multi-purpose utility in the Linux system. It can be used to create a file, display content of the file, copy the content of one file to another file.
  * **rm**  is used to remove a file.
  * **mv** is used to move a file or a directory from one location to another location.
  * **cp** is used to copy a file or directory.
  * **rename** is used to rename files. It is useful for renaming a large group of files.

Linux File Content Commands

  * **head** is used to display the content of a file. It displays the first 10 lines of a file.
  * **tail** is similar to the head command. The difference between both commands is that it displays the last ten lines of the file content. It is useful for reading the error message.
  * **tac** is the reverse of cat command, as its name specified. It displays the file content in reverse order (from the last line).
  * **more** is quite similar to the cat command, as it is used to display the file content in the same way that the cat command does. The only difference between both commands is that, in case of larger files, the more command displays screenful output at a time.
  * **less** is similar to the more command. It also includes some extra features such as &#8216;adjustment in width and height of the terminal.&#8217; Comparatively, the more command cuts the output in the width of the terminal.

## Linux User Commands {.wp-block-heading}

  * **su** provides administrative access to another user
  * **id** command is used to display the user ID (UID) and group ID (GID).
  * **useradd** is used to add user on a Linux server.
  * **passwd**  is used to create and change the password for a user.

## Linux Filter Commands {.wp-block-heading}

  * **cat** is also used as a filter
  * **cut** is used to select a specific column of a file.
  * **grep** is the most powerful and used filter in a Linux system. The &#8216;grep&#8217; stands for &#8220;**global regular expression print**.&#8221; It is useful for searching the content from a file. Generally, it is used with the pipe.
  * **wc** is used to count the lines, words, and characters in a file.
  * **sort** is used to sort files in alphabetical order.

## Linux Utility Commands {.wp-block-heading}

  * **find** is used to find a particular file within a directory. It also supports various options to find a file such as by name, by type, by date.
  * **locate** is used to search a file by file name in the background.
  * **date** is used to display date, time, time zone, and more.
  * **df** is used to display the disk space used in the file system
  * **mount** is used to connect an external device file system to the system&#8217;s file system.
  * **exit** is used to exit from the current shell.
  * **clear** is used to clear the terminal screen.

## Linux Networking Commands {.wp-block-heading}

  * **ip** is used to assign an IP address, initialize an interface, disable an interface.
  * **ssh** is used to create a remote connection through the ssh protocol.
  * **ping** is used to check the connectivity between two nodes, that is whether the server is connected.
  * **host** is used to display the IP address for a given domain name. It performs the DNS lookups for the DNS Query.
  * **wget** is used to download files from a different repository

## Process management commands  {.wp-block-heading}

  * **ps** is used to display active processes 
  * **kill** is used to terminate a process
  * **killall** is used to terminate all running processes 

## Conclusion  {.wp-block-heading}

From the above we have seen various commands to assist us in daily work on a Linux system.

Check this articles for further reading 

  * [Linux Commands to check disk space utilization][1]
  * [Creating, Moving and deleting files in a Linux System][2]
  * [Finding your way on a Linux system part 2][3]
  * [Finding your way on a Linux system part 1][4]

 [1]: https://nextgentips.com/2021/10/14/top-linux-commands-to-check-disk-space-utilization/
 [2]: https://nextgentips.com/2021/10/13/creating-moving-and-deleting-files-in-linux/
 [3]: https://nextgentips.com/2021/10/08/finding-your-way-on-a-linux-system-part-2/
 [4]: https://nextgentips.com/2021/10/06/finding-your-way-on-a-linux-system/