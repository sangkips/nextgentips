---
title: How to install Ajenti 2 Control Panel on Debian 11
author: Kipkoech Sang
type: post
date: 2021-11-26T12:17:03+00:00
url: /2021/11/26/how-to-install-ajenti-2-control-panel-on-debian-11/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 84
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Ajenti 2 is an open-source, web-based control panel that can be used for a large variety of server management tasks. It can install packages and run commands, and you can view basic server information such as RAM in use, free disk space, etc. All this can be accessed from a web browser. Optionally, an add-on package called Ajenti V allows you to manage multiple websites from the same control panel.

In this tutorial, we are going to install Ajenti 2 on our Debian 11 server.

## Related Articles  {.wp-block-heading}

  * [How to setup Ubuntu 20.04 Server for the first time][1]

## Prerequisites  {.wp-block-heading}

  * Freshly installed Debian 11 distro
  * User with sudo privileges 
  * User with basic terminal usage

## Table of Contents {#table-of-contents.wp-block-heading}

  1. Run system updates 
  2. Add new user to the system
  3. Install Ajenti on Debian 11
  4. Open Ajenti on the browser
  5. Conclusion

## 1. Run System updates  {#1-run-system-updates.wp-block-heading}

The fresh install system needs to refresh its packages before using it. To do that run updates to make the system up to date. Use the following command;

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

## 2. Add new user to the system {#2-add-new-user-to-the-system.wp-block-heading}

<pre class="wp-block-code"><code>$ adduser nextgentips</code></pre>

<pre class="wp-block-code"><code>Sample output
Adding user `nextgentips' ...
Adding new group `nextgentips' (1000) ...
Adding new user `nextgentips' (1000) with group `nextgentips' ...
Creating home directory `/home/nextgentips' ...
Copying files from `/etc/skel' ...
New password: 
Retype new password: 
passwd: password updated successfully
Changing the user information for nextgentips
Enter the new value, or press ENTER for the default
        Full Name &#91;]: 
        Room Number &#91;]: 
        Work Phone &#91;]: 
        Home Phone &#91;]: 
        Other &#91;]: 
Is the information correct? &#91;Y/n] y</code></pre>

### Add nextgentips to sudoers group {#add-nileotech-to-sudoers-group.wp-block-heading}

To add the new user to sudo group use the following command from your terminal.

<pre class="wp-block-code"><code>$ usermod -aG sudo nextgentips</code></pre>

## 3. Install Ajenti 2 on Debian 11 {#3-install-ajenti-on-debian-11.wp-block-heading}

<pre class="wp-block-code"><code>$ curl https://raw.githubusercontent.com/ajenti/ajenti/master/scripts/install.sh | sudo bash -s -</code></pre>

<pre class="wp-block-code"><code>Sample output
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  4854  100  4854    0     0  26096      0 --:--:-- --:--:-- --:--:-- 25957
:: OS: debian
:: Distro: debian
:: Installing prerequisites
Hit:1 http://security.debian.org/debian-security bullseye-security InRelease
Hit:2 http://deb.debian.org/debian bullseye InRelease                                                    
Hit:3 http://deb.debian.org/debian bullseye-updates InRelease                                            
Hit:4 http://deb.debian.org/debian bullseye-backports InRelease                                          
Hit:5 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                            
Reading package lists... Done                                   
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
python3-dbus is already the newest version (1.2.16-5).
python3-dbus set to manually installed.
python3-apt is already the newest version (2.2.1).
python3-apt set to manually installed.
....
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
:: Installing initscript
/usr/bin/systemctl
Created symlink /etc/systemd/system/multi-user.target.wants/ajenti.service → /lib/systemd/system/ajenti.service.
:: Complete

Ajenti will be listening at HTTP port 8000
Log in with your root password or another OS user</code></pre>

Now that you have successfully installed Ajenti on Debian, we can open our control panel on our preferred browser.

### Check the status of Ajenti 2 {.wp-block-heading}

To check if Ajenti is running we can invoke the following command on our terminal.

<pre class="wp-block-code"><code>$ systemctl status ajenti</code></pre>

You will get the following sample output.

<pre class="wp-block-code"><code>Sample output
● ajenti.service - Ajenti panel
     Loaded: loaded (/lib/systemd/system/ajenti.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2021-11-26 10:39:06 UTC; 40min ago
    Process: 21080 ExecStart=/usr/bin/python3 /usr/local/bin/ajenti-panel -d (code=exited, status=0/SUCCESS)
    Process: 21082 ExecStartPost=/bin/sleep 5 (code=exited, status=0/SUCCESS)
   Main PID: 21083 (ajenti-panel)
      Tasks: 8 (limit: 1132)
     Memory: 359.9M
        CPU: 35.994s
     CGroup: /system.slice/ajenti.service
             ├─21083 /usr/bin/python3 /usr/local/bin/ajenti-panel
             ├─21659 /usr/local/bin/ajenti-panel worker &#91;restricted session]
             └─21697 /usr/local/bin/ajenti-panel worker &#91;session 2]

Nov 26 11:09:15 debian-11 su&#91;21662]: pam_unix(su-l:auth): authentication failure; logname= uid=65534 euid=0 tty=>
Nov 26 11:09:18 debian-11 su&#91;21662]: FAILED SU (to root) nobody on pts/1
Nov 26 11:09:34 debian-11 su&#91;21664]: (to nextgentips) nobody on pts/1</code></pre>

if you find that the service is active, then you are good to proceed to the browser. 

### Start Ajenti 2 service. {.wp-block-heading}

If the Ajenti 2 is not running, then you have to start the service with the following command;

<pre class="wp-block-code"><code>$ service ajenti restart</code></pre>

or you can use the following;

<pre class="wp-block-code"><code>$ /etc/init.d/ajenti restart</code></pre>

Ajenti can be run in a verbose debug mode with the following command 

<pre class="wp-block-code"><code>$ ajenti-panel -v</code></pre>

### 4. Open Ajenti 2 on the browser {#4-open-ajenti-on-the-browser.wp-block-heading}

Ajenti runs on port 8000. Head to your preferred browser and open the following

<pre class="wp-block-code"><code>http:&#47;&#47;&lt;server_IP_Address&gt;:8000</code></pre>

Input your password and username created or you can use default username **root** and password **admin**

You will get the following from the browser<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="365" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44.png?resize=810%2C365&#038;ssl=1" alt="Ajenti Dashboard" class="wp-image-731" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44.png?resize=1024%2C462&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44.png?resize=300%2C135&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44.png?resize=768%2C347&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-13-46-44.png?w=1179&ssl=1 1179w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Ajenti Dashboard</figcaption></figure> 

You can install plugins once inside the dashboard. Elevate to root user if you are a regular user to be able to install plugins. See below figure.<figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="433" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-14-10-37.png?resize=810%2C433&#038;ssl=1" alt="Ajenti Plugins" class="wp-image-732" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-14-10-37.png?resize=1024%2C547&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-14-10-37.png?resize=300%2C160&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-14-10-37.png?resize=768%2C410&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-26-14-10-37.png?w=1172&ssl=1 1172w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Ajenti Plugins </figcaption></figure> 

## 5. Conclusion {.wp-block-heading}

We have successfully installed Ajenti on Debian 11. To continue learning, you can consult [Ajenti documentation][2] for more insight.

 [1]: https://nextgentips.com/2021/10/10/initial-server-setup-for-ubuntu-20-04/
 [2]: http://docs1.ajenti.org/en/latest/man/run.html