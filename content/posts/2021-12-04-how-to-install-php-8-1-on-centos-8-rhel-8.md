---
title: How to install PHP 8.1 on CentOS 8/RHEL 8
author: Kipkoech Sang
type: post
date: 2021-12-04T06:45:03+00:00
url: /2021/12/04/how-to-install-php-8-1-on-centos-8-rhel-8/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"66.249.64.134";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 76
rank_math_internal_links_processed:
  - 1
categories:
  - Database

---
In today&#8217;s guide, we are going to learn how to install PHP 8.1 on CentOS 8/RHEL 8

PHP is a general-purpose scripting language suitable for web development. Most websites out there are programmed using PHP language because it is:

  * **Fast**&nbsp;– PHP is fast because it runs on its own memory space. Also PHP has Zend engine that parses PHP code and turns it into opcodes which are then interpreted.
  * **Flexible&nbsp;**– PHP is flexible because it allows to almost all databases. MySQL being the de-facto database to connect to because it is open source.
  * Free and open source
  * **PHP is forgiving&nbsp;**– Why do we say PHP is forgiving? It is forgiving meaning its learning it is not so hard therefore suitable to almost all programmers starting out.
  * PHP supports major communication protocols i.e LDAP, IMAP, POP3.

## Prerequisites for Installing PHP 8.1 {.wp-block-heading}

  * Before you can use PHP you must have&nbsp;**web server installed**. Apache web server is the most widely used with PHP.
  * **Database**&nbsp;must be there for PHP to run. It works well on all databases such Oracle, MySQL, PostgreSQL.
  * **PHP parser**&nbsp;must be there. What is PHP parser if you can ask? PHP Parser is a library that takes a source code written in PHP, passes it through a lexical analyzer and creates its respective syntax tree.

PHP 8.1 is a major update release version. It contains many new features and optimizations e.g

  * It introduced Enumerations feature. Enumerations or Enum feature allow a developer to define a custom type that is limited to one of the discrete number of possible values. This is helpful while defining domain model.
  * Improvement of RFC readonly property version 2. This prevents modification of the property after initialization.
  * Introduction of RFC fibers call action. This seeks to eliminate synchronous and asychronous function call by allowing functions to become interruptible without affecting the entire fuction.
  * Introduction of a non return type. This indicates that a function either exits, throws an exception or doesn’t terminate.
  * Introduction of first class collable actions.
  * Introduction of&nbsp;[fsync][1]&nbsp;and&nbsp;[fdatasync][2]&nbsp;functions
  * Introduction of&nbsp;[array\_is\_list][3]&nbsp;functions
  * Introduction of explicit octal numeral notation

## Related Articles {.wp-block-heading}

  * [How to Install PHP 8 on Ubuntu 21.04][4]
  * [How to Install Apache, MYSQL, PHP on Ubuntu 20.04][5]
  * [How to install PHP 8.1 on Ubuntu 21.04][6]

## Table of contents {.wp-block-heading}

  1. Run system update
  2. Enable EPEL repository
  3. Enable Remi repository
  4. Install PHP 8.1

## 1. Run system updates {.wp-block-heading}

The first thing you need to do is to run system updates to make all repositories up to date. To do so run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

When updates are complete, proceed to check the list of available modules lists belonging to PHP. Use the following command to do so;

<pre class="wp-block-code"><code>$ sudo dnf module list php</code></pre>

The output will look like the one below;

<pre class="wp-block-code"><code>Output
DigitalOcean Droplet Agent                                                        46 kB/s | 3.3 kB     00:00    
CentOS Linux 8 - AppStream
Name              Stream               Profiles                                Summary                           
php               7.2 &#91;d]              common &#91;d], devel, minimal              PHP scripting language            
php               7.3                  common &#91;d], devel, minimal              PHP scripting language            
php               7.4                  common &#91;d], devel, minimal              PHP scripting language            

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

From the above output, you can see that we don&#8217;t have PHP 8.1, so what we need to do is to enable the Remi package repository. This is the maintainer of this RPM package. But first, enable EPEL repository for CentOS 8/RHEL 8.

## 2. Enable EPEl for CentOS 8/RHEL 8 {.wp-block-heading}

Extra Packages for Enterprise Linux (EPEL) is a Fedora Special Interest Group that creates, maintains, and manages a high-quality set of additional packages for Enterprise Linux, including, but not limited to, Red Hat Enterprise Linux (RHEL), CentOS, and Scientific Linux (SL), Oracle Linux (OL).

To enable EPEL on CentOS 8/RHEL 8, run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm</code></pre>

Output will look like this;

<pre class="wp-block-code"><code>Last metadata expiration check: 0:01:13 ago on Sat 04 Dec 2021 06:22:28 AM UTC.
epel-release-latest-8.noarch.rpm                                                  51 kB/s |  23 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                      Architecture           Version                  Repository                    Size
=================================================================================================================
Installing:
 &lt;strong>epel-release                 noarch                 8-13.el8                 @commandline                  23 k&lt;/strong>

Transaction Summary
=================================================================================================================
Install  1 Package

Total size: 23 k
Installed size: 35 k
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : epel-release-8-13.el8.noarch                                                            1/1 
  Running scriptlet: epel-release-8-13.el8.noarch                                                            1/1 
  Verifying        : epel-release-8-13.el8.noarch                                                            1/1 

Installed:
  epel-release-8-13.el8.noarch                                                                                   

Complete!</code></pre>

When this is complete, then we can now add remi report repository.

## 3. Enable Remi report repository {.wp-block-heading}

Let&#8217;s enable remi repository with the following command;

<pre class="wp-block-code"><code>$ sudo dnf -y install https://rpms.remirepo.net/enterprise/remi-release-8.rpm</code></pre>

You will get output as shown below;

<pre class="wp-block-code"><code>output 
Extra Packages for Enterprise Linux Modular 8 - x86_64                           502 kB/s | 980 kB     00:01    
Extra Packages for Enterprise Linux 8 - x86_64                                   6.8 MB/s |  11 MB     00:01    
Last metadata expiration check: 0:00:01 ago on Sat 04 Dec 2021 06:25:07 AM UTC.
remi-release-8.rpm                                                               217 kB/s |  26 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                          Architecture       Version                      Repository                Size
=================================================================================================================
Installing:
&lt;strong> remi-release                     noarch             8.5-2.el8.remi               @commandline              26 k&lt;/strong>
Upgrading:
 centos-linux-release             noarch             8.5-1.2111.el8               baseos                    22 k

Transaction Summary
=================================================================================================================
Install  1 Package
Upgrade  1 Package

Total size: 48 k
Total download size: 22 k
Downloading Packages:
centos-linux-release-8.5-1.2111.el8.noarch.rpm                                   410 kB/s |  22 kB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                             85 kB/s |  22 kB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Running scriptlet: centos-linux-release-8.5-1.2111.el8.noarch                                              1/1 
  Upgrading        : centos-linux-release-8.5-1.2111.el8.noarch                                              1/3 
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                      2/3 
  Cleanup          : centos-linux-release-8.3-1.2011.el8.noarch                                              3/3 
  Running scriptlet: centos-linux-release-8.3-1.2011.el8.noarch                                              3/3 
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                      1/3 
  Verifying        : centos-linux-release-8.5-1.2111.el8.noarch                                              2/3 
  Verifying        : centos-linux-release-8.3-1.2011.el8.noarch                                              3/3 

Upgraded:
 &lt;strong> centos-linux-release-8.5-1.2111.el8.noarch&lt;/strong>                                                                     

Installed:
  &lt;strong>remi-release-8.5-2.el8.remi.noarch &lt;/strong>                                                                            

Complete!  </code></pre>

Now you can list PHP modules to see if PHP 8.1 has been added.

<pre class="wp-block-code"><code>$ sudo dnf module list php</code></pre>

This time you will get the following output.

<pre class="wp-block-code"><code>Output
Remi's Modular repository for Enterprise Linux 8 - x86_64                        4.1 kB/s | 858  B     00:00    
Remi's Modular repository for Enterprise Linux 8 - x86_64                        3.0 MB/s | 3.1 kB     00:00    
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository &lt;remi@remirepo.net&gt;"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok &#91;y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                        2.8 MB/s | 945 kB     00:00    
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                       4.6 kB/s | 858  B     00:00    
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                       3.0 MB/s | 3.1 kB     00:00    
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository &lt;remi@remirepo.net&gt;"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok &#91;y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                       6.4 MB/s | 2.0 MB     00:00    
CentOS Linux 8 - AppStream
Name              Stream                Profiles                               Summary                           
php               7.2 &#91;d]               common &#91;d], devel, minimal             PHP scripting language            
php               7.3                   common &#91;d], devel, minimal             PHP scripting language            
php               7.4                   common &#91;d], devel, minimal             PHP scripting language            

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name              Stream                Profiles                               Summary                           
php               remi-7.2              common &#91;d], devel, minimal             PHP scripting language            
php               remi-7.3              common &#91;d], devel, minimal             PHP scripting language            
php               remi-7.4              common &#91;d], devel, minimal             PHP scripting language            
php               remi-8.0              common &#91;d], devel, minimal             PHP scripting language            
&lt;strong>php               remi-8.1              common &#91;d], devel, minimal &lt;/strong>            PHP scripting language            

Hint: &#91;d]efault, &#91;e]nabled, &#91;x]disabled, &#91;i]nstalled</code></pre>

We have successfully added PHP 8.1 into our repository, let&#8217;s enable it before running the installation.

Enable remi PHP 8.1 with the following command;

<pre class="wp-block-code"><code>$ sudo dnf module enable php:remi-8.1 -y</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>Output
Last metadata expiration check: 0:00:21 ago on Fri 03 Dec 2021 03:13:27 PM UTC.
Dependencies resolved.
=================================================================================================================
 Package                   Architecture             Version                      Repository                 Size
=================================================================================================================
Enabling module streams:
 &lt;strong>php                                                remi-8.1      &lt;/strong>                                              

Transaction Summary
=================================================================================================================

Is this ok &#91;y/N]: y
Complete!</code></pre>

Now that we have enabled remi repository, we need to install web server to server the PHP. I am going to use Nginx webserver. Check this on [how to install Nginx][7]. 

<pre class="wp-block-code"><code>$ sudo dnf install php php-cli php-common php-fpm</code></pre>

<pre class="wp-block-code"><code>Sample output
Installed:
  apr-1.6.3-12.el8.x86_64                                                                                        
  apr-util-1.6.1-6.el8.x86_64                                                                                    
  apr-util-bdb-1.6.1-6.el8.x86_64                                                                                
  apr-util-openssl-1.6.1-6.el8.x86_64                                                                            
  centos-logos-httpd-85.8-2.el8.noarch                                                                           
  httpd-2.4.37-43.module_el8.5.0+1022+b541f3b1.x86_64                                                            
  httpd-filesystem-2.4.37-43.module_el8.5.0+1022+b541f3b1.noarch                                                 
  httpd-tools-2.4.37-43.module_el8.5.0+1022+b541f3b1.x86_64                                                      
  libsodium-1.0.18-2.el8.x86_64                                                                                  
  libxslt-1.1.32-6.el8.x86_64                                                                                    
  mailcap-2.1.48-3.el8.noarch                                                                                    
  mod_http2-1.15.7-3.module_el8.4.0+778+c970deab.x86_64                                                          
  nginx-filesystem-1:1.14.1-9.module_el8.0.0+184+e34fea82.noarch                                                 
  oniguruma5php-6.9.7.1-1.el8.remi.x86_64                                                                        
  php-8.1.0-1.el8.remi.x86_64                                                                                    
  php-cli-8.1.0-1.el8.remi.x86_64                                                                                
  php-common-8.1.0-1.el8.remi.x86_64                                                                             
  php-fpm-8.1.0-1.el8.remi.x86_64                                                                                
  php-mbstring-8.1.0-1.el8.remi.x86_64                                                                           
  php-opcache-8.1.0-1.el8.remi.x86_64                                                                            
  php-pdo-8.1.0-1.el8.remi.x86_64                                                                                
  php-sodium-8.1.0-1.el8.remi.x86_64                                                                             
  php-xml-8.1.0-1.el8.remi.x86_64                                                                                

Complete!</code></pre>

Check PHP version you have installed.

<pre class="wp-block-code"><code>$ php -v
PHP 8.1.0 (cli) (built: Nov 23 2021 18:56:11) (NTS gcc x86_64)
Copyright (c) The PHP Group
Zend Engine v4.1.0, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.0, Copyright (c), by Zend Technologies</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed PHP 8.1 on CentOS 8/RHEL 8. I hope you are comfortable with how to add remi now.

 [1]: https://www.php.net/manual/en/function.fsync.php
 [2]: https://www.php.net/manual/en/function.fdatasync.php
 [3]: https://www.php.net/manual/en/function.array-is-list.php
 [4]: https://nextgentips.com/2021/10/19/how-to-install-php-8-on-ubuntu-21-04/
 [5]: https://nextgentips.com/2021/10/11/how-to-install-apache-mysql-php-on-ubuntu-20-04/
 [6]: https://nextgentips.com/2021/12/03/how-to-install-php-8-1-on-ubuntu-21-04/
 [7]: https://nextgentips.com/2021/11/09/how-to-install-nginx-on-fedora-35/