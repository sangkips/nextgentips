---
title: How to install Python Django on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-22T12:06:40+00:00
url: /2022/01/22/how-to-install-python-django-on-ubuntu-20-04/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:15:"114.119.132.183";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 26
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
Django is a Python-based free and open-source web framework that allows model-template-view architectural patterns. Django encourages rapid development and clean and pragmatic codes. It takes care of much of the hassle of web development so that you can focus on the code without reinventing the wheel.

## Why Django? {.wp-block-heading}

  * Django is **exceedingly fast** as the developers was focusing on quick way to release the project as fast as possible.
  * **Django is scalable**. Building apps in Django makes it easy to scale as the traffic increases saving you on the hassle of adding additional server resources to accommodate the surge in traffic.
  * Django takes care of **security** seriously. It has different ways of authentication which makes it hard for intruders to penetrate.

## Prerequisites {.wp-block-heading}

  * Install Python 3
  * Have a working Virtual environment 
  * Have Pip installed 
  * Install [psycopg2][1] if you are using PostgreSQL
  * Install [DB API driver][2] like msqlclient if you are planning on MariaDB
  * Database such as Postgresql, MariaDB, SQLite if planning on large scale production code.
  * Basic programming skills

## Related Articles  {.wp-block-heading}

  * [How to install Python Django in Ubuntu 21.10][3]
  * [How to install PostgreSQL 14 on Ubuntu 20.04][4]
  * [How to install MariaDB 10.7 on Ubuntu 21.04][5]

## Installing Python Django on Ubuntu 20.04 {.wp-block-heading}

### 1. Run system updates  {.wp-block-heading}

The first thing to always do while installing any program is for you to update and do an upgrade of your system in order to make all the repositories up to date. This will ensure you eliminate running into unwarranted errors during your installation. So you can begin by using the following command;

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

When updates and upgrades are complete then begin by installing Python 3 into your system.

### 2. Install Python 3 on Ubuntu 20.04 {.wp-block-heading}

Django 2 doesn’t support Python 2 anymore that is why we need to install python 3 in our system. So let’s install the version of Python we require with the following command;

<pre class="wp-block-code"><code>$ sudo apt install python3</code></pre>

After the installation is over check the version of installed Python with the following command;

<pre class="wp-block-code"><code>$ Python3 --version
Python 3.8.10</code></pre>

### 3. Installing Django virtual environment  {.wp-block-heading}

We are going to create a virtual environment for our installation. 

It is important to create a virtual environment because each project you create often comes with its installed libraries, so it is important to isolate each so that dependencies so that it would not clash at any time. To install a virtual environment run the following command;

<pre class="wp-block-code"><code># create a directory
$ sudo mkdir nextgentips
$ cd nextgentips</code></pre>

Now we can create a virtual environment. I will call it nextgen.

<pre class="wp-block-code"><code>$ python3 -m venv nextgen</code></pre>

If you are running into this errors ensure you installed ensurepip first before coming here.

<pre class="wp-block-code"><code># output
The virtual environment was not created successfully because ensurepip is not
available.  On Debian/Ubuntu systems, you need to install the python3-venv
package using the following command.

    apt install python3.8-venv

You may need to use sudo with that command.  After installing the python3-venv
package, recreate your virtual environment.

Failing command: &#91;'/root/nextgentips/nextgen/bin/python3', '-Im', 'ensurepip', '--upgrade', '--default-pip']</code></pre>

To install ensurepip run this command on your terminal.

<pre class="wp-block-code"><code>$ sudo apt install python3.8-venv -y</code></pre>

After installation is over, move ahead and run **python3 -m venv nexgen** again.

To ensure that the virtual environment was created successfully, activate the environment first using the following command;

<pre class="wp-block-code"><code>source nextgen/bin/activate</code></pre>

You will see the following output, which shows that you have successfully installed and activated your virtual environment.

<pre class="wp-block-code"><code>(nextgen) root@ubuntu:~/nextgentips# </code></pre>

Now it is time to install Django.

### 4. Install Django on Ubuntu 20.04 {.wp-block-heading}

Having completed all the prerequisites we can now install Django using pip.

I am going to show the effective way to install Django. We will be creating a requirements.txt file that will have the Django version to install, psycopg2 for the ****PostgreSQL database. Everything you need to install with Django must be written inside the requiremts.txt file.

First, let&#8217;s ensure we are using the latest version of pip. use the following command to upgrade pip.

<pre class="wp-block-code"><code>$ python -m pip install upgrade pip</code></pre>

You can check the version of pip 

<pre class="wp-block-code"><code>$ pip --version
pip 20.0.2 from /root/nextgentips/nextgen/lib/python3.8/site-packages/pip (python 3.8)</code></pre>

Let&#8217;s now create the requirements.txt file using your preferred editor.

<pre class="wp-block-code"><code>$ touch requirements.txt</code></pre>

Add the following inside requirements.txt file using either vi or nano

<pre class="wp-block-code"><code># add django
djanog~=4.0.1</code></pre>

Lastly, run the following command to install Django.

<pre class="wp-block-code"><code>$ pip install -r requirements.txt</code></pre>

You will get the following output.

<pre class="wp-block-code"><code>#output
Collecting django~=4.0.1
  Using cached Django-4.0.1-py3-none-any.whl (8.0 MB)
Collecting asgiref&lt;4,>=3.4.1
  Downloading asgiref-3.4.1-py3-none-any.whl (25 kB)
Collecting backports.zoneinfo; python_version &lt; "3.9"
  Downloading backports.zoneinfo-0.2.1-cp38-cp38-manylinux1_x86_64.whl (74 kB)
     |████████████████████████████████| 74 kB 3.7 MB/s 
Collecting sqlparse>=0.2.2
  Downloading sqlparse-0.4.2-py3-none-any.whl (42 kB)
     |████████████████████████████████| 42 kB 2.4 MB/s 
Installing collected packages: asgiref, backports.zoneinfo, sqlparse, django
Successfully installed asgiref-3.4.1 backports.zoneinfo-0.2.1 django-4.0.1 sqlparse-0.4.2</code></pre>

That&#8217;s all with the installation with Django.

### 5. Starting a Django project. {.wp-block-heading}

Let&#8217;s have a look at one example of how to start a Django project on your virtual environment using the following command.

<pre class="wp-block-code"><code>$ django-admin startproject nextgentips .</code></pre>

The period at the end is very important as it tells Django that the project is created at the current directory.

There are some settings you need to change before moving forward. On **nextgentips/settings.py** make the following changes.

NB: The best way to edit all these commands is by using a text editor such as VScode, Pycharm, etc.

<pre class="wp-block-code"><code>$ cd nextgentips
$ ls
__init__.py  asgi.py  settings.py  urls.py  wsgi.py</code></pre>

Modify settings.py and add the following 

<pre class="wp-block-code"><code>$ nano settings.py</code></pre>

<pre class="wp-block-code"><code># Timezone 
TIME_ZONE = "Africa/Nairobi"
STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'</code></pre>

Save and exit then run migrations to validate and update database.

<pre class="wp-block-code"><code>Python manage.py migrate</code></pre>

NB: Make sure you are running the above command inside a folder that has **manage.py**

You will get the following output.

<pre class="wp-block-code"><code>Output
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK</code></pre>

To run the server use the following command.

<pre class="wp-block-code"><code>$ python manage.py runserver</code></pre>

If you get the following output then you are good to continue.

<pre class="wp-block-code"><code>#output
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
January 22, 2022 - 14:58:05
Django version 4.0.1, using settings 'nextgentips.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.</code></pre>

Go ahead and open it on your browser

<pre class="wp-block-code"><code>$ http://127.0.0.1:8000/</code></pre>

## Conclusion {.wp-block-heading}

Congratulations you have installed and set up the Django environment. Happy coding!

 [1]: https://www.psycopg.org/
 [2]: https://docs.djangoproject.com/en/3.2/ref/databases/#mysql-db-api-drivers
 [3]: https://nextgentips.com/2021/11/04/how-to-install-python-django-in-ubuntu-21-10/
 [4]: https://nextgentips.com/2021/10/09/how-to-install-postgresql-on-ubuntu-20-04/
 [5]: https://nextgentips.com/2021/10/15/how-to-install-mariadb-10-7-on-ubuntu-21-04/