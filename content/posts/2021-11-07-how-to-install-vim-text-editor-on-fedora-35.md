---
title: How to install Vim Text Editor on Fedora 35
author: Kipkoech Sang
type: post
date: 2021-11-07T13:55:36+00:00
url: /2021/11/07/how-to-install-vim-text-editor-on-fedora-35/
mtm_data:
  - 'a:1:{i:0;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 108
rank_math_internal_links_processed:
  - 1
categories:
  - Programming

---
In this guide we are going to learn how to install vim text editor on fedora 35.

Vim text editor is a free and open source screen based text editor program for Unix. It is highly configurable that make it easy to use and adapt to different programming languages. It also integrates well with other tools. It supports extensive plugin systems. 

Vim supports extensive community, that is why vim keep evolving. The recent updates for vim includes:

  * Introduction of [vimcrypt2][1] extension which will encrypt/decrypt files when written and read by vim. You enter password once for the same buffer and you can restore cursor on writing.
  * Introduction of GitBlame plugin which provides minimal command set for working with git 
  * Introduction of todo list, it provides syntax coloring for simple todo files.
  * Introduction of tabpages which can load multiple files in tabs upon startup. 

## Prerequisites {.wp-block-heading}

  * Fedora 35 workstation
  * Access to internet 
  * Basic familiarity to command line 



## Installing Vim (vi) on Fedora  {.wp-block-heading}

First we need to run **sudo dnf update -y** to update our repository. 

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

On fedora vi-minimal come preinstalled, this vi comes without syntax highlighting. For us to get enhanced features such as syntax highlighting, interpreters for all languages, we need to use vim-enhanced. To check the information about vim-enhanced we can run the following command on our terminal.

<pre class="wp-block-code"><code>$ sudo dnf info vim-enhanced </code></pre>

Sample Output.

<pre class="wp-block-code"><code># sudo dnf info vim-enhanced 
DigitalOcean Droplet Agent                                                        65 kB/s | 3.3 kB     00:00    
Available Packages
Name         : vim-enhanced
Epoch        : 2
Version      : 8.2.3512
Release      : 1.fc34
Architecture : x86_64
Size         : 1.8 M
Source       : vim-8.2.3512-1.fc34.src.rpm
Repository   : updates
Summary      : A version of the VIM editor which includes recent enhancements
URL          : http://www.vim.org/
License      : Vim and MIT
Description  : VIM (VIsual editor iMproved) is an updated and improved version of the
             : vi editor.  Vi was the first real screen-based editor for UNIX, and is
             : still very popular.  VIM improves on vi by adding new features:
             : multiple windows, multi-level undo, block highlighting and more.  The
             : vim-enhanced package contains a version of VIM with extra, recently
             : introduced features like Python and Perl interpreters.
             : 
             : Install the vim-enhanced package if you'd like to use a version of the
             : VIM editor which includes recently added enhancements like
             : interpreters for the Python and Perl scripting languages.  You'll also
             : need to install the vim-common package.
</code></pre>

Now we need to run the installer for vim-enhanced in our terminal. We install vim improved with the following command which will also install all the vim improve dependencies.

<pre class="wp-block-code"><code>$ sudo dnf install vim-enhanced </code></pre>

Press yes and hit enter to allow the installation to continue.

Sample output

<pre class="wp-block-code"><code># sudo dnf install vim-enhanced 
DigitalOcean Droplet Agent                                                        73 kB/s | 3.3 kB     00:00    
Dependencies resolved.
=================================================================================================================
 Package                      Architecture         Version                           Repository             Size
=================================================================================================================
Installing:
 vim-enhanced                 x86_64               2:8.2.3512-1.fc34                 updates               1.8 M
Installing dependencies:
 gpm-libs                     x86_64               1.20.7-26.fc34                    fedora                 20 k
 libsodium                    x86_64               1.0.18-7.fc34                     fedora                165 k
 vim-common                   x86_64               2:8.2.3512-1.fc34                 updates               6.7 M
 vim-filesystem               noarch               2:8.2.3512-1.fc34                 updates                22 k

Transaction Summary
=================================================================================================================
Install  5 Packages

Total download size: 8.7 M
Installed size: 35 M
Is this ok &#91;y/N]: y
Downloading Packages:
(1/5): vim-common-8.2.3512-1.fc34.x86_64.rpm                                      23 MB/s | 6.7 MB     00:00    
(2/5): vim-enhanced-8.2.3512-1.fc34.x86_64.rpm                                    54 MB/s | 1.8 MB     00:00    
(3/5): vim-filesystem-8.2.3512-1.fc34.noarch.rpm                                 3.5 MB/s |  22 kB     00:00    
(4/5): libsodium-1.0.18-7.fc34.x86_64.rpm                                        406 kB/s | 165 kB     00:00    
(5/5): gpm-libs-1.20.7-26.fc34.x86_64.rpm                                         48 kB/s |  20 kB     00:00    
-----------------------------------------------------------------------------------------------------------------
Total                                                                            6.5 MB/s | 8.7 MB     00:01     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                         1/1 
  Installing       : vim-filesystem-2:8.2.3512-1.fc34.noarch                                                 1/5 
  Installing       : vim-common-2:8.2.3512-1.fc34.x86_64                                                     2/5 
  Installing       : libsodium-1.0.18-7.fc34.x86_64                                                          3/5 
  Installing       : gpm-libs-1.20.7-26.fc34.x86_64                                                          4/5 
  Installing       : vim-enhanced-2:8.2.3512-1.fc34.x86_64                                                   5/5 
  Running scriptlet: vim-enhanced-2:8.2.3512-1.fc34.x86_64                                                   5/5 
  Verifying        : gpm-libs-1.20.7-26.fc34.x86_64                                                          1/5 
  Verifying        : libsodium-1.0.18-7.fc34.x86_64                                                          2/5 
  Verifying        : vim-common-2:8.2.3512-1.fc34.x86_64                                                     3/5 
  Verifying        : vim-enhanced-2:8.2.3512-1.fc34.x86_64                                                   4/5 
  Verifying        : vim-filesystem-2:8.2.3512-1.fc34.noarch                                                 5/5 

Installed:
  gpm-libs-1.20.7-26.fc34.x86_64                           libsodium-1.0.18-7.fc34.x86_64                        
  vim-common-2:8.2.3512-1.fc34.x86_64                      vim-enhanced-2:8.2.3512-1.fc34.x86_64                 
  vim-filesystem-2:8.2.3512-1.fc34.noarch                 

Complete!</code></pre>

The above installation will install command line vim only. To install gui-based vim we can use the following command:

<pre class="wp-block-code"><code>$ sudo dnf install vim-X11</code></pre>

This will install gVim which has gui features like +clipboard. To start gVim we need to run **vimx** on our terminal.

If you are starting out on vim it is best you try vimtutor first to give you tutorial guide on how to proceed with vim. Launch tutorial with the following command.

<pre class="wp-block-code"><code>$ vimtutor</code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="583" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-07-16-30-13-1024x737.png?resize=810%2C583&#038;ssl=1" alt="Vim vimtutor" class="wp-image-538" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-07-16-30-13.png?resize=1024%2C737&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-07-16-30-13.png?resize=300%2C216&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-07-16-30-13.png?resize=768%2C553&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-07-16-30-13.png?resize=120%2C85&ssl=1 120w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/11/Screenshot-from-2021-11-07-16-30-13.png?w=1054&ssl=1 1054w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Vim vimtutor </figcaption></figure> 

In order to exit from the vim terminal type **<shift:q and enter>**

## Conclusion {.wp-block-heading}

We have learnt how to install vim on our Fedora 35 workstation. Learn more using vimtutor to guide you all through. Incase of a problem please comment below we will get back to you in no time.

 [1]: https://www.vim.org/scripts/script.php?script_id=5985