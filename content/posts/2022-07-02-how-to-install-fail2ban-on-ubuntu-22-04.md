---
title: How to install Fail2ban on Ubuntu 22.04
author: Kipkoech Sang
type: post
date: 2022-07-02T08:22:59+00:00
url: /2022/07/02/how-to-install-fail2ban-on-ubuntu-22-04/
categories:
  - Uncategorized

---
Fail2ban is an intrusion prevention software that protects computer servers from brute-force attacks. It can run on <a href="https://en.wikipedia.org/wiki/POSIX" target="_blank" rel="noreferrer noopener">POSIX </a>systems that have an interface to a packet-control system or has a firewall installed locally.

In this tutorial, we will learn how to install Fail2ban on Ubuntu 22.04.

How Fail2ban works is that it scans files such as /var/log/auth.log and bans IP addresses conducting too many failed login attempts. It updates firewall system rules and blocks out all those IPs trying to log in with the wrong credentials.

### Install Fail2ban on Ubuntu 22.04 {.wp-block-heading}

Fail2ban most probably comes preinstalled on your server, but if not there you can install using the following steps.

### 1. Update system repositories. {.wp-block-heading}

To make our systems repositories up to date, use the following command.

<pre class="wp-block-code"><code>$ sudo apt update && upgrade -y</code></pre>

### 2. Install Fail2ban {.wp-block-heading}

After the updates are complete, we can go ahead and install fail2ban. Use the following command to do so.

<pre class="wp-block-code"><code>sudo apt install fail2ban</code></pre>

You should be in a position to see the following from your terminal

<pre class="wp-block-code"><code>#output
The following additional packages will be installed:
  &lt;strong>python3-pyinotify whois&lt;/strong>
Suggested packages:
  &lt;strong>mailx monit sqlite3 python-pyinotify-doc&lt;/strong>
The following NEW packages will be installed:
  &lt;strong>fail2ban python3-pyinotify whois&lt;/strong>
0 upgraded, 3 newly installed, 0 to remove and 182 not upgraded.
Need to get 473 kB of archives.
After this operation, 2,486 kB of additional disk space will be used.
Do you want to continue? &#91;Y/n] y</code></pre>

### 3. Configuring Fail2ban  {.wp-block-heading}

To configure Fail2ban, we can edit two files, `<mark style="background-color:#abb8c3" class="has-inline-color">fail2ban.local</mark>` and `<mark style="background-color:#abb8c3" class="has-inline-color">jail.local</mark>` files.

Fail2ban configurations reside in `fail2ban.conf` file and you are not advised to modify them. What you can do is to copy the contents of fail2ban.conf file and create another file called `fail2ban.local` file which you can then edit. to do so use the following command.

<pre class="wp-block-code"><code>cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local</code></pre>

Now we need to open fail2ban.local file on a text editor.

<pre class="wp-block-code"><code>sudo nano /etc/fail2ban/fail2ban.local</code></pre>

Here you don&#8217;t have to change anything for now but whenever you need to make any change, always do it at **.local** file 

**Jail.local** file have configuration options such as, `<mark style="background-color:#abb8c3" class="has-inline-color">ignoreip</mark>` where you can set a range of IPs to be ignored.

`<mark style="background-color:#abb8c3" class="has-inline-color">Bantime</mark>` use to set the amount of time an IP address is banned from accessing the server. It is 600 seconds.

`<mark style="background-color:#abb8c3" class="has-inline-color">Maxretry</mark>` use to set the number of failures before an IP address is banned. It is 5 trials.

`<mark style="background-color:#abb8c3" class="has-inline-color">Findtime</mark>` use to set the time in which the host should not use up the **`maxretry`** number in order not to get banned (generically set to **`10`** minutes).

`<mark style="background-color:#abb8c3" class="has-inline-color">Backend</mark>` allows you to set the backend configurations for file modifications. The default is always **auto**.

So to create a `<mark style="background-color:#abb8c3" class="has-inline-color">jail.local</mark>` file use the following command.

<pre class="wp-block-code"><code>cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local</code></pre>

Now you can open in your favorite text editor with the following command.

<pre class="wp-block-code"><code>sudo nano jail.local</code></pre>

After doing your configurations remember to restart the fail2ban service. For me I haven&#8217;t started the service, so let&#8217;s start fail2ban service.

<pre class="wp-block-code"><code>$ sudo systemctl start fail2ban
$ sudo systemctl status fail2ban</code></pre>

<pre class="wp-block-code"><code>● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/lib/systemd/system/fail2ban.service; disabled; vendor pre>
     Active: active (running) since Sat 2022-07-02 11:16:49 EAT; 9s ago
       Docs: man:fail2ban(1)
   Main PID: 27843 (fail2ban-server)
      Tasks: 5 (limit: 9355)
     Memory: 11.9M
        CPU: 123ms
     CGroup: /system.slice/fail2ban.service
             └─27843 /usr/bin/python3 /usr/bin/fail2ban-server -xf start

Jul 02 11:16:49 zx-pc systemd&#91;1]: Started Fail2Ban Service.
Jul 02 11:16:50 zx-pc fail2ban-server&#91;27843]: Server ready</code></pre>

Fail2ban is now running, you can now make your desired configurations.

To check the version of installed Fail2ban use the following command.

<pre class="wp-block-code"><code>fail2ban-client version
0.11.2</code></pre>

## Conclusion {.wp-block-heading}

Congratulations you have successfully installed Fail2ban. For more information consult <a href="https://www.fail2ban.org/wiki/index.php/Main_Page" target="_blank" rel="noreferrer noopener">Fail2ban documentation</a>.