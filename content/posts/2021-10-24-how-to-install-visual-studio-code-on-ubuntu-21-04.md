---
title: How to install Visual Studio Code on Ubuntu 21.04
author: Kipkoech Sang
type: post
date: 2021-10-24T16:15:00+00:00
url: /2021/10/24/how-to-install-visual-studio-code-on-ubuntu-21-04/
ocean_gallery_link_images:
  - off
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
ocean_link_format_target:
  - self
ocean_quote_format_link:
  - post
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 127
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this article we are going to learn how to install Visual Studio code also know as (VScode) on Ubuntu 21.04. Visual Studio Code is a lightweight source code editor which runs on desktops and is available to all operating systems out there. It comes with built in Javascript, Node.js and Typescript. One can do programming for almost languages with ease with Visual Studio Code. The languages supported are like, Go, PHP, C++, C#, Java, Python and also .NET. 

## Why do we like Visual Studio Code? {.wp-block-heading}

The reason VS code is such a nice tool is because of the following reasons:

  * It is available on all operating systems so it does not limit one on where to run the VS code, you can easily hit the ground running.
  * Vscode has a rich build-in developer tooling for example IntelliSense code completion and debugging which become handy when you don not know the complete code snippet. IT acts a guide and also lessen time to code.
  * VScode can be customized to suit your needs. You can customize every feature the way you wish and also you can add third party extensions easily.
  * VScode is an open source project. So it means you can contribute to the project development, also it means that you have a bigger community where you can ask questions whenever you are faced with problem. 
  * VScode is built for the web. It includes great tools for web development such React JSx, JSON, SCSS,CSS, HTML and Less.

## Install Visual Studio code {.wp-block-heading}

VScode can be installed either from the snap store or downloaded directly from source.

### Install Visual Studio Code via Snap. {.wp-block-heading}

Snap packages can be installed directly from either the command line or through Ubuntu software repository.

Let us first update our system repository with the following command:

<pre class="wp-block-code"><code>$ sudo apt update
$ sudo apt upgrade -y</code></pre>

After the update and upgrade is complete, we can install VScode classic into our system.

<pre class="wp-block-code"><code>$ sudo snap install --classic code </code></pre>

<pre class="wp-block-code"><code>Output 
# sudo snap install --classic code 
code 6cba118a from Visual Studio Code (vscode✓) installed</code></pre>

We can start Visual Studio code with the following command:

<pre class="wp-block-code"><code>$ code </code></pre><figure class="wp-block-image size-large">

<img decoding="async" loading="lazy" width="810" height="589" src="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-24-18-26-04-1024x744.png?resize=810%2C589&#038;ssl=1" alt="" class="wp-image-377" srcset="https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-24-18-26-04.png?resize=1024%2C744&ssl=1 1024w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-24-18-26-04.png?resize=300%2C218&ssl=1 300w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-24-18-26-04.png?resize=768%2C558&ssl=1 768w, https://i0.wp.com/nextgentips.com/wp-content/uploads/2021/10/Screenshot-from-2021-10-24-18-26-04.png?w=1044&ssl=1 1044w" sizes="(max-width: 810px) 100vw, 810px" data-recalc-dims="1" /> <figcaption>Visual Studio Code image </figcaption></figure> 

You will be greeted with the following:

## Uninstall Visual Studio code  {.wp-block-heading}

Now that we do not need Vscode, we can remove with the following command:

<pre class="wp-block-code"><code>$ sudo snap remove code </code></pre>

<pre class="wp-block-code"><code>Output
# sudo snap remove code 
code removed</code></pre>

## 2. Installing Visual Studio code via wget command  {.wp-block-heading}

This installation procedure require some dependencies to work. We are going to update our repository first

<pre class="wp-block-code"><code>$ sudo apt update</code></pre>

After the update is complete then we can install the dependencies first.

<pre class="wp-block-code"><code>$ sudo apt install software-properties-common apt-transport-https wget</code></pre>

<pre class="wp-block-code"><code>Output
# sudo apt install software-properties-common apt-transport-https wget
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
software-properties-common is already the newest version (0.99.10).
software-properties-common set to manually installed.
wget is already the newest version (1.21-1ubuntu3).
wget set to manually installed.
apt-transport-https is already the newest version (2.2.4ubuntu0.1).
The following package was automatically installed and is no longer required:
  net-tools
Use 'sudo apt autoremove' to remove it.
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</code></pre>

Let Import the Microsoft GPG key with the following command:

<pre class="wp-block-code"><code>$ wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -</code></pre>

<pre class="wp-block-code"><code>Output
# wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
OK</code></pre>

Enable Visual Studio code repository with the following:

<pre class="wp-block-code"><code>$ sudo add-apt-repository "deb &#91;arch=amd64] https://packages.microsoft.com/repos/vscode stable main"</code></pre>

<pre class="wp-block-code"><code>Output
Repository: 'deb &#91;arch=amd64] https://packages.microsoft.com/repos/vscode stable main'
Description:
Archive for codename: stable components: main
More info: https://packages.microsoft.com/repos/vscode
Adding repository.
Press &#91;ENTER] to continue or Ctrl-c to cancel.
Adding deb entry to /etc/apt/sources.list.d/archive_uri-https_packages_microsoft_com_repos_vscode-hirsute.list
Adding disabled deb-src entry to /etc/apt/sources.list.d/archive_uri-https_packages_microsoft_com_repos_vscode-hirsute.list
Hit:1 http://mirrors.digitalocean.com/ubuntu hirsute InRelease
Hit:2 http://mirrors.digitalocean.com/ubuntu hirsute-updates InRelease                                          
Hit:3 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease                                   
Hit:4 http://mirrors.digitalocean.com/ubuntu hirsute-backports InRelease                                        
Get:5 https://packages.microsoft.com/repos/vscode stable InRelease &#91;3959 B]                          
Get:6 http://security.ubuntu.com/ubuntu hirsute-security InRelease &#91;110 kB]    
Get:7 https://packages.microsoft.com/repos/vscode stable/main amd64 Packages &#91;266 kB]
Fetched 380 kB in 1s (511 kB/s)   
Reading package lists... Done</code></pre>

## Install Visual Studio code  {.wp-block-heading}

We can now install VScode with the following command:

<pre class="wp-block-code"><code>$ sudo apt install code </code></pre>

To start VScode on the terminal just type **code**

## Conclusion {.wp-block-heading}

We have learn how to install Visual Studio Code via [snapcraft][1] and also via the terminal. Take the easy route to install VScode which is via snap. What is important is what is in that code editor. Happy coding! You can learn more on the [documentation page][2]

 [1]: https://snapcraft.io/
 [2]: https://code.visualstudio.com/docs