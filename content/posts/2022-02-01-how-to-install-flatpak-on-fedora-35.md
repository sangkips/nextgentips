---
title: How to install Flatpak on Fedora 35
author: Kipkoech Sang
type: post
date: 2022-02-01T09:32:01+00:00
url: /2022/02/01/how-to-install-flatpak-on-fedora-35/
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 12
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to learn how to install Flatpak on Fedora 35.

Flatpak is a utility for software deployment and package management for Linux. Flatpak offers a sandbox environment in which users can run application software in isolation from the rest of the system.

Flatpak can be used by all kinds of desktop environments and aims to be agnostic as possible regarding how applications are built.

Flatpak runtimes and applications are built as <a href="https://github.com/opencontainers/image-spec/blob/main/spec.md" target="_blank" rel="noreferrer noopener" title="OCI images">OCI images</a> and are distributed with Fedora registry 

Flatpaks are a new way of deploying applications.

**Advantages of using Flatpaks on Fedora** 

  * Applications can easily be updated without rebooting the system
  * Applications can easily be installed on Fedora silverblue
  * Flatpaks works along all supported Fedora versions 
  * Flatpaks can be run by all users running on other distributions.

## Prerequisites {#prerequisites.wp-block-heading}

  * Have Fedora 35 workstation up and running
  * User with sudo privileges
  * Have basic knowlege of terminal commands

Related Articles 

  * <a href="https://nextgentips.com/2021/11/22/how-to-install-and-configure-flatpak-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install and configure Flatpak on Ubuntu 20.04</a>

## Install Flatpak on Fedora 35 {#install-flatpak-on-fedora-35.wp-block-heading}

### 1. Install updates  {#1-install-updates.wp-block-heading}

The first thing for any system is to update its repositories in order to make them up to date.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

### 2. Install Flatpak dependencies  {#2-install-flatpak-dependencies.wp-block-heading}

Flatpak needs some dependencies before you can run the installation. To install those dependencies, use the following command.

<pre class="wp-block-code"><code>$ sudo dnf install fedmod</code></pre>

We will the following sample output

<pre class="wp-block-code"><code># Sample output
  Installed:
  fedmod-0.6.4-2.fc35.noarch                                   
  gobject-introspection-1.70.0-1.fc35.x86_64                      
  libmodulemd1-1.8.16-6.fc35.x86_64                            
  libxslt-1.1.34-6.fc35.x86_64                                    
  python3-aiodns-3.0.0-2.fc35.noarch                           
  python3-aiohttp-3.7.4-3.fc35.x86_64                             
  python3-async-timeout-3.0.1-14.fc35.noarch                   
  python3-chardet-4.0.0-4.fc35.noarch                             
  python3-click-8.0.1-3.fc35.noarch                            
  python3-click-completion-0.5.2-6.fc35.noarch                    
  python3-decorator-5.0.9-3.fc35.noarch                        
  python3-gobject-base-3.42.0-1.fc35.x86_64                       
  python3-gssapi-1.6.14-2.fc35.x86_64                          
  python3-koji-1.27.1-1.fc35.noarch                               
  python3-libmodulemd1-1.8.16-6.fc35.x86_64                    
  python3-lxml-4.6.5-1.fc35.x86_64                                
  python3-multidict-5.1.0-4.fc35.x86_64                        
  python3-pycares-4.0.0-5.fc35.x86_64                             
  python3-requests-gssapi-1.2.3-3.fc35.noarch                  
  python3-requests-toolbelt-0.9.1-15.fc35.noarch                  
  python3-shellingham-1.3.2-4.fc35.noarch                      
  python3-smartcols-0.3.0-14.fc35.x86_64                          
  python3-solv-0.7.19-3.fc35.x86_64                            
  python3-typing-extensions-3.7.4.3-4.fc35.noarch                 
  python3-yarl-1.6.3-5.fc35.x86_64                       </code></pre>

### 3. Install Flatpak on Fedora 35 {#3-install-flatpak-on-fedora-35.wp-block-heading}

Now we can install Flatpak 

<pre class="wp-block-code"><code>$ sudo dnf install flatpak-module-tools</code></pre>

Sample output will look like this 

<pre class="wp-block-code"><code>#
Installed:
  ModemManager-glib-1.18.2-1.fc35.x86_64                        
  abattis-cantarell-fonts-0.301-5.fc35.noarch                   
  adobe-source-code-pro-fonts-2.030.1.050-11.fc35.noarch        
  alsa-lib-1.2.6.1-3.fc35.x86_64                                
  avahi-glib-0.8-14.fc35.x86_64                                 
  avahi-libs-0.8-14.fc35.x86_64                                 
  bluez-libs-5.63-1.fc35.x86_64                                 
  bubblewrap-0.5.0-1.fc35.x86_64                                
  cairo-1.17.4-4.fc35.x86_64                                    
  cairo-gobject-1.17.4-4.fc35.x86_64                            
  dconf-0.40.0-5.fc35.x86_64                                    
  fdk-aac-free-2.0.0-7.fc35.x86_64                              
  flac-libs-1.3.3-9.fc35.x86_64                                 
  flatpak-1.12.4-1.fc35.x86_64                                  
  flatpak-selinux-1.12.4-1.fc35.noarch                          
  flatpak-session-helper-1.12.4-1.fc35.x86_64                   
  fontconfig-2.13.94-3.fc35.x86_64                              
  fribidi-1.0.10-5.fc35.x86_64   </code></pre>

Check the version of installed Flatpak 

<pre class="wp-block-code"><code>$ flatpak --version</code></pre>

### 4. Add user to mock group {#3-add-user-to-mock-group.wp-block-heading}

**Mock** is a tool for a reproducible build of RPM packages. It is used by the Fedora build system to populate a chroot environment which is then used in building a source RPM.

To add the user into mock group use the following command 

<pre class="wp-block-code"><code>$ sudo usermod -a -G mock $USER</code></pre>

Restart your system for the changes to take effect.

### 5. Install Flatpak runtime environment  {#5-install-flatpak-runtime-environment.wp-block-heading}

Flatpak runs as a container in the Fedora platform, therefore we are supposed to enable remote testing on our system.

**Add remote testing** 

<pre class="wp-block-code"><code>$ flatpak remote-add fedora-testing oci+https://registry.fedoraproject.org#testing</code></pre>

**Enable remote testing** 

<pre class="wp-block-code"><code>$ flatpak remote-modify --enable fedora-testing</code></pre>

**Test Flatpak** 

To know if it&#8217;s working try the following command.

<pre class="wp-block-code"><code>$ flatpak install fedora-testing org.fedoraproject.Platform/x86_64/f35</code></pre>

<pre class="wp-block-code"><code>Looking for matches…
Info: org.fedoraproject.Platform//f34 is end-of-life, with reason:
   Fedora 34 runtime is no longer supported.


        ID                           Branch Op   Remote         Download
        ID                           Branch Op   Remote         Download
 1. &#91;✓] org.fedoraproject.Platform   f35    i    fedora-testing 672.8 MB / 672.8 MB

Installing… ████████████████████ 100%  112.1 MB/s  00:00</code></pre>

### 6. Enable Flathub {#6-enable-flathub.wp-block-heading}

Flathub is an app repository in Flatpak. It&#8217;s like Dockerhub for docker images. So to install any app you want to use run the following command.

<pre class="wp-block-code"><code>$ sudo wget https://flathub.org/repo/flathub.flatpakrepo</code></pre>

For some reason, the following command can work alone on GNOME and KDE Fedora installation. If it fails for some reason add Fathub remote manually with the following command.

<pre class="wp-block-code"><code>$ flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo</code></pre>

### Install app with Flathub {#install-app-with-flathub.wp-block-heading}

Let&#8217;s look at an example of how to install an application using Flathub. Let&#8217;s install steam 

<pre class="wp-block-code"><code>$ flatpak install flathub com.valvesoftware.Steam</code></pre>

You will see the following 

<pre class="wp-block-code"><code>Looking for matches…
Required runtime for com.valvesoftware.Steam/x86_64/stable (runtime/org.freedesktop.Platform/x86_64/21.08) found in remote flathub
Do you want to install it? &#91;Y/n]: y

com.valvesoftware.Steam permissions:
    ipc                      network                  pulseaudio                     wayland           x11
    devices                  bluetooth                devel                          multiarch         per-app-dev-shm
    file access &#91;1]          dbus access &#91;2]          system dbus access &#91;3]         tags &#91;4]

    &#91;1] xdg-music:ro, xdg-pictures:ro, xdg-run/app/com.discordapp.Discord:create
    &#91;2] org.freedesktop.Notifications, org.freedesktop.PowerManagement, org.freedesktop.ScreenSaver, org.gnome.SettingsDaemon.MediaKeys,
        org.kde.StatusNotifierWatcher
    &#91;3] org.freedesktop.NetworkManager, org.freedesktop.UDisks2, org.freedesktop.UPower
    &#91;4] proprietary


        ID                                               Branch            Op            Remote            Download
        ID                                               Branch            Op            Remote            Download
 1. &#91;✓] org.freedesktop.Platform.Compat.i386             21.08             i             flathub            88.2 MB / 101.4 MB
 2. &#91;✓] org.freedesktop.Platform.GL.default              21.08             i             flathub           130.9 MB / 131.2 MB
 3. &#91;✓] org.freedesktop.Platform.GL32.default            21.08             i             flathub           109.0 MB / 138.5 MB
 4. &#91;✓] org.freedesktop.Platform.Locale                  21.08             i             flathub            17.7 kB / 325.8 MB
 5. &#91;✓] org.freedesktop.Platform.openh264                2.0               i             flathub             1.5 MB / 1.5 MB
 6. &#91;✓] org.freedesktop.Platform                         21.08             i             flathub           154.4 MB / 199.6 MB
 7. &#91;✓] com.valvesoftware.Steam                          stable            i             flathub            12.6 MB / 13.8 MB

Installing 7/7… ████████████████████ 100%  12.6 MB/s</code></pre>

To **run steam** do the following 

<pre class="wp-block-code"><code>$ flatpak run com.valvesoftware.Steam</code></pre>

## Conclusion {#conclusion.wp-block-heading}

We have successfully installed Flatpak on Fedora 35. To learn more about Flatpak, use its <a href="https://docs.flatpak.org/en/latest/getting-started.html" target="_blank" rel="noreferrer noopener" title="documentation">documentation</a>. I hope you enjoyed and learned something new.