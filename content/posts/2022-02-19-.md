---
title: How to install Windows Subsystem for Linux (WSL2)
author: Kipkoech Sang
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=1066
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
categories:
  - Uncategorized

---
## Prerequisites  {#prerequisites.wp-block-heading}

  * Have a windows 10 or 11 up and running 

## Enable Windows subsystem for linux  {#enable-windows-subsystem-for-linux.wp-block-heading}

Before installing Linux in Windows, we must enable windows sub system for Linux on powershell 

Use the following command 

<pre class="wp-block-code"><code>dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart</code></pre>

Restart your computer 

<pre class="wp-block-code"><code>Restart-Computer</code></pre>

## **Install Linux distribution of your choice** {#install-linux-distribution-of-your-choice.wp-block-heading}