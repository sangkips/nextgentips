---
title: How to Dockerize a Django Application
author: Kipkoech Sang
type: post
date: 2022-11-01T07:14:53+00:00
url: /2022/11/01/how-to-dockerize-a-django-application/
categories:
  - Programming

---
In this tutorial, we are going to learn how to dockerize a Django application so that it will be in a position to run on any platform. 

Docker is a set of platform-as-a-service products that uses OS-level virtualization to deliver software in packages called containers. 

Docker containers allow an application to carry all its dependencies and to take the container anywhere you want it to run reliably be it a laptop, local data center, or on the cloud. What makes Docker very useful in today&#8217;s production ecosystem is very efficient and takes away repetitiveness and mundane configuration tasks. So build, share and run it anywhere.

To dockerize a Django application we require the following.

  * PostgreSQL- PostgreSQL is a preferred database for production-ready and scaling capabilities in the future. 
  * .env file- stores all the environment secrets. Do not push this to production.
  * Dockerfile &#8211; a text document that contains all the commands used to build a docker image.
  * docker-compose.yml file &#8211; compose is a tool for defining and running multi-container Docker applications.
  * Gunicorn &#8211; a python web server gateway interface HTTP server. We need this for production purposes only.
  * Whitenoise &#8211; required in order to serve static files such as images
  * Django-environ: is the Python package that allows you to use the Twelve-factor methodology to configure your Django application with environment variables.
  * dotenv &#8211; loads environment variables from .env

So to begin with, let&#8217;s create a skeleton Django project. You can refer to this <a href="https://nextgentips.com/2022/01/22/how-to-install-python-django-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">article</a> for the creation of a Django application 

I hope you have a Django project up and running. Go to the root of your Django project and create Dockerfile and docker-compose.yml files.

## Create a Dockerfile {.wp-block-heading}

A Dockerfile must begin with FROM keyword. Here instructions are not case-sensitive but it&#8217;s good to distinguish between arguments and instructions. 

<pre class="wp-block-code"><code>FROM python:3.10-slim

ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app

RUN apt update && apt install --no-install-recommends -y \
  build-essential \
  libpq-dev

COPY requirements.txt .

RUN python3 -m pip install -r requirements.txt --no-cache-dir

COPY . .</code></pre>

## Docker-compose file {.wp-block-heading}

After creating a Dockerfile we can proceed to create `docker-compose.yml` file. Do this still while on the root of your Django project.

<pre class="wp-block-code"><code>version: "3.9"

services:
  db:
    image: postgres:14.5-alpine
    volumes:
      - .api_pg:/var/lib/postgresql/data
    env_file:
      - .env
  app:
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/usr/src/app
    ports:
      - "8000:8000"
    env_file:
      - .env
    depends_on:
      - db

volumes:
  api_pg:</code></pre>

## PostgreSQL {.wp-block-heading}

For you to run the PostgreSQL database, you need to have a Psycopg adaptor installed in your system. To install Psycopg2 in your system run `<mark style="background-color:#abb8c3" class="has-inline-color">pip install Psycopg2</mark>`.

<pre class="wp-block-code"><code>pip install Psycopg2</code></pre>

Head over to `settings.py` file, go to databases, remove dbsqlite3 and add this code.

<pre class="wp-block-code"><code>DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ&#91;"PGDATABASE"],
        'USER': os.environ&#91;"PGUSER"],
        'PASSWORD': os.environ&#91;"PGPASSWORD"],
        'HOST': os.environ&#91;"PGHOST"],
        'PORT': os.environ&#91;"PGPORT"],
    }
}</code></pre>

Still on `settings.py` file on top `import os` and add the following:

<pre class="wp-block-code"><code>import os

from dotenv import load_dotenv

load_dotenv(os.path.join(BASE_DIR, ".env"))

SECRET_KEY = os.getenv("SECRET_KEY")
DEBUG = os.getenv("DEBUG")</code></pre>

## Create .env file {.wp-block-heading}

This we use to store our environment secret variables.

<pre class="wp-block-code"><code>SECRET_KEY = &lt;your secret key&gt;
DEBUG = True in development and False in production
#database
DATABASE_URL = postgresql://${{ PGUSER }}:${{ PGPASSWORD }}@${{ PGHOST }}:${{ PGPORT }}/${{ PGDATABASE }}
PGDATABASE = postgres
PGHOST = &lt;your host&gt;
PGPASSWORD = &lt;your password&gt;
PGPORT = &lt;your port&gt;
PGUSER = &lt;db_user&gt;</code></pre>

## Build project {.wp-block-heading}

The first thing to do is to build our Docker images. To do that we use `<mark style="background-color:#abb8c3" class="has-inline-color">docker-compose -f docker-compose.yml build</mark>`.

<pre class="wp-block-code"><code>docker-compose -f docker-compose.yml build</code></pre>

And lastly to run the docker image we use `<mark style="background-color:#abb8c3" class="has-inline-color">docker-compose up</mark>` or `<mark style="background-color:#abb8c3" class="has-inline-color">docker-compose up -d</mark>` if you want to run in detached mode 

<pre class="wp-block-code"><code>docker-compose up or docker-compose up -d</code></pre>

From here you can now continue to work on your project without worrying on compatibility issues with your team

## Conclusion {.wp-block-heading}

We have successfully dockerized our Django application. We have seen what needs to be in a Dockerfile, Docker compose file, .env file, and the PostgreSQL.