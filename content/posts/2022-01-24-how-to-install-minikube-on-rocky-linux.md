---
title: How to install Minikube on Rocky Linux
author: Kipkoech Sang
type: post
date: 2022-01-24T12:39:41+00:00
url: /2022/01/24/how-to-install-minikube-on-rocky-linux/
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:13:"18.234.30.224";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 23
rank_math_internal_links_processed:
  - 1
categories:
  - Kubernetes

---
In his tutorial, we are going to learn how to install Minikube on Rocky Linux. I am going to use Docker as a containerization platform. You can either VirtualBox, Podman, KVM, etc.

[Minikube][1]&nbsp;is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your local machine. Kubernetes quickly set up a local Kubernetes cluster on Linux, Windows, and macOS.

It is always advisable before diving into Kubernetes, you will need a minimal Kubernetes setup. Such a setup should spin up fast and integrate well with other tools.

Minikube is the best fit because of the following:

  * It runs on Windows, Linux and MacOS.
  * It supports the latest Kubernetes release
  * It supports multiple container runtimes i.e Containerd, KVM, Docker, Podman etc
  * It has supports for adbvanced features such as Load balancing, Featuregates and filesystems mounts.
  * It has support for Addons. Addons being a marketplace for developers to share configurations for running services on Minikube.
  * It supports CI environments

## Prerequisites {.wp-block-heading}

To run Kubernetes effectively you need to allocate the following to Minikube on your system.

  * 2 CPUs or more
  * 2 GB of memory
  * 20 GB or more of free disk space
  * Reliable internet connections.
  * Conatiner or virtual machine manager such as Docker, KVM, Podman, Virtual Box etc.

## Related Articles {.wp-block-heading}

  * [How to install and configure Minikube on Ubuntu 21.10][2]
  * [How to install and use Minikube on Fedora 35][3]
  * [How to install Minikube on Ubuntu 20.04][4]

## Install Minikube on Rocky Linux {.wp-block-heading}

### 1. Update system repositories  {.wp-block-heading}

The best thing to do before you run any installation is to update your system repositories in order to make them up to date. This will help avoid running into any errors during the installation process. Use the following command to update your system.

<pre class="wp-block-code"><code>$ sudo dnf update -y</code></pre>

### 2. Install Docker  {.wp-block-heading}

As a requirement, we are going to use the Docker platform but you can try others like Podman, KVM, Virtualbox, etc. 

For newer systems, you need to add the Docker repository into your system. For this learning, I am running a fresh system install of Rocky Linux. So we need to add a Docker repository with the following command;

First, add the following plugins, it provides the commands to manage dnf repositories.

<pre class="wp-block-code"><code>$ sudo dnf install dnf-plugins-core -y</code></pre>

Then proceed to add the repository with the following command;

<pre class="wp-block-code"><code>$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo</code></pre>

If your addition is successful then you can proceed to install Docker Engine.

<pre class="wp-block-code"><code>$ sudo dnf -y install docker-ce docker-ce-cli containerd.io</code></pre>

Check the version of the installed docker after the installation is complete.

<pre class="wp-block-code"><code>$ docker --version
Docker version 20.10.12, build e91ed57</code></pre>

Enable and start docker 

<pre class="wp-block-code"><code># Enable docker
$ sudo systemctl enable docker
# Start docker
$sudo systemctl start docker 
# Check docker status
$ sudo systemctl status docker  </code></pre>

<pre class="wp-block-code"><code># docker status
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2022-01-24 11:14:18 UTC; 8s ago
     Docs: https://docs.docker.com
 Main PID: 73020 (dockerd)
    Tasks: 8
   Memory: 31.3M
   CGroup: /system.slice/docker.service
           └─73020 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Jan 24 11:14:18 rockylinux dockerd&#91;73020]: time="2022-01-24T11:14:18.035319428Z" level=error msg="Failed to built-in GetDriver>
Jan 24 11:14:18 rockylinux dockerd&#91;73020]: time="2022-01-24T11:14:18.068789994Z" level=warning msg="Your kernel does not suppo>
Jan 24 11:14:18 rockylinux dockerd&#91;73020]: time="2022-01-24T11:14:18.068840772Z" level=warning msg="Your kernel does not suppo>
....</code></pre>

Create a user and give permission because Minikube cant run as root.

<pre class="wp-block-code"><code>$ useradd --create-home nextgentips
$ sudo usermod -aG docker nextgentips</code></pre>

### 3. Install Kubectl on Rocky Linux {.wp-block-heading}

Lets install Kubectl with the following;

<pre class="wp-block-code"><code>$ cat &lt;&lt;EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
&#91;kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF</code></pre>

Then install with the following command:

<pre class="wp-block-code"><code>$ sudo dnf install -y kubectl</code></pre>

You will see the following sample output

<pre class="wp-block-code"><code># sample output
Running transaction
  Preparing        :                                                                                                       1/1 
  Installing       : kubectl-1.23.2-0.x86_64                                                                               1/1 
  Verifying        : kubectl-1.23.2-0.x86_64                                                                               1/1 

Installed:
  kubectl-1.23.2-0.x86_64  </code></pre>

<pre class="wp-block-code"><code>$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.2", GitCommit:"9d142434e3af351a628bffee3939e64c681afa4d", GitTreeState:"clean", BuildDate:"2022-01-19T17:35:46Z", GoVersion:"go1.17.5", Compiler:"gc", Platform:"linux/amd64"}</code></pre>

### 4. Install Minikube on Rocky Linux {.wp-block-heading}

Lets go ahead and download Minikube using curl into our system then install it.

<pre class="wp-block-code"><code>$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm</code></pre>

Then install it with the following:

<pre class="wp-block-code"><code>$ sudo rpm -Uvh minikube-latest.x86_64.rpm</code></pre>

You will see the following output.

<pre class="wp-block-code"><code># output
Verifying...                          ############################### &#91;100%]
Preparing...                          ############################### &#91;100%]
Updating / installing...
   1:minikube-1.25.1-0                ############################### &#91;100%]</code></pre>

Check the version of minikube installed 

<pre class="wp-block-code"><code>$ minikube version
minikube version: v1.25.1
commit: 3e64b11ed75e56e4898ea85f96b2e4af0301f43d</code></pre>

### 5. Minikube start {.wp-block-heading}

To start minikube, switch to the user you created earlier because Minikube doesn&#8217;t run in root account.

<pre class="wp-block-code"><code>$ sudo su nextgentips</code></pre>

Then you can check the account with this:

<pre class="wp-block-code"><code>$ whoami
nextgentips</code></pre>

If it is showing you the user you created earlier then go ahead and run **minikube start** 

<pre class="wp-block-code"><code>$ minikube start</code></pre>

You will see the following output.

<pre class="wp-block-code"><code># output
* minikube v1.25.1 on Rocky 8.5 (kvm/amd64)
* Automatically selected the docker driver

X Requested memory allocation (1817MB) is less than the recommended minimum 1900MB. Deployments may fail.


X The requested memory allocation of 1817MiB does not leave room for system overhead (total system memory: 1817MiB). You may face stability issues.
* Suggestion: Start minikube with less memory allocated: 'minikube start --memory=1817mb'

* Starting control plane node minikube in cluster minikube
* Pulling base image ...
* Downloading Kubernetes v1.23.1 preload ...
    > preloaded-images-k8s-v16-v1...: 504.42 MiB / 504.42 MiB  100.00% 283.13 M
    > gcr.io/k8s-minikube/kicbase: 378.98 MiB / 378.98 MiB  100.00% 36.23 MiB p
* Creating docker container (CPUs=2, Memory=1817MB) ...
* Preparing Kubernetes v1.23.1 on Docker 20.10.12 ...
  - kubelet.housekeeping-interval=5m
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Verifying Kubernetes components...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Enabled addons: default-storageclass, storage-provisioner
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default</code></pre>

### 6. Enable Kubernetes Dashboard {.wp-block-heading}

Kubernetes comes with a dashboard where you will manage your clusters. To enable dashboard, use the following command:

<pre class="wp-block-code"><code>$ minikube dashboard</code></pre>

Running the following command will get us the following output.

<pre class="wp-block-code"><code>* Enabling dashboard ...
  - Using image kubernetesui/dashboard:v2.3.1
  - Using image kubernetesui/metrics-scraper:v1.0.7
* Verifying dashboard health ...
* Launching proxy ...
* Verifying proxy health ...
* Opening http://127.0.0.1:41141/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
  http:&#47;&#47;127.0.0.1:41141/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/
</code></pre>

### 7. Deploy minikube application. {.wp-block-heading}

Lets look at an example on how to deploy your application.

Use the following code example to deploy and application.

<pre class="wp-block-code"><code>$ kubectl create deployment nextgen-minikube --image=k8s.gcr.io/echoserver:1.4</code></pre>

Then we need to expose the application on port 8080

<pre class="wp-block-code"><code>kubectl expose deployment nextgen-minikube --type=NodePort --port=8080</code></pre>

To run the application use the following code;

<pre class="wp-block-code"><code>$ kubectl get services nextgen-minikube</code></pre>

To access the application you need to run the following:

<pre class="wp-block-code"><code>$ minikube service nextgen-minikube</code></pre>

You will see the following output

<pre class="wp-block-code"><code>|-----------|----------------|-------------|---------------------------|
| NAMESPACE |      NAME      | TARGET PORT |            URL            |
|-----------|----------------|-------------|---------------------------|
| default   |nextgen-minikube|      8080 | http://192.168.49.2:31148 |
|-----------|----------------|-------------|---------------------------|
* Opening service default/hello-minikube in default browser...
  http:&#47;&#47;192.168.49.2:31148</code></pre>

If you want to stop the application run the following 

<pre class="wp-block-code"><code># stop
$ minikube stop
# pause 
$ minikube pause
# delete
$ minikube delete --all</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Minikube on Rocky Linux. If you face any challenge do give us a comment and we will be happy to assist.

 [1]: https://minikube.sigs.k8s.io/docs/
 [2]: https://nextgentips.com/2021/12/24/how-to-install-and-configure-minikube-on-ubuntu-21-10/
 [3]: https://nextgentips.com/2021/12/18/how-to-install-and-configure-minikube-on-fedora-35/
 [4]: https://nextgentips.com/2022/01/23/how-to-install-minikube-on-ubuntu-20-04/