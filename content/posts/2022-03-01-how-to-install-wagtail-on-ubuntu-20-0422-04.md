---
title: How to install Wagtail on Ubuntu 20.04|22.04
author: Kipkoech Sang
type: post
date: 2022-03-01T08:43:19+00:00
url: /2022/03/01/how-to-install-wagtail-on-ubuntu-20-0422-04/
rank_math_primary_category:
  - 6
rank_math_description:
  - Wagtail is a free and open-source content management system written in Python language and build on Django web framework.
rank_math_focus_keyword:
  - install wagtail
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 2
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial we are going to install wagtail on Ubuntu 20.04. 

Wagtail is a free and open-source content management system written in Python language and build on Django web framework. **It offers intuitive and interactive user interface for developers.**

## Features of Wagtail {.wp-block-heading}

  * It offers interactive user interface for both developers and content creators.
  * It offers full control over development whereby the developer has control over frontend side of the CMS and built upon strong security backend.
  * Its excellent and rich support for features like images and embedded content 
  * Wagtail can scale depending on the structure and traffic it receives.
  * Integrated Elasticsearch and Postgresql provides powerful search around the CMS.

## Prerequisites  {.wp-block-heading}

  * Wagtail works with Python 3 so make sure you have Python 3 installed. Check out this article. <a href="https://nextgentips.com/2021/11/23/how-to-install-python-3-11-on-ubuntu-20-04/" target="_blank" rel="noreferrer noopener">How to install Python 3.11 on Ubuntu 20.04</a>
  * Make sure you are working on a virtual environment also.
  * libjpeg and zlib, libraries required for Dango&#8217;s Pillow library

## Install Wagtail on Ubuntu 20.04|22.04 {.wp-block-heading}

To install Wagtail we will follow the following steps:

### 1. Update system repositories {.wp-block-heading}

The best thing to do first is to make your system repositories up to date by running system updates with the following command.

<pre class="wp-block-code"><code>sudo apt update && apt upgrade -y</code></pre>

### 2. Install Python 3 an Pip 3 {.wp-block-heading}

First lets check if your system has Python 3 preinstalled with the following command.

<pre class="wp-block-code"><code>python3 --version
Python 3.8.10</code></pre>

If you get an output same as the one above, then you can move ahead and install pip 3, else install python 3 like this 

<pre class="wp-block-code"><code>sudo apt install python3 python3-pip</code></pre>

That will install both python 3 and pip on your system.

Get to know the pip version installed with the following command.

<pre class="wp-block-code"><code>pip --version
pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)</code></pre>

### 3. Create a virtual environment  {.wp-block-heading}

Virtual environment comes with its preinstalled libraries, so to create one use the following:

Create a directory and cd into that directory.

<pre class="wp-block-code"><code>sudo mkdir nextgen
cd nextgen</code></pre>

To create a virtual environment, ensure that you have **ensurepip installed.**

<pre class="wp-block-code"><code>sudo apt install python3.8-venv -y</code></pre>

The following sample output will be displayed.

<pre class="wp-block-code"><code>The following NEW packages will be installed:
  &lt;strong>python3.8-venv&lt;/strong>
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 5444 B of archives.
After this operation, 27.6 kB of additional disk space will be used.
Get:1 http://mirrors.digitalocean.com/ubuntu focal-updates/universe amd64 python3.8-venv amd64 3.8.10-0ubuntu1~20.04.2 &#91;5444 B]
Fetched 5444 B in 0s (79.8 kB/s)   
Selecting previously unselected package python3.8-venv.
(Reading database ... 100788 files and directories currently installed.)
Preparing to unpack .../python3.8-venv_3.8.10-0ubuntu1~20.04.2_amd64.deb ...
Unpacking python3.8-venv (3.8.10-0ubuntu1~20.04.2) ...
Setting up python3.8-venv (3.8.10-0ubuntu1~20.04.2) ...</code></pre>

Then create a virtual environment with the following command.

<pre class="wp-block-code"><code>python3 -m venv nextgen</code></pre>

After you have successfully created a virtual environment, make sure you activated it with the following command.

<pre class="wp-block-code"><code>source nextgen/bin/activate</code></pre>

### 4. Install Wagtail  {.wp-block-heading}

Lets now install wagtail using pip we have just installed.

<pre class="wp-block-code"><code>pip install wagtail</code></pre>

The following output will be dispalyed.

<pre class="wp-block-code"><code># sample output
Collecting wagtail
  Downloading wagtail-2.16.1-py3-none-any.whl (11.1 MB)
     |████████████████████████████████| 11.1 MB 16.1 MB/s 
Collecting django-modelcluster&lt;6.0,>=5.2
  Downloading django_modelcluster-5.2-py2.py3-none-any.whl (24 kB)
Collecting tablib&#91;xls,xlsx]>=0.14.0
  Downloading tablib-3.2.0-py3-none-any.whl (48 kB)
     |████████████████████████████████| 48 kB 8.2 MB/s 
Collecting telepath&lt;1,>=0.1.1
  Downloading telepath-0.2-py35-none-any.whl (10 kB)
Collecting anyascii>=0.1.5
  Downloading anyascii-0.3.0-py3-none-any.whl (284 kB)
     |████████████████████████████████| 284 kB 41.7 MB/s 
Collecting Willow&lt;1.5,>=1.4
  Downloading Willow-1.4.1-py2.py3-none-any.whl (106 kB)
     |████████████████████████████████| 106 kB 43.9 MB/s 
Collecting Django&lt;4.1,>=3.2
  Downloading Django-4.0.2-py3-none-any.whl (8.0 MB)
     |████████████████████████████████| 8.0 MB 40.4 MB/s 
Collecting django-taggit&lt;3.0,>=2.0
  Downloading django_taggit-2.1.0-py3-none-any.whl (59 kB)
     |████████████████████████████████| 59 kB 7.5 MB/s 
Collecting django-filter&lt;22,>=2.2
  Downloading django_filter-21.1-py3-none-any.whl (81 kB)
     |████████████████████████████████| 81 kB 11.9 MB/s 
Collecting Pillow&lt;10.0.0,>=4.0.0
  Downloading Pillow-9.0.1-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (4.3 MB)
     |████████████████████████████████| 4.3 MB 10.1 MB/s </code></pre>

### 5. Create a wagtail site {.wp-block-heading}

After installation is successful, we can now run our first wagtail site with the following command.

<pre class="wp-block-code"><code>wagtail start mysite</code></pre>

Sample output

<pre class="wp-block-code"><code>Creating a Wagtail project called mysite
Success! mysite has been created</code></pre>

This will create a new folder called mysite. Next we will cd into mysite and perform the following:

Install requirements.txt file first, this contains all the requirements you need to install in your virtual environment.

<pre class="wp-block-code"><code>pip install -r requirements.txt</code></pre>

After this we will do migration to validate all the components.

<pre class="wp-block-code"><code>python manage.py migrate</code></pre>

Sample output

<pre class="wp-block-code"><code>Operations to perform:
  Apply all migrations: admin, auth, contenttypes, home, sessions, taggit, wagtailadmin, wagtailcore, wagtaildocs, wagtailembeds, wagtailforms, wagtailimages, wagtailredirects, wagtailsearch, wagtailusers
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
......</code></pre>

Then create a superuser for your account.

<pre class="wp-block-code"><code>python manage.py createsuperuser</code></pre>

Sample output

<pre class="wp-block-code"><code>Username (leave blank to use 'root'): admin
Email address:   
Password: 
Password (again): 
This password is too short. It must contain at least 8 characters.
Bypass password validation and create user anyway? &#91;y/N]: y
Superuser created successfully.</code></pre>

On production make sure you create a strong password.

And lastly run your server with the following command.

<pre class="wp-block-code"><code>python manage.py runserver</code></pre>

Sample output

<pre class="wp-block-code"><code>Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
March 01, 2022 - 08:34:41
Django version 4.0.2, using settings 'mysite.settings.dev'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.</code></pre>

You can now access your server from here: **http://localhost:8000** and the admin backend is found here: **http://localhost:8000/admin/**.

## Conclusion {.wp-block-heading}

We have successfully installed and run our site using wagtail. For more information head over to <a href="https://docs.wagtail.org/en/stable/getting_started/tutorial.html" target="_blank" rel="noreferrer noopener">wagtail documentation</a>.