---
title: How to install Kapacitor on Ubuntu 20.04.
author: Kipkoech Sang
type: post
date: 2022-02-11T18:56:08+00:00
url: /2022/02/11/how-to-install-kapacitor-on-ubuntu-20-04/
rank_math_primary_category:
  - 6
rank_math_description:
  - Kapacitor is an open-source data processing framework that makes it easy to create alerts, run ETL jobs and detect anomalies.
rank_math_focus_keyword:
  - Kapacitor
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 6
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
Kapacitor is an open-source data processing framework that makes it easy to create alerts, run ETL jobs and detect anomalies. It provides real-time streaming of data. It helps reduce pressure from the InfluxDB database. In today&#8217;s topic, we will learn how to install, start, and configure Kapacitor 1.6 on Ubuntu 20.04 distribution.

## Kapacitor Capabilities  {#kapacitor-capabilities.wp-block-heading}

  * It is able to process both streaming and batch data making it possible to release preasure on the InfluxDB databases.
  * It stores transformed data back in the InfluxDB databases.
  * It integrtes easily with platforms like slack, hitchat, alerta, sensu etc.
  * Can easily query data from influxDB on a schedule.

## What Kapacitor can provide. {#what-kapacitor-can-provide.wp-block-heading}

  * It provides alerting services to the whole TICK stack.
  * It makes it by processing all ETL jobs 

## Installing Kapacitor on Ubuntu 20.04 {#installing-kapacitor-on-ubuntu-20-04.wp-block-heading}

### 1. Update system repositories  {#1-update-system-repositories.wp-block-heading}

The first thing to do on any system is to update its repositories in order to make them up to date as possible, this will ensure that errors are avoided during the installation process.

<pre class="wp-block-code"><code>$ sudo apt update && apt upgrade -y</code></pre>

### 2. Download Kapacitor  {#2-download-kapacitor.wp-block-heading}

To install Kapacitor we need to download it from its <a href="https://portal.influxdata.com/downloads/" target="_blank" rel="noreferrer noopener">InfluxDB download page</a>. Select the version you will like to download. As of this writing version 1.6 is the newest version.

<pre class="wp-block-code"><code>$ wget https://dl.influxdata.com/kapacitor/releases/kapacitor_1.6.3-1_amd64.deb</code></pre>

Sample output

<pre class="wp-block-code"><code>Resolving dl.influxdata.com (dl.influxdata.com)... 13.225.34.30, 13.225.34.34, 13.225.34.28, ...
Connecting to dl.influxdata.com (dl.influxdata.com)|13.225.34.30|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 58069962 (55M) &#91;application/x-debian-package]
Saving to: ‘kapacitor_1.6.3-1_amd64.deb’

kapacitor_1.6.3-1_a 100%&#91;===================>]  55.38M   427KB/s    in 3m 4s   

2022-02-11 21:22:55 (308 KB/s) - ‘kapacitor_1.6.3-1_amd64.deb’ saved &#91;58069962/58069962]</code></pre>

After the download is complete, then we can now install Kapacitor.

### 3. Install Kapacitor on Ubuntu 20.04 {#3-install-kapacitor-on-ubuntu-20-04.wp-block-heading}

To install the downloaded Kapacitor use the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dpkg -i kapacitor_1.6.3-1_amd64.deb</code></pre>

The following will be the sample output obtained.

<pre class="wp-block-code"><code># output 
Selecting previously unselected package kapacitor.
(Reading database ... 271046 files and directories currently installed.)
Preparing to unpack kapacitor_1.6.3-1_amd64.deb ...
Unpacking kapacitor (1.6.3-1) ...
Setting up kapacitor (1.6.3-1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/kapacitor.service → /lib/systemd/system/kapacitor.service.</code></pre>

### 4. Start service  {#4-start-service.wp-block-heading}

Before we can start using Kacapitor we need to ensure that the kapacitor service is up and running. If you can check the service with systemctl status command you will find that the service isn&#8217;t running because we haven&#8217;t started it yet.

<pre class="wp-block-code"><code>$ sudo systemctl status kapacitor </code></pre>

<pre class="wp-block-code"><code>● kapacitor.service - Time series data processing engine.
     Loaded: loaded (/lib/systemd/system/kapacitor.service; enabled; vendor pre>
     Active: inactive (dead)
       Docs: https://github.com/influxdb/kapacitor</code></pre>

From the above, you can see that it is not active because we haven&#8217;t started the service. First lets start out.

<pre class="wp-block-code"><code>$ sudo systemctl start kapacitor </code></pre>

Now let&#8217;s check the status again to see if Kapacitor service is up and running.

<pre class="wp-block-code"><code>$ sudo systemctl status kapacitor</code></pre>

<pre class="wp-block-code"><code>● kapacitor.service - Time series data processing engine.
     Loaded: loaded (/lib/systemd/system/kapacitor.service; enabled; vendor pre>
     &lt;strong>Active: active (running) since Fri 2022-02-11 21:32:41 EAT; 1min 26s ago&lt;/strong>
       Docs: https://github.com/influxdb/kapacitor
   Main PID: 7980 (kapacitord)
      Tasks: 9 (limit: 9353)
     Memory: 49.7M
     CGroup: /system.slice/kapacitor.service
             └─7980 /usr/bin/kapacitord -config /etc/kapacitor/kapacitor.conf

Feb 11 21:32:41 nextgentips-pc systemd&#91;1]: Started Time series data processing </code></pre>

Now that our service is up and running, we can check the configuration if its ok with the following command 

<pre class="wp-block-code"><code>$ kapacitord config</code></pre>

Also, we can check the installed version of Kapacitor like this:

<pre class="wp-block-code"><code>$ kapacitor version
Kapacitor OSS 1.6.3 (git: HEAD ac4381f9ce6a161c5ca052c16f4016076cf0e857)</code></pre>

### 5. Startup {#5-startup.wp-block-heading}

Before starting Kapcitor make sure you specify correctly the following options, this will ensure that the Kapacitor knows where to load and run the daemon from.

  * **config:** Path to the configuration file.
  * **hostname:** Hostname that will override the hostname specified in the configuration file.
  * **pidfile:** File where the process ID will be written.
  * **log-file:** File where logs will be written.
  * **log-level:** Threshold for writing messages to the log file. Valid values include debug, info, warn, error.

## Conclusion {#conclusion.wp-block-heading}

Glad that you have learned how to install Kapacitor on Ubuntu 20.04. I hope you enjoyed it.