---
title: How to Flush DNS cache on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-03T14:42:10+00:00
url: /2021/10/03/how-to-flush-dns-cache-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 6
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 150
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
The DNS cache is a temporary database on an operating system that stores DNS lookups. And first what is DNS? DNS is Domain Name Service responsible for resolving websites names into their respective IP addresses.

DNS lookup shows information about visited websites and internet domains. But how do DNS lookups work? When you visit a website on numerous occasions, your operating system retrieves the information for the website from the locally stored cache rather than using the slower DNS public records. This helps to fasten the load time.

There are times when a DNS cache becomes corrupted. When this happens DNS cache needs to be flushed on your operating system.

To ensure that the DNS is flushed, the operating system should be using **systemd** and the user is running Sudo **privileges**.

The first thing we should do is ensure that systemd-resolved is running. To do that open your terminal and type the following command on your server.

<pre class="wp-block-code"><code>$ sudo systemctl is-active systemd-resolved
active </code></pre>

From here we need to check the statistics for the DNS cache. Use the following command:

<pre class="wp-block-code"><code>$ sudo systemd-resolve --statistics
DNSSEC supported by current servers: no

Transactions              
Current Transactions: 0   
  Total Transactions: 4913
                          
Cache                     
  Current Cache Size: 45  
          Cache Hits: 481 
        Cache Misses: 366 
                          
DNSSEC Verdicts           
              Secure: 0   
            Insecure: 0   
               Bogus: 0   
       Indeterminate: 0 </code></pre>

Now you can see that the current cache size is 45. We need to reset that to 0 by using the following command:

<pre class="wp-block-code"><code>$ sudo systemd-resolve --flush-caches</code></pre>

Once the command has been initiated run the statistics command again to check

<pre class="wp-block-code"><code>$ sudo systemd-resolve --statistics
Transactions              
Current Transactions: 0   
  Total Transactions: 4916
                          
Cache                     
  Current Cache Size: 0   
          Cache Hits: 482 
        Cache Misses: 368 
                          
DNSSEC Verdicts           
              Secure: 0   
            Insecure: 0   
               Bogus: 0   
       Indeterminate: 0  </code></pre>

#### Conclusion {.wp-block-heading}

In this article, we have learned how to flush DNS cache from Ubuntu server 20.04. One way or another you are faced with the challenge of your website not loading properly and you can.t figure out the problem. Flush the DNS cache and everything will be fine.