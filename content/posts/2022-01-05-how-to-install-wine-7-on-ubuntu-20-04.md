---
title: How to install Wine 7 on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2022-01-05T19:04:56+00:00
url: /2022/01/05/how-to-install-wine-7-on-ubuntu-20-04/
mtm_data:
  - 'a:1:{i:0;a:3:{s:9:"reference";s:0:"";s:4:"type";s:0:"";s:5:"value";s:0:"";}}'
entry_views:
  - 1
view_ip:
  - 'a:1:{i:0;s:14:"54.242.182.186";}'
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 41
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
In this tutorial, we are going to install Wine 7 on Ubuntu 20.04.

Wine is a compatibility layer capable of running Windows applications on several&nbsp;[POSIX][1]-compliant operating systems i.e Linux, macOS, and BSD. Instead of simulating internal Windows logic like a virtual machine or emulator, Wine translates Windows API calls into POSIX calls instantly eliminating the performance and memory penalties of other methods and allowing you to integrate Windows applications in your desktop.

## What’s new in Wine 7 release {.wp-block-heading}

  * Reimplimentation of winMM joystick driver
  * All unix libraries converted to the syscall-based libraries

## Benefits of Using Wine {.wp-block-heading}

  * Unix has made it possible to write powerful scripts, Wine makes it possible to call windows applications from scripts that can also laverage Unix environment.
  * Wine makes it possible to access Windows applications remotely.
  * Wine makes it possible to use thin clients i.e install Wine and you will easily connect to windows applications using x-terminal.
  * Wine is open-source so you can easily extend to suite your needs.
  * Wine can be used to make existing windows applications available on the web by using VNC and its Java/HTML5 client.

## Related Articles {.wp-block-heading}

  * [How to install Wine 7 on Fedora 35][2]
  * [How to install and configure Wine on Ubuntu 21.10/21.04][3]

## Install Wine on Ubuntu 20.04 {.wp-block-heading}

Wine comes preinstalled with Ubuntu systems, but there is one thing to consider, for 64-bit architecture you need to enable 32 bit for Wine to run effectively.

Before running Wine installation make sure you don&#8217;t have previous Wine in your system. Uninstall before running the newer version.

## 1. Run system updates  {.wp-block-heading}

First, let’s run system updates with the following command.

<pre class="wp-block-syntaxhighlighter-code">$ sudo apt update $$ apt upgrade -y</pre>

## 2. Enable 32 bit wine architecture {.wp-block-heading}

Wine runs on a 32-bit architecture, so to get it up and running we need to enable 32-bit architecture. To do so run the following command on your terminal.

<pre class="wp-block-code"><code>$ sudo dpkg --add-architecture i386</code></pre>

## 3. Add repository key  {.wp-block-heading}

To sign our repository with key, we need to download the key from the following:

<pre class="wp-block-code"><code>$ wget -nc https://dl.winehq.org/wine-builds/winehq.key</code></pre>

Then you have to add the downloaded key. Use the following command to accomplish that.

<pre class="wp-block-code"><code>$ sudo apt-key add winehq.key</code></pre>

Ok will be the output.

## 4. Add WineHQ Repository to Ubuntu 20.04 {.wp-block-heading}

To add winehq repository to our system run the following command.

<pre class="wp-block-code"><code>$ sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'</code></pre>

If you don&#8217;t run into any errors, now then you can run system updates to update our repositories again 

<pre class="wp-block-code"><code>$ sudo apt update 
Hit:1 http://security.ubuntu.com/ubuntu focal-security InRelease
&lt;strong>Hit:2 https://dl.winehq.org/wine-builds/ubuntu focal InRelease &lt;/strong>                                                 
Hit:3 http://mirrors.digitalocean.com/ubuntu focal InRelease                                                    
Hit:4 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease         
Hit:5 http://mirrors.digitalocean.com/ubuntu focal-updates InRelease
Hit:6 http://mirrors.digitalocean.com/ubuntu focal-backports InRelease
Reading package lists... Done
Building dependency tree       
Reading state information... Done
All packages are up to date.</code></pre>

## 5. Install Wine on Ubuntu 20.04 {.wp-block-heading}

There are three versions of wine to be installed, stable, development, and staging branches. Let&#8217;s see how we can install it on Ubuntu 20.04.

### Install Wine stable version {.wp-block-heading}

To install the stable version of Wine run the following command.

<pre class="wp-block-code"><code>$ sudo apt install --install-recommends winehq-stable</code></pre>

Sample output will look like this 

<pre class="wp-block-code"><code>....
Adding saned group and user...
Adding user saned to group scanner
Setting up wine-stable-amd64 (6.0.2~focal-1) ...
Processing triggers for udev (245.4-4ubuntu3.14) ...
Processing triggers for mime-support (3.64ubuntu1) ...
Processing triggers for libglib2.0-0:amd64 (2.64.6-1~ubuntu20.04.4) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
Processing triggers for systemd (245.4-4ubuntu3.14) ...
Processing triggers for man-db (2.9.1-1) ...
Setting up libgdk-pixbuf2.0-0:i386 (2.40.0+dfsg-3ubuntu0.2) ...
Setting up libharfbuzz0b:i386 (2.6.4-1ubuntu4) ...
Setting up libcairo-gobject2:i386 (1.16.0-4ubuntu1) ...
Setting up libgstreamer1.0-0:i386 (1.16.2-2) ...
Setcap worked! gst-ptp-helper is not suid!
Setting up libpango-1.0-0:i386 (1.44.7-2ubuntu4) ...
Setting up libgstreamer-plugins-base1.0-0:i386 (1.16.2-4ubuntu0.1) ...
Setting up gstreamer1.0-plugins-base:i386 (1.16.2-4ubuntu0.1) ...
Setting up libpangoft2-1.0-0:i386 (1.44.7-2ubuntu4) ...
Setting up libpangocairo-1.0-0:i386 (1.44.7-2ubuntu4) ...
Setting up librsvg2-2:i386 (2.48.9-1ubuntu0.20.04.1) ...
Setting up librsvg2-common:i386 (2.48.9-1ubuntu0.20.04.1) ...
Setting up libavcodec58:i386 (7:4.2.4-1ubuntu0.1) ...
Setting up libfaudio0:i386 (20.04-2) ...
Setting up wine-stable-i386:i386 (6.0.2~focal-1) ...
Setting up wine-stable (6.0.2~focal-1) ...
Setting up winehq-stable (6.0.2~focal-1) ...
Processing triggers for libgdk-pixbuf2.0-0:amd64 (2.40.0+dfsg-3ubuntu0.2) ...
Processing triggers for libc-bin (2.31-0ubuntu9.2) ...
Processing triggers for libgdk-pixbuf2.0-0:i386 (2.40.0+dfsg-3ubuntu0.2) ...</code></pre>

### Install Wine development version {.wp-block-heading}

To install the development version run the following command 

<pre class="wp-block-code"><code>$ sudo apt install --install-recommends winehq-devel</code></pre>

### Install Wine staging version {.wp-block-heading}

To install the staging version run the following command

<pre class="wp-block-code"><code>$ sudo apt install --install-recommends winehq-staging</code></pre>

## 6. Configure wine  {.wp-block-heading}

To configure wine run the following command, it will install gecko and mono into our system.

<pre class="wp-block-code"><code>$ winecfg</code></pre>

You can also check the version you have installed with the following command. The command is for the staging version.

<pre class="wp-block-code"><code>$ wine --version
wine-7.0-rc4 (Staging)</code></pre>

## Conclusion {.wp-block-heading}

We have successfully installed Wine 7 into our Ubuntu 20.04. I am happy you have known how to install it.

 [1]: https://www.gnu.org/software/libc/manual/html_node/POSIX.html
 [2]: https://nextgentips.com/2021/12/22/how-to-install-wine-7-on-fedora-35/
 [3]: https://nextgentips.com/2021/12/12/how-to-install-and-configure-wine-on-ubuntu-21-10-21-04/