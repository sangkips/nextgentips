---
title: How to write tests in Python
author: Kipkoech Sang
type: post
date: 2022-11-24T10:37:54+00:00
url: /2022/11/24/how-to-write-tests-in-python/
categories:
  - Testing

---
Testing your code often is a good thing to do. Imagine running a big project without any tests, what could be the implications if you change a feature it affects the functionality of the overall app? Will that be a good thing to do? For many, it is not. Let&#8217;s see how we can perform and write tests right from the start.

When starting out as a developer often write small unit tests to guarantee that the feature you have introduced really doing the intended function. Also, make sure you write some failing tests. 

## Rules of Testing {.wp-block-heading}

  * Use long and descriptive names for testing functions, this often helps those working on your code to know where to change if something is not working right.
  * Whenever you are creating tests, make sure it&#8217;s running fast, this will ensure that you don&#8217;t slow down the development process.
  * Only focus on one tiny bit of functionality. Write tests to test only one single function and make sure it passes successfully.
  * Always write failing tests to guarantee that you are on the right path of development. This will improve your testing and development strategies.
  * Ensure that each unit test is fully independent and run each test alone.
  * Properly implement ways to run tests before pushing the code to a shared repository.

## Types of Tests to perform {.wp-block-heading}

In Python, we have different types of tests such as:

  * Unittest
  * Pytest

### Unittest {.wp-block-heading}

`<mark style="background-color:#abb8c3" class="has-inline-color">Unittest</mark>` is built into the Python standard library. It supports test automation, sharing of setup and shutdown code for tests, aggregation of tests into collections, and independence of tests from the reporting framework.

Unittest provides a rich set of tools for constructing and running tests. Let&#8217;s look at an example.

<pre class="wp-block-code"><code>import unittest


class Test_If_Sum_Equal(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(sum(&#91;45,4,1,20]), 70, "Should be 70")

if __name__ == '__main__':
    unittest.main()</code></pre>

To run the above code use the following command:

<pre class="wp-block-code"><code>python -m unittest test</code></pre>

If you run the above code, it will pass the test because if you add up it will be 70, but if you change a figure, tests will not pass because it will not be the same as the intended outcome.

Tests need to be validated against the output, you need to be aware of the output so that what you have in your code will give you the intended response, this is called `<mark style="background-color:#abb8c3" class="has-inline-color">assertion</mark>`.

### Types of assert in unittest {.wp-block-heading}<figure class="wp-block-table">

<table class="has-fixed-layout">
  <tr>
    <th>
      Method
    </th>
    
    <th>
      Equivalent to
    </th>
  </tr>
  
  <tr>
    <td>
      Method
    </td>
    
    <td>
      Equivalent to
    </td>
  </tr>
  
  <tr>
    <td>
      .assertEqual(a, b)
    </td>
    
    <td>
      a==b
    </td>
  </tr>
  
  <tr>
    <td>
      .assertFalse(x)
    </td>
    
    <td>
      bool(x) is False
    </td>
  </tr>
  
  <tr>
    <td>
      assertTrue(x)
    </td>
    
    <td>
      bool(x)is True
    </td>
  </tr>
  
  <tr>
    <td>
      .assertIs(a, b)
    </td>
    
    <td>
      a is b
    </td>
  </tr>
  
  <tr>
    <td>
      .assertIn(a, b)
    </td>
    
    <td>
      a in b
    </td>
  </tr>
  
  <tr>
    <td>
      .assertIsInstance(a, b)
    </td>
    
    <td>
      isinstance(a,b)
    </td>
  </tr>
  
  <tr>
    <td>
      .assertIsNone(x)
    </td>
    
    <td>
      x is None
    </td>
  </tr>
</table><figcaption class="wp-element-caption">assert properties </figcaption></figure> 

### Pytest {.wp-block-heading}

Pytest supports the execution of unittests. Pytest is a Python testing framework, it can be used to write various types of software tests such as integration tests, unit tests, functional tests, etc.

Features of pytest

  * Has support for test case filtering 
  * Has support for a built-in assert statement 
  * Has support for many plugins to extend its functionality.

To start using pytest, you need to install it first.

<pre class="wp-block-code"><code>pip install -U pytest</code></pre>

Let&#8217;s create our first test case 

<pre class="wp-block-code"><code>def test_sum():
        assert sum(&#91;45,4,1,20]) == 80, "Should be 70")</code></pre>

To run the following test use command `<mark style="background-color:#abb8c3" class="has-inline-color">pytest</mark>`

<pre class="wp-block-code"><code>pytest</code></pre>

The above test will fail because if you add up the figures it&#8217;s not the same as the figure given.

### Ways of specifying tests in Pytest. {.wp-block-heading}

**Run tests in a directory**. Specify a directory to run tests. Mostly named the directory test, and from there pytest will pick it automatically. 

<pre class="wp-block-code"><code>pytest testing/</code></pre>

**Run tests in a module**. You can also specify which module you want specifically started. This is when you want that one specific test

<pre class="wp-block-code"><code>pytest test_mod.py</code></pre>

## Conclusion {.wp-block-heading}

We have seen how to write simple tests using unittest and pytest, build on this foundation to write more tests because testing is a key part of development.