---
title: How to install VLC on Ubuntu 20.04
author: Kipkoech Sang
type: post
date: 2021-10-01T15:23:49+00:00
url: /2021/10/01/how-to-install-vlc-on-ubuntu-20-04/
site-sidebar-layout:
  - default
site-content-layout:
  - default
theme-transparent-header-meta:
  - default
rank_math_primary_category:
  - 6
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 152
rank_math_internal_links_processed:
  - 1
categories:
  - Linux

---
VLC is a free and open source cross-platform multimedia player that plays most of the multimedia files as well as DVDs, VCDs, Audio CDs and all other streaming protocols. 

This article describes how to install vlc using [snapcraft][1] . This allows for easy distribution of the latest VLC versions directly to end users, this helps to address security and critical bug fixes. If you love to use deb package then use the [APT][2] package management.

#### Benefits of Using VLC {.wp-block-heading}

  * VLC is supported by every operating system. 
  * VLC does not limit one from not only playing audio and videos stored in a portable device such as pen drive but also you can do online streaming 
  * It allows one to record live videos as it happens.
  * The best thing about VLC is that it can installed on any portbale device such iphone, ipad and all also supports all smartphones.
  * VLC is free from viruses reason being it doesn&#8217;t allow add-ons.
  * It&#8217;s simple and quick to download. 

#### Install VLC as a snap package {.wp-block-heading}

Snap is a software packaging and deployment system developed by canonical for operating systems that use Linux kernel. The packages work across a range of Linux distributions and allow software developers to distribute their applications directly to users.

To install VLC open terminal and run the following command:

<pre class="wp-block-syntaxhighlighter-code">Type your code here</pre>

<pre class="wp-block-code"><code>$ sudo snap install vlc</code></pre>

That&#8217;s all, you have installed VLC. 

#### Installing VLC with apt {.wp-block-heading}

Installing VLC through this method may lack behind in terms of updates. You will have to be looking up for the latest releases every time.

Open terminal and run the following commands as a sudo user.

<pre class="wp-block-code"><code>$ sudo apt update

$ sudo apt install vlc</code></pre>

With that short code you have installed VLC media player. To start using VLC go to activities search bar and type &#8216;VLC&#8217; and click on the VLC icon.

You can now explore the features that comes with VLC media player and start enjoy watching your favorite videos.

 [1]: https://snapcraft.io/
 [2]: apt://vlc