---
title: About Us
author: Kipkoech Sang
type: page
date: 2021-10-25T11:04:49+00:00
ocean_disable_margins:
  - enable
ocean_display_top_bar:
  - default
ocean_display_header:
  - default
ocean_disable_title:
  - default
ocean_disable_heading:
  - default
ocean_disable_breadcrumbs:
  - default
ocean_display_footer_widgets:
  - default
ocean_display_footer_bottom:
  - default
rank_math_news_sitemap_robots:
  - index
rank_math_robots:
  - 'a:1:{i:0;s:5:"index";}'
rank_math_analytic_object_id:
  - 125
rank_math_internal_links_processed:
  - 1

---
NextGenTips is a technology blog.

Here you will find content on

  * Linux server installation and administration
  * Security
  * Databases
  * Kubernetes
  * Containers and Orchestrations
  * Automation technologies such Ansible, puppet
  * Monitoring technoloes susch Nagios, Grafana etc
  * Open source tools and technologies
  * Virtualization technologies such as VMware

Contact us for more details